import { defineConfig } from 'dumi';

const AMAP_JS_API_KEY = 'defa440eb59351703ec4d9e84920f52e';

export default defineConfig({
  title: '平行线',
  mode: 'site',
  favicon: '/favicon.ico',
  logo: '/logo.png',
  outputPath: 'docs-dist',
  base: '/',
  publicPath: '/',
  exportStatic: {},
  history: { type: 'browser' },
  sitemap: { hostname: 'https://pxx-design.gitee.io' },
  proxy: {
    '/web': {
      target: `https://dev.pxxtech.com/web`,
      changeOrigin: true,
      pathRewrite: { '^/host': '' },
    },
    '/host': {
      target: `https://dev.pxxtech.com/host`,
      changeOrigin: true,
      pathRewrite: { '^/host': '' },
    },
  },
  headScripts: [
    { src: '/countryTree.js' },
    { src: '/touch-emulator.js' },
    `//webapi.amap.com/maps?v=2.0&key=${AMAP_JS_API_KEY}&plugin=AMap.MoveAnimation,AMap.AutoComplete,AMap.Geocoder,AMap.Driving,AMap.ToolBar`,
  ],
  navs: {
    'zh-CN': [
      null,
      { title: 'Gitee', path: 'https://gitee.com/pxx-design/pxx-design' },
    ],
    'en-US': [
      null,
      { title: 'Gitee', path: 'https://gitee.com/pxx-design/pxx-design' },
    ],
  },
  menus: {
    '/common': [
      {
        title: '组件介绍',
        path: '/common',
      },
      {
        title: '基础组件',
        children: ['common/style/index.md', 'common/icon/index.md'],
      },
      {
        title: '布局',
        children: ['common/space/index.md', 'common/space-between/index.md'],
      },
      {
        title: '数据展示',
        children: ['common/tag/index.md', 'common/button-group/index.md'],
      },
      {
        title: '高德地图',
        children: [
          'common/a-map/marker.md',
          'common/a-map/polyline.md',
          'common/a-map/components.md',
          'common/a-map/circle.md',
          'common/a-map/monitor.md',
        ],
      },
      {
        title: '反馈',
        children: ['common/spin/index.md'],
      },
      {
        title: '业务组件',
        children: ['common/access/index.md', 'common/env-info/index.md'],
      },
    ],
    '/components': [
      {
        title: '组件介绍',
        path: '/components',
      },
      // {
      //   title: '组件总览',
      //   path: '/components',
      // },
      {
        title: '通用',
        children: ['components/button/index.md'],
      },
      {
        title: '布局',
        children: ['components/whiteboard/index.md'],
      },
      // {
      //   title: '导航',
      //   children: ['button/index.md'],
      // },
      {
        title: '数据录入',
        children: [
          'components/city-select/index.md',
          'components/date-picker/index.md',
          'components/input/index.md',
          'components/input-number/index.md',
          'components/select/index.md',
          'components/radio/index.md',
          'components/rich-text-editor/index.md',
          'components/upload/index.md',
        ],
      },
      {
        title: '数据展示',
        children: [
          'components/pro-table/index.md',
          'components/base-table/index.md',
          'components/image/index.md',
          'components/text/index.md',
          'components/descriptions/index.md',
        ],
      },
      {
        title: '反馈',
        children: ['components/page-loading/index.md'],
      },
    ],
    '/mobile': [
      {
        title: '组件介绍',
        path: '/mobile',
      },
      {
        title: '基础组件',
        children: [
          'mobile/button/index.md',
          'mobile/image/index.md',
          'mobile/popup/index.md',
        ],
      },
      {
        title: '布局组件',
        children: ['mobile/whiteboard/index.md'],
      },
      {
        title: '表单组件',
        children: [
          'mobile/input/index.md',
          'mobile/select/index.md',
          'mobile/switch/index.md',
          'mobile/form/index.md',
          'mobile/picker/index.md',
          'mobile/picker-view/index.md',
          'mobile/date-picker/index.md',
          'mobile/district-picker/index.md',
        ],
      },
      {
        title: '展示组件',
        children: [
          'mobile/card/index.md',
          'mobile/list-view/index.md',
          'mobile/image-preview/index.md',
          'mobile/swipe/index.md',
          'mobile/cell-group/index.md',
          'mobile/menu/index.md',
          'mobile/tab-bar/index.md',
          'mobile/result/index.md',
        ],
      },
      {
        title: '反馈组件',
        children: [
          'mobile/pull-refresh/index.md',
          'mobile/overlay/index.md',
          'mobile/modal/index.md',
          'mobile/drawer/index.md',
          'mobile/action-sheet/index.md',
        ],
      },
      {
        title: '业务',
        children: [
          'mobile/login/index.md',
        ],
      },
    ],
  },
  externals: {
    AMap: 'window.AMap',
  },
  chainWebpack(config: any) {
    config.module
      .rule('otf')
      .test(/.otf$/)
      .use('file-loader')
      .loader('file-loader');
    config.plugins.delete('locale');
    config.plugins.delete('qiankun');
    config.plugins.delete('layout');
    config.plugins.delete('sass');
  },
  // algolia: {
  //   apiKey: 'yourapikey',
  //   indexName: 'dumi',
  // }
});
