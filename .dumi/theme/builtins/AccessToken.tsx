import * as React from 'react';
import { Input, Space } from 'antd';

export default () => {
  const onSearch = (searchvalue: string) => {
    localStorage.setItem('pxx_accessToken', searchvalue);
  };
  const onSearch2 = (searchvalue: string) => {
    localStorage.setItem('pxx_userId', searchvalue);
  };
  return (
    <Space direction="vertical" style={{ width: '100%' }}>
      <Input.Search
        placeholder="设置AccessToken"
        onSearch={onSearch}
        style={{ width: '80%' }}
        enterButton="确定"
        allowClear
      />
      <Input.Search
        placeholder="设置userId"
        onSearch={onSearch2}
        style={{ width: '80%' }}
        enterButton="确定"
        allowClear
      />
    </Space>
  );
};
