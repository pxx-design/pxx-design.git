import React from 'react';
import { Space, SpaceProps } from 'antd';
import styled from 'styled-components';

const NewSpace = styled<React.FC<SpaceProps & { width?: string }>>(Space)`
  width: 100%;

  > .ant-space-item {
    flex: ${(props) => `0 ${props.width || 'auto'}`};
    width: ${(props) => `${props.width || 'auto'}`};
    margin-bottom: 16px;
    padding-right: 16px;
  }
`;

export default ({
  width = '50%',
  children,
}: React.PropsWithChildren<{ width?: string }>) => {
  return (
    <NewSpace
      wrap
      size={0}
      width={width}
      align="start"
      className="__dumi-default-previewer-demo-space"
    >
      {children}
    </NewSpace>
  );
};
