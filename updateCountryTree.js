/**
 * 更新countryTree方法，此方法想起来更新下就行
 */
const fs = require('fs');
const _ = require('lodash');
const umiRequest = require('umi-request');

const request = umiRequest.extend({timeout: 10000});

request.get('https://restapi.amap.com/v3/config/district', {
  params: {
    key: 'ef96f75c50278a8a75c10519cbc1c325',
    subdistrict: 3,
  }
}).then((response) => {
  const districts = _.get(response, "districts[0].districts", []);
  const adcodes = ["110000", "120000", "310000", "500000"];
  const citycodes = ["021", "023", "022", "010"];
  const levels = ["province", "city", "district"];
  let index = 0;
  const loop = (districts) => _.sortBy(districts, (item) => {
    delete item.citycode
    if (adcodes.includes(item.adcode)) {
      // 直辖市处理
      item.districts = item.districts.reduce((prev, v) => {
        return [...prev, ...v.districts];
      }, []);
    }
    if (
      _.isArray(item.districts) &&
      item.districts.length > 0 &&
      item.districts.some((v) => levels.includes(v.level))
    ) {
      index = 0;
      item.districts = loop(item.districts);
    } else {
      if (citycodes.includes(item.citycode)) {
        item.districts = [];
      } else {
        item.districts = undefined;
      }
    }
    if (!levels.includes(item.level)) {
      index += 1;
      districts[(index += 1)] = undefined;
    }
    return +item?.adcode;
  });
  const data = `import {CountryInfo} from './interface';const countryTree: CountryInfo[] = ${JSON.stringify(loop(districts))};export {countryTree};`
  fs.writeFile('./packages/utils/src/utils/global/countryTree.ts', data, (error) => {
    console.log(error)
  })
})
