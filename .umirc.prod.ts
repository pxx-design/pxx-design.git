import { defineConfig } from 'dumi';
import { v4 as uuid } from 'uuid';

const h = (uuid() as string).replace(/-/g, '');

export default defineConfig({
  hash: true,
  copy: [
    {
      from: 'packages/common/dist/index.css',
      to: `assets/common/index.${h}.css`,
    },
    {
      from: 'packages/components/dist/antd.css',
      to: `assets/components/antd.${h}.css`,
    },
    {
      from: 'packages/components/dist/index.css',
      to: `assets/components/index.${h}.css`,
    },
    {
      from: 'packages/mobile/dist/index.css',
      to: `assets/mobile/index.${h}.css`,
    },
  ],
  extraBabelPlugins: [
    [
      'babel-plugin-import',
      {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: false,
      },
      'antd',
    ],
  ],
  links: [
    { rel: 'stylesheet', href: '/style.css' },
    { rel: 'stylesheet', href: `/assets/common/index.${h}.css` },
    { rel: 'stylesheet', href: `/assets/components/antd.${h}.css` },
    { rel: 'stylesheet', href: `/assets/components/index.${h}.css` },
    { rel: 'stylesheet', href: `/assets/mobile/index.${h}.css` },
  ],
});
