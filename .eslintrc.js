module.exports = {
  extends: [require.resolve('@umijs/fabric/dist/eslint')],
  rules: {
    '@typescript-eslint/indent': ['error', 2], // 空格格式占两个字符串
    'no-console': 0, // 可console
    'prefer-template': 0, // 关闭必须使用es6字符串模板拼接字符串
    'prefer-destructuring': 0, //
    'no-unused-expressions': 0, // 允许使用运算符判断是否执行函数
    '@typescript-eslint/no-unused-expressions': 0,
    '@typescript-eslint/no-throw-literal': 0, // 允许自由throw返回值
    'react-hooks/exhaustive-deps': 0, // hooks useEffect
    'prefer-promise-reject-errors': 0, // 允许promise.reject 返回非Error类型值
    'no-param-reassign': 0, // 允许对对象参数赋值
    '@typescript-eslint/consistent-type-imports': 0, // import types 路径不需要添加type
    'no-async-promise-executor': 0, // 允许在promise回调中执行async
    'no-underscore-dangle': 0,
    'max-classes-per-file': 0, // 允许一个文件有多个class或hooks Function
    'react/no-array-index-key': 0, // react 同级使用index作为key,暂时运行
  },
  overrides: [
    // 格式化和eslint冲突，tsx文件禁用eslint
    {
      files: ['*/**/*.tsx'],
      rules: {
        indent: 'off',
        '@typescript-eslint/indent': 'off',
      },
    },
  ],
};
