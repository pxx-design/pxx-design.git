---
title: 平行线
hero:
  title: Parallel Line
  desc: 平行线前端常用库
  actions:
    - text: 快速上手
      link: /components
features:
  - icon: https://gw.alipayobjects.com/zos/bmw-prod/813f5ed9-6bc4-43d4-9f74-ec81ecf35733/k7htg6n4_w144_h144.png
    title: 内容丰富
    desc: 拥有丰富的自定义组件，每个组件都有丰富的在线示例供您体验。
  - icon: https://gw.alipayobjects.com/os/q/cms/images/k9ziitmp/13668549-b393-42a2-97c3-a6365ba87ac2_w96_h96.png
    title: 简单易用
    desc: 在 Ant Design 上进行了自己的封装，更加易用
  - icon: https://gw.alipayobjects.com/os/q/cms/images/k9ziik0f/487a2685-8f68-4c34-824f-e34c171d0dfd_w96_h96.png
    title: Ant Design
    desc: 与 Ant Design 设计体系一脉相承，无缝对接 antd 项目
  - icon: https://gw.alipayobjects.com/mdn/rms_05efff/afts/img/A*-3XMTrwP85wAAAAAAAAAAABkARQnAQ
    title: 预设样式
    desc: 样式风格与 antd 一脉相承，无需魔改，浑然天成
  - icon: https://gw.alipayobjects.com/os/q/cms/images/k9ziieuq/decadf3f-b53a-4c48-83f3-a2faaccf9ff7_w96_h96.png
    title: 预设行为
    desc: 更少的代码，更少的 Bug
  - icon: https://gw.alipayobjects.com/os/q/cms/images/k9zij2bh/67f75d56-0d62-47d6-a8a5-dbd0cb79a401_w96_h96.png
    title: TypeScript
    desc: 使用 TypeScript 开发，提供完整的类型定义文件
footer: Open-source MIT Licensed | Copyright © 2020-present<br />Powered by [dumi](https://d.umijs.org)
---

## 轻松上手

```bash
// 安装依赖
npm i @parallel-line/utils --save
npm i @parallel-line/hooks --save
npm i @parallel-line/components --save

// 使用 工具库
import { calc } from '@parallel-line/utils';

// 使用 hooks
import { useMemoDeep } from '@parallel-line/hooks';

// 使用 组件
import { Text } from '@parallel-line/components';
```
