---
order: 12
title: 贡献指南
toc: menu
---

这篇指南会指导你如何为 Parallel Line 贡献一份自己的力量，请在你要提 issue 或者 pull request 之前花几分钟来阅读一遍这篇指南。

## 行为准则

我们有一份 [行为准则](/code_of_conduct)，希望所有的贡献者都能遵守，请花时间阅读一遍全文以确保你能明白哪些是可以做的，哪些是不可以做的。

## 开发流程

在你 clone 了 parallel-line 的代码后，你还可以运行下面几个常用的命令：

1. `npm run init` 在本地初始化`parallel-line`, 会自动删除本地`packages`所有的`NM`并安装，该命令行只在 Linux 系统有效（包括 MacBook）。
2. `npm run init:win` 命令`init`的`win`版本，window 系统的用户可以执行该命令行做初始化，但是需要自行先删除`NM`（目前没做测试）。
3. `npm run init:cnpm` 命令`init`的`cnpm`版本。
4. `npm run lint` 检查代码风格。
5. `npm run build` 编译 packages 代码到相应的 lib 和 es 目录，每次`init`后建议使用。
6. `npm run compile` 编译 packages 代码到相应的 lib 和 es 目录，每次`pull`代码后建议使用。
7. `npm run pub` 检查发生变动的 package 并发布到 npm，这里需要在本地通过`npm login`登录你的 npm 账号。
8. `npm run deploy` 构建 parallel-line 的 文档站点并发布到 gh-pages 分支。

PS: 这里建议 window 系统的用户通过 WSL 进行开发。

## 提交代码

提交代码需要编写符合约定的 commit message 规范，格式如下：

```
git commit -m <type>[<package>](<scope>): <message>
```

其中 type 允许的类型可以在 [https://gitmoji.dev/](https://gitmoji.dev/) 查看

其中 package 表示 包名，和 packages 下的文件夹名称保持一致

其中 scope 表示作用的组件或者方法，必须使用 Pascal-Case，例如

- Button
- DatePicker

最后，message 为您此次提交的描述。例如：

```bash
git commit -m :sparkles:[components](DatePicker): add onChange
```

# Pull Request 流程

请参考：[如何优雅地在 github 上贡献代码](https://segmentfault.com/a/1190000000736629)

1. 克隆仓库
2. 创建分支
3. 提交代码
4. 合并修改 `git rebase master`
5. 发起 MR

## FAQ

### vscode 下组件开发 demo 的本库 ts 声明失效

```bash
cd ./packages/components/node_modules/@parallel-line/
ln -s ../../ components
```

### 如何在 [pxx-web](https://git.pxxtech.com/web/pxx-web) 项目中调试依赖包

> 这里通过 @parallel-line/components 包做例子

1. 创建软链

```bash
cd /path/to/parallel-line/packages/components
npm link
cd /path/to/pxx-web
npm link @parallel-line/components
```

2. 修改代码后 compile 然后在 start 项目（每一次修改后都要重新执行）

```bash
cd /path/to/parallel-line/packages/components
npm run compile
cd /path/to/pxx-web
npm run start-m-d # or npm run start-e-d

# 也可以通过软链进行开发
rm -rf es lib
ln -s src es
ln -s src lib
```

### FATAL ERROR: wasm code commit Allocation failed - process out of memory

如果在执行npm命令行出现内存不足，可以在 `~/.zshrc` 最后添加以下代码。

```bash
export NODE_OPTIONS=--max_old_space_size=6144
```

如果还不行，查看你的npm版本，如果是npm@14，那么这是npm@14版本的Bug，针对npm升级处理即可。

```bash
nvm install 15
```
