---
order: 11
title: 行为准则
toc: menu
---

# 行为准则

## 开发规范

### 安装 npm 包

在通过 `npm i` 或 `yarn add` 添加一个依赖包时，需要明确该依赖包用途：

- 如果是用到 src 则需要放到 dependencies
- 如果只是在 nodejs 下使用则放到 devDependencies
- 如果是大包，比如 react 则放到 peerDependencies

#### 相关知识点

- [specifying-dependencies-and-devdependencies-in-a-package-json-file](https://docs.npmjs.com/specifying-dependencies-and-devdependencies-in-a-package-json-file)
- [dependencies 和 devDependencies 的区别](https://sengmitnick.com/blog/312/)
