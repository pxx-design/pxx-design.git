import { defineConfig } from 'dumi';
import { generateImportConfig } from './packages/common/src/util';

export default defineConfig({
  lessLoader: {
    javascriptEnabled: true,
  },
  extraBabelPlugins: [
    [
      'babel-plugin-import',
      { libraryName: 'antd', libraryDirectory: 'es', style: true },
      'antd',
    ],
    [
      'babel-plugin-import',
      {
        libraryName: '@parallel-line/common',
        libraryDirectory: 'es/common',
        style: true,
      },
      'pxx-common',
    ],
    [
      'babel-plugin-import',
      generateImportConfig({
        libraryName: '@parallel-line/components',
        libraryDirectory: 'es/components',
        style: true,
      }),
      'pxx-web',
    ],
    [
      'babel-plugin-import',
      generateImportConfig({
        libraryName: '@parallel-line/mobile',
        libraryDirectory: 'es/mobile',
        style: true,
      }),
      'pxx-mobile',
    ],
  ],
  links: [{ rel: 'stylesheet', href: '/style.css' }],
});
