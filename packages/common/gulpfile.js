var gulp = require('gulp');
var del = require('del');
var less = require('gulp-less');
var LessNpmImport = require('less-plugin-npm-import');

gulp.task('clean', () => del(['dist/**/*']));

gulp.task('less', () =>
  gulp
    .src('src/common/**/style/index.less')
    .pipe(
      less({
        plugins: [new LessNpmImport({ prefix: '~' })],
        javascriptEnabled: true,
        // modifyVars: theme,
      }),
    )
    .pipe(gulp.dest('lib/common/'))
    .pipe(gulp.dest('es/common/')),
);

gulp.task('less_style', () =>
  gulp
    .src('src/common/style/*.less')
    .pipe(
      less({
        plugins: [new LessNpmImport({ prefix: '~' })],
        javascriptEnabled: true,
        // modifyVars: theme,
      }),
    )
    .pipe(gulp.dest('dist/')),
);

gulp.task('default', gulp.series('clean', 'less', 'less_style'));
