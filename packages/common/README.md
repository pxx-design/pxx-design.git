# @parallel-line/common

> 平行线前端通用组件库。

## 📦 安装

```bash
npm install @parallel-line/mobile --save
```

```bash
yarn add @parallel-line/mobile
```

## 更多

[使用文档](https://supermoonlmy.gitee.io/parallel-line/)
