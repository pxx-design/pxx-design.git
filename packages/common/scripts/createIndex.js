const fs = require('fs');
const autoExport = require('pxx-plugin-auto-export');

const pkgData = [];

autoExport({
  libName: 'common',
  excludeFiles: ['config-provider', 'popup'],
  callbackfn: (_, file) => pkgData.push(file),
});

fs.writeFileSync(
  './src/pkg.ts',
  `/** babel-plugin-pxx-import 判断来源 */
export default ${JSON.stringify(pkgData)};

`,
);
