import common from './pkg';

interface ImportConfig {
  style?: boolean;
  libraryName: string;
  libraryDirectory?: string;
}

/**
 * @deprecated
 */
export const generateImportConfig = (params: ImportConfig) => {
  const { libraryName, libraryDirectory = 'lib' } = params;
  return {
    ...params,
    customName: (name: string) => {
      if (common.includes(name)) {
        return `@parallel-line/common/es/common/${name}`;
      }
      return `${libraryName}/${libraryDirectory}/${name}`;
    },
    customStyleName: (name: string) => {
      if (common.includes(name)) {
        return `@parallel-line/common/es/common/${name}/style`;
      }
      return `${libraryName}/${libraryDirectory}/${name}/style`;
    },
  };
};
