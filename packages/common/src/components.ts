export {
  default as ConfigProvider,
  ConfigConsumer,
  SizeContextProvider,
} from './common/config-provider';

export type {
  SizeType,
  ConfigProviderProps,
  ConfigConsumerProps,
} from './common/config-provider';
