/**
 * title: 基本用法
 * desc: 由于antd的Space用着不舒服，所以新写个。
 */

import React from 'react';
import {Button} from 'antd';
import {Space} from '@parallel-line/common';

export default () => (
  <div style={{width: 200}}>
    <Space size={[5, 6]} wrap>
      <Button>1</Button>
      <Button>2</Button>
      <Button>3</Button>
      <Button>4</Button>
      <Button>1</Button>
      <Button>2</Button>
      <Button>3</Button>
      <Button>4</Button>
    </Space>
    <Space wrap>
      <Button>1</Button>
      <Button>2</Button>
      <Button>3</Button>
      <Button>4</Button>
      <Button>1</Button>
      <Button>2</Button>
      <Button>3</Button>
      <Button>4</Button>
    </Space>
    <Space direction="vertical">
      <Button>1</Button>
      <Button>2</Button>
      <Button>3</Button>
      <Button>4</Button>
    </Space>
  </div>
);
