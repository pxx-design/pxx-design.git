import React from "react";
import Item from "./Item";
import classNames from "classnames";
import {useCreateNamespace} from "../hooks";
import toArray from "rc-util/lib/Children/toArray";
import {isString, isNil} from 'lodash-es';

export const SpaceContext = React.createContext({
  latestIndex: 0,
  horizontalSize: 0,
  verticalSize: 0,
});

export type SpaceSize = "small" | "middle" | "large" | undefined | number;

export interface SpaceProps {
  prefixCls?: string;
  className?: string;
  style?: React.CSSProperties;
  size?: SpaceSize | [SpaceSize, SpaceSize];
  direction?: "horizontal" | "vertical";
  // No `stretch` since many components do not support that.
  align?: "start" | "end" | "center" | "baseline";
  split?: React.ReactNode;
  wrap?: boolean;
  directionConfig?: any;
  isMarginRight?: boolean;
}

const spaceSize = {
  small: 8,
  middle: 12,
  large: 16,
};

function getNumberSize(size: SpaceSize) {
  return isString(size) ? spaceSize[size] : size || 0;
}

const Space: React.FC<SpaceProps> = (props) => {
  const {
    size = 'middle',
    align,
    className,
    children,
    direction = "horizontal",
    directionConfig,
    split,
    style,
    wrap = false,
    ...otherProps
  } = props;
  const [cls, bem] = useCreateNamespace('space');

  const [horizontalSize, verticalSize] = React.useMemo(
    () =>
      (
        (Array.isArray(size) ? size : [size, size]) as [SpaceSize, SpaceSize]
      ).map((item) => getNumberSize(item)),
    [size]
  );

  const childNodes = toArray(children, {keepEmpty: true});
  const mergedAlign = align === undefined && direction === 'horizontal' ? 'center' : align;

  const cn = classNames(
    cls,
    bem(direction),
    {
      [bem('rtl')]: directionConfig === "rtl",
      [bem(`align-${mergedAlign}`)]: mergedAlign,
    },
    className
  );

  const itemClassName = bem('item');

  const marginDirection = directionConfig === 'rtl' ? 'marginLeft' : 'marginRight';

  // Calculate latest one
  let latestIndex = 0;
  const nodes = childNodes.map((child, i) => {
    if (!isNil(child)) {
      latestIndex = i;
    }
    /* eslint-disable react/no-array-index-key */
    return (
      <Item
        index={i}
        wrap={wrap}
        split={split}
        direction={direction}
        className={itemClassName}
        key={`${itemClassName}-${i}`}
        marginDirection={marginDirection}
      >
        {child}
      </Item>
    );
    /* eslint-enable */
  });
  if (childNodes.length === 0) {
    return null;
  }
  return (
    <div
      className={cn}
      style={{
        ...(wrap && {flexWrap: "wrap", marginBottom: -verticalSize}),
        ...style,
      }}
      {...otherProps}
    >
      <SpaceContext.Provider
        value={{horizontalSize, verticalSize, latestIndex}}
      >
        {nodes}
      </SpaceContext.Provider>
    </div>
  );
};

export default Space;
