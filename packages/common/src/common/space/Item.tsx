import React from "react";
import {SpaceContext} from ".";
import {isNil} from 'lodash-es';

export interface ItemProps {
  className: string;
  children: React.ReactNode;
  index: number;
  direction?: "horizontal" | "vertical";
  marginDirection: "marginLeft" | "marginRight";
  split?: string | React.ReactNode;
  wrap?: boolean;
}

const Item = (props: ItemProps) => {
  const {
    className,
    direction,
    index,
    marginDirection,
    children,
    split,
    wrap,
  } = props;
  const {horizontalSize, verticalSize, latestIndex} = React.useContext(
    SpaceContext
  );

  let style: React.CSSProperties = {};

  if (direction === 'vertical') {
    if (index < latestIndex) {
      style = {marginBottom: horizontalSize / (split ? 2 : 1)};
    }
  } else {
    style = {
      ...(index < latestIndex && {[marginDirection]: horizontalSize / (split ? 2 : 1)}),
      ...(wrap && {paddingBottom: verticalSize}),
    };
  }
  if (isNil(children)) {
    return null;
  }

  return (
    <>
      <div className={className} style={style}>
        {children}
      </div>
      {index < latestIndex && split && (
        <span className={`${className}-split`} style={style}>
          {split}
        </span>
      )}
    </>
  );
}
export default Item;
