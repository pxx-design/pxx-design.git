import InternalAMap, { AMapProps } from './AMap';
import Marker from './Marker';
import Polyline from './Polyline';
import InfoWindow from './InfoWindow';
import Circle from './Circle'
import Monitor from './Monitor'
import * as icons from './icons';
import * as windows from './windows';
import * as components from './components';
import MoveMarker from "./MoveMarker";

type InternalAMapType = typeof InternalAMap;

interface AMapInterface extends InternalAMapType {
  components: typeof components;
  icons: typeof icons;
  Marker: typeof Marker;
  windows: typeof windows;
  Polyline: typeof Polyline;
  InfoWindow: typeof InfoWindow;
  Circle: typeof Circle;
  Monitor: typeof Monitor;
  MoveMarker: typeof MoveMarker;
}

const AMap = InternalAMap as AMapInterface;

AMap.icons = icons;
AMap.Marker = Marker;
AMap.windows = windows;
AMap.Polyline = Polyline;
AMap.InfoWindow = InfoWindow;
AMap.components = components;
AMap.Circle = Circle;
AMap.Monitor = Monitor;
AMap.MoveMarker = MoveMarker;

export type { AMapProps };

export default AMap;
