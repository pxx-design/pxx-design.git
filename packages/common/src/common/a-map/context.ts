import { createContext } from 'react';

export default createContext<any>({
  loading: false,
  complete: false,
});
