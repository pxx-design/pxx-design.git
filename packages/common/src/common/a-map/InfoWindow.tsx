import React, {useContext, useRef} from 'react';
import ReactDOM from 'react-dom';
import "@amap/amap-jsapi-types";
import Context from './context';
import {global, isEmpty} from '@parallel-line/utils';
import {useEffectDeep} from '@parallel-line/hooks';

interface InfoWindowProps {
  id?: any,
  offset?: any;
  extData?: any;
  content?: any;
  position: any;
  onClick?: any;
  onClose?: any;
  visible?: boolean;
  isCustom?: boolean;
  autoMove?: boolean;
  retainWhenClose?: boolean;
}

const InfoWindow = (props: InfoWindowProps) => {
  const element = useRef<any>();
  const {amap} = useContext(Context);
  const {position} = props;
  const contentNode = useRef<any>(document.createElement('div'));
  const handleClose = () => {
    global?.eventEmitter?.emit({
      id: props.id,
      value: false,
      action: 'visible',
      uniqueKey: 'AMAP_MARKER_WINDOW',
    });
    props.onClose?.();
  };
  const updateContent = () => {
    if (props.content) {
      ReactDOM.render(
        <props.content
          onClose={handleClose}
          extData={props.extData}/>,
        contentNode.current);
    }
  };
  const createElement = () => {
    if (isEmpty(position)) return;
    if (element.current) {
      element.current.setPosition(position);
      return;
    }

    element.current = new AMap.InfoWindow({
      position,
      offset: props.offset,
      autoMove: props.autoMove,
      isCustom: props.isCustom,
      content: contentNode.current,
    });

    if (props.visible) element.current.open(amap);
  };
  // 首次创建
  useEffectDeep(() => {
    createElement();
    return () => {
      element.current?.setMap(null);
    };
  }, position);

  useEffectDeep(() => {
    updateContent();
  }, [props.extData]);


  global?.eventEmitter?.useSubscription(({uniqueKey, id, action, value}: any) => {
    if (uniqueKey === 'AMAP_MARKER_WINDOW' && action === 'visible' && props.id === id) {
      amap.setCenter(position, true);
      if (value) {
        element.current.open(amap);
      } else {
        element.current.close();
      }
    }
  });
  return null;
};
InfoWindow.defaultProps = {
  visible: false,
  isCustom: true,
  autoMove: true,
  retainWhenClose: true,
};
export default InfoWindow;
