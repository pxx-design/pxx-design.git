import React, {useRef, useState} from 'react';
import {useHover} from 'ahooks';
import classNames from 'classnames';
import site from './images/site.svg';
import vehicle from './images/vehicle.svg';
import report from './images/report.svg';
import warning from './images/warning.svg';
import reportTip from './images/reportTip.svg'
import warningBg0 from './images/warningbg0.svg'
import warningBg1 from './images/warningbg1.svg'
import {global, toNumber} from '@parallel-line/utils';

export const IconWarning = (props: any) => {
  // type
  // 0: '车辆超速',
  // 1: '疲劳驾驶'
  const prefixCls = `${props.prefixCls}-amap-site-icon`;
  const [infoWindowVisible, setInfoWindowVisible] = useState(false);
  const {extData} = props;
  const ref = useRef<any>();
  const isHovering = useHover(ref);
  global.eventEmitter?.useSubscription(
    ({uniqueKey, action, id, value}: any) => {
      if (uniqueKey === 'AMAP_WARNING_WINDOW' && action === 'visible' && props.id === id) {
        setInfoWindowVisible(value);
      } else {
        setInfoWindowVisible(false);
      }
    });


  return (
    <div className={prefixCls}>
      {!infoWindowVisible && (
        <>
          {isHovering && (
            <div
              className={classNames(
                `${prefixCls}-tip`,
                `${prefixCls}-hover-tip-plus`,
                `${prefixCls}-hover-tip-plus-warning`
              )}
              style={{
                backgroundImage: `url(${extData.type == 0 ? warningBg0 : warningBg1})`,
                backgroundRepeat: 'no-repeat',
                backgroundSize: '100% 100%'
              }}
            >
            </div>
          )}
        </>
      )}
      <img
        alt=''
        ref={ref}
        src={warning}
        className={`${prefixCls}-img`}
      />
    </div>
  );
}

IconWarning.defaultProps = {
  prefixCls: 'pxx',
};

export const IconReport = (props: any) => {
  const prefixCls = `${props.prefixCls}-amap-site-icon`;
  const [infoWindowVisible, setInfoWindowVisible] = useState(false);
  const ref = useRef<any>();
  const isHovering = useHover(ref);
  global.eventEmitter?.useSubscription(
    ({uniqueKey, action, id, value}: any) => {
      if (uniqueKey === 'AMAP_REPORT_WINDOW' && action === 'visible' && props.id === id) {
        setInfoWindowVisible(value);
      } else {
        setInfoWindowVisible(false);
      }
    });


  return (
    <div className={prefixCls}>
      {!infoWindowVisible && (
        <>
          {isHovering && (
            <div
              className={classNames(
                `${prefixCls}-tip`,
                `${prefixCls}-hover-tip-plus`,
                `${prefixCls}-hover-tip-plus-report`
              )}
              style={{
                backgroundImage: `url(${reportTip})`,
                backgroundRepeat: 'no-repeat',
                backgroundSize: '100% 100%'
              }}
            >
            </div>
          )}
        </>
      )}
      <img
        alt=''
        ref={ref}
        src={report}
        className={`${prefixCls}-img`}
      />
    </div>
  );
}

IconReport.defaultProps = {
  prefixCls: 'pxx',
};

export const IconSite = (props: any) => {
  const prefixCls = `${props.prefixCls}-amap-site-icon`;
  const [infoWindowVisible, setInfoWindowVisible] = useState(false);
  const {extData} = props;
  const ref = useRef<any>();
  const isHovering = useHover(ref);
  global.eventEmitter?.useSubscription(
    ({uniqueKey, action, id, value}: any) => {
      if (uniqueKey === 'AMAP_MARKER_WINDOW' && action === 'visible' && props.id === id) {
        setInfoWindowVisible(value);
      } else {
        setInfoWindowVisible(false);
      }
    });
  const number = toNumber(extData?.index, 0) + 1;
  return (
    <div className={prefixCls}>
      {!infoWindowVisible && (
        <>
          {isHovering ? (
            <div
              className={classNames(
                `${prefixCls}-tip`,
                `${prefixCls}-hover-tip`,
              )}
            >
              站点{number}
            </div>
          ) : (
            <div className={`${prefixCls}-tip`}>{number}</div>
          )}
        </>
      )}
      <img
        alt=''
        ref={ref}
        src={site}
        className={`${prefixCls}-img`}
      />
    </div>
  );
};

IconSite.defaultProps = {
  prefixCls: 'pxx',
};

export const IconOffline = (props: any) => {
  const prefixCls = `${props.prefixCls}-amap-site-icon`;
  const [infoWindowVisible, setInfoWindowVisible] = useState(false);
  const {extData} = props;
  const ref = useRef<any>();
  const isHovering = useHover(ref);
  global.eventEmitter?.useSubscription(
    ({uniqueKey, action, id, value}: any) => {
      if (uniqueKey === 'AMAP_MARKER_WINDOW' && action === 'visible' && props.id === id) {
        setInfoWindowVisible(value);
      } else {
        setInfoWindowVisible(false);
      }
    });
  const number = toNumber(extData?.index, 0) + 1;
  return (
    <div className={prefixCls}>
      {!infoWindowVisible && (
        <>
          {isHovering ? (
            <div
              className={classNames(
                `${prefixCls}-tip`,
                `${prefixCls}-hover-tip`,
              )}
            >
              离线
            </div>
          ) : (
            <div className={`${prefixCls}-tip`}>离线</div>
          )}
        </>
      )}
      <img
        alt=''
        ref={ref}
        src={site}
        className={`${prefixCls}-img`}
      />
    </div>
  );
};


IconOffline.defaultProps = {
  prefixCls: 'pxx',
};


export const IconVehicle = (props: any) => {
  const prefixCls = `${props.prefixCls}-amap-car-icon`;
  return (
    <img
      alt=''
      src={vehicle}
      className={prefixCls}
    />
  );
};

IconVehicle.defaultProps = {
  prefixCls: 'pxx',
};
