import React, {
  useRef,
  useEffect,
  useImperativeHandle,
  forwardRef,
} from 'react';
import '@amap/amap-jsapi-types';
import { useSetState } from 'ahooks';
import classnames from 'classnames';
import AMapContext from './context';

interface AMapInstance {
  (): {
    amap: typeof AMap.Map;
  };
}

export interface AMapProps {
  prefixCls?: string;
  width?: number | string;
  height?: number | string;
  onClick?: any;
  zoom?: number; // 初始地图级别
  zooms?: [number, number];
  center?: any; // 初始地图中心点
  onComplete?: any; // 地图加载完成
  className?: string;
  mapStyle?: string;
  pitch?: number;
  viewMode?: '2D' | '3D';
  resizeEnable?: boolean; // 是否监控地图容器尺寸变化
  children?: React.ReactNode;
}

const InlineAMap = forwardRef<AMapInstance, AMapProps>((props, ref) => {
  const prefixCls = `${props.prefixCls}-amap`;
  const amap = useRef<any>();
  const mapElement = useRef<any>();

  const [state, setState] = useSetState({ complete: false });

  useEffect(() => {
    amap.current = new AMap.Map(mapElement.current, {
      zoom: props.zoom,
      zooms: props.zooms,
      pitch: props.pitch,
      center: props.center,
      mapStyle: props.mapStyle,
      viewMode: props.viewMode,
      // @ts-ignore
      resizeEnable: props.resizeEnable,
    });

    amap.current.on('click', (...arg: any) => {
      props.onClick?.(amap.current, ...arg);
    });

    amap.current.on('complete', (...arg: any) => {
      setState({ complete: true });
      props.onComplete?.(amap.current, ...arg);
    });
  }, []);

  useImperativeHandle(ref, () => {
    return () => ({
      amap: amap.current,
      amapElement: mapElement.current,
    });
  });

  return (
    <AMapContext.Provider
      value={{
        amap: amap.current,
        complete: state.complete,
      }}
    >
      <div className={classnames(prefixCls, props.className)}>
        <div ref={mapElement} className={`${prefixCls}-container`} />
        {state.complete && props.children}
      </div>
    </AMapContext.Provider>
  );
});

InlineAMap.defaultProps = {
  zooms: [4, 20],
  prefixCls: 'pxx',
  resizeEnable: true,
};

export default InlineAMap;
