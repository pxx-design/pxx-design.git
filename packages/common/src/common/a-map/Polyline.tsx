import { useRef, useContext } from 'react';
import "@amap/amap-jsapi-types";
import Context from './context';
import { isEmpty, global } from '@parallel-line/utils';
import { useEffectDeep } from '@parallel-line/hooks';

interface LineProps {
  path: any;
  defaultVisible?: boolean;
  strokeColor?: string;
  strokeStyle?: string;
  strokeDasharray?: any[];
  zIndex?: string | number;
  strokeWeight?: string | number;
  strokeOpacity?: string | number;
  lineCap?: 'butt' | 'round' | 'square';
  lineJoin?: 'miter' | 'round' | 'bevel';
  name?: 'amapPlan' | 'amapOffline' | 'amapRectify' | 'appOnline' | 'sinoiovOnline' | any;
}

const styleMap: any = {
  amapPlan: {
    strokeWeight: '5',
    strokeOpacity: '1',
    strokeColor: '#1D56ED',
  },
  amapOffline: {
    strokeWeight: '5',
    strokeOpacity: '1',
    strokeStyle: 'dashed',
    strokeColor: '#7A86A5',
    strokeDasharray: [10, 5],
  },
  amapRectify: {
    strokeWeight: '5',
    strokeOpacity: '1',
    strokeColor: '#7A86A5',
  },
  appOnline: {
    strokeWeight: '3',
    strokeOpacity: '1',
    strokeColor: '#62D6B3',
  },
  sinoiovOnline: {
    strokeColor: '#B46AD7',
    strokeDasharray: [10, 5],
  },
  sinoiovRectifyTrack: {
    strokeColor: '#8BAA89',
    strokeWeight: '3',
    strokeOpacity: '1',
  },
  sinoiovOfflineSections: {
    strokeColor: '#8BAA89',
    strokeDasharray: [10, 5],
  },
  synthesisSections: {
    strokeColor: '#708BC2',
    strokeWeight: '3',
    strokeOpacity: '1',
  },
  offlineCompositeSections: {
    strokeWeight: '3',
    strokeOpacity: '1',
    strokeStyle: 'dashed',
    strokeColor: '#708BC2',
    strokeDasharray: [10, 5],
  }
};

const Polyline = (props: LineProps) => {
  const element = useRef<any>();
  const { amap } = useContext(Context);
  global?.eventEmitter?.useSubscription(({ uniqueKey, action, value }: any) => {
    if (uniqueKey === `AMAP_POLYLINE_${props.name}` || (uniqueKey === `AMAP_POLYLINE_synthesisSections` && ['synthesisSections', 'offlineCompositeSections'].includes(props.name))) {
      if (action === 'visible') {
        if (value) {
          element.current?.show();
        } else {
          element.current?.hide();
        }
      }
    }
  });
  const createElement = () => {
    if (isEmpty(props.path)) return;
    if (element.current) {
      element.current.setPath(props.path);
      return;
    }
    element.current = new AMap.Polyline({
      map: amap,
      path: props.path,
      zIndex: props.zIndex,
      lineCap: props.lineCap,
      lineJoin: props.lineJoin,
      strokeColor: props.strokeColor,
      strokeStyle: props.strokeStyle,
      strokeWeight: props.strokeWeight,
      strokeOpacity: props.strokeOpacity,
      strokeDasharray: props.strokeDasharray,
      // @ts-ignore
      ...styleMap[props.name],
    });
    if (props.defaultVisible) {
      element.current?.hide()
    }
  };
  // 首次创建
  useEffectDeep(() => {
    createElement();
    return () => {
      element.current?.setMap(null);
    };
  }, props.path);

  return null;
};

Polyline.defaultProps = {
  path: [],
  lineCap: 'round',
  lineJoin: 'round',
  strokeWeight: 5,
  strokeOpacity: 1,
  defaultVisible: false,
};

export default Polyline;
