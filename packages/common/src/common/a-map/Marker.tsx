import React, {useContext, useRef} from 'react';
import ReactDOM from 'react-dom';
import "@amap/amap-jsapi-types";
import Context from './context';
import InfoWindow from './InfoWindow';
import {useEffectDeep} from '@parallel-line/hooks';
import {global, isEmpty} from '@parallel-line/utils';
import {IconSite, IconVehicle, IconWarning, IconReport, IconOffline} from './icons';
import {WindowSite, WindowVehicle, WindowWarning, WindowReport, windowOffLine} from './windows';

interface MarkerProps {
  id?: any;
  name?: any;
  offset?: any;
  content?: any;
  position: any;
  extData?: any;
  onClick?: any;
  uniqueKey?: any;
  infoWindow?: any;
  visible?: boolean;
  zIndex?: any
}


const iconMap: any = {
  site: IconSite,
  offline: IconOffline,
  vehicle: IconVehicle,
  warning: IconWarning,
  report: IconReport
};
const windowMap: any = {
  site: WindowSite,
  vehicle: WindowVehicle,
  warning: WindowWarning,
  report: WindowReport,
  offline: windowOffLine
};
const Marker = (props: MarkerProps) => {
  const {name, id} = props;
  const element = useRef<any>();
  const { amap } = useContext(Context);
  const Content = props.content ?? iconMap[name];
  const InfoWindowContent = props.infoWindow?.content ?? windowMap[name];
  const contentNode = useRef<any>(document.createElement('div'));
  const createElement = () => {
    if (isEmpty(props.position)) return;
    if (element.current) {
      element.current.setPosition(props.position);
      return;
    }

    element.current = new AMap.Marker({
      map: amap,
      zIndex: props.zIndex,
      offset: props.offset,
      extData: props.extData,
      position: props.position,
      content: contentNode.current,
    });

    if (Content) {
      ReactDOM.render(<Content id={id} extData={props.extData}/>, contentNode.current);
    }
    // 绑定相关事件
    element.current.on('click', () => {
      global?.eventEmitter?.emit({
        id,
        value: true,
        action: 'visible',
        uniqueKey: 'AMAP_MARKER_WINDOW',
      });
      props.onClick?.();
    });
  };

  global?.eventEmitter?.useSubscription(({ uniqueKey, action, value }: any) => {
    if(name ==='vehicle'){
      if (uniqueKey == `AMARKER_MOVE` && action === 'MOVE_START') {
        element.current.hide()
      }

      if (uniqueKey == `replay` && action === 'moveend') {
        element.current.show()
      }
    }
    if (name=== 'report') {
      if (uniqueKey == `AMAP_MARKER_report` && action === 'visible') {
        if (value) {
          element.current.show()
        } else {
          element.current.hide()
        }
      }
    }
    if (name=== 'warning') {
      if (uniqueKey == `AMAP_MARKER_warning` && action === 'visible') {
        if (value) {
          element.current.show()
        } else {
          element.current.hide()
        }
      }
    }
  });


  // 首次创建
  useEffectDeep(() => {
    createElement();
    return () => {
      element.current?.setMap(null);
    };
  }, props.position);

  if (InfoWindowContent) {
    return <InfoWindow
      {...props.infoWindow}
      id={id}
      extData={props.extData}
      position={props.position}
      content={InfoWindowContent}/>;
  }

  return null;
};
Marker.defaultProps = {
  visible: true,
  position: [],
};
export default Marker;
