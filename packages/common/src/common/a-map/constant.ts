/**
 * 时间列宽
 */
export const DateTimeColWidth = 220;
/**
 * 城市列宽
 */
export const CityColWidth = 220;
/**
 * 行选择列宽
 */
export const SelectionColWidth = 40;

/**
 * 空值时显示的文本
 */
export const EmptyText = '--';
