import React, { useContext, useRef, useState } from 'react';
import '@amap/amap-jsapi-types';
import ReactDOM from 'react-dom';
import Context from './context';
import { useEffectDeep } from '@parallel-line/hooks';
import { global, isEmpty, toTime } from '@parallel-line/utils';
import vehicleImg from './images/vehicle.png';
import { WindowReplay } from './windows';

interface MarkerProps {
  id?: any;
  name?: any;
  offset?: any;
  content?: any;
  position: any;
  pointInfo?: any;
  onClick?: any;
  uniqueKey?: any;
  infoWindow?: any;
  visible?: boolean;
  zIndex?: any;
  path?: any[];
}

const styleMap: any = {
  amapPlan: {
    strokeWeight: '5',
    strokeOpacity: '1',
    strokeColor: '#1D56ED',
  },
  amapOffline: {
    strokeWeight: '5',
    strokeOpacity: '1',
    strokeStyle: 'dashed',
    strokeColor: '#7A86A5',
    strokeDasharray: [10, 5],
  },
  amapRectify: {
    strokeWeight: '5',
    strokeOpacity: '1',
    strokeColor: '#7A86A5',
  },
  appOnline: {
    strokeWeight: '5',
    strokeOpacity: '1',
    strokeColor: '#62D6B3',
  },
  sinoiovOnline: {
    strokeColor: '#B46AD7',
    // strokeDasharray: [10, 5],
    strokeOpacity: '1',
    strokeWeight: '5',
  },
  sinoiovRectifyTrack: {
    strokeColor: '#8BAA89',
    strokeWeight: '5',
    strokeOpacity: '1',
  },
  sinoiovOfflineSections: {
    strokeColor: '#8BAA89',
    // strokeDasharray: [10, 5],
    strokeOpacity: '1',
    strokeWeight: '5',
  },
  synthesisSections: {
    strokeColor: '#708BC2',
    strokeWeight: '5',
    strokeOpacity: '1',
  },
};

const MoveMarker = (props: MarkerProps) => {
  const { name, path = [], position = [], pointInfo = [] } = props;
  const element = useRef<any>();
  const lineElement = useRef<any>();
  const { amap } = useContext(Context);
  const [isMoving, setIsMoving] = useState<boolean>(false);
  const windowElement = useRef<any>();
  const contentDom = useRef<any>(document.createElement('div'));
  const length = AMap.GeometryUtil.distanceOfLine(path); // 米

  global?.eventEmitter?.useSubscription(({ uniqueKey, action, value }: any) => {
    if (uniqueKey === `AMARKER_MOVE`) {
      if (action === 'MOVE_START') {
        element.current.show();
        windowElement.current.open(amap);
        if (isMoving) {
          element.current.resumeMove();
        } else {
          element.current.moveAlong(path, {
            autoRotation: true,
            speed: length * 1000 / 10,
          });
        }
        setIsMoving(true);
      }
      if (action === 'MOVE_PAUSE') {
        element.current.pauseMove();
      }
      if (action === 'MOVE_RESTART') {
        element.current.moveAlong(path, {
          autoRotation: true,
          speed: length * 1000 / 10,
        });
      }
    }

    if (uniqueKey === `AMAP_POLYLINE_synthesisSections`) {
      if (action === 'visible') {
        if (value) {
          lineElement.current?.show();
        } else {
          lineElement.current?.hide();
        }
      }
    }
  });

  const createElement = () => {
    if (isEmpty(position)) return;
    if (element.current) {
      element.current.setPosition(position);
      return;
    }
    if (!lineElement.current) {
      lineElement.current = new AMap.Polyline({
        //@ts-ignore
        map: amap,
        ...styleMap[name],
      });
    }


    if (!windowElement.current) {
      windowElement.current = new AMap.InfoWindow({
        isCustom: true,
        position,
        offset: [32, -9],
        content: contentDom.current,
      });
    }

    element.current = new AMap.Marker({
      map: amap,
      zIndex: props.zIndex,
      position: position,
      icon: vehicleImg,
      offset: new AMap.Pixel(-13, -26),
    });

    element.current.hide();

    amap.setFitView();

    element.current.on('moving', (e: any) => {
      const pointData = {
        gpsPosTime: toTime(pointInfo[e.index]?.gpsPosTime, {
          format: 'YYYY/MM/DD HH:mm:ss',
        }),
        lon: pointInfo[e.index]?.lon,
        lat: pointInfo[e.index]?.lat,
        speed: pointInfo[e.index]?.speed ? `${pointInfo[e.index]?.speed}` : '',
      };

      ReactDOM.render(<WindowReplay extData={pointData} />, contentDom.current);
      windowElement.current.setPosition(e.passedPos);
      lineElement.current.setPath(e.passedPath);
      amap.setCenter(e.target.getPosition(), true);
    });

    element.current.on('moveend', (e: any) => {
      const { pos = [] } = e;
      const endPoint: any[] = path[path.length - 1] ?? [];
      if (pos[0] == endPoint[0] && pos[1] == endPoint[1]) {
        global?.eventEmitter?.emit({
          uniqueKey: 'replay',
          action: 'moveend',
        });
        windowElement.current.close();
        element.current.hide();
        setIsMoving(false);
      }
    });
  };

  // 首次创建
  useEffectDeep(() => {
    createElement();
    return () => {
      element.current?.setMap(null);
    };
  }, props.position);

  return null;
};

export default MoveMarker;
