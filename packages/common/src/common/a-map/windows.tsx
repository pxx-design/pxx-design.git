import React from 'react';
import classNames from 'classnames';
import { emptyString, isMobile } from '@parallel-line/utils';
import {
  CloseOutlined,
  TimeOutlined,
  CheckCircleFill,
  MoreArrowOutlined,
} from '@parallel-line/icons';

export const WindowSite = (props: any) => {
  const { prefixCls = 'pxx', extData } = props;
  const _isMobile = isMobile();
  const cls = `${prefixCls}-amap-window`;
  const clsSite = `${cls}-site`;
  const clsMobile = `${clsSite}-mobile`;
  return (
    <div
      className={classNames(cls, clsSite, {
        [clsMobile]: _isMobile,
      })}
    >
      <div className={`${clsSite}-title`}>
        {emptyString(extData?.title)}
        <CloseOutlined onClick={props.onClose} className={`${clsSite}-close`} />
      </div>
      <div className={`${clsSite}-addressName`}>
        {emptyString(extData?.addressName)}
      </div>
      <div className={`${clsSite}-address`}>
        {emptyString(extData?.address)}
      </div>
      <div className={`${clsSite}-contacts`}>
        {emptyString(extData?.contacts)}
        {emptyString(extData?.contactsPhone)}
      </div>
      <div className={`${clsSite}-time`}>
        <div className={`${clsSite}-time-item`}>
          {
            extData?.planArriveTime && <div className={`${clsSite}-time-plan`}>
              <TimeOutlined className={`${clsSite}-time-icon`} />
              计划到达：
              {extData?.planArriveTime}
            </div>}
          <div className={`${clsSite}-time-actual`}>
            <TimeOutlined className={`${clsSite}-time-icon`} />
            实际到达：
            {
              extData.autoClickIn && (
                <div>
                  <div style={{
                    backgroundColor: 'rgba(0,0,0,0.4)',
                    padding: 3,
                    borderRadius: 4,
                    marginBottom: 3,
                  }}>{extData.arriveFenceTime ? `${extData.arriveFenceTime}(电子围栏记录)` : '无电子围栏记录'}</div>
                  <div style={{
                    backgroundColor: 'rgba(0,0,0,0.4)',
                    padding: 3,
                    borderRadius: 4,
                    minWidth: 150,
                  }}>{extData?.actualArriveTime ? `${extData?.actualArriveTime}(司机手动操作)` : '无司机手动操作记录'}</div>
                </div>
              )
            }
            {
              !extData.autoClickIn &&
              <div style={{ minWidth: 115, backgroundColor: 'rgba(0,0,0,0.4)', padding: 3, borderRadius: 4 }}>
                {emptyString(extData?.actualArriveTime)}
              </div>
            }
            {extData?.isArriveLate && !extData.autoClickIn && (
              <div className={`${clsSite}-time-late`}>晚点</div>
            )}
          </div>
        </div>
        {!extData?.isLast && (
          <div className={`${clsSite}-time-item`}>
            {extData?.planDepartTime && <div className={`${clsSite}-time-plan`}>
              <TimeOutlined className={`${clsSite}-time-icon`} />
              计划发车：
              {extData?.planDepartTime}
            </div>}
            <div className={`${clsSite}-time-actual`}>
              <TimeOutlined className={`${clsSite}-time-icon`} />
              实际发车：
              {
                extData.autoClickIn && (
                  <div>
                    <div style={{
                      backgroundColor: 'rgba(0,0,0,0.4)',
                      padding: 3,
                      borderRadius: 4,
                      marginBottom: 3,
                    }}>{extData.departFenceTime ? `${extData.departFenceTime}(电子围栏记录)` : '无电子围栏记录'}</div>
                    <div style={{
                      backgroundColor: 'rgba(0,0,0,0.4)',
                      padding: 3,
                      borderRadius: 4,
                    }}>{extData?.actualDepartTime ? `${extData?.actualDepartTime}(司机手动操作)` : '无司机手动操作记录'}</div>
                  </div>
                )
              }
              {
                !extData.autoClickIn && (
                  <div style={{ minWidth: 115, backgroundColor: 'rgba(0,0,0,0.4)', padding: 3, borderRadius: 4 }}>
                    {emptyString(extData?.actualDepartTime)}
                  </div>)
              }
              {extData?.isDepartLate && (
                <div className={`${clsSite}-time-late`}>晚点</div>
              )}
            </div>
          </div>
        )}
        {extData?.isUnLoad && (
          <div className={`${clsSite}-time-item`}>
            {extData?.planUnloadTime && <div className={`${clsSite}-time-plan`}>
              <TimeOutlined className={`${clsSite}-time-icon`} />
              计划卸货：
              {extData?.planUnloadTime}
            </div>}
            {extData?.actualUnloadTime && <div className={`${clsSite}-time-actual`}>
              <TimeOutlined className={`${clsSite}-time-icon`} />
              实际卸货：
              <div style={{ minWidth: 115 }}>{extData?.actualUnloadTime}</div>
            </div>}
          </div>
        )}
      </div>
      <div>
        <div className={`${clsSite}-operation-wrap`}>
          <div className={`${clsSite}-operation`}>
            <div
              className={classNames(`${clsSite}-operation-item`, {
                [`${clsSite}-operation-active`]: extData?.isArrived,
              })}
            >
              <CheckCircleFill className={`${clsSite}-operation-icon`} />
              到达
            </div>
            {extData?.isLoad && (
              <div
                className={classNames(`${clsSite}-operation-item`, {
                  [`${clsSite}-operation-active`]: extData?.isLoaded,
                })}
              >
                <CheckCircleFill className={`${clsSite}-operation-icon`} />
                装货
              </div>
            )}
            {extData?.isUnLoad && (
              <div
                className={classNames(`${clsSite}-operation-item`, {
                  [`${clsSite}-operation-active`]: extData?.isUnloaded,
                })}
              >
                <CheckCircleFill className={`${clsSite}-operation-icon`} />
                卸货
              </div>
            )}
            {!extData?.isLast && (
              <div
                className={classNames(`${clsSite}-operation-item`, {
                  [`${clsSite}-operation-active`]: extData?.isDeparted,
                })}
              >
                <CheckCircleFill className={`${clsSite}-operation-icon`} />
                发车
              </div>
            )}
          </div>
          {
            (extData.loadNum || extData.unLoadNum) &&
            (
              <div
                className={`${clsSite}-operation-load`}
                onClick={() => {
                  extData.openLoad?.();
                }}>
                <span>{extData.loadNum || 0}装{extData.unLoadNum || 0}卸</span>
                <MoreArrowOutlined />
              </div>
            )
          }
        </div>
      </div>
      <div className={`${cls}-arrow`} />
    </div>
  );
};

export const WindowVehicle = (props: any) => {
  const { prefixCls = 'pxx', extData } = props;
  const _isMobile = isMobile();
  const cls = `${prefixCls}-amap-window`;
  const clsVehicle = `${cls}-vehicle`;
  const clsMobile = `${clsVehicle}-mobile`;

  return (
    <div
      className={classNames(cls, clsVehicle, {
        [clsMobile]: _isMobile,
      })}
    >
      <div className={`${clsVehicle}-title`}>
        {emptyString(extData?.number)}
        <CloseOutlined
          onClick={props.onClose}
          className={`${clsVehicle}-close`}
        />
      </div>
      <div className={`${clsVehicle}-info`}>
        <div className={`${clsVehicle}-item`}>
          <span className={`${clsVehicle}-label`}>速度：</span>
          {emptyString(extData?.speed)}km/h
        </div>
        <div className={`${clsVehicle}-item`}>
          <span className={`${clsVehicle}-label`}>定位时间：</span>
          {emptyString(extData?.locationTime)}
        </div>
      </div>
      <div className={`${clsVehicle}-info`}>
        <div className={`${clsVehicle}-item`}>
          <span className={`${clsVehicle}-label`}>经度：</span>
          {emptyString(extData.position?.[0])}
        </div>
        <div className={`${clsVehicle}-item`}>
          <span className={`${clsVehicle}-label`}>纬度：</span>
          {emptyString(extData.position?.[0])}
        </div>
      </div>
      <div className={`${clsVehicle}-item`}>
        <span className={`${clsVehicle}-label`}>位置：</span>
        {emptyString(extData?.address)}
      </div>
      <div className={`${cls}-arrow`} />
    </div>
  );
};

const titleMap: any = {
  0: '超速',
  1: '疲劳驾驶',
};

export const WindowWarning = (props: any) => {
  const { prefixCls = 'pxx', extData } = props;
  const _isMobile = isMobile();
  const cls = `${prefixCls}-amap-window`;
  const clsWarning = `${cls}-warning`;
  const clsMobile = `${clsWarning}-mobile`;

  if (extData.type === 0) {
    return (
      <div
        className={classNames(cls, clsWarning, {
          [clsMobile]: _isMobile,
        })}
      >
        <div className={`${clsWarning}-title`}>
          {titleMap[extData.type]}
          <CloseOutlined
            onClick={props.onClose}
            className={`${clsWarning}-close`}
          />
        </div>
        <div className={`${clsWarning}-info`}>
          <div className={`${clsWarning}-item`}>
            <span className={`${clsWarning}-label`}>超速时长：</span>
            {emptyString(extData?.duration)}
          </div>
        </div>
        <div className={`${clsWarning}-info`}>
          <div className={`${clsWarning}-item`}>
            <span className={`${clsWarning}-label`}>经度：</span>
            {emptyString(extData.position?.[0])}
          </div>
        </div>
        <div className={`${clsWarning}-info`}>
          <div className={`${clsWarning}-item`}>
            <span className={`${clsWarning}-label`}>纬度：</span>
            {emptyString(extData.position?.[1])}
          </div>
        </div>
        <div className={`${cls}-warning-arrow`} />
      </div>
    );
  }

  return (
    <div
      className={classNames(cls, clsWarning, {
        [clsMobile]: _isMobile,
      })}
    >
      <div className={`${clsWarning}-title`}>
        {titleMap[extData.type]}
        <CloseOutlined
          onClick={props.onClose}
          className={`${clsWarning}-close`}
        />
      </div>
      <div className={`${clsWarning}-info`}>
        <div className={`${clsWarning}-item`}>
          <span className={`${clsWarning}-label`}>连续驾驶时长：</span>
          {emptyString(extData?.duration)}
        </div>
      </div>
      <div className={`${clsWarning}-info`}>
        <div className={`${clsWarning}-item`}>
          <span className={`${clsWarning}-label`}>定位时间：</span>
          {emptyString(extData?.locationTime)}
        </div>
      </div>
      <div className={`${clsWarning}-info`}>
        <div className={`${clsWarning}-item`}>
          <span className={`${clsWarning}-label`}>经度：</span>
          {emptyString(extData.position?.[0])}
        </div>
      </div>
      <div className={`${clsWarning}-info`}>
        <div className={`${clsWarning}-item`}>
          <span className={`${clsWarning}-label`}>纬度：</span>
          {emptyString(extData.position?.[1])}
        </div>
      </div>
      <div className={`${cls}-warning-arrow`} />
    </div>
  );
};

export const WindowReport = (props: any) => {
  const { prefixCls = 'pxx', extData } = props;
  const _isMobile = isMobile();
  const cls = `${prefixCls}-amap-window`;
  const clsWarning = `${cls}-warning`;
  const clsMobile = `${clsWarning}-mobile`;

  return (
    <div
      className={classNames(cls, clsWarning, {
        [clsMobile]: _isMobile,
      })}
    >
      <div className={`${clsWarning}-title`}>
        异常上报
        <CloseOutlined
          onClick={props.onClose}
          className={`${clsWarning}-close`}
        />
      </div>
      <div className={`${clsWarning}-info`}>
        <div className={`${clsWarning}-item`}>
          <span className={`${clsWarning}-label`}>上报时间：</span>
          {emptyString(extData?.reportTime)}
        </div>
      </div>
      <div className={`${clsWarning}-info`}>
        <div className={`${clsWarning}-item`}>
          <span className={`${clsWarning}-label`}>位置：</span>
          {emptyString(extData?.address)}
        </div>
      </div>
      <div className={`${clsWarning}-info`}>
        <div className={`${clsWarning}-item`}>
          <span className={`${clsWarning}-label`}>异常类型：</span>
          {emptyString(extData?.reportType)}
        </div>
      </div>
      <div className={`${clsWarning}-info`}>
        <div className={`${clsWarning}-item`}>
          <span className={`${clsWarning}-label`}>异常情况说明：</span>
          {emptyString(extData?.reportDesc)}
        </div>
      </div>
      <div className={`${clsWarning}-info`}>
        <div className={`${clsWarning}-item`}>
          <span className={`${clsWarning}-label`}>凭证：</span>
          {
            extData?.imgUrls?.length ?
              <>
                {
                  extData?.imgUrls?.map((url: string) => {
                    return <img style={{ marginRight: 5, cursor: 'pointer' }} className={`${clsWarning}-img`}
                                onClick={() => {
                                  extData.clickImg?.(url);
                                }} src={url} alt='' />;
                  })
                }
              </> : null
          }
        </div>
      </div>
      <div className={`${cls}-warning-arrow`} />
    </div>
  );
};

export const windowOffLine = (props: any) => {
  const { prefixCls = 'pxx', extData } = props;
  const _isMobile = isMobile();
  const cls = `${prefixCls}-amap-window`;
  const clsWarning = `${cls}-warning`;
  const clsMobile = `${clsWarning}-mobile`;

  return (
    <div
      className={classNames(cls, clsWarning, {
        [clsMobile]: _isMobile,
      })}
    >
      <div className={`${clsWarning}-title`}>
        离线
        <CloseOutlined
          onClick={props.onClose}
          className={`${clsWarning}-close`}
        />
      </div>
      <div className={`${clsWarning}-info`}>
        <div className={`${clsWarning}-item`}>
          <span className={`${clsWarning}-label`}>离线开始时间：</span>
          {emptyString(extData?.offlineStartTime)}
        </div>
      </div>
      <div className={`${clsWarning}-info`}>
        <div className={`${clsWarning}-item`}>
          <span className={`${clsWarning}-label`}>离线结束时间：</span>
          {emptyString(extData?.offlineEndTime)}
        </div>
      </div>
      <div className={`${clsWarning}-info`}>
        <div className={`${clsWarning}-item`}>
          <span className={`${clsWarning}-label`}>经度：</span>
          {emptyString(extData.lon)}
        </div>
      </div>
      <div className={`${clsWarning}-info`}>
        <div className={`${clsWarning}-item`}>
          <span className={`${clsWarning}-label`}>纬度：</span>
          {emptyString(extData.lat)}
        </div>
      </div>
      <div className={`${clsWarning}-info`}>
        <div className={`${clsWarning}-item`}>
          <span className={`${clsWarning}-label`}>离线时长：</span>
          {extData.duration || '--'}
        </div>
      </div>
      <div className={`${cls}-warning-arrow`} />
    </div>
  );
};

export const WindowReplay = (props: any) => {
  const { prefixCls = 'pxx', extData } = props;
  const _isMobile = isMobile();
  const cls = `${prefixCls}-amap-window`;
  const clsWarning = `${cls}-warning`;
  const clsMobile = `${clsWarning}-mobile`;

  return (
    <div
      className={classNames(cls, clsWarning, {
        [clsMobile]: _isMobile,
      })}
    >
      <div className={`${clsWarning}-info`}>
        <div className={`${clsWarning}-item`}>
          <span className={`${clsWarning}-label`}>定位时间：</span>
          {emptyString(extData?.gpsPosTime)}
        </div>
      </div>
      <div className={`${clsWarning}-info`}>
        <div className={`${clsWarning}-item`}>
          <span className={`${clsWarning}-label`}>经度：</span>
          {emptyString(extData.lon)}
        </div>
      </div>
      <div className={`${clsWarning}-info`}>
        <div className={`${clsWarning}-item`}>
          <span className={`${clsWarning}-label`}>纬度：</span>
          {emptyString(extData.lat)}
        </div>
      </div>
      <div className={`${clsWarning}-info`}>
        <div className={`${clsWarning}-item`}>
          <span className={`${clsWarning}-label`}>速度：</span>
          {extData.speed || '--'}km/h
        </div>
      </div>
      <div className={`${cls}-warning-arrow`} />
    </div>
  );
};


