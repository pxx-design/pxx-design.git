import {useContext, useEffect, useRef} from 'react';
import {isEmpty} from '@parallel-line/utils';
import "@amap/amap-jsapi-types";
import Context from './context';

interface CircleProps {
  center: AMap.LngLatLike;
  radius?: number;
  fillColor?: string;
  fillOpacity?: number;
  strokeColor?: string;
  strokeOpacity?: number;
  strokeWeight?: number;
}


const Circle = (props: CircleProps) => {
  const {amap} = useContext(Context);
  const element = useRef<any>();

  const createElement = () => {
    if (isEmpty(props.center)) return;
    if (element.current) {
      element.current.setCenter(props.center);
      element.current.setRadius(props.radius);
      return;
    }
    element.current = new AMap.Circle({
      center: props.center,
      radius: props.radius,
      fillColor: props.fillColor,
      fillOpacity: props.fillOpacity,
      strokeColor: props.strokeColor,
      strokeOpacity: props.strokeOpacity,
      strokeWeight: props.strokeWeight,
    });
    amap.add(element.current)
  };

  useEffect(() => {
    createElement();
    return () => {
      element.current?.setMap(null);
    };
  }, [props.center]);

  return null;
};

Circle.defaultProps = {
  radius: 0,
  fillColor: '#0085ff',
  fillOpacity: 0.1,
  strokeColor: '#1e58f0', // 描边颜色
  strokeOpacity: 0.8,
  strokeWeight: 2, // 描边宽度
};

export default Circle;
