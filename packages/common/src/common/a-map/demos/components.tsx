import React, {useState} from 'react';
import moment from 'moment';
import {message} from 'antd';
import {sleep, toTime} from '@parallel-line/utils';
import {AMap} from '@parallel-line/common';

const {Polyline, Marker, MoveMarker} = AMap;
const {Zoom, UpdateTime, Legend, Replay} = AMap.components;


const demoPath = [[114.116428, 22.545426], [114.116286, 22.545609], [114.116286, 22.545609], [114.116385, 22.545703], [114.116629, 22.545955], [114.116629, 22.545955], [114.116749, 22.546042], [114.116854, 22.546024], [114.117215, 22.545708], [114.117215, 22.545708], [114.116914, 22.545430], [114.116620, 22.545253], [114.116529, 22.545166], [114.116529, 22.545166]];
const polylinePath = [
  [
    [75.757904, 35.118117],
    [90.375719, 35.118117],
  ],
  [
    [95.757904, 35.118117],
    [110.375719, 35.118117],
  ],
  [
    [75.757904, 38.118117],
    [90.375719, 38.118117]
  ],
  [
    [95.757904, 38.118117],
    [110.375719, 38.118117]
  ],
  [
    [75.757904, 41.118117],
    [90.375719, 41.118117]
  ],
  [
    [95.757904, 41.118117],
    [110.375719, 41.118117]
  ],
  [
    [75.757904, 44.118117],
    [90.375719, 44.118117]
  ],
  [
    [95.757904, 44.118117],
    [110.375719, 44.118117]
  ],
  [
    [75.757904, 47.118117],
    [90.375719, 47.118117]
  ],
];

const markers = [
  [75.757904, 35.118117],
  [90.375719, 35.118117],
  [95.757904, 35.118117],
  [110.375719, 35.118117],
  [75.757904, 38.118117],
  [90.375719, 38.118117],
  [95.757904, 38.118117],
  [110.375719, 38.118117],
  [75.757904, 41.118117],
  [90.375719, 41.118117],
  [95.757904, 41.118117],
  [110.375719, 41.118117],
  [75.757904, 44.118117],
  [90.375719, 44.118117],
  [95.757904, 44.118117],
  [110.375719, 44.118117],
];

const legendConfig = [
  {
    label: '当前位置',
    name: 'vehicle',
    type: 'MARKER',
  },
  {
    label: '站点',
    name: 'site',
    type: 'MARKER',
  },
  {
    label: '异常上报',
    name: 'report',
    type: 'MARKER',
    checkable: true,
    defaultChecked: true,
    disabled: true
  },
  {
    label: '疲劳价值/超速预警',
    name: 'warning',
    type: 'MARKER',
    checkable: true,
    defaultChecked: true,
  },
  {
    label: '中交原始轨迹',
    name: 'sinoiovOnline',
    type: 'POLYLINE',
    checkable: true,
    defaultChecked: true,
  },
  {
    label: '中交纠偏轨迹',
    name: 'sinoiovRectifyTrack',
    type: 'POLYLINE',
    checkable: true,
    defaultChecked: true,
  },
  {
    label: '中交离线轨迹',
    name: 'sinoiovOfflineSections',
    type: 'POLYLINE',
    checkable: true,
    defaultChecked: false,
  },
  {
    label: 'APP 原始轨迹',
    name: 'appOnline',
    type: 'POLYLINE',
    checkable: true,
    defaultChecked: true,
  },
  {
    label: 'APP 纠偏轨迹',
    name: 'amapRectify',
    type: 'POLYLINE',
    checkable: true,
    defaultChecked: true,
  },
  {
    label: 'APP 中断轨迹',
    name: 'amapOffline',
    type: 'POLYLINE',
    checkable: true,
    defaultChecked: true,
  },
  {
    label: '规划轨迹',
    name: 'amapPlan',
    type: 'POLYLINE',
    checkable: true,
    defaultChecked: true,
  },
  {
    label: '综合定位轨迹',
    name: 'synthesisSections',
    type: 'POLYLINE',
    checkable: true,
    defaultChecked: true,
  }
];

const polylineType = [
  'sinoiovOnline',
  'sinoiovRectifyTrack',
  'sinoiovOfflineSections',
  'appOnline',
  'amapRectify',
  'amapOffline',
  'amapPlan',
  'synthesisSections',
  'offlineCompositeSections'
];
const Components = () => {
  const curTime = toTime(moment());
  const [time, setTime] = useState(curTime);
  const handleComplete = async (amap: any) => {
    await sleep();
    const allOverlays = amap.getAllOverlays();
    amap.setFitView(allOverlays);
  };
  const handleUpdateTime = () => {
    message.success('更新成功');
    setTime(curTime);
  };

  return (
    <div style={{height: 500}}>
      <AMap onComplete={handleComplete}>
        {polylinePath?.map((path: any, index: number) => {
          return (
            <Polyline key={index} path={path} defaultVisible={false} name={polylineType[index]}/>
          );
        })}
        {markers.map((position, index) => {
          return (
            <Marker
              key={index}
              id={index}
              name="site"
              offset={[-9, -9]}
              extData={{
                index,
                title: `站点${index + 1}`,
                address: '深圳市南山区南山街道学府路大新时代大厦A座601',
                contacts: '王麻子',
                contactsPhone: '12345678901',
                planArriveTime: '2021/12/08 12:00',
                actualArriveTime: '2021/12/08 12:00',
                planDepartTime: '2021/12/08 12:00',
                actualDepartTime: '2021/12/08 12:00',
                isDepartLate: true,
                isArriveLate: true,
                isLoad: true,
                isUnLoad: true,
                isArrived: true,
                isLoaded: true,
              }}
              infoWindow={{
                offset: [0, -16],
              }}
              position={position}
            />
          );
        })}
        <AMap.Marker
          id="2"
          name="vehicle"
          offset={[-35, -17.5]}
          extData={{
            number: '粤 BAD218',
            speed: 60,
            locationTime: '2021/12/08 12:00',
            address:
              '深圳市南山区南山街道学府路大新时代大厦A座601深圳市南山区南山街道学府路大新时代大厦A座601',
          }}
          infoWindow={{
            offset: [0, -18],
          }}
          position={[116.445467, 39.935467]}
        />
        <MoveMarker
          id={'replay'}
          position={demoPath[0]}
          offset={[-35, -17.5]}
          name={'sinoiovOnline'}
          path={demoPath}
          pointInfo={[
            {lon: "113.911575", lat: "22.528181", speed: null, gpsPosTime: null},
            {lon: "113.911700", lat: "22.528163", speed: null, gpsPosTime: null},
            {lon: "113.911888", lat: "22.528158", speed: null, gpsPosTime: null},
            {lon: "113.911957", lat: "22.528152", speed: null, gpsPosTime: null},
            {lon: "113.912290", lat: "22.528142", speed: null, gpsPosTime: null},
            {lon: "113.913269", lat: "22.528158", speed: null, gpsPosTime: null},
            {lon: "113.913605", lat: "22.528164", speed: null, gpsPosTime: null}
          ]}
        />
        <Replay/>
        <UpdateTime time={time} onUpdateTime={handleUpdateTime}/>
        <Legend config={legendConfig}/>
        <Zoom/>
      </AMap>
    </div>
  );
};

export default Components;
