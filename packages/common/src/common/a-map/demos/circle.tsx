import React from 'react';
import { AMap } from '@parallel-line/common';
import { sleep } from '@parallel-line/utils';

export default () => {
  const handleComplete = async (amap: any) => {
    await sleep();
    const allOverlays = amap.getAllOverlays();
    amap.setFitView(allOverlays);
  };

  return (
    <div style={{ height: 500 }}>
      <AMap zoom={13} center={[116.43, 39.92]} onComplete={handleComplete}>
        {/* size 单位为米（m） */}
        <AMap.Circle
          radius={100}
          center={[116.425467, 39.935467]}
          fillColor={'#0085ff'} // 填充颜色 不填默认为#0085ff
          strokeColor={'#1e58f0'} // 描边颜色 不填默认为#1e58f0
          fillOpacity={0.1} // 填充透明度 不填默认为0.1
          strokeOpacity={0.8} // 描边透明度 不填默认为0.8
          strokeWeight={2} // 描边宽度 不填默认为2
        />
      </AMap>
    </div>
  );
};
