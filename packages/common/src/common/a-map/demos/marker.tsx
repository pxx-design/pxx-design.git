import React from 'react';
import { AMap } from '@parallel-line/common';
import { sleep } from '@parallel-line/utils';

export default () => {
  const handleComplete = async (amap: any) => {
    await sleep();
    const allOverlays = amap.getAllOverlays();
    amap.setFitView(allOverlays);
  };

  return (
    <div style={{ height: 500 }}>
      <AMap zoom={13} center={[116.43, 41.92]} onComplete={handleComplete}>
        <AMap.Marker
          id="5"
          name="report"
          offset={[-35, -17.5]}
          extData={{
            reportTime: '2021/11/21 10:08:01',
            address: '这是一个地址',
            reportType: '车坏了',
            reportDesc: '车轮子掉了,不知道丢哪里去了',
            imgUrls: []
          }}
          infoWindow={{
            offset: [0, -18],
          }}
          position={[116, 40.5]}
        />
        <AMap.Marker
          id="4"
          name="warning"
          offset={[-35, -17.5]}
          extData={{
            type: 0,  // 0超速 1疲劳驾驶
            duration: '20分钟',
            position: [116, 39],
            address: '这是一个地址'
          }}
          infoWindow={{
            offset: [0, -18],
          }}
          position={[116, 40.3]}
        />
        <AMap.Marker
          id="3"
          name="warning"
          offset={[-35, -17.5]}
          extData={{
            type: 1,
            duration: '20分钟',
            position: [116, 39],
            locationTime: '2021/10/11 12:23:12',
            address: '这是一个地址'
          }}
          infoWindow={{
            offset: [0, -18],
          }}
          position={[116.445467, 40.3]}
        />
        <AMap.Marker
          id="2"
          name="vehicle"
          offset={[-35, -17.5]}
          extData={{
            number: '粤 BAD218',
            speed: 60,
            locationTime: '2021/12/08 12:00',
            address:
              '深圳市南山区南山街道学府路大新时代大厦A座601深圳市南山区南山街道学府路大新时代大厦A座601',
          }}
          infoWindow={{
            offset: [0, -18],
          }}
          position={[116.445467, 39.935467]}
        />
        <AMap.Marker
          id="1"
          name="site"
          offset={[-9, -9]}
          extData={{
            title: '站点1',
            addressName: '站点名称',
            address:
              '深圳市南山区南山街道学府路大新时代大厦A座601深圳市南山区南山街道学府路大新时代大厦A座601',
            contacts: '王麻子',
            contactsPhone: '12345678901',
            planArriveTime: '2021/12/08 12:00',
            actualArriveTime: '2021/12/08 12:00',
            planDepartTime: '2021/12/08 12:00',
            actualDepartTime: '',//'2021/12/08 12:00',
            isDepartLate: true,
            isArriveLate: true,
            isLoad: true,
            isUnLoad: true,
            isArrived: true,
            isLoaded: true,
            // autoClickIn: true,
            // arriveFenceTime: '2021/12/08 12:00',
            // departFenceTime: '2021/12/08 12:00',
            // loadNum: 1,
            // unLoadNum: 2,
            // openLoad: () => {
            //   console.log('openLoad')
            // }
          }}
          infoWindow={{
            offset: [0, -16],
          }}
          position={[116.425467, 39.935467]}
        />
      </AMap>
    </div>
  );
};
