import React from 'react';
import { AMap } from '@parallel-line/common';
import { sleep } from '@parallel-line/utils';

const { Polyline } = AMap;

export default () => {
  const handleComplete = async (amap: any) => {
    await sleep();
    const allOverlays = amap.getAllOverlays();
    amap.setFitView(allOverlays);
  };
  
  return (
    <div style={{ height: 500 }}>
      <AMap onComplete={handleComplete}>
        {/* amap规划轨迹 */}
        <Polyline
          defaultVisible={true}
          strokeWeight="5"
          strokeOpacity="1"
          strokeColor="#1D56ED"
          path={[
            [75.757904, 38.118117],
            [97.375719, 24.598057],
            [117.375719, 38.118117],
          ]}
        />
        {/* amap离线(中断)轨迹 */}
        <Polyline
          strokeWeight="5"
          strokeOpacity="1"
          strokeStyle="dashed"
          strokeColor="#7A86A5"
          strokeDasharray={[10, 5]}
          path={[
            [75.757904, 41.118117],
            [97.375719, 27.598057],
            [117.375719, 41.118117],
          ]}
        />
        {/* amap纠偏轨迹 */}
        <Polyline
          strokeWeight="5"
          strokeOpacity="1"
          strokeColor="#7A86A5"
          path={[
            [75.757904, 44.118117],
            [97.375719, 30.598057],
            [117.375719, 44.118117],
          ]}
        />
        {/* app原始轨迹 */}
        <Polyline
          strokeWeight="3"
          strokeOpacity="1"
          strokeColor="#62D6B3"
          path={[
            [75.757904, 47.118117],
            [97.375719, 33.598057],
            [117.375719, 47.118117],
          ]}
        />
        {/* 中交纠偏轨迹 */}
        <Polyline
          strokeWeight="3"
          strokeOpacity="1"
          strokeColor="#80a07e"
          path={[
            [75.757904, 50.118117],
            [97.375719, 36.598057],
            [117.375719, 50.118117],
          ]}
        />
      </AMap>
    </div>
  );
};
