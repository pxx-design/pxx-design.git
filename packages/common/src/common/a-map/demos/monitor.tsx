import React from 'react';
import {AMap} from '@parallel-line/common';
import {sleep} from '@parallel-line/utils';

const monitor = {
  offline: {
    title: '离线',
    list: [
      {
        sectionId: '552',
        startTime: '2021-06-29T13:51:28',
        type: 'offline',
      },
      {
        sectionId: '553',
        startTime: '2021-06-28T13:51:28',
        type: 'offline',
      },
    ],
  },
  abnormalReports: {
    title: '异常上报',
    list: [
      {
        sectionId: '3',
        startTime: '2021-06-30T13:51:28',
        type: 'abnormalReports',
        description: '轮胎漏气',
      },
      {
        sectionId: '555',
        startTime: '2021-07-01T13:51:28',
        type: 'abnormalReports',
      },
      {
        sectionId: 'sinoiov_1',
        startTime: '2921/12/1T22:33:12',
        description: '疲劳驾驶',
        type: 'abnormalReports',
      }
    ],
  },
  autoClockIn: {
    title: '自动打卡',
    list: [
      {
        startTime: '2021-07-04T13:51:28',
        type: 'autoClockIn',
        point: [116.425467, 39.935467],
        operationType: 20,
        description: '下雨路滑',
        id: 'abc'
      },
    ],
  },
};

export default () => {
  const handleComplete = async (amap: any) => {
    await sleep();
    const allOverlays = amap.getAllOverlays();
    amap.setFitView(allOverlays);
  };

  return (
    <div style={{height: 500}}>
      <AMap zoom={13} center={[116.43, 39.92]} onComplete={handleComplete}>
        {
          /*
        status 0 待执行' 1 在线 2 已完成 3 已关闭 null或不传为离线

        monitor 为监控内容 目前支持四种type：
          offline 离线
          abnormalReports 异常上报
          operationAbnormals 晚点
          autoClockIn 自动打卡
        其中非自动打卡sectionId 对应地图window或点的id 点击查看详情回调返回sectionId
        自动打卡 operationType 为打卡类型  0: '启动'  10: '到车'  20: '全部装货完成'  30: '全部卸货完成' 40: '发车'
        点击查看详情返回 自动打卡坐标(数组类型)
       */
        }
        <AMap.Marker
          id="sinoiov_1"
          name="warning"
          offset={[-35, -17.5]}
          extData={{
              type: 0,
              duration: `${0} 分钟`,
              position: [113, 23],
              locationTime: '2021/12/08 12:00',
          }}
          infoWindow={{
            offset: [0, -18],
          }}
          position={[116.445467, 40.3]}
        />
        <AMap.Circle
          radius={100}
          center={[116.425467, 39.935467]}
          fillColor={'#0085ff'} // 填充颜色 不填默认为#0085ff
          strokeColor={'#1e58f0'} // 描边颜色 不填默认为#1e58f0
          fillOpacity={0.1} // 填充透明度 不填默认为0.1
          strokeOpacity={0.8} // 描边透明度 不填默认为0.8
          strokeWeight={2} // 描边宽度 不填默认为2
        />
        <AMap.Marker
          id="abc"
          name="site"
          offset={[-9, -9]}
          extData={{
            title: '站点1',
            address:
              '深圳市南山区南山街道学府路大新时代大厦A座601深圳市南山区南山街道学府路大新时代大厦A座601',
            contacts: '王麻子',
            contactsPhone: '12345678901',
            planArriveTime: '2021/12/08 12:00',
            actualArriveTime: '2021/12/08 12:00',
            planDepartTime: '2021/12/08 12:00',
            actualDepartTime: '2021/12/08 12:00',
            isDepartLate: true,
            isArriveLate: true,
            isLoad: true,
            isUnLoad: true,
            isArrived: true,
            isLoaded: true,
          }}
          infoWindow={{
            offset: [0, -16],
          }}
          position={[116.425467, 39.935467]}
        />
        <AMap.Marker
          id="3"
          name="warning"
          offset={[-35, -17.5]}
          extData={{
            type: 1,
            duration: '20分钟',
            position: [116, 39],
            locationTime: '2021/10/11 12:23:12',
            address: '这是一个地址'
          }}
          infoWindow={{
            offset: [0, -18],
          }}
          position={[116.445467, 40.3]}
        />
        <AMap.Monitor
          status="-1"
          monitor={monitor}
        />
      </AMap>
    </div>
  );
};
