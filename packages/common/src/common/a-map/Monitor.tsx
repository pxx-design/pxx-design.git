import React, {useState} from 'react';
import classNames from 'classnames';
import {useToggle} from 'ahooks';
import Timeline from '../timeline';
import {toTime, global} from '@parallel-line/utils';
import moment from 'moment';
import open from './images/open.svg';
import close from './images/close.svg';

interface MonitorProps {
  status: string;
  monitor: any;
  openWindow?: any;
}

const prefixCls = `amap-monitor`;

const statusMap: any = {
  '0': {
    title: '待执行',
  },
  '1': {
    title: '在线',
  },
  '2': {
    title: '已完成',
  },
  '3': {
    title: '已关闭',
  },
};

const operationTypeSource: any = {
  0: '启动',
  10: '到车',
  20: '全部装货完成',
  30: '全部卸货完成',
  40: '发车',
};

const monitorTitleMap: any = {
  offline: '离线',
  abnormalReports: '异常上报',
  operationAbnormals: '晚点',
  autoClockIn: '自动打卡',
};

const getMonitorList = (monitor: any) => {
  const list = Object.keys(monitor)?.reduce((prev: any, key: any) => {
    prev = prev.concat(monitor[key]?.list || []);
    return prev;
  }, []);

  return list.sort(
    // @ts-ignore
    (a: any, b: any) => moment(b.startTime) - moment(a.startTime),
  );
};

const TimeLineItem = ({color, monitor, click}: any) => {
  return (
    <Timeline.Item color={color} style={{paddingBottom: 0}}>
      <div className={`${prefixCls}-time-line-item-title`}>
        {monitorTitleMap[monitor.type]}
      </div>
      <div className={`${prefixCls}-time-line-item-description`}>
        {monitor.type === 'offline' && <div>司机GPS信号离线。</div>}
        {monitor.type === 'autoClockIn' && (
          <div>{`自动${
            operationTypeSource[monitor.operationType] || '发车'
          }打卡`}</div>
        )}
        {monitor.type === 'abnormalPoints' && (
          <div>异常上报</div>
        )}
        {monitor.type !== 'autoClockIn' &&
        monitor.type !== 'offline' &&
        monitor.description && <div>{monitor.description}</div>}
        <span
          onClick={() => {
            click?.(
              monitor.type !== 'autoClockIn'
                ? monitor.sectionId
                : monitor.id,
            );
          }
          }
        >
          查看详情
        </span>
      </div>
      <div className={`${prefixCls}-time-line-item-time`}>
        {toTime(monitor.startTime, {format: 'YYYY/MM/DD HH:mm'})}
      </div>
    </Timeline.Item>
  );
};

const Monitor = (props: MonitorProps) => {
  const [activeMenu, setActiveMenu] = useState<any>('');
  const monitorList = getMonitorList(props.monitor ?? {});

  return (
    <div className={prefixCls}>
      <div className={`${prefixCls}-head`}>
        <div className={`${prefixCls}-head-tips`}>
          <div
            className={`${prefixCls}-head-status`}
            style={{
              color: ['1', '2', '3'].includes(`${props.status}`)
                ? '#3EC528'
                : 'rgba(250, 125, 0, 0.85)',
            }}
          >
            {statusMap[`${props.status}`]?.title || '司机GPS信号已离线'}
          </div>
          {activeMenu && <img alt={''} src={close} onClick={() => setActiveMenu('')}/>}
        </div>
        {!statusMap[`${props.status}`]?.title && <div className={`${prefixCls}-head-status-offline`}>请联系司机查看执行情况</div>}
        <div className={`${prefixCls}-head-menu`}>
          {Object.keys(props.monitor).map((key: string) => {
            if (!props.monitor?.[key]?.list?.length) {
              return null;
            }

            return (
              <span
                onClick={() => {
                  setActiveMenu(activeMenu === key ? '' : key);
                }}
                key={key}
                className={classNames(`${prefixCls}-head-menu-item`, {
                  [`${prefixCls}-head-menu-item-active`]: activeMenu === key,
                })}
              >
                {props.monitor?.[key]?.title}(
                {props.monitor?.[key]?.list?.length})
              </span>
            );
          })}
        </div>
      </div>
      {activeMenu && monitorList.length > 0 && (
        <div className={`${prefixCls}-wrap`}>
          <Timeline>
            {monitorList.map((monitor: any, index: number) => {
              if (activeMenu !== '' && monitor.type !== activeMenu) {
                return null;
              }

              return (
                <TimeLineItem
                  monitor={monitor}
                  click={(sectionId: string) => {
                    global?.eventEmitter?.emit({
                      id: sectionId,
                      value: true,
                      action: 'visible',
                      uniqueKey: 'AMAP_MARKER_WINDOW',
                    });
                  }}
                  key={`${monitor.sectionId}-${index}`}
                  color={index === 0 ? '#1E56FF' : '#CCCED2'}
                />
              );
            })}
          </Timeline>
        </div>
      )}
    </div>
  );
};

Monitor.defaultProps = {
  status: '-1',
  monitor: {},
};

export default Monitor;
