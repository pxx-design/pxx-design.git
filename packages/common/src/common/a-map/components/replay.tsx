import React, {useState} from 'react';
// @ts-ignore
import {PlayOutlined, PauseOutlined} from '@parallel-line/icons';
import disablePlay from '../images/disablePlay.svg'
import play from '../images/play.svg'
import {global} from '@parallel-line/utils';

const Replay = (props: any) => {
  const [active, setActive] = useState(false);
  const [isPause, setIsPause] = useState(false);
  const prefixCls = `${props.prefixCls}-amap-replay`;

  global?.eventEmitter?.useSubscription(({uniqueKey, action}: any) => {
    if (uniqueKey != 'replay') return
    if (action == 'moveend') {
      setActive(false)
      setIsPause(false)
    }
  })

  return (
    <div className={prefixCls}>
      <span className={`${prefixCls}-text`}>轨迹回放</span>
      <div className={`${prefixCls}-play`} onClick={() => {
        global?.eventEmitter?.emit({
          action: isPause ? 'MOVE_PAUSE' : 'MOVE_START',
          uniqueKey: 'AMARKER_MOVE',
        });
        setIsPause(!isPause)
        setActive(true)
      }}>
        {!isPause ? <PlayOutlined style={{color: '#0256ff'}}/> : <PauseOutlined style={{color: '#0256ff'}}/>}
        <span style={{paddingRight: 8, paddingLeft: 6}}>{isPause ? '暂停' : '播放'}</span>
      </div>
      <img src={active ? play : disablePlay} style={{cursor: !active ? 'not-allowed' : 'pointer'}} onClick={() => {
        if (active) {
          setIsPause(true)
          global?.eventEmitter?.emit({
            action: 'MOVE_RESTART',
            uniqueKey: 'AMARKER_MOVE',
          });
        }
      }} className={`${prefixCls}-rePlay`} alt=""/>
    </div>
  )
}

Replay.defaultProps = {
  prefixCls: 'pxx',
};


export default Replay
