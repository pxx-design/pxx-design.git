import React, { useContext } from 'react';
import Context from '../context';
import { MinusOutlined, PlusOutlined } from '@parallel-line/icons';

interface ZoomProps {
  prefixCls?: string;
}

const Zoom = (props: ZoomProps) => {
  const prefixCls = `${props.prefixCls}-amap-zoom`;
  const { amap } = useContext(Context);
  const setZoom = (type: string) => {
    const zoom = amap.getZoom();
    amap.setZoom(type === 'plus' ? zoom + 1 : zoom - 1);
  };
  return (
    <div className={prefixCls}>
      <MinusOutlined
        className={`${prefixCls}-icon`}
        onClick={() => setZoom('minus')}
      />
      <PlusOutlined
        className={`${prefixCls}-icon`}
        onClick={() => setZoom('plus')}
      />
    </div>
  );
};
Zoom.defaultProps = {
  prefixCls: 'pxx',
};

export default Zoom;
