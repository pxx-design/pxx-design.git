import React from 'react';
import classNames from 'classnames';
import {useToggle} from 'ahooks';
import {isBoolean} from 'lodash-es';
import {global} from '@parallel-line/utils';
import site from '../images/site.svg';
import vehicle from '../images/vehicle.svg';
import legendImg from '../images/legend.svg';
import amapPlan from '../images/amapPlan.svg';
import appOnline from '../images/appOnline.svg';
import amapOffline from '../images/amapOffline.svg';
import amapRectify from '../images/amapRectify.svg';
import sinoiovOnline from '../images/sinoiovOnline.svg';
import zjjp from '../images/zjjp.svg'
import report from '../images/report.svg'
import warning from '../images/warning.svg'
import zjOffline from '../images/zjOffline.svg'
import synthesisSections from '../images/synthesisSections.svg'
import {ShrinkOutlined} from '@parallel-line/icons';

const imageMaps: any = {
  site: {
    src: site,
    width: '18px',
  },
  vehicle: {
    src: vehicle,
    width: '32px',
  },
  report: {
    src: report,
    width: '18px'
  },
  warning: {
    src: warning,
    width: '18px'
  },
  amapPlan: {
    src: amapPlan,
    width: '28px',
  },
  appOnline: {
    src: appOnline,
    width: '28px',
  },
  amapOffline: {
    src: amapOffline,
    width: '28px',
  },
  amapRectify: {
    src: amapRectify,
    width: '28px',
  },
  sinoiovOnline: {
    src: sinoiovOnline,
    width: '28px',
  },
  sinoiovRectifyTrack: { // 中交纠偏
    src: zjjp,
    width: '28px',
  },
  sinoiovOfflineSections: { // 中交离线
    src: zjOffline,
    widht: '28px'
  },
  synthesisSections: { // 综合定位轨迹
    src: synthesisSections,
    widht: '28px'
  }
};

const Legend = (props: any) => {
  const prefixCls = `${props.prefixCls}-amap-legend`;
  const [active, {toggle}] = useToggle();
  const [config, setConfig] = React.useState<any[]>(props.config);

  const handleChange = (item: any, value: boolean) => {
    if (!isBoolean(item.defaultChecked)) return;
    const {type, name} = item;
    const inx = config.findIndex((v) => v === item);
    config[inx].checked = value;
    setConfig([...config]);
    props.onChange?.(type, name, value);
    global.eventEmitter.emit({
      uniqueKey: `AMAP_${type}_${name}`,
      action: 'visible',
      value,
    });
  };

  return (
    <div className={classNames(prefixCls, {[`${prefixCls}-active`]: active})}>
      <div className={`${prefixCls}-min`} onClick={() => toggle()}>
        <img alt="" src={legendImg} className={`${prefixCls}-img`}/>
        <span>图例</span>
      </div>
      <div className={`${prefixCls}-max`}>
        <div className={`${prefixCls}-max-close`}>
          <ShrinkOutlined
            onClick={() => toggle()}
            className={`${prefixCls}-max-close-icon`}
          />
        </div>
        {props.config?.map((item: any) => {
          const {checked = item.defaultChecked, checkable = false, disabled = false, name, label} = item;
          {/* checkable 是否显示checkbox  disabled 是否disabled  checked(defaultChecked) 默认是否选中 */}
          return (
            <div key={name} className={`${prefixCls}-max-item`}>
              <div className={`${prefixCls}-max-item-imgWrapper`}>
                <img
                  alt=""
                  src={imageMaps[name]?.src}
                  width={imageMaps[name]?.width}
                  className={`${prefixCls}-max-item-img`}
                />
              </div>
              <span>
               {label}
              </span>
              {checkable ? (
                <>
                  {
                    disabled && (
                      <div className={classNames({
                        [`${prefixCls}-max-checkbox`]: true,
                        [`${prefixCls}-max-checkbox-disabled`]: true,
                      })}
                      >
                        <span className={`${prefixCls}-max-checkbox-inner`}/>
                      </div>
                    )
                  }
                  {
                    !disabled && <div
                      className={classNames({
                        [`${prefixCls}-max-checkbox`]: true,
                        [`${prefixCls}-max-checkbox-checked`]: checked,
                      })}
                      onClick={() => handleChange(item, !checked)}
                    >
                      <span className={`${prefixCls}-max-checkbox-inner`}/>
                    </div>
                  }
                </>
              ) : null}
            </div>
          );
        })}
      </div>
    </div>
  );
};

Legend.defaultProps = {
  prefixCls: 'pxx',
};

export default Legend;
