import React from 'react';
import { emptyString } from '@parallel-line/utils';
import { RefreshOutlined } from '@parallel-line/icons';

const UpdateTime = (props: any) => {
  const prefixCls = `${props.prefixCls}-amap-updateTime`;
  return (
    <div className={prefixCls}>
      位置更新于：{emptyString(props.time)}
      <RefreshOutlined onClick={props.onUpdateTime}
                       className={`${prefixCls}-icon`} />
    </div>
  );
};
UpdateTime.defaultProps = {
  prefixCls: 'pxx',
};
export default UpdateTime;
