import * as React from 'react';
import useMemo from 'rc-util/lib/hooks/useMemo';
import { FormProvider as RcFormProvider } from 'rc-field-form';
import type { ValidateMessages } from 'rc-field-form/lib/interface';
import { RequiredMark } from '../util';
import SizeContext, { SizeContextProvider, SizeType } from './SizeContext';
import { ConfigContext, ConfigConsumer, ConfigConsumerProps } from './context';

export interface ConfigProviderProps extends Partial<ConfigConsumerProps> {
  prefixCls?: string;
  componentSize?: SizeType;
  form?: {
    validateMessages?: ValidateMessages;
    requiredMark?: RequiredMark;
  };
}

export interface ProviderChildrenProps extends ConfigProviderProps {
  parentContext: ConfigConsumerProps;
}

const _hd = (px: string | number) => +px;

const ProviderChildren: React.FC<ProviderChildrenProps> = (props) => {
  const {
    hd = _hd,
    locale = {},
    direction,
    form,
    children,
    componentSize,
    parentContext,
  } = props;
  const getPrefixCls = React.useCallback(
    (suffixCls?: string, customizePrefixCls?: string) => {
      const { prefixCls } = props;

      if (customizePrefixCls) return customizePrefixCls;

      const mergedPrefixCls = prefixCls || parentContext.getPrefixCls();

      return suffixCls ? `${mergedPrefixCls}-${suffixCls}` : mergedPrefixCls;
    },
    [parentContext.getPrefixCls, props.prefixCls],
  );

  const config: ConfigConsumerProps = {
    ...parentContext,
    hd,
    form,
    locale,
    direction,
    getPrefixCls,
  };

  const memoedConfig = useMemo(
    () => config,
    config,
    (prevConfig: Record<string, any>, currentConfig) => {
      const prevKeys = Object.keys(prevConfig);
      const currentKeys = Object.keys(currentConfig);
      return (
        prevKeys.length !== currentKeys.length ||
        prevKeys.some((key) => prevConfig[key] !== currentConfig[key])
      );
    },
  );

  let childNode = children;
  // Additional Form provider
  let validateMessages: ValidateMessages = {};

  if (locale && locale.Form && locale.Form.defaultValidateMessages) {
    validateMessages = locale.Form.defaultValidateMessages;
  }
  if (form && form.validateMessages) {
    validateMessages = { ...validateMessages, ...form.validateMessages };
  }

  if (Object.keys(validateMessages).length > 0) {
    childNode = (
      <RcFormProvider validateMessages={validateMessages}>
        {children}
      </RcFormProvider>
    );
  }

  if (componentSize) {
    childNode = (
      <SizeContextProvider size={componentSize}>
        {childNode}
      </SizeContextProvider>
    );
  }

  return (
    <ConfigContext.Provider value={memoedConfig}>
      {childNode}
    </ConfigContext.Provider>
  );
};

const ConfigProvider: React.FC<ConfigProviderProps> & {
  Context: typeof ConfigContext;
  SizeContext: typeof SizeContext;
} = (props) => {
  return (
    <ConfigConsumer>
      {(context) => <ProviderChildren parentContext={context} {...props} />}
    </ConfigConsumer>
  );
};

ConfigProvider.Context = ConfigContext;
ConfigProvider.SizeContext = SizeContext;

export { SizeContextProvider, ConfigConsumer, ConfigContext };

export type { SizeType, ConfigConsumerProps };

export default ConfigProvider;
