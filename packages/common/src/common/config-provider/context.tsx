import * as React from 'react';
import { DirectionType, RequiredMark } from '../util';

export interface ConfigConsumerProps {
  locale: Record<string, any>;
  direction?: DirectionType;
  form?: {
    requiredMark?: RequiredMark;
  };

  hd: (px: number | string) => number;
  getPrefixCls: (
    suffixCls?: string,
    customizePrefixCls?: string | undefined,
  ) => string;
}

export const getPrefixCls = (
  suffixCls: string = '',
  customizePrefixCls?: string,
) => (customizePrefixCls ? customizePrefixCls : `pxx-${suffixCls}`);

export const ConfigContext = React.createContext<ConfigConsumerProps>({
  locale: {},
  getPrefixCls,
  hd: (px) => +px,
});

export const ConfigConsumer = ConfigContext.Consumer;
