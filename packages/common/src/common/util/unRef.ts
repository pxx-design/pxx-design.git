import { MutableRefObject } from 'react';

function unRef<T>(target: T | MutableRefObject<T>): T;
function unRef(target: any): any {
  return target?.current ? target?.current : target;
}

export { unRef };
