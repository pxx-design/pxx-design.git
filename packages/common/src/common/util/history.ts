import { Key } from 'react';
import { get } from '@parallel-line/utils';

export interface HistoryProps {
  /**
   * @description 额外的路由跳转Query字典库，对应ActionClickType.query[key].
   */
  externalQueryDic?: Record<string, any>;
}

export interface HistoryPushParams {
  /**
   * 文档：https://umijs.org/zh-CN/api#history
   */
  pathname?: string;
  /**
   * 跳转类型
   */
  type?: 'push' | 'goBack';
  query?: Record<
    string,
    {
      /**
       * 从queryDicKey对应的数据中获取，优先级比 value 高，作用同 columns.dataIndex
       */
      key?: Key | Key[];
      /**
       * 不可变的常量值
       */
      value?: any;
      /**
       * 默认值 record ，为当前行数据，可配置为其他值，从 externalQueryDic 中获取
       */
      queryDicKey?: string;
    }
  >;
}

export function toRoute(
  history: any,
  QueryDic: Record<string, any>,
  params: HistoryPushParams,
) {
  const { pathname, type } = params;
  if (type === 'goBack') {
    history.goBack();
    return;
  }
  const query: Record<string, any> = {};

  if (params.query) {
    Object.entries(params.query).forEach(
      ([k, { key, value, queryDicKey = 'record' }]) => {
        if (value != null) {
          query[k] = value;
          return;
        }
        let v: any;
        if (Array.isArray(key)) {
          v = get(QueryDic, [queryDicKey, ...key], undefined);
        } else if (key) {
          v = get(QueryDic, [queryDicKey, key], undefined);
        }
        if (v != null) {
          query[k] = v;
        }
      },
    );
  }

  history.push({ pathname, query });
}
