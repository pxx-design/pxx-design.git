declare function tinycolor(
  color: string,
): {
  isValid(): boolean;
  setAlpha(alpha: number): void;
  toRgbString(): string;
  [k: string]: any;
};
export default tinycolor;
