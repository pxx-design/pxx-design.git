import type { EventEmitter } from 'ahooks/lib/useEventEmitter';

export interface EventEmitterProps {
  /**
   * @description ahooks.useEventEmitter, 默认 global.eventEmitter
   */
  useEventEmitter?: EventEmitter<any>;
  /**
   * @description 唯一标识，需要保证全局唯一，建议通过文件位置进行命名
   */
  uniqueKey?: string;
}
