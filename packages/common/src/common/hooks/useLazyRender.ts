import { cloneElement, useEffect } from 'react';
import { useBoolean } from 'ahooks';

interface IRenderFn {
  (render: () => JSX.Element): () => React.FunctionComponentElement<any>;
  <T>(render: (arg: T) => JSX.Element): (
    arg: T,
  ) => React.FunctionComponentElement<any>;
  <T, T2>(render: (arg: T, arg2: T2) => JSX.Element): (
    arg: T,
    arg2: T2,
  ) => React.FunctionComponentElement<any>;
  <T, T2, T3>(render: (arg: T, arg2: T2, arg3: T3) => JSX.Element): (
    arg: T,
    arg2: T2,
    arg3: T3,
  ) => React.FunctionComponentElement<any>;
  <T, T2, T3, T4>(
    render: (arg: T, arg2: T2, arg3: T3, arg4: T4) => JSX.Element,
  ): (
    arg: T,
    arg2: T2,
    arg3: T3,
    arg4: T4,
  ) => React.FunctionComponentElement<any>;
  <T, T2, T3, T4, T5>(
    render: (arg: T, arg2: T2, arg3: T3, arg4: T4, arg5: T5) => JSX.Element,
  ): (
    arg: T,
    arg2: T2,
    arg3: T3,
    arg4: T4,
    arg5: T5,
  ) => React.FunctionComponentElement<any>;
  (render: (...args: any[]) => JSX.Element): (
    ...args: any[]
  ) => React.FunctionComponentElement<any>;
}

export function useLazyRender(show: boolean | undefined): IRenderFn {
  const [inited, { toggle }] = useBoolean(show);

  useEffect(() => {
    toggle(show);
  }, [show]);

  return (render: (...args: any[]) => JSX.Element) => (...args: any[]) => {
    // return inited ? render() : null;
    const node = render(...args);
    const style = { ...node?.props?.style };
    if (!inited) style.display = 'none';
    return cloneElement(node, { ...node?.props, style });
  };
}
