import { useContext } from 'react';
import { createBEM } from './bem';
import { createTranslate } from './translate';
import { ConfigContext } from '../../../config-provider';

/**
 * 创建 Namespace
 *
 * 通用组件库专用
 */
export function useCreateNamespace(name: string) {
  const prefixedName = `pxx-${name}`;
  return [prefixedName, createBEM(prefixedName)] as const;
}

/**
 * 创建 Namespace
 */
export function useNamespace(name: string, customizePrefixCls?: string) {
  const { getPrefixCls } = useContext(ConfigContext);
  const prefixedName = getPrefixCls(name, customizePrefixCls);
  return [
    prefixedName,
    createBEM(prefixedName),
    createTranslate(name),
  ] as const;
}
