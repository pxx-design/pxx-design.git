import { useContext } from 'react';
import { get, isFunction } from 'lodash-es';
import { ConfigContext } from '../../../config-provider';
import { camelize } from '../../../util/format/string';

export function createTranslate(name: string) {
  const prefix = camelize(name) + '.';
  const { locale } = useContext(ConfigContext);

  return function (path: string, ...args: any[]): any {
    const message = get(locale, prefix + path) || get(locale, path);

    return isFunction(message) ? message(...args) : message;
  };
}

export type Translate = ReturnType<typeof createTranslate>;
