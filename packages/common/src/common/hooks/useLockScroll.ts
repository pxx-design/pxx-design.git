import { MutableRefObject, useEffect } from 'react';
import { useMount, useUnmount } from 'ahooks';
import { useTouch } from './useTouch';
import { getScrollParent } from './useScrollParent';
import { supportsPassive } from './useEventListener';
import { preventDefault } from '../util';

let totalLockCount = 0;

const BODY_LOCK_CLASS = 'van-overflow-hidden';

export function useLockScroll<E extends Element = Element>(
  rootRef: MutableRefObject<E | null>,
  shouldLock: Array<boolean | undefined>,
) {
  const touch = useTouch();
  const shouldLockFn = () =>
    shouldLock.reduce<boolean>((p, c) => {
      if (p && c) {
        return true;
      }
      return false;
    }, true);

  const onTouchMove = (event: TouchEvent) => {
    touch.move(event);

    const direction = touch.deltaY.current > 0 ? '10' : '01';
    const el: HTMLElement = getScrollParent(
      event.target as Element,
      rootRef.current as any,
    ) as any;
    if (!el) return;
    const { scrollHeight, offsetHeight, scrollTop } = el;
    let status = '11';

    if (scrollTop === 0) {
      status = offsetHeight >= scrollHeight ? '00' : '01';
    } else if (scrollTop + offsetHeight >= scrollHeight) {
      status = '10';
    }

    if (
      status !== '11' &&
      touch.isVertical() &&
      !(parseInt(status, 2) & parseInt(direction, 2))
    ) {
      preventDefault(event, true);
    }
  };

  const lock = () => {
    document.addEventListener('touchstart', touch.start);
    document.addEventListener(
      'touchmove',
      onTouchMove,
      supportsPassive ? { passive: false } : false,
    );

    if (!totalLockCount) {
      document.body.classList.add(BODY_LOCK_CLASS);
    }

    totalLockCount++;
  };

  const unlock = () => {
    if (totalLockCount) {
      document.removeEventListener('touchstart', touch.start);
      document.removeEventListener('touchmove', onTouchMove);

      totalLockCount--;

      if (!totalLockCount) {
        document.body.classList.remove(BODY_LOCK_CLASS);
      }
    }
  };

  const init = () => {
    if (shouldLockFn()) {
      lock();
    }
  };

  const destroy = () => {
    if (shouldLockFn()) {
      unlock();
    }
  };

  // onMountedOrActivated
  useMount(init);
  // onDeactivated(destroy);
  useUnmount(destroy);

  useEffect(() => {
    shouldLockFn() ? lock() : unlock();
  }, shouldLock);
}
