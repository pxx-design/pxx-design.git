import { useRef } from 'react';
import { useMount } from 'ahooks';
import { inBrowser, unRef } from '../util';

type ScrollElement = HTMLElement | Window;

const overflowScrollReg = /scroll|auto/i;
const defaultRoot = inBrowser ? window : undefined;

function isElement(node: Element) {
  const ELEMENT_NODE_TYPE = 1;
  return (
    node.tagName !== 'HTML' &&
    node.tagName !== 'BODY' &&
    node.nodeType === ELEMENT_NODE_TYPE
  );
}

// https://github.com/youzan/vant/issues/3823
export function getScrollParent(
  el: Element,
  root: ScrollElement | undefined = defaultRoot,
) {
  let node = el;

  while (node && node !== root && isElement(node)) {
    const { overflowY } = window.getComputedStyle(node);
    if (overflowScrollReg.test(overflowY)) {
      return node;
    }
    node = node.parentNode as Element;
  }

  return root;
}

export function useScrollParent<E extends Element = Element>(
  el: React.MutableRefObject<E | null> | E,
  root: ScrollElement | undefined = defaultRoot,
) {
  const scrollParent = useRef<Element | Window | null>(null);

  useMount(() => {
    let node = unRef(el);
    if (node) {
      scrollParent.current = getScrollParent(node, root)!;
    }
  });

  return scrollParent;
}
