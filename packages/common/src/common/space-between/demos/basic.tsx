import React from 'react';
import {SpaceBetween} from '@parallel-line/common'

const Basic = () => {
  return (
    <div style={{width: 200}}>
      <SpaceBetween>
        <div>金额：</div>
        <div>100000元</div>
      </SpaceBetween>
      <SpaceBetween>
        <div>地址：</div>
        <div>广东省南山区大新时代大厦819</div>
      </SpaceBetween>
      <SpaceBetween ellipsis>
        <div>货主：张三/13177889900</div>
        <div>车长/车型/颜色/轴数</div>
      </SpaceBetween>
    </div>
  );
};

export default Basic;
