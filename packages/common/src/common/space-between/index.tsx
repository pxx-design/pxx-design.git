import React from 'react';
import classNames from "classnames";
import {useCreateNamespace} from "../hooks";

interface spaceBetweenProps {
  nowrap?: boolean;
  className?: string;
  ellipsis?: boolean;
  onClick?: () => void;
  children?: React.ReactNode;
  style?: React.CSSProperties;
  align?: 'start' | 'end' | 'center' | 'baseline';
}

const spaceBetween = (props: spaceBetweenProps) => {
  const {className, children, ellipsis = false, nowrap = false, align = 'start', style} = props;
  const [cls, bem] = useCreateNamespace('space-between')
  return (
    <div className={classNames(cls,
      {[bem('nowrap')]: nowrap},
      {[bem('ellipsis')]: ellipsis},
      className)}
         style={{
           ...style,
           alignItems: align
         }}
         onClick={props.onClick}>
      {children}
    </div>
  );
};

export default spaceBetween;
