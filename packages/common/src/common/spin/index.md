---
title: Spin 加载中
group:
  title: Spin 加载中
  order: 14
---

# Spin 加载中

用于页面和区块的加载中状态。

## 何时使用

页面局部处于等待异步数据或正在渲染过程时，合适的加载动效会有效缓解用户的焦虑。

## 代码演示

<Space>
  <code src="./demos/basic.tsx"></code>
  <code src="./demos/size.tsx"></code>
  <code src="./demos/nested.tsx"></code>
  <code src="./demos/custom-indicator.tsx"></code>
</Space>

<API exports='["default"]'></API>
