/**
 * title: 自定义指示符
 * desc: 使用自定义指示符。
 */

import React from 'react';
import { LoadingOutlined } from '@parallel-line/icons';
import { Spin } from '@parallel-line/common';

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

export default () => <Spin indicator={antIcon} />;
