/**
 * title: 各种大小
 * desc: 小的用于文本加载，默认用于卡片容器级加载，大的用于页面级加载。
 */

import React from 'react';
import { Space } from 'antd';
import { Spin } from '@parallel-line/common';

export default () => (
  <Space size="middle">
    <Spin size="small" />
    <Spin />
    <Spin size="large" />
  </Space>
);
