/**
 * title: 页面加载中
 * desc: 可以直接把内容内嵌到 Spin 中，将现有容器变为加载状态。 通过 `full` 可以保证在高度为0的容器依然保持和外层高度一致
 */

import React from 'react';
import { Switch, Alert } from 'antd';
import { Spin } from '@parallel-line/common';

export default class Card extends React.Component {
  state = { loading: false };

  toggle = (value: any) => {
    this.setState({ loading: value });
  };

  render() {
    return (
      <div>
        <div style={{ height: 120, background: '#fff', padding: 8 }}>
          <Spin full spinning={this.state.loading}>
            {this.state.loading ? (
              <div></div>
            ) : (
              <Alert
                message="Alert message title"
                description="Further details about the context of this alert."
                type="info"
              />
            )}
          </Spin>
        </div>

        <div style={{ marginTop: 16 }}>
          Loading state：
          <Switch checked={this.state.loading} onChange={this.toggle} />
        </div>
      </div>
    );
  }
}
