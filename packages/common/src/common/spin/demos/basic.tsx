/**
 * title: 基本用法
 * desc: 一个简单的 loading 状态。
 */

import React from 'react';
import { Spin } from '@parallel-line/common';

export default () => <Spin />;
