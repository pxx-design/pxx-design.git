import React, { useState } from 'react';
import { useBoolean } from 'ahooks';
import styled from 'styled-components';
import { Transition } from '@parallel-line/mobile';
import '@parallel-line/mobile/es/mobile/style/global.less'

const Container = styled.div`
  background-color: #f7f8fa;
  padding-bottom: 16px;
`;
const Title = styled.div`
  margin: 0;
  padding: 32px 16px 16px;
  color: rgba(69, 90, 100, 0.6);
  font-weight: normal;
  font-size: 14px;
  line-height: 16px;
`;
const Desc = styled.div`
  color: #34495e;
  font-size: 15px;
  line-height: 26px;
  padding: 16px;
`;
const Ellipsis = styled.div`
  /* min-height: 30px; */
  border-radius: 8px;
  background-color: #fff;
  margin: 16px;
`;
const Cell = styled.div`
  min-height: 30px;
  border-radius: 8px;
  padding: 10px 16px;
  background-color: #fff;
  margin-top: 8px;
  margin-bottom: 8px;
`;
const Content = styled.div`
  margin: 12px 12px 0;
  margin-top: 0;
  overflow: hidden;
  border-radius: 8px;
  background: #fff;
`;
const AnimateBlock = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  width: 160px;
  height: 160px;
  margin: -80px 0 0 -80px;
  background-color: blue;
  border-radius: 8px;
  text-align: center;
  line-height: 160px;
  color: #fff;
`;

export default () => {
  const [name, setName] = useState('pxx-fade');
  const [show, { setTrue, setFalse }] = useBoolean();
  const animate = (name: string) => {
    setName(name);
    setTrue();

    setTimeout(() => {
      setFalse();
    }, 1200);
  };
  return (
    <Container>
      <Title>文字省略</Title>
      <Desc>当文本内容长度超过容器最大宽度时，自动省略多余的文本。</Desc>
      <Content>
        <Ellipsis className="pxx-ellipsis">
          这是一段最多显示一行的文字，多余的内容会被省略。
        </Ellipsis>
        <Ellipsis className="pxx-multi-ellipsis--l2">
          这是一段最多显示两行的文字，多余的内容会被省略。这是一段最多显示两行的文字，多余的内容会被省略。这是一段最多显示两行的文字，多余的内容会被省略。
        </Ellipsis>
        <Ellipsis className="pxx-multi-ellipsis--l3">
          这是一段最多显示三行的文字，多余的内容会被省略。这是一段最多显示三行的文字，多余的内容会被省略。这是一段最多显示三行的文字，多余的内容会被省略。这是一段最多显示三行的文字，多余的内容会被省略。这是一段最多显示三行的文字，多余的内容会被省略。
        </Ellipsis>
      </Content>

      <Title>1px 边框</Title>
      <Desc>
        为元素添加 Retina 屏幕下的 1px 边框（即 hairline），基于伪类 transform
        实现。
      </Desc>
      <Content>
        <Cell className="pxx-hairline--top">上边框</Cell>

        <Cell className="pxx-hairline--bottom">下边框</Cell>

        <Cell>
          <div className="pxx-hairline--left" style={{ textAlign: 'center' }}>
            左边框
          </div>
        </Cell>

        <Cell>
          <div className="pxx-hairline--right" style={{ textAlign: 'center' }}>
            右边框
          </div>
        </Cell>

        <Cell className="pxx-hairline--top-bottom">上下边框</Cell>

        <Cell>
          <div className="pxx-hairline--surround">全边框</div>
        </Cell>
      </Content>

      <Title>动画</Title>
      <Desc>可以通过 Transition 组件使用内置的动画</Desc>
      <Content>
        <Cell onClick={() => animate('pxx-fade')}>淡入</Cell>
        <Cell onClick={() => animate('pxx-slide-up')}>上滑进入</Cell>
        <Cell onClick={() => animate('pxx-slide-down')}>下滑进入</Cell>
        <Cell onClick={() => animate('pxx-slide-left')}>左滑进入</Cell>
        <Cell onClick={() => animate('pxx-slide-right')}>右滑进入</Cell>

        <Transition show={show} name={name}>
          <AnimateBlock style={{ display: show ? undefined : 'none' }}>
            {name}
          </AnimateBlock>
        </Transition>
      </Content>
    </Container>
  );
};
