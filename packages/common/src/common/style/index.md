---
title: 内置样式
mobile: true
---

# 内置样式

Pxx 中默认包含了一些常用样式，可以直接通过 `className` 的方式使用。

> 需要在项目中手动引入哦～
## 代码演示

<code src="./demos/basic.tsx"></code>

