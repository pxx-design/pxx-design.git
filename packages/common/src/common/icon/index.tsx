import Icon, { IconProps } from "@parallel-line/icons";

export type {IconProps}

export default Icon;
