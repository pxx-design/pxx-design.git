---
title: Icon 图标
order: 10
---

# Icon 图标

语义化的字体图形。使用图标组件，你需要安装 `@parallel-line/icons` 图标组件包：

```bash
npm install --save @parallel-line/icons
```

## 图标列表

```jsx
/**
 * inline: true
 */

import React from 'react';
import IconDisplay from './demos/IconDisplay';

export default () => <IconDisplay />;
```
