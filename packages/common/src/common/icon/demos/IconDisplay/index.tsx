import * as React from 'react';
import Icon from '@ant-design/icons';
import _, * as PxxIcons from '@parallel-line/icons';
import { Radio, Input } from 'antd';
import { RadioChangeEvent } from 'antd/es/radio/interface';
import debounce from 'lodash/debounce';
import Category from './Category';
import IconPicSearcher from './IconPicSearcher';
import { FilledIcon, OutlinedIcon } from './themeIcons';
import { categories, Categories, CategoriesKeys } from './fields';

// import './index.less';

export enum ThemeType {
  Filled = 'Fill',
  Outlined = 'Outlined',
  TwoTone = 'TwoTone',
}

const allIcons: {
  [key: string]: any;
} = PxxIcons;

interface IconDisplayProps {}

interface IconDisplayState {
  theme: ThemeType;
  searchKey: string;
}

const messages = {
  'app.docs.components.icon.search.placeholder':
    '在此搜索图标，点击图标可复制代码',
  'app.docs.components.icon.outlined': '线框风格',
  'app.docs.components.icon.filled': '实底风格',
  'app.docs.components.icon.two-tone': '双色风格',
};

class IconDisplay extends React.PureComponent<
  IconDisplayProps,
  IconDisplayState
> {
  static categories: Categories = categories;

  static newIconNames: string[] = [];

  state: IconDisplayState = {
    theme: ThemeType.Outlined,
    searchKey: '',
  };

  constructor(props: IconDisplayProps) {
    super(props);
    this.handleSearchIcon = debounce(this.handleSearchIcon, 300);
  }

  handleChangeTheme = (e: RadioChangeEvent) => {
    this.setState({
      theme: e.target.value as ThemeType,
    });
  };

  handleSearchIcon = (searchKey: string) => {
    this.setState((prevState) => ({
      ...prevState,
      searchKey,
    }));
  };

  renderCategories() {
    const { searchKey = '', theme } = this.state;

    return Object.keys(categories)
      .map((key: any) => {
        let iconList: string[] = categories[key];
        if (searchKey) {
          iconList = iconList.filter((iconName) =>
            iconName.toLowerCase().includes(searchKey.toLowerCase()),
          );
        }

        // CopyrightCircle is same as Copyright, don't show it
        iconList = iconList.filter((icon) => icon !== 'CopyrightCircle');

        return {
          category: key,
          icons: iconList
            .map((iconName) => iconName + theme)
            .filter((iconName) => !!allIcons[iconName]),
        };
      })
      .filter(({ icons }) => !!icons.length)
      .map(({ category, icons }) => (
        <Category
          key={category}
          title={category as CategoriesKeys}
          theme={theme}
          icons={icons}
          newIcons={IconDisplay.newIconNames}
        />
      ));
  }

  render() {
    return (
      <div className="icon-display">
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <Radio.Group
            value={this.state.theme}
            onChange={this.handleChangeTheme}
            size="large"
            buttonStyle="solid"
          >
            <Radio.Button value={ThemeType.Outlined}>
              <Icon component={OutlinedIcon} />
              {messages['app.docs.components.icon.outlined']}
            </Radio.Button>
            <Radio.Button value={ThemeType.Filled}>
              <Icon component={FilledIcon} />
              {messages['app.docs.components.icon.filled']}
            </Radio.Button>
          </Radio.Group>
          <Input.Search
            placeholder={
              messages['app.docs.components.icon.search.placeholder']
            }
            style={{ margin: '0 10px', flex: 1 }}
            allowClear
            onChange={(e) => this.handleSearchIcon(e.currentTarget.value)}
            size="large"
            autoFocus
            suffix={<IconPicSearcher />}
          />
        </div>
        {this.renderCategories()}
      </div>
    );
  }
}

export default IconDisplay;
