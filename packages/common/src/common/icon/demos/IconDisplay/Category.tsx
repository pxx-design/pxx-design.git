import * as React from 'react';
import { message } from 'antd';
import CopyableIcon from './CopyableIcon';
import { ThemeType } from './index';
import { CategoriesKeys } from './fields';

interface CategoryProps {
  title: CategoriesKeys;
  icons: string[];
  theme: ThemeType;
  newIcons: string[];
}

interface CategoryState {
  justCopied: string | null;
}

const messages = {
  'app.docs.components.icon.category.direction': '方向性图标',
  'app.docs.components.icon.category.suggestion': '提示建议性图标',
  'app.docs.components.icon.category.file': '导入导出图标',
  'app.docs.components.icon.category.payment': '支付类图标',
  'app.docs.components.icon.category.editor': '编辑类图标',
  'app.docs.components.icon.category.data': '数据类图标',
  'app.docs.components.icon.category.other': '网站通用图标',
  'app.docs.components.icon.category.logo': '品牌和标识',
};

class Category extends React.Component<CategoryProps, CategoryState> {
  copyId?: number;

  state = {
    justCopied: null,
  };

  componentWillUnmount() {
    window.clearTimeout(this.copyId);
  }

  onCopied = (type: string, text: string) => {
    message.success(
      <span>
        <code className="copied-code">{text}</code> copied 🎉
      </span>,
    );
    this.setState({ justCopied: type }, () => {
      this.copyId = window.setTimeout(() => {
        this.setState({ justCopied: null });
      }, 2000);
    });
  };

  render() {
    const { icons, title, newIcons, theme } = this.props;
    const items = icons.map((name) => (
      <CopyableIcon
        key={name}
        name={name}
        theme={theme}
        isNew={newIcons.indexOf(name) >= 0}
        justCopied={this.state.justCopied}
        onCopied={this.onCopied}
      />
    ));

    return (
      <div>
        <h3>{messages[`app.docs.components.icon.category.${title}`]}</h3>
        <ul className="anticons-list">{items}</ul>
      </div>
    );
  }
}

export default Category;
