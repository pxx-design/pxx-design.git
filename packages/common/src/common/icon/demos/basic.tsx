/**
 * title: 基本用法
 * desc: 基本用法
 */

import React from 'react';
import { Space } from 'antd';
import { Icon } from '@parallel-line/common';

export default () => (
  <Space>
    <Icon type="loading" />
    <Icon type="spinner" />
    <Icon type="close" />
    <Icon type="more-arrow" />
    <Icon type="photo" />
    <Icon type="photo-fail" />
  </Space>
);
