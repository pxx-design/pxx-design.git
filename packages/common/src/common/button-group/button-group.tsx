import React, { Children, cloneElement } from 'react';
import Item from './item';
import Space from '../space';
import { isArray } from 'lodash-es';
import classNames from 'classnames';
import { useControllableValue } from 'ahooks';
import { useCreateNamespace } from '../hooks';
import { DicInfo } from '@parallel-line/utils';

interface ButtonGroupProps {
  options?: DicInfo[];
  pave?: boolean;
  wrap?: boolean;
  disabled?: boolean;
  required?: boolean;
  className?: string;
  fieldNames?: {
    label: string;
    value: string;
  };
  onChange?: (value: any) => void;
  value?: any;
  defaultValue?: any;
  direction?: 'vertical' | 'horizontal';
  mode?: 'checkbox' | 'radio';
  size?: any;
  children?: React.ReactNode;
}

const ButtonGroup = (props: ButtonGroupProps) => {
  const {
    pave,
    wrap,
    disabled = false,
    size = 'small',
    className,
    options,
    required = false,
    mode = 'checkbox',
    direction = 'horizontal',
    fieldNames = { label: 'value', value: 'key' },
  } = props;
  const isRadio = mode === 'radio';
  const [value, setValue] = useControllableValue(props);
  const [, bem] = useCreateNamespace('button-group');
  const handleClick = (_value: any) => {
    if (isRadio) {
      if (value === _value) {
        if (required) return;
        setValue(undefined);
      } else {
        setValue(_value);
      }
      return;
    }
    const index = value?.indexOf(_value);
    if (index > -1) {
      value.splice(index, 1);
      if (required && !value.length) return;
      setValue([...value]);
    } else {
      setValue(isArray(value) ? value.concat(_value) : [_value]);
    }
  };
  return (
    <Space
      size={size}
      wrap={wrap}
      direction={direction}
      className={classNames(bem({ pave, wrap }), className)}
    >
      {Children.map(props.children, (child: any) => {
        return cloneElement(child, {
          isRadio,
          _value: value,
          onClick: handleClick,
        });
      })}
      {options?.map((t) => {
        return (
          <Item
            _value={value}
            status={t.status}
            color={t.color}
            isRadio={isRadio}
            disabled={disabled || t.disabled}
            onClick={handleClick}
            key={t[fieldNames.value]}
            value={t[fieldNames.value]}
          >
            {t[fieldNames.label]}
          </Item>
        );
      })}
    </Space>
  );
};
export default ButtonGroup;
