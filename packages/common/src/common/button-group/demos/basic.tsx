import React from 'react';
import { Divider } from 'antd';
import { ButtonGroup, Space } from '@parallel-line/common';

const options = [
  { value: '待处理', key: '0' },
  { value: '已处理', disabled: true, key: '1' },
  { value: '已关闭', key: '99' },
];
const options1 = [
  { value: '蓝色', status: 'processing', key: '0' },
  { value: '黄色', status: 'warning', key: '1' },
  { value: '绿色', status: 'success', key: '2' },
];
const options2 = [
  { key: '1', value: '蓝牌', color: '#1E58F0' },
  { key: '2', value: '黄牌', color: '#FF7800' },
  { key: '5', value: '绿牌', color: '#31C158' },
];
const options3 = [
  { key: '18', value: '1.8米' },
  { key: '27', value: '2.7米' },
  { key: '35', value: '3.5米' },
  { key: '42', value: '4.2米' },
  { key: '52', value: '5.2米' },
  { key: '62', value: '6.2米' },
  { key: '68', value: '6.8米' },
  { key: '76', value: '7.6米' },
  { key: '96', value: '9.6米' },
  { key: '135', value: '13.5米' },
  { key: '150', value: '15米' },
  { key: '165', value: '16.5米' },
  { key: '175', value: '17.5米' },
];

const Basic = () => {
  return (
    <Space direction="vertical">
      <div>
        <Divider orientation="left">required、checkbox</Divider>
        <ButtonGroup
          required
          defaultValue={['1']}
          options={options}
          onChange={(value) => console.log(value)}
        />
      </div>
      <div>
        <Divider orientation="left">required、radio</Divider>
        <ButtonGroup
          required
          mode="radio"
          options={options}
          onChange={(value) => console.log(value)}
        />
      </div>
      <div>
        <Divider orientation="left">disabled、checkbox</Divider>
        <ButtonGroup
          disabled
          defaultValue={['1']}
          options={options}
          onChange={(value) => console.log(value)}
        />
      </div>
      <div>
        <Divider orientation="left">checkbox</Divider>
        <ButtonGroup options={options} />
      </div>
      <div>
        <Divider orientation="left">radio</Divider>
        <ButtonGroup mode="radio" options={options} />
      </div>
      <div>
        <Divider orientation="left">status、彩色</Divider>
        <ButtonGroup required mode="radio" options={options1} />
      </div>
      <div>
        <Divider orientation="left">color、彩色</Divider>
        <ButtonGroup required mode="radio" options={options2} />
      </div>
      <div>
        <Divider orientation="left">pave 填充</Divider>
        <div style={{ width: 500 }}>
          <ButtonGroup pave mode="radio" options={options} />
        </div>
      </div>
      <div>
        <Divider orientation="left">wrap 换行</Divider>
        <div style={{ width: 300 }}>
          <ButtonGroup required wrap mode="radio" options={options3} />
        </div>
      </div>
      <div>
        <Divider orientation="left">其他</Divider>
        <ButtonGroup>
          <ButtonGroup.Item value="0">待处理</ButtonGroup.Item>
          <ButtonGroup.Item value="1">已处理</ButtonGroup.Item>
          <ButtonGroup.Item value="2">已关闭</ButtonGroup.Item>
        </ButtonGroup>
      </div>
      <div>
        <ButtonGroup mode="radio">
          <ButtonGroup.Item value="0">待处理</ButtonGroup.Item>
          <ButtonGroup.Item value="1">已处理</ButtonGroup.Item>
          <ButtonGroup.Item value="2">已关闭</ButtonGroup.Item>
        </ButtonGroup>
      </div>
      <div>
        <ButtonGroup mode="radio">
          <ButtonGroup.Item value="0">待处理</ButtonGroup.Item>
          <ButtonGroup.Item disabled value="1">
            已处理
          </ButtonGroup.Item>
          <ButtonGroup.Item value="2">已关闭</ButtonGroup.Item>
        </ButtonGroup>
      </div>
      <div>
        <ButtonGroup defaultValue="1" mode="radio">
          <ButtonGroup.Item value="0">待处理</ButtonGroup.Item>
          <ButtonGroup.Item disabled value="1">
            已处理
          </ButtonGroup.Item>
          <ButtonGroup.Item value="2">已关闭</ButtonGroup.Item>
        </ButtonGroup>
      </div>
    </Space>
  );
};

export default Basic;
