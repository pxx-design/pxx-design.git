import React from 'react';
import classNames from 'classnames';
import { useCreateNamespace } from '../hooks';
import { tinycolor } from '../util';

interface ItemProps {
  value?: any;
  _value?: any;
  status?: string;
  color?: string;
  isRadio?: boolean;
  disabled?: boolean;
  className?: string;
  children?: React.ReactNode;
  onClick?: (value: any) => void;
}

const Item = (props: ItemProps) => {
  const { status, value, _value, isRadio = false, disabled = false } = props;
  const [cls, bem] = useCreateNamespace('button-group-item');
  const active = () => (isRadio ? _value === value : _value?.includes(value));
  const style = React.useMemo(() => {
    const style: React.CSSProperties = {};
    if (props.color) {
      style.color = props.color;
      if (active()) {
        style.color = '#ffffff';
        style.borderColor = props.color;
        style.backgroundColor = props.color;
      }
    }
    return style;
  }, [props.color, _value]);
  return (
    <div
      key={value}
      style={style}
      className={classNames(
        cls,
        {
          [bem(status)]: !!status,
          [bem('disabled')]: disabled,
          [bem('active')]: active(),
        },
        props.className,
      )}
      onClick={() => {
        if (disabled) return;
        props.onClick?.(value);
      }}
    >
      {props.children}
    </div>
  );
};

export default Item;
