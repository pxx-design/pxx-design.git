import Item from './item';
import InternalButtonGroup from './button-group';


type InternalButtonGroupType = typeof InternalButtonGroup;

interface ButtonGroupInterface extends InternalButtonGroupType {
  Item: typeof Item;
}

const ButtonGroup = InternalButtonGroup as ButtonGroupInterface;

ButtonGroup.Item = Item;

export default ButtonGroup;
