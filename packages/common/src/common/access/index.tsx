import React, { Fragment } from 'react';
import { useAccess } from '@parallel-line/hooks';

interface AccessProps {
  /** 权限码 */
  code?: string;
  /** 无权限时显示的内容 */
  fallback?: React.ReactNode;
}

const Access: React.FC<AccessProps> = ({ code, fallback, children }) => {
  const accessible = useAccess(code);
  return <Fragment>{accessible ? children : fallback}</Fragment>;
};

Access.defaultProps = {
  fallback: null,
};

export default Access;
