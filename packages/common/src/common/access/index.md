---
title: Access 权限
group:
  title: Access 权限
  order: 14
---

# Access 权限

在项目中经常有的场景是不同的用户的权限不同，通常有如下场景：

- 不同的用户在页面中可以看到的元素和操作不同
- 不同的用户对页面的访问权限不同

## 如何使用

- 通过 `useAccess` hook 来获取权限定义
- 通过 `Access` 组件用于页面的元素显示和隐藏的控制

## 代码演示

``` tsx | pure
import React from 'react';
import { useAccess } from '@parallel-line/hooks';
import { Access } from '@parallel-line/common';

const PageA = () => {
  const inquiryAccess = useAccess('70'); // access 实例的成员: canReadFoo, canUpdateFoo, canDeleteFoo

  if (inquiryAccess) {
    // 任意操作
  }

  return (
    <div>
      <Access code="71" fallback={<div>Can not read foo content.</div>}>
        Foo content.
      </Access>
      <Access code="72" fallback={<div>Can not update foo.</div>}>
        Update foo.
      </Access>
      <Access code="73" fallback={<div>Can not delete foo.</div>}>
        Delete foo.
      </Access>
    </div>
  );
};
```

<API></API>
