import React from 'react';
import { Divider, Space } from 'antd';
import { Tag } from '@parallel-line/common';

const Basic = () => {
  return (
    <Space direction="vertical">
      <Divider orientation="left">Presets</Divider>
      <Space>
        <Tag color="magenta">magenta</Tag>
        <Tag color="red">red</Tag>
        <Tag color="volcano">volcano</Tag>
        <Tag color="orange">orange</Tag>
        <Tag color="gold">gold</Tag>
        <Tag color="lime">lime</Tag>
        <Tag color="green">green</Tag>
        <Tag color="cyan">cyan</Tag>
        <Tag color="blue">blue</Tag>
        <Tag color="geekblue">geekblue</Tag>
        <Tag color="purple">purple</Tag>
      </Space>
      <Divider orientation="left">Light Presets</Divider>
      <Space>
        <Tag light color="magenta">
          magenta
        </Tag>
        <Tag light color="red">
          red
        </Tag>
        <Tag light color="volcano">
          volcano
        </Tag>
        <Tag light color="orange">
          orange
        </Tag>
        <Tag light color="gold">
          gold
        </Tag>
        <Tag light color="lime">
          lime
        </Tag>
        <Tag light color="green">
          green
        </Tag>
        <Tag light color="cyan">
          cyan
        </Tag>
        <Tag light color="blue">
          blue
        </Tag>
        <Tag light color="geekblue">
          geekblue
        </Tag>
        <Tag light color="purple">
          purple
        </Tag>
      </Space>
      <Divider orientation="left">Status</Divider>
      <Space>
        <Tag color="success">success</Tag>
        <Tag color="processing">processing</Tag>
        <Tag color="warning">warning</Tag>
        <Tag color="error">error</Tag>
        <Tag color="default">default</Tag>
      </Space>
      <Divider orientation="left">Light Status</Divider>
      <Space>
        <Tag light color="success">
          success
        </Tag>
        <Tag light color="processing">
          processing
        </Tag>
        <Tag light color="warning">
          warning
        </Tag>
        <Tag light color="error">
          error
        </Tag>
        <Tag light color="default">
          default
        </Tag>
      </Space>
      <Divider orientation="left">Custom</Divider>
      <Space>
        <Tag color="#f50">#f50</Tag>
        <Tag color="#2db7f5">#2db7f5</Tag>
        <Tag color="#87d068">#87d068</Tag>
        <Tag color="#108ee9">#108ee9</Tag>
        <Tag color="#108fe97f">#108fe97f</Tag>
      </Space>
      <Divider orientation="left">Light Custom</Divider>
      <Space>
        <Tag light color="#f50">
          #f50
        </Tag>
        <Tag light color="#2db7f5">
          #2db7f5
        </Tag>
        <Tag light color="#87d068">
          #87d068
        </Tag>
        <Tag light color="#108ee9">
          #108ee9
        </Tag>
        <Tag light color="#108fe97f">
          #108fe97f
        </Tag>
      </Space>
    </Space>
  );
};

export default Basic;
