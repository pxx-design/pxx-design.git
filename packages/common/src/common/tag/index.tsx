import * as React from 'react';
import classNames from 'classnames';
import { useCreateNamespace } from '../hooks';
import { tinycolor, LiteralUnion } from '../util';
import {
  PresetColorTypes,
  PresetStatusColors,
  PresetColorType,
  PresetStatusColorType,
} from '../util/colors';

interface TagProps {
  light?: boolean;
  className?: string;
  children?: React.ReactNode;
  color?: LiteralUnion<PresetColorType | PresetStatusColorType, string>;
}

const Tag = (props: TagProps) => {
  const { color = 'default', className, light = false } = props;
  const [, bem] = useCreateNamespace('tag');
  const status = React.useMemo(() => {
    if (PresetStatusColors.includes(color as PresetStatusColorType))
      return color;
    if (PresetColorTypes.includes(color as PresetColorType)) return color;
    return 'default';
  }, [color]);
  const backgroundColor = React.useMemo(() => {
    const backgroundColor = (PresetStatusColors.includes(
      color as PresetStatusColorType,
    )
      ? undefined
      : color) as string;
    if (!backgroundColor) return undefined;
    if (PresetColorTypes.includes(backgroundColor as PresetColorType)) {
      return undefined;
    }

    const tinyBackgroundColor = tinycolor(backgroundColor);
    if (!tinyBackgroundColor.isValid()) return undefined;
    if (backgroundColor && light) {
      tinyBackgroundColor.setAlpha(0.2);
    }
    return tinyBackgroundColor.toRgbString();
  }, [color]);
  return (
    <div
      className={classNames(
        bem(),
        bem(status, { 'light-color': light, color: !light }),
        className,
      )}
      style={{ backgroundColor }}
    >
      {props.children}
    </div>
  );
};

export default Tag;
