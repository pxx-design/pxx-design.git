import React from 'react';
import { SwitchTransition, CSSTransition } from 'react-transition-group';

interface TransitionProps {
  /** 用于自动生成 CSS 过渡类名。例如：name: 'fade' 将自动拓展为 .fade-enter，.fade-enter-active 等。 */
  name: string;
  /** 是否开始动画 */
  show: boolean | undefined;
  /** 是否在初始渲染时使用过渡。默认为 false。 */
  appear?: boolean;
  onAfterEnter?(): void;
  onAfterLeave?(): void;
}

const Transition: React.FC<TransitionProps> = (props) => {
  return (
    <SwitchTransition>
      <CSSTransition
        key={props.show as any}
        classNames={{
          appear: `${props.name}-appear`,
          appearActive: `${props.name}-appear-active`,
          appearDone: `${props.name}-appear-to`,
          enter: `${props.name}-enter`,
          enterActive: `${props.name}-enter-active`,
          enterDone: `${props.name}-enter-to`,
          exit: `${props.name}-leave`,
          exitActive: `${props.name}-leave-active`,
          exitDone: `${props.name}-leave-to`,
        }}
        timeout={{ enter: 66, exit: 300 }}
        mountOnEnter
        onEntered={props.onAfterEnter}
        onExited={props.onAfterLeave}
      >
        {props.children}
      </CSSTransition>
    </SwitchTransition>
  );
};

export default Transition;
