---
title: 组件介绍
order: 1
nav:
  title: 通用组件
  order: 2
---

> 平行线前端通用组件库。

## 开始

该通用组件库内嵌在 [移动端组件库](/mobile) 和 [WEB组件库](/components) ，文档中引入的 `@parallel-line/common` 在不同的包引入不一样，下面以 Access 为例子。

### WEB组件库

``` tsx | pure
import React from 'react';
import { Access } from '@parallel-line/components';

const PageA = () => {
  return (
    <div>
      <Access code="71" fallback={<div>Can not read foo content.</div>}>
        Foo content.
      </Access>
    </div>
  );
};
```

### 移动端组件库

``` tsx | pure
import React from 'react';
import { Access } from '@parallel-line/mobile';

const PageA = () => {
  return (
    <div>
      <Access code="71" fallback={<div>Can not read foo content.</div>}>
        Foo content.
      </Access>
    </div>
  );
};
```
