---
title: EnvInfo 环境信息
---

# EnvInfo 环境信息

在 console 显示当前系统环境信息

## 如何使用

- 在应用环境信息初始化完成后的布局组件中使用。
- 在 console 调用 pxx 即可显示当前系统环境信息

## 代码演示

```jsx | pure
import { EnvInfo } from '@parallel-line/common';

export default (props) => {
  return (
    <>
      {initDone && <EnvInfo />}
      {props.children}
    </>
  );
}
```

![运行截图](https://pic4.zhimg.com/80/v2-b9626928555f066fe70d8fa2b7e9b0a2.png)
