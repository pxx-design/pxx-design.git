import { useMount, useSetState } from 'ahooks';
import { global, GlobalTagProps, isMiniAPP } from '@parallel-line/utils';
import { PresetColorType, PresetStatusColorType } from '../util/colors';

type ColorKey = PresetColorType | PresetStatusColorType;

const colorMap: Record<ColorKey, string> = {
  success: '#00b86b',
  processing: '#0256ff',
  warning: '#ff8b1c',
  default: '#000000',
  error: '#f0364a',
  pink: '#eb2f96',
  red: '#f5222d',
  yellow: '#fadb14',
  orange: '#fa8c16',
  cyan: '#13c2c2',
  green: '#52c41a',
  blue: '#1890ff',
  purple: '#722ed1',
  geekblue: '#2f54eb',
  magenta: '#eb2f96',
  volcano: '#fa541c',
  gold: '#faad14',
  lime: '#a0d911',
};

function getColor(color: string) {
  if (colorMap[color as ColorKey]) {
    return colorMap[color as ColorKey];
  }
  return color;
}

function getTags() {
  const map = { ...global.PXX_ENV, ...global.PXX_SOLUTION };
  return Object.keys(map)
    .filter((k) => !!k.match(/__.*?__$/g)?.length)
    .filter((k) => {
      const newKey = k.replace(/__/g, '');
      return !!map[newKey];
    })
    .map((k) => {
      return { ...map[k], color: getColor(map[k].color) } as GlobalTagProps;
    });
}

interface EnvInfoState {
  msg: string;
  colors: string[];
}

const EnvInfoKey = Symbol('EnvInfo');
Object.defineProperty(window, 'pxx', {
  get() {
    global.eventEmitter.emit({ uniqueKey: EnvInfoKey });
    return null;
  },
});

const logColor = (color: string, radius = 6, right = 8) =>
  `display: inline-block;margin-right: ${right}px;border-radius: ${radius}px;padding: 2px 4px;font-size: 12px;color: #FFFFFF;background: ${color};border: 1px solid ${color};`;

const EnvInfo = () => {
  const [state, setState] = useSetState<EnvInfoState>({ msg: '', colors: [] });
  useMount(() => {
    const colors: string[] = [];
    const msg = getTags().reduce((pre, { name, color }) => {
      colors.push(logColor(color));
      return `${pre} %c${name}`;
    }, '');
    setState({ msg, colors });
    if (isMiniAPP()) {
      console.log('[system]', msg, ...colors);
      return;
    }
    if (global.PXX_ENV.isDevTest) {
      console.log(
        ` %c输入 %cpxx %c可查看环境信息哦～`,
        ...['#fa8c16', '#0256ff', '#fa8c16'].map((color) =>
          logColor(color, 0, 0),
        ),
      );
      console.log(msg, ...colors);
    }
  });
  global.eventEmitter.useSubscription(({ uniqueKey }) => {
    if (uniqueKey === EnvInfoKey) {
      const { msg, colors } = state;
      console.log(msg, ...colors);
    }
  });

  return null;
};

EnvInfo.pxx = function () {
  global.eventEmitter.emit({ uniqueKey: EnvInfoKey });
};

export default EnvInfo;
