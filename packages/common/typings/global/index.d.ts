declare module '*.svg';
declare module '*.png';
declare module '*.less';
declare module '*.ttf';
declare module '*.otf';

interface Window {
  GetDic: any;
  countryTree: any;
  Icon: any;
  g_history: any;
  eventEmitter: any;
  request: any;
  AMap: any;
}
