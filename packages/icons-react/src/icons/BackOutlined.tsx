// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import BackOutlinedSvg from '@parallel-line/icons-svg/lib/asn/BackOutlined';
import AntdIcon, { AntdIconProps } from '../components/AntdIcon';

const BackOutlined = (
  props: AntdIconProps,
  ref: React.MutableRefObject<HTMLSpanElement>,
) => <AntdIcon {...props} ref={ref} icon={BackOutlinedSvg} />;

BackOutlined.displayName = 'BackOutlined';
export default React.forwardRef<HTMLSpanElement, AntdIconProps>(BackOutlined);
