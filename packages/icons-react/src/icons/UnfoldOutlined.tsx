// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import UnfoldOutlinedSvg from '@parallel-line/icons-svg/lib/asn/UnfoldOutlined';
import AntdIcon, { AntdIconProps } from '../components/AntdIcon';

const UnfoldOutlined = (
  props: AntdIconProps,
  ref: React.MutableRefObject<HTMLSpanElement>,
) => <AntdIcon {...props} ref={ref} icon={UnfoldOutlinedSvg} />;

UnfoldOutlined.displayName = 'UnfoldOutlined';
export default React.forwardRef<HTMLSpanElement, AntdIconProps>(UnfoldOutlined);
