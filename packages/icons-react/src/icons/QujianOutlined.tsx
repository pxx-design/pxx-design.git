// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import QujianOutlinedSvg from '@parallel-line/icons-svg/lib/asn/QujianOutlined';
import AntdIcon, { AntdIconProps } from '../components/AntdIcon';

const QujianOutlined = (
  props: AntdIconProps,
  ref: React.MutableRefObject<HTMLSpanElement>,
) => <AntdIcon {...props} ref={ref} icon={QujianOutlinedSvg} />;

QujianOutlined.displayName = 'QujianOutlined';
export default React.forwardRef<HTMLSpanElement, AntdIconProps>(QujianOutlined);
