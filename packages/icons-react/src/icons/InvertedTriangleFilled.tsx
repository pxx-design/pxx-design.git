// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import InvertedTriangleFilledSvg from '@parallel-line/icons-svg/lib/asn/InvertedTriangleFilled';
import AntdIcon, { AntdIconProps } from '../components/AntdIcon';

const InvertedTriangleFilled = (
  props: AntdIconProps,
  ref: React.MutableRefObject<HTMLSpanElement>,
) => <AntdIcon {...props} ref={ref} icon={InvertedTriangleFilledSvg} />;

InvertedTriangleFilled.displayName = 'InvertedTriangleFilled';
export default React.forwardRef<HTMLSpanElement, AntdIconProps>(InvertedTriangleFilled);
