// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import InvalidOutlinedSvg from '@parallel-line/icons-svg/lib/asn/InvalidOutlined';
import AntdIcon, { AntdIconProps } from '../components/AntdIcon';

const InvalidOutlined = (
  props: AntdIconProps,
  ref: React.MutableRefObject<HTMLSpanElement>,
) => <AntdIcon {...props} ref={ref} icon={InvalidOutlinedSvg} />;

InvalidOutlined.displayName = 'InvalidOutlined';
export default React.forwardRef<HTMLSpanElement, AntdIconProps>(InvalidOutlined);
