// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import ResetOutlinedSvg from '@parallel-line/icons-svg/lib/asn/ResetOutlined';
import AntdIcon, { AntdIconProps } from '../components/AntdIcon';

const ResetOutlined = (
  props: AntdIconProps,
  ref: React.MutableRefObject<HTMLSpanElement>,
) => <AntdIcon {...props} ref={ref} icon={ResetOutlinedSvg} />;

ResetOutlined.displayName = 'ResetOutlined';
export default React.forwardRef<HTMLSpanElement, AntdIconProps>(ResetOutlined);
