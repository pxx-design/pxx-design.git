// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import FileImportOutlinedSvg from '@parallel-line/icons-svg/lib/asn/FileImportOutlined';
import AntdIcon, { AntdIconProps } from '../components/AntdIcon';

const FileImportOutlined = (
  props: AntdIconProps,
  ref: React.MutableRefObject<HTMLSpanElement>,
) => <AntdIcon {...props} ref={ref} icon={FileImportOutlinedSvg} />;

FileImportOutlined.displayName = 'FileImportOutlined';
export default React.forwardRef<HTMLSpanElement, AntdIconProps>(FileImportOutlined);
