// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import NarrowOutlinedSvg from '@parallel-line/icons-svg/lib/asn/NarrowOutlined';
import AntdIcon, { AntdIconProps } from '../components/AntdIcon';

const NarrowOutlined = (
  props: AntdIconProps,
  ref: React.MutableRefObject<HTMLSpanElement>,
) => <AntdIcon {...props} ref={ref} icon={NarrowOutlinedSvg} />;

NarrowOutlined.displayName = 'NarrowOutlined';
export default React.forwardRef<HTMLSpanElement, AntdIconProps>(NarrowOutlined);
