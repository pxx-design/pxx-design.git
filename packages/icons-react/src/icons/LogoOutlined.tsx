// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import LogoOutlinedSvg from '@parallel-line/icons-svg/lib/asn/LogoOutlined';
import AntdIcon, { AntdIconProps } from '../components/AntdIcon';

const LogoOutlined = (
  props: AntdIconProps,
  ref: React.MutableRefObject<HTMLSpanElement>,
) => <AntdIcon {...props} ref={ref} icon={LogoOutlinedSvg} />;

LogoOutlined.displayName = 'LogoOutlined';
export default React.forwardRef<HTMLSpanElement, AntdIconProps>(LogoOutlined);
