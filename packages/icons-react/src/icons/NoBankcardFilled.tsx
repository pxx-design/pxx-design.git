// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import NoBankcardFilledSvg from '@parallel-line/icons-svg/lib/asn/NoBankcardFilled';
import AntdIcon, { AntdIconProps } from '../components/AntdIcon';

const NoBankcardFilled = (
  props: AntdIconProps,
  ref: React.MutableRefObject<HTMLSpanElement>,
) => <AntdIcon {...props} ref={ref} icon={NoBankcardFilledSvg} />;

NoBankcardFilled.displayName = 'NoBankcardFilled';
export default React.forwardRef<HTMLSpanElement, AntdIconProps>(NoBankcardFilled);
