// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import FileExportOutlinedSvg from '@parallel-line/icons-svg/lib/asn/FileExportOutlined';
import AntdIcon, { AntdIconProps } from '../components/AntdIcon';

const FileExportOutlined = (
  props: AntdIconProps,
  ref: React.MutableRefObject<HTMLSpanElement>,
) => <AntdIcon {...props} ref={ref} icon={FileExportOutlinedSvg} />;

FileExportOutlined.displayName = 'FileExportOutlined';
export default React.forwardRef<HTMLSpanElement, AntdIconProps>(FileExportOutlined);
