// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import AddBankcardFilledSvg from '@parallel-line/icons-svg/lib/asn/AddBankcardFilled';
import AntdIcon, { AntdIconProps } from '../components/AntdIcon';

const AddBankcardFilled = (
  props: AntdIconProps,
  ref: React.MutableRefObject<HTMLSpanElement>,
) => <AntdIcon {...props} ref={ref} icon={AddBankcardFilledSvg} />;

AddBankcardFilled.displayName = 'AddBankcardFilled';
export default React.forwardRef<HTMLSpanElement, AntdIconProps>(AddBankcardFilled);
