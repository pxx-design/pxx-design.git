// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import QuickPayFilledSvg from '@parallel-line/icons-svg/lib/asn/QuickPayFilled';
import AntdIcon, { AntdIconProps } from '../components/AntdIcon';

const QuickPayFilled = (
  props: AntdIconProps,
  ref: React.MutableRefObject<HTMLSpanElement>,
) => <AntdIcon {...props} ref={ref} icon={QuickPayFilledSvg} />;

QuickPayFilled.displayName = 'QuickPayFilled';
export default React.forwardRef<HTMLSpanElement, AntdIconProps>(QuickPayFilled);
