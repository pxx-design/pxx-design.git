// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import PhotoFailFilledSvg from '@parallel-line/icons-svg/lib/asn/PhotoFailFilled';
import AntdIcon, { AntdIconProps } from '../components/AntdIcon';

const PhotoFailFilled = (
  props: AntdIconProps,
  ref: React.MutableRefObject<HTMLSpanElement>,
) => <AntdIcon {...props} ref={ref} icon={PhotoFailFilledSvg} />;

PhotoFailFilled.displayName = 'PhotoFailFilled';
export default React.forwardRef<HTMLSpanElement, AntdIconProps>(PhotoFailFilled);
