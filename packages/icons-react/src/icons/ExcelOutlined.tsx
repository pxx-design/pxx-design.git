// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import ExcelOutlinedSvg from '@parallel-line/icons-svg/lib/asn/ExcelOutlined';
import AntdIcon, { AntdIconProps } from '../components/AntdIcon';

const ExcelOutlined = (
  props: AntdIconProps,
  ref: React.MutableRefObject<HTMLSpanElement>,
) => <AntdIcon {...props} ref={ref} icon={ExcelOutlinedSvg} />;

ExcelOutlined.displayName = 'ExcelOutlined';
export default React.forwardRef<HTMLSpanElement, AntdIconProps>(ExcelOutlined);
