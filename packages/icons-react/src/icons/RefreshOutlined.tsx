// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import RefreshOutlinedSvg from '@parallel-line/icons-svg/lib/asn/RefreshOutlined';
import AntdIcon, { AntdIconProps } from '../components/AntdIcon';

const RefreshOutlined = (
  props: AntdIconProps,
  ref: React.MutableRefObject<HTMLSpanElement>,
) => <AntdIcon {...props} ref={ref} icon={RefreshOutlinedSvg} />;

RefreshOutlined.displayName = 'RefreshOutlined';
export default React.forwardRef<HTMLSpanElement, AntdIconProps>(RefreshOutlined);
