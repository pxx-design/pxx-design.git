// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import OnlinebankPayFilledSvg from '@parallel-line/icons-svg/lib/asn/OnlinebankPayFilled';
import AntdIcon, { AntdIconProps } from '../components/AntdIcon';

const OnlinebankPayFilled = (
  props: AntdIconProps,
  ref: React.MutableRefObject<HTMLSpanElement>,
) => <AntdIcon {...props} ref={ref} icon={OnlinebankPayFilledSvg} />;

OnlinebankPayFilled.displayName = 'OnlinebankPayFilled';
export default React.forwardRef<HTMLSpanElement, AntdIconProps>(OnlinebankPayFilled);
