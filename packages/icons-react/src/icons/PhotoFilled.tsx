// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import PhotoFilledSvg from '@parallel-line/icons-svg/lib/asn/PhotoFilled';
import AntdIcon, { AntdIconProps } from '../components/AntdIcon';

const PhotoFilled = (
  props: AntdIconProps,
  ref: React.MutableRefObject<HTMLSpanElement>,
) => <AntdIcon {...props} ref={ref} icon={PhotoFilledSvg} />;

PhotoFilled.displayName = 'PhotoFilled';
export default React.forwardRef<HTMLSpanElement, AntdIconProps>(PhotoFilled);
