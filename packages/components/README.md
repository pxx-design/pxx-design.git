# @parallel-line/components

> 平行线前端通用组件库。

## 📦 安装

```bash
npm install @parallel-line/components --save
```

```bash
yarn add @parallel-line/components
```

## 更多

[使用文档](https://supermoonlmy.gitee.io/parallel-line/)
