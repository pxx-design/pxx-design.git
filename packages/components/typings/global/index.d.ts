declare module '*.svg';
declare module '*.less';
declare module '*.ttf';
declare module '*.otf';
declare module '*.json';

declare module 'to-style' {
  export function string(...agrs: any[]): string
}
