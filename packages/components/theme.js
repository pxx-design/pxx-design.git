module.exports = {
  '@hd': '1px',
  '@primary-color': '#1E78FF',
  '@border-radius-base': '6px',
  //button
  '@btn-border-width': '0px',
  '@btn-default-bg': '#E4EFFF',
  //Radio
  '@radio-button-bg': '#ffffff',
  //Checkbox
  '@checkbox-color-radius-base': '2px',
  // Pagination
  '@pagination-item-bg': '#F2F3F7',
  '@pagination-item-bg-active': '#485871',
};
