import React from 'react';
import { Empty } from 'antd';
import { useMount } from 'ahooks';
import useAntTable, {
  CombineService,
  OptionsWithFormat,
  PaginatedParams,
  Result,
} from 'ahooks/lib/useAntdTable';
import { useAccess } from '@parallel-line/hooks';

interface Options<R, Item, U> extends OptionsWithFormat<R, Item, U> {
  code?: string;
}

interface AntdResult<Item> extends Result<Item> {
  accessible: boolean;
}

const EmptyText: React.FC<{ accessible: boolean }> = ({ accessible }) => {
  if (!accessible)
    return (
      <Empty
        image={Empty.PRESENTED_IMAGE_SIMPLE}
        description="您暂无权限访问"
      />
    );
  return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="暂无数据" />;
};

function useAntdTable<R = any, Item = any, U extends Item = any>(
  service: CombineService<R, PaginatedParams>,
  options: Options<R, Item, U>,
): AntdResult<Item> {
  const accessible = useAccess(options.code);
  const result = useAntTable(service, { ...options, manual: true });
  const newResult: AntdResult<Item> = {
    ...result,
    accessible,
    tableProps: {
      ...result.tableProps,
      locale: { emptyText: <EmptyText accessible={accessible} /> },
    },
    run: (...args: any[]) => {
      if (!accessible) return;
      return (result.run as any)(...args);
    },
    refresh: (): any => {
      if (!accessible) return;
      return result.refresh();
    },
    search: {
      ...result.search,
      submit: () => {
        if (!accessible) return;
        return result.search.submit();
      },
      reset: () => {
        if (!accessible) return;
        return result.search.reset();
      },
    },
  };

  useMount(() => {
    if (!options.manual) {
      newResult.search.submit();
    }
  });

  return newResult;
}

export type { PaginatedParams };

export default useAntdTable;
