import Table from './Table';

export type {
  ProTableInstance,
  ProTableConfig,
  ProTableProps,
  ProColumnType,
  RequestFn,
} from './interface';

export default Table;
