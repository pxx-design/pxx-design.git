import React from 'react';
import { ProTableConfig } from '@parallel-line/components';
import { PlusOutlined } from '@parallel-line/icons';

const config: ProTableConfig = {
  tableRequest: [
    {
      url: 'https://randomuser.me/api?c=c&status=all',
      sign: [{ status: '__ALL__' }],
    },
    { url: 'https://randomuser.me/api?a=a&status=0', sign: [{ status: '0' }] },
    { url: 'https://randomuser.me/api?b=b&status=0', sign: [{ status: '1' }] },
  ],
  menus: [
    {
      status: {
        value: '__ALL__',
        options: [{ '0': '选项1' }, { '1': '选项2' }],
      },
    },
    null,
  ],
  actionConfig: { majorNumber: 3 },
  actions: [
    {
      name: '查看用户',
      major: true,
      click: {
        pathname: '/look',
        query: {
          constVal: { value: 'look' },
          id: { key: ['login', 'uuid'] },
          nat: { key: 'nat' },
        },
      },
    },
    { name: '编辑用户', hide: 'record.status === 1' },
    { name: '删除', hide: 'record.status === 1' },
    { name: '禁用', hide: 'record.status === 1', major: true },
    { name: '变女性', hide: "record.gender === 'female' || record.status === 1", major: true },
    { name: '启用', hide: 'record.status === 0', major: true },
    { name: '变男性', hide: "record.gender === 'male' || record.status === 1" },
    { name: '修改密码' },
    { name: '选项1', sign: [{ status: '0' }] },
    { name: '选项2', sign: [{ status: '1' }] },
  ],
  options: {
    reload: true,
    search: true,
    setting: true,
    buttons: [
      { name: '新增管理员', icon: <PlusOutlined /> },
      {
        name: '新增用户',
        icon: <PlusOutlined />,
        type: 'primary',
        click: {
          pathname: '/add',
          query: {
            constVal: { value: 'add' },
          },
        },
      },
      { name: '选项1', icon: 'icongonggaoguanli', sign: [{ status: '0' }] },
      { name: '选项2', icon: 'icongonggaoguanli', sign: [{ status: '1' }] },
    ],
  },
};

export default config;
