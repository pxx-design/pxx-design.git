import { createBrowserHistory } from 'history-with-query';
import { createFromIconfontCN } from '@ant-design/icons';
import { global } from '@parallel-line/utils';

export default () => {
  global.dictionary = window.dictionary;

  global.Icon = createFromIconfontCN({
    scriptUrl: '//at.alicdn.com/t/font_2134175_fv49jafys7.js',
  });

  global.g_history = createBrowserHistory();
};
