/**
 * title: 设置列默认配置
 * desc: 通过设置列配置默认配置，可以快速设置列中的查询项是否启用，在列很多的Table中，可以快速关闭查询项功能。
 */

import React from 'react';
import { ProTable } from '@parallel-line/components';
import columns from '../columns';
import config from '../config';

import init from '../init';
import { uniqueId } from 'lodash-es';

init();

export default () => (
  <ProTable
    config={config}
    columns={columns}
    defaultPageSize={5}
    style={{ height: 600, padding: 16 }}
    tableProps={{ rowKey: () => uniqueId() }}
    defaultColumnConfig={{ hideInSearch: true }}
  />
);
