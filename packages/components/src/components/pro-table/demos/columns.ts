import { ProColumnType } from '@parallel-line/components';

const columns: ProColumnType[] = [
  {
    title: '名字',
    width: 160,
    sorter: true,
    hideInForm: true,
    searchKey: 'name',
    dataIndex: ['name', 'last'],
  },
  {
    title: 'ellipsis测试ellipsis测试',
    width: 120,
    dataIndex: 'ellipsis',
  },
  {
    title: '邮箱(选项1)',
    sign: [{ status: '0' }],
    width: 120,
    dataIndex: 'email',
  },
  {
    title: 'Nat(选项2)',
    sign: [{ status: '1' }],
    width: 160,
    dataIndex: 'nat',
  },
  {
    title: '手机',
    width: 200,
    dataIndex: 'phone',
    type: 'Numbering',
  },
  {
    title: '性别',
    width: 120,
    dataIndex: 'gender',
    dicKey: 'common.common.gender',
    type: 'Tag',
  },
  {
    width: 200,
    title: '来源',
    dataIndex: 'carrierRequest',
    fieldProps: { mode: 'multiple' },
    options: [
      { text: '自建订单', value: '0' },
      { text: '货主指派', value: '1' },
      { text: '商家外派', value: '2' },
      { text: '自动抢单', value: '3' },
    ],
  },
  {
    title: '余额',
    width: 200,
    dataIndex: '余额',
  },
  {
    title: '货物统计',
    width: 200,
    unit: '条货物',
    dataIndex: 'goods',
    hideInSearch: true,
  },
  {
    title: '长度',
    unit: 'KM',
    valueType: 'metreToKilometre',
    width: 200,
    dataIndex: ['location', 'street', 'number'],
    sorter: true,
  },
  {
    title: '金额',
    unit: '元',
    valueType: ['centToYuan', 'toThousandth', 'toFixed'],
    type: 'Link',
    width: 200,
    dataIndex: ['registered', 'age'],
    sorter: true,
  },
  {
    title: '注册时间',
    type: 'Date',
    dataIndex: ['registered', 'date'],
    defaultSortOrder: 'descend',
  },
  {
    title: '城市',
    dataIndex: ['location', 'city'],
    type: 'City',
  },
];

export default columns;
