/**
 * title: 全量使用
 * desc: 全量使用。
 */

import React from 'react';
import { Badge, Tooltip, message } from 'antd';
import { ProTable } from '@parallel-line/components';
import columns from '../columns';
import config from '../config';

import init from '../init';
import { uniqueId } from 'lodash-es';

init();

export default () => (
  <ProTable
    config={config}
    columns={columns}
    defaultPageSize={5}
    style={{ height: 600, padding: 16 }}
    tableProps={{ rowKey: () => uniqueId() }}
    renderColumn={({ text }, key) => {
      if (key === 'email') return <Badge dot>{text}</Badge>;
      return text;
    }}
    onLinkClick={(text, record, key) =>
      message.info(`点击了 ${key} 链接：${text}`)
    }
    onOptionClick={(key, config, DefaultOption) => {
      if (DefaultOption && key === '新增管理员')
        return (
          <Badge key={key} dot>
            {DefaultOption}
          </Badge>
        );
      !DefaultOption && message.info(`点击了 ${key} 按钮`);
    }}
    actionRule={(key, record) => {
      if (key === '变女性') return [record.gender === 'female', true];
      else if (key === '变男性') return [record.gender === 'male', true];
      else if (key === '启用') return [record.status === 0, true];
      else if (key === '禁用') return [record.status === 1, true];
      return [false, false];
    }}
    onActionClick={(key, record, DefaultAction) => {
      if (DefaultAction && key === '启用')
        return (
          <Tooltip key={key} title="自定义按钮">
            {DefaultAction}
          </Tooltip>
        );
      !DefaultAction && message.info(`${key}: ${record.name.last}`);
    }}
  />
);
