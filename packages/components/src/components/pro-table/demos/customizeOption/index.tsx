/**
 * title: 自定义ToolBar操作栏
 * desc: 通过 onOptionClick 可以响应操作栏事件和自定义node节点
 */

import React from 'react';
import { Badge, message } from 'antd';
import { ProTable } from '@parallel-line/components';
import columns from '../columns';
import config from '../config';

import init from '../init';
import { uniqueId } from 'lodash-es';

init();

export default () => (
  <ProTable
    config={config}
    columns={columns}
    defaultPageSize={5}
    style={{ height: 600, padding: 16 }}
    tableProps={{ rowKey: () => uniqueId() }}
    onOptionClick={(key, config, DefaultOption) => {
      if (DefaultOption && key === '新增管理员')
        return (
          <Badge key={key} dot>
            {DefaultOption}
          </Badge>
        );
      !DefaultOption && message.info(`点击了 ${key} 按钮`);
    }}
  />
);
