/**
 * title: 动态控制表格属性
 * desc: 选择不同配置组合查看效果。
 */

import React from 'react';
import { Switch, Form } from 'antd';
import { global } from '@parallel-line/utils';
import {
  Input,
  Select,
  ProTable,
  ProTableProps,
} from '@parallel-line/components';
import { useSetState, useBoolean } from 'ahooks';
import columns from './columns';
import config from './config';

import init from './init';

init();

// rowSelection object indicates the need for row selection
const rowSelection: ProTableProps['tableProps']['rowSelection'] = {
  onChange: (selectedRowKeys: React.Key[], selectedRows: any[]) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      'selectedRows: ',
      selectedRows,
    );
  },
  getCheckboxProps: (record: any) => {
    return {
      disabled: record.dob.age > 60, // Column configuration not to be checked
      name: record.name.first,
    };
  },
};

const colspanRowspanProps: Partial<ProTableProps> = {
  renderColumn: ({ text, index }, key) => {
    if (['_action', 'phone'].includes(key) && [0, 1].includes(index)) {
      return {
        children: <a>{text}</a>,
        props: {
          rowSpan: index === 0 ? 2 : 0,
        },
      };
    }

    if (index === 0 && ['goods', '余额'].includes(key)) {
      return {
        children: <a>{text}</a>,
        props: {
          colSpan: key === 'goods' ? 2 : 0,
        },
      };
    }

    if (index === 1 && ['goods', '余额'].includes(key)) {
      return {
        children: <a>{text}</a>,
        props: {
          colSpan: key === 'goods' ? 0 : 2,
        },
      };
    }

    return text;
  },
};

export default () => {
  const [rest, { toggle: toggleRest }] = useBoolean(true);
  const [colspanRowspan, { toggle: toggleColspanRowspan }] = useBoolean();
  const [filtered, { toggle: toggleFilteredValues }] = useBoolean();
  const [state, setState] = useSetState<ProTableProps>({
    config,
    columns,
    showFilter: true,
    defaultColumnConfig: {},
    uniqueKey: 'ProTable-basic',
    tableProps: { rowKey: 'id' },
    style: { height: 600, padding: 16 },
    request: async (url, { page, pageSize, query: formData }) => {
      let query = `page=${page}&size=${pageSize}`;
      Object.entries(formData).forEach(([key, value]) => {
        if (value) {
          query += `&${key}=${value}`;
        }
      });

      return fetch(`${url}&results=55&${query}`)
        .then((res) => res.json())
        .then((res) => ({
          data: {
            rowTotal: res.info.results,
            rows: res.results.map((r, index) => {
              r.id = page * pageSize + index + 1;
              r.createTime = '2021-03-10T10:18:30';
              r.status = index % 2;
              r.goods = index + 2;
              r.ellipsis =
                index % 2
                  ? '测试ellipsis的/-/测试ellipsis的/-/测试ellipsis的/-/测试ellipsis的/-/测试ellipsis的'
                  : '测试ellipsis的';
              return r;
            }),
            statistics: { 1: { __ALL__: 55, female: 25, male: 30 } },
          },
        }));
    },
  });
  const onFilteredValuesChange: ProTableProps['onFilteredValuesChange'] = (
    allValues,
    changedValues,
  ) => {
    const newFilteredValues = { ...allValues };
    const changedKey = Object.keys(changedValues)[0] || '';
    if (['name.last', 'gender'].includes(changedKey)) {
      newFilteredValues.gender = undefined;
      newFilteredValues['name.last'] = undefined;
      newFilteredValues[changedKey] = changedValues[changedKey];
    }
    setState({ filteredValues: newFilteredValues });
  };
  const reload = () =>
    global.eventEmitter.emit({ uniqueKey: 'ProTable-basic' });

  const handleRowSelectionChange = (enable?: boolean) => {
    setState({
      tableProps: {
        ...state.tableProps,
        rowSelection: enable ? rowSelection : undefined,
      },
    });
  };
  const handleMenuBarRenderChange = (enable?: boolean) => {
    setState({
      menuBarRender: enable
        ? (menuVal) => (
            <div>我是自定义的menuBar，当前菜单是{JSON.stringify(menuVal)}</div>
          )
        : undefined,
    });
  };
  const handleFilteredValuesChange = (enable?: boolean) => {
    toggleFilteredValues(enable);
    setState({
      onFilteredValuesChange: enable ? onFilteredValuesChange : undefined,
    });
  };
  const handleShowFilterChange = (enable?: boolean) => {
    const newState = {
      showFilter: !!enable,
      config: { ...config, actions: enable ? config.actions : [] },
    };
    setState(newState);

    toggleRest(false);
    setTimeout(() => {
      toggleRest(true);
    }, 0);
  };
  const handleHideFilterChange = (enable?: boolean) => {
    const newState = {
      defaultColumnConfig: {
        ...state.defaultColumnConfig,
        hideInSearch: enable,
      },
    };
    setState(newState);
    toggleRest(false);
    setTimeout(() => {
      toggleRest(true);
    }, 0);
  };
  const handleActionRuleChange = (enable?: boolean) => {
    setState({
      actionRule: enable
        ? (key, record) => {
            if (key === '变女性') return [record.gender === 'female', true];
            else if (key === '变男性') return [record.gender === 'male', true];
            else if (key === '启用') return [record.status === 0, true];
            else if (key === '禁用') return [record.status === 1, true];
            return [false, false];
          }
        : undefined,
    });

    setTimeout(reload, 66);
  };

  const handleColspanRowspanChange = (enable: boolean = false) => {
    toggleColspanRowspan(enable);
    setState(enable ? colspanRowspanProps : { renderColumn: undefined });
  };

  const handleCustomizeFilterChange = (enable: boolean = false) => {
    setState({
      renderFilter: enable
        ? ({ key, node, handleFilteredChange }) => {
            switch (key) {
              case 'ellipsis':
                return (
                  <Input
                    bordered={false}
                    onClose={handleFilteredChange}
                    onPressEnter={handleFilteredChange}
                  />
                );
              case 'gender':
                return (
                  <Select bordered={false} onChange={handleFilteredChange} />
                );
              default:
                break;
            }
            return node;
          }
        : undefined,
    });
  };
  const handleHideInFormChange = (enable: boolean = false) => {
    setState((prevState) => {
      const [one, ...restColumns] = prevState.columns;
      one.hideInForm = !enable;
      prevState.columns = [one, ...restColumns];
      return prevState;
    });
    toggleRest(false);
    setTimeout(() => {
      toggleRest(true);
    }, 0);
  };

  const handleSummaryChange = (enable: boolean = false) => {
    setState((prevState) => {
      const [one, ...restColumns] = prevState.columns;
      prevState.columns = [
        one,
        ...restColumns.map((c) => {
          if (['余额', '货物统计', '长度', '金额'].includes(c.title))
            c.summary = enable ? true : undefined;
          return c;
        }),
      ];
      prevState.columns[1].summary = enable ? true : undefined;
      return prevState;
    });
    toggleRest(false);
    setTimeout(() => {
      toggleRest(true);
    }, 0);
  };

  const handleOnlyCurrentPageSelectChange = (enable: boolean = false) => {
    setState({ onlyCurrentPageSelect: enable });
  }

  return (
    <>
      <Form layout="inline" style={{ marginBottom: 16 }}>
        <Form.Item label="Checkbox">
          <Switch
            checked={!!state.tableProps?.rowSelection}
            onChange={handleRowSelectionChange}
          />
        </Form.Item>
        <Form.Item label="menuBarRender">
          <Switch
            checked={!!state.menuBarRender}
            onChange={handleMenuBarRenderChange}
          />
        </Form.Item>
        <Form.Item
          label="受控筛选"
          tooltip="通过受控模式可以控制筛选项多选一功能，这里演示 名称和性别 二选一。⚠️注意：存在通过URL传参的异常～"
        >
          <Switch checked={filtered} onChange={handleFilteredValuesChange} />
        </Form.Item>
        <Form.Item
          label="显示操作列"
          tooltip={`通过 showFilter=false && config.actions=[] 即可隐藏操作列～`}
        >
          <Switch
            checked={!!state.showFilter}
            onChange={handleShowFilterChange}
          />
        </Form.Item>
        <Form.Item
          label="隐藏筛选项"
          tooltip={`通过设置每一列的 hideInSearch=true 即可隐藏筛选项.`}
        >
          <Switch
            checked={!!state.defaultColumnConfig?.hideInSearch}
            onChange={handleHideFilterChange}
          />
        </Form.Item>
        <Form.Item
          label="actionRule"
          tooltip={`通过 actionRule 配置操作列动作点是否隐藏和呈现优先级。`}
        >
          <Switch
            checked={!!state.actionRule}
            onChange={handleActionRuleChange}
          />
        </Form.Item>
        <Form.Item
          label="表格支持行/列合并"
          tooltip={`表格支持行/列合并，使用 renderColumn 里的单元格属性 colSpan 或者 rowSpan 设值为 0 时，设置的表格不会渲染。如果是操作列，通过key === '_action' 判断即可。`}
        >
          <Switch
            checked={colspanRowspan}
            onChange={handleColspanRowspanChange}
          />
        </Form.Item>
        <Form.Item
          label="自定义筛选Node"
          tooltip={`通过 renderFilter 自定义筛选Node, demo中 ellipsis测试 和 性别 为自定义的筛选Node`}
        >
          <Switch
            checked={!!state.renderFilter}
            onChange={handleCustomizeFilterChange}
          />
        </Form.Item>
        <Form.Item
          label="查询表单"
          tooltip={`通过设置列配置中的 hideInForm 可以切换到查询表单模式`}
        >
          <Switch
            checked={!state.columns?.[0]?.hideInForm}
            onChange={handleHideInFormChange}
          />
        </Form.Item>
        <Form.Item
          label="统计栏"
          tooltip={`通过设置列配置中的 summary 可以开启统计栏`}
        >
          <Switch
            checked={!!state.columns?.[1]?.summary}
            onChange={handleSummaryChange}
          />
        </Form.Item>
        <Form.Item
          label="onlyCurrentPageSelect"
          tooltip={`通过设置 onlyCurrentPageSelect 开启只能单页选择，一旦翻页自动清空已有选择项。`}
        >
          <Switch
            checked={!!state.onlyCurrentPageSelect}
            onChange={handleOnlyCurrentPageSelectChange}
          />
        </Form.Item>
      </Form>

      {rest ? (
        <ProTable {...state} />
      ) : (
        <div style={{ background: '#ff', height: 600 }} />
      )}
    </>
  );
};
