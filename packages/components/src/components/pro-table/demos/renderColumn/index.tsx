/**
 * title: 自定义单元格和操作列
 * desc: 通过 renderColumn 可以对单元格进行自定义，通过 onActionClick 响应操作列Action事件和自定义node节点。
 */

import React from 'react';
import { Badge, Tooltip, message } from 'antd';
import { ProTable } from '@parallel-line/components';
import columns from '../columns';
import config from '../config';

import init from '../init';
import { uniqueId } from 'lodash-es';

init();

export default () => (
  <ProTable
    config={config}
    columns={columns}
    defaultPageSize={5}
    style={{ height: 600, padding: 16 }}
    tableProps={{ rowKey: () => uniqueId() }}
    renderColumn={({ text }, key) => {
      if (key === 'email') return <Badge dot>{text}</Badge>;
      return text;
    }}
    onActionClick={(key, record, DefaultAction) => {
      if (DefaultAction && key === '启用')
        return (
          <Tooltip key={key} title="自定义按钮">
            {DefaultAction}
          </Tooltip>
        );
      !DefaultAction && message.info(`${key}: ${record.name.last}`);
    }}
  />
);
