/**
 * title: 查询表单
 * desc: 通过设置列配置中的hideInForm可以切换到查询表单模式
 */

import React from 'react';
import { ProTable } from '@parallel-line/components';
import columns from './columns';
import config from '../config';

import init from '../init';
import { uniqueId } from 'lodash-es';

init();

export default () => (
  <ProTable
    config={config}
    columns={columns}
    defaultPageSize={5}
    style={{ height: 600 }}
    tableProps={{ rowKey: () => uniqueId() }}
  />
);
