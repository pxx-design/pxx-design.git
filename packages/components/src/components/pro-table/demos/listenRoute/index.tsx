/**
 * title: 监听路由参数
 * desc: ProTable会自动监听路由参数进行刷新，若不需要可以设置 history 为 null */

import React from 'react';
import { ProTable } from '@parallel-line/components';
import columns from '../columns';
import config from './config';

import init from '../init';
import { uniqueId } from 'lodash-es';

init();

export default () => (
  <ProTable
    config={config}
    columns={columns}
    defaultPageSize={5}
    style={{ height: 600, padding: 16 }}
    tableProps={{ rowKey: () => uniqueId() }}
  />
);
