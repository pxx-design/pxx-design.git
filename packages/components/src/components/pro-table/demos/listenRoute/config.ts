import { ProTableConfig } from '@parallel-line/components';
const config: ProTableConfig = {
  tableRequest: [
    {
      url: 'https://randomuser.me/api?c=c&status=all',
      sign: [{ status: '__ALL__' }],
    },
    { url: 'https://randomuser.me/api?a=a&status=0', sign: [{ status: '0' }] },
    { url: 'https://randomuser.me/api?b=b&status=0', sign: [{ status: '1' }] },
  ],
  menus: [
    {
      status: {
        value: '__ALL__',
        options: [{ '0': '选项1' }, { '1': '选项2' }],
      },
    },
    {
      gender: {
        value: '__ALL__',
        dicKey: 'common.common.gender',
      },
    },
  ],
  actions: [
    {
      name: '查看',
      major: true,
      click: {
        pathname: '/',
        query: {
          constVal: { value: 'look' },
          gender: { key: 'gender' },
        },
      },
    },
    { name: '编辑' },
    { name: '删除' },
    { name: '禁用', hide: 'record.status === 1', major: true },
    { name: '启用', hide: 'record.status === 0', major: true },
    { name: '变女性', hide: "record.gender === 'female'", major: true },
    { name: '变男性', hide: "record.gender === 'male'", major: true },
    { name: '修改密码' },
    { name: '选项1', sign: [{ status: '0' }] },
    { name: '选项2', sign: [{ status: '1' }] },
  ],
  options: {
    reload: true,
    search: true,
    setting: true,
    buttons: [
      { name: '新增管理员', icon: 'iconPlus' },
      { name: '新增用户', icon: 'iconPlus', type: 'primary' },
      { name: '选项1', icon: 'icongonggaoguanli', sign: [{ status: '0' }] },
      { name: '选项2', icon: 'icongonggaoguanli', sign: [{ status: '1' }] },
    ],
  },
};

export default config;
