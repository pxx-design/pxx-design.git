import { ProColumnType } from '@parallel-line/components';

const columns: ProColumnType[] = [
  {
    title: '名字',
    width: 160,
    sorter: true,
    dataIndex: ['name', 'last'],
    hideInSearch: { sign: [{ status: '0' }] },
  },
  {
    title: '手机',
    width: 200,
    dataIndex: 'phone',
    hideInSearch: { sign: [{ status: '1' }] },
  },
  {
    title: '性别',
    width: 120,
    dataIndex: 'gender',
    dicKey: 'common.common.gender',
    hideInSearch: true,
  },
  {
    width: 120,
    title: '来源',
    dataIndex: 'carrierRequest',
    options: [
      { text: '自建订单', value: 0 },
      { text: '货主指派', value: 1 },
    ],
  },
  {
    title: '余额',
    width: 200,
    dataIndex: '余额',
    summary: true,
  },
  {
    title: '长度',
    unit: 'KM',
    valueType: 'metreToKilometre',
    width: 200,
    dataIndex: ['location', 'street', 'number'],
    sorter: true,
  },
  {
    title: '金额',
    unit: '元',
    valueType: ['centToYuan', 'toThousandth'],
    type: 'Link',
    width: 200,
    dataIndex: ['registered', 'age'],
    sorter: true,
    summary: true,
  },
  {
    title: '货物统计',
    width: 200,
    unit: '条货物',
    dataIndex: ['goods'],
    hideInSearch: true,
  },
  {
    title: '注册时间',
    type: 'Date',
    dataIndex: ['registered', 'date'],
    defaultSortOrder: 'descend',
  },
  {
    title: '城市',
    width: 120,
    dataIndex: ['location', 'city'],
  },
];

export default columns;
