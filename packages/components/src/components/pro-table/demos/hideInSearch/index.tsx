/**
 * title: 查询项
 * desc: 通过 Column.hideInSearch 可以隐藏 Column 上的查询项，支持通过设置 sign 进行过滤
 */

import React from 'react';
import { ProTable } from '@parallel-line/components';
import columns from './columns';
import config from '../config';

import init from '../init';
import { uniqueId } from 'lodash-es';

init();

export default () => (
  <ProTable
    config={config}
    columns={columns}
    defaultPageSize={5}
    style={{ height: 600, padding: 16 }}
    tableProps={{ rowKey: () => uniqueId() }}
  />
);
