/* eslint-disable no-nested-ternary */
/* eslint-disable func-names */
/* eslint-disable @typescript-eslint/no-use-before-define */
import React, { useRef } from 'react';
import moment from 'moment';
import classnames from 'classnames';
import { Tooltip, Form, Input } from 'antd';
import { useHistory, useUrlState, useUniqueKey } from '@parallel-line/hooks';
import { calc, global, isEmpty, request } from '@parallel-line/utils';
import { get, set, omit, pickBy, isObject, isArray } from 'lodash-es';
import { useMemoDeep } from '@parallel-line/hooks';
import { useNamespace } from '@parallel-line/common/es/hooks';
import { useEffect, forwardRef, useImperativeHandle } from 'react';
import { SearchOutlined, RefreshOutlined } from '@parallel-line/icons';
import {
  toRoute,
  devWarning,
  getPathValue,
  isFunction,
} from '@parallel-line/common/es/utils';
import {
  useSize,
  useMount,
  useRequest,
  useSetState,
  useUpdateEffect,
  useControllableValue,
} from 'ahooks';

import Table from '../table';
import Button from '../button';
import FormFilter from './FormFilter';
import WhiteBoard from '../white-board';
import QuickSearch from './QuickSearch';
import ColumnSetting from './ColumnSetting';
import { handleSign, treeToArray } from './utils';
import useAntdTable, { PaginatedParams } from './hooks/useAntdTable';
import {
  DateTimeColWidth,
  CityColWidth,
  DefaultColWidth,
  EmptyText,
} from '../table/constant';
import type { TableProps, TableInstance, ColumnType } from '../table';
import type {
  ProTableProps,
  ProTableState,
  ProTableInstance,
  ProTableFilterType,
  ProColumnType,
  MenusType,
  OptionItemType,
  RequestResult,
} from './interface';

const sortMap: Record<string, string> = { ascend: 'ASC', descend: 'DESC' };

function setValue(isPath: boolean, object: any, path: string, value: any) {
  const newValue = value || undefined;
  if (!newValue) return;
  if (isPath) {
    set(object, path, newValue);
    return;
  }
  object[path] = newValue;
}

function _formatMenuParams(
  filteredValues: Record<string, any>,
  menus: MenusType,
) {
  return menus.reduce((pre: any, cur: any) => {
    if (isObject(cur)) {
      const newCur = omit(cur, ['sign']);
      const key = Object.keys(newCur)[0];
      if (filteredValues[key]) {
        pre[key] = filteredValues[key];
      } else {
        pre[key] = (cur as any)[key].value;
      }
    }
    return pre;
  }, {});
}

function formatMenuParams(
  filteredValues: Record<string, any>,
  menus?: MenusType,
  externalSign?: Record<string, any>,
) {
  const _externalSign = _formatMenuParams(
    filteredValues,
    handleSign(menus, externalSign),
  );
  return _formatMenuParams(
    filteredValues,
    handleSign(menus, { ..._externalSign, ...externalSign }),
  );
}

function formatColumn(
  {
    key,
    sorter,
    hideInSearch,
    hideInTable,
    hideInForm = true,
    children,
    ellipsis = { showTitle: false },
    ...column
  }: ProColumnType,
  signData?: Record<string, any>,
) {
  if (hideInForm === false) {
    hideInSearch = true;
  }
  // if (isObject(hideInForm)) {
  //   const r = handleSign([hideInForm], signData);
  //   if (r.length === 0) return undefined;
  // } else if (!hideInForm) return undefined;

  if (isObject(hideInTable)) {
    const r = handleSign([hideInTable], signData);
    if (r.length > 0) return undefined;
  } else if (hideInTable) return undefined;

  const tableColumn: ColumnType = {
    ...column,
    ellipsis,
    sorter: sorter == null ? !!column.defaultSortOrder : sorter,
    key: key
      ? key + ''
      : column.dataIndex instanceof Array
      ? column.dataIndex.join('.')
      : column.dataIndex,
  };

  if (isObject(hideInSearch)) {
    const r = handleSign([hideInSearch], signData);
    if (r.length > 0) {
      tableColumn.hideInSearch = true;
      return tableColumn;
    }
  } else {
    tableColumn.hideInSearch = hideInSearch;
  }

  if (tableColumn.type === 'Date') {
    tableColumn.width = tableColumn.width || DateTimeColWidth;
  } else if (tableColumn.type === 'City') {
    tableColumn.width = tableColumn.width || CityColWidth;
  } else {
    tableColumn.width = tableColumn.width || DefaultColWidth;
  }

  return tableColumn;
}

function formatColumn2Filter(
  column: ProColumnType,
  defaultHideInSearch?: boolean,
  signData?: Record<string, any>,
) {
  const { hideInSearch = defaultHideInSearch } = column;
  if (isObject(hideInSearch)) {
    const r = handleSign([hideInSearch], signData);
    if (r.length > 0) return undefined;
  } else if (hideInSearch) return undefined;

  const filter: ProTableFilterType = {
    dicKey: column.dicKey,
    options: column.options,
    type: column.type || 'Text',
    label: column.title,
    name:
      column.dataIndex instanceof Array
        ? column.dataIndex.join('.')
        : column.dataIndex,
  };

  if (column.dicKey || column.options) {
    filter.type = 'Enum';
    filter.props = { dicKey: column.dicKey, options: column.options };
  }

  let dataIndex = column.searchKey || column.dataIndex;

  if (isArray(dataIndex)) {
    filter.key = dataIndex.join('.');
    filter.isPath = true;
  } else {
    filter.key = dataIndex;
    filter.isPath = false;
  }

  return filter;
}

function formatColumn2FormFilter(
  column: ProColumnType,
  defaultHideInForm?: boolean,
  signData?: Record<string, any>,
) {
  const { hideInForm = defaultHideInForm } = column;
  if (isObject(hideInForm)) {
    const r = handleSign([hideInForm], signData);
    if (r.length > 0) return undefined;
  } else if (hideInForm) return undefined;

  const filter: ProTableFilterType = {
    dicKey: column.dicKey,
    options: column.options,
    type: column.dicKey ? 'Enum' : column.type || 'Text',
    label: column.title,
    name:
      column.dataIndex instanceof Array
        ? column.dataIndex.join('.')
        : column.dataIndex,
  };

  let dataIndex = column.searchKey || column.dataIndex;

  if (isArray(dataIndex)) {
    filter.key = dataIndex.join('.');
    filter.isPath = true;
  } else {
    filter.key = dataIndex;
    filter.isPath = false;
  }

  return filter;
}

const CommonTable = forwardRef<ProTableInstance, ProTableProps>(
  (props, ref) => {
    const {
      // 防止 global.Icon 不存在时报错
      Icon = global.Icon || (() => null),
      style,
      config,
      columns,
      showFilter = true,
      externalSign,
      menuBarRender,
      customTotalCalc,
      onlyCurrentPageSelect,
      formatTotalResult = (r) => r,
      externalQueryDic = {},
      useEventEmitter = global.eventEmitter,
      defaultPageSize = 20,
      defaultColumnConfig,
      tableProps: pTableProps = {},
      customParams = (d) => d,
      renderColumn,
      renderTitle,
      renderFilter,
      onOptionClick = function () {},
      actionRule,
      onActionClick,
      onLinkClick,
      onMenuChange,
    } = props;
    const { menus, actions, actionConfig, options, tableRequest } = config;
    const { reload, setting, search: optionSearch, buttons = [] } =
      options || {};

    const tableRef = useRef<TableInstance>(null);
    const history = useHistory();
    const [prefixCls] = useNamespace('pro-table', props.prefixCls);
    const [uniqueKey] = useUniqueKey({
      key: props.uniqueKey,
      prefix: prefixCls,
    });
    const { width: containerWidth } = useSize(
      tableRef?.current?.container || null,
    );
    const [filteredValues, setFilteredValues] = useControllableValue<
      Record<string, any>
    >(props, {
      valuePropName: 'filteredValues',
      defaultValuePropName: 'defaultFilteredValues',
      trigger: 'onFilteredValuesChange',
    });

    // ============================ Listen Route =============================
    // 当路由中有参数时，设置到 filteredValues 上
    const formatFilteredValues = () => {
      const newFilteredValues: Record<string, any> = {};
      if (formatFilter instanceof Array) {
        formatFilter.forEach((filter) => {
          const key = filter.name;
          newFilteredValues[key] = undefined;
        });
      }

      Object.entries(urlState).forEach(([key, value]) => {
        if (value instanceof Array) {
          value = value.filter((v) => !!v);
        }
        newFilteredValues[key] = value;
      });
      return newFilteredValues;
    };
    const [urlState, setUrlState] = useUrlState<any>({}, { scope: true });
    const listenRoute = () => {
      const newFilteredValues = formatFilteredValues();
      setFilteredValues(newFilteredValues, {});
      setState({
        oldFilteredValues: newFilteredValues,
        menuParams: {
          ...state.menuParams,
          ...formatMenuParams(newFilteredValues, menus, externalSign),
        },
      });
    };
    // TODO: 在WEB端不需要了。但是如果其他地方要用需要考虑怎么兼容～
    // useUpdateEffect(listenRoute, [urlState]);
    useMount(() => {
      if (!props.manual) listenRoute();
    });

    // ============================ eventEmitter =============================
    useEventEmitter.useSubscription(({ uniqueKey: k, action }) => {
      if (k !== uniqueKey) return;
      switch (action) {
        default:
        case 'reload':
          onSubmit();
          break;
        case 'reset':
          onReset();
          break;
      }
    });

    // ============================ Misc =============================
    const [state, setState] = useSetState<ProTableState>(
      (() => {
        const newFilteredValues = formatFilteredValues();
        return {
          total: 0,
          totalMap: {},
          allTotalMap: {},
          totalFail: false,
          tableBodyHeight: 900,
          selectedRows: [],
          selectedRowKeys: [],
          oldFilteredValues: newFilteredValues,
          menuParams: formatMenuParams(newFilteredValues, menus, externalSign),
        };
      })(),
    );
    /** 所有标识 */
    const allSign = { ...props.externalSign, ...state.menuParams };
    useUpdateEffect(() => {
      onMenuChange && onMenuChange(state.menuParams);
      setTimeout(() => onSubmit(), 66);
    }, [state.menuParams]);

    useImperativeHandle(ref, () => ({
      ...search,
      getDataSource: () => tableProps?.dataSource || [],
      getSelectedRows: () => state.selectedRows,
      getDataTotal: () => state.total,
      createReal: () => ({
        onReset,
        onSubmit,
        state,
      }),
    }));

    // ============================ Filter =============================
    /** 通过筛选标记，重新生成新的Filter */
    const formatFilter = useMemoDeep(() => {
      const newColumns = handleSign(columns, allSign);
      return treeToArray(newColumns)
        .map((column) =>
          formatColumn2Filter(
            column,
            defaultColumnConfig?.hideInSearch,
            allSign,
          ),
        )
        .filter((f) => !!f) as ProTableFilterType[];
    }, [columns, allSign]);

    useEffect(() => {
      const newFilteredValues: Record<string, any> = {};
      formatFilter.forEach((filter) => {
        const key = filter.name;
        newFilteredValues[key] = state.oldFilteredValues[key] || undefined;
      });
      setFilteredValues(newFilteredValues, {});
    }, [formatFilter]);

    // ============================ Form =============================
    const [form] = Form.useForm();
    /** 通过筛选标记，重新生成新的Filter */
    const formatFormFilter = useMemoDeep(() => {
      const newColumns = handleSign(columns, allSign);
      return treeToArray(newColumns)
        .map((column) => formatColumn2FormFilter(column, true, allSign))
        .filter((f) => !!f) as ProTableFilterType[];
    }, [columns, allSign]);

    // ============================ Table =============================
    /** 通过筛选标识，重新生成新的 tableRequest */
    const formatTableRequest = useMemoDeep(() => {
      if (tableRequest instanceof Array)
        return handleSign(tableRequest, allSign)[0];
      return tableRequest;
    }, [allSign]);
    /** 通过筛选标识，重新生成新的 操作类动作点 */
    const formatActions = useMemoDeep(() => handleSign(actions, allSign), [
      allSign,
    ]);
    /** 通过筛选标记，重新生成新的 columns */
    const formatColumns = useMemoDeep(() => {
      const newColumns = handleSign(columns, allSign)
        .map((column) => formatColumn(column, allSign))
        .filter((f) => !!f) as ColumnType[];

      if (containerWidth) {
        const columnsWidth = newColumns.reduce((prev, cur) => {
          return calc(`${prev} + ${cur.width}`);
        }, 100);

        if (columnsWidth < containerWidth) {
          const separWidth = parseInt(
            calc(
              `(${containerWidth} - ${columnsWidth}) / ${newColumns.length}`,
            ) + '',
          );
          return newColumns.map((column) => {
            column.width = calc(`${column.width} + ${separWidth}`);
            return column;
          });
        }
      }

      return newColumns;
    }, [columns, formatFilter, allSign, containerWidth]);
    /** 通过formatColumns，生成 defaultSort */
    const defaultSort = useMemoDeep(() => {
      return formatColumns.reduce<Record<string, any>>((prev, cur) => {
        if (cur.key != null && cur.defaultSortOrder != null) {
          prev[cur.key] = sortMap[cur.defaultSortOrder];
        }
        return prev;
      }, {});
    }, [config]);

    const totalColumnMap = useMemoDeep(() => {
      const [first, ...restColumns] = formatColumns;
      devWarning(!first?.summary, 'Table', '第一列不能设置为统计栏');
      const totalColumnMap: Record<string, { column: ColumnType }> = {};
      restColumns
        .filter((column) => !!column.summary)
        .forEach((column) => {
          if (column.summary instanceof Array) {
            column.summary.forEach((t) => {
              if (t === 'Total') {
                totalColumnMap[column.key] = { column };
              }
            });
          } else {
            totalColumnMap[column.key] = { column };
          }
        });
      return totalColumnMap;
    }, [formatColumns]);

    const getTableData = async (
      { current, pageSize, sorter }: PaginatedParams[0],
      formData: Record<string, any>,
    ): Promise<RequestResult> => {
      let requestAPI = props.request;
      if (!requestAPI) {
        requestAPI = (url, data) => request.post(url, { data });
      }

      const sort: Record<string, any> = {};
      if (sorter) {
        if (sorter instanceof Array) {
          sorter.forEach(({ columnKey, order }) => {
            sort[columnKey] = sortMap[order];
          });
        } else {
          const { columnKey, order } = sorter;
          sort[columnKey] = sortMap[order];
        }
      }

      const singleQuery: Record<string, any> = {};
      const rangeQuery: Record<string, any> = {};

      function runFilter(filter: ProTableFilterType, value: any) {
        const key = filter.key || filter.name;
        if (value == null) return;
        if (filter.type === 'Date') {
          if (!value?.length) return;
          const rangeDate: Record<string, any> = {};
          if (value[0])
            rangeDate.start = moment(value[0]).format('YYYY-MM-DDT00:00:00');
          if (value[1])
            rangeDate.end = moment(value[1]).format('YYYY-MM-DDT23:59:59');
          setValue(!!filter.isPath, rangeQuery, key, rangeDate);
          return;
        }
        // if (isArray(value)) {
        //   // 区间搜索，多选筛选，待后端提供相应的API
        //   setValue(!!filter.isPath, singleQuery, key, value.join(','));
        //   return;
        // }
        setValue(!!filter.isPath, singleQuery, key, value);
      }

      formatFilter.forEach((filter) =>
        runFilter(filter, filteredValues[filter.name]),
      );
      formatFormFilter.forEach((filter) =>
        runFilter(filter, formData[filter.name]),
      );

      const newFilteredValues = formatFilteredValues();
      const menuParams = formatMenuParams(newFilteredValues, menus, allSign);

      const newMenuParams = pickBy(
        { ...menuParams, ...state.menuParams },
        (m: string) => m !== '__ALL__',
      );

      const data = customParams({
        sort: {
          ...defaultSort,
          ...sort,
        },
        query: {
          ...singleQuery,
          ...newMenuParams,
          ...formatTableRequest?.query,
        },
        rangeQuery,
        pageSize,
        page: current,
      });

      totalReq.run(data);
      return requestAPI(formatTableRequest.url, data);
    };
    const getTableTotalData = async (data: Record<string, any>) => {
      const map: ProTableState['allTotalMap'] = {};

      // Object.keys(totalColumnMap).forEach((key) => {
      //   map[key] = { total: null } as any;
      // });
      let requestAPI = props.request;
      if (!requestAPI) {
        requestAPI = (url, data) => request.post(url, { data });
      }

      setState({ totalFail: false });
      if (formatTableRequest.totalUrl) {
        const res: any = await requestAPI(formatTableRequest.totalUrl, data);
        const result = formatTotalResult(res.data);
        for (const key in result) {
          map[key] = { total: result[key] };
        }
      }

      return map;
    };
    const { tableProps, loading, search, data } = useAntdTable(getTableData, {
      form,
      manual: true,
      defaultPageSize,
      throwOnError: true,
      code: formatTableRequest.code,
      defaultParams: [{ current: 1, pageSize: defaultPageSize }],
      formatResult: (response: any) => {
        return {
          response,
          list: get(response, 'data.rows', []),
          total: get(response, 'data.rowTotal', 0),
          statistics: get(response, 'data.statistics'),
        };
      },
      onSuccess: (res) => {
        const newState: Partial<ProTableState> = { total: res.total };

        if (
          isObject(pTableProps.rowSelection) &&
          isFunction(pTableProps.rowSelection.getCheckboxProps)
        ) {
          const getCheckboxProps = pTableProps.rowSelection.getCheckboxProps;
          const rowKey = pTableProps.rowKey;
          const rowKeyFn = (record: any): React.Key => {
            let _key: React.Key = '';
            if (typeof rowKey === 'function') {
              try {
                _key = rowKey(record);
              } catch (error) {
                console.error(error);
                _key = 'id';
              }
            }

            if (typeof rowKey === 'string') {
              _key = record[rowKey];
            }
            if (!_key) {
              _key = 'id';
            }
            return _key;
          };
          const selectedRows = state.selectedRows.filter(
            (row) => !getCheckboxProps(row).disabled,
          );

          newState.selectedRowKeys = selectedRows.map(rowKeyFn);
          newState.selectedRows = selectedRows;
        }

        setState(newState);
        props.onSuccess?.(res);
      },
      onError: (error) => {
        props.onError?.(error);
      },
    });
    const totalReq = useRequest(getTableTotalData, {
      manual: true,
      throwOnError: true,
      onSuccess(res) {
        const allTotalMap = { ...res };
        if (!state.selectedRows.length) {
          const map: ProTableState['totalMap'] = {};
          Object.keys(totalColumnMap).forEach((key) => {
            const { column } = totalColumnMap[key];
            const summaryKey = column.summaryKey ?? column.key;
            map[summaryKey] = allTotalMap[summaryKey];
          });
          setState({ totalMap: map, allTotalMap });
        } else {
          setState({ allTotalMap });
        }
      },
      onError() {
        setState({ totalFail: true });
      },
    });

    const rowSelectionChange = (
      selectedRowKeys: React.Key[] = [],
      selectedRows: any[] = [],
    ) => {
      setState({ selectedRowKeys, selectedRows });

      if (isObject(pTableProps.rowSelection)) {
        pTableProps.rowSelection?.onChange?.(selectedRowKeys, selectedRows);
      }
    };

    useEffect(() => {
      const newState: Partial<ProTableState> = {};
      const map: ProTableState['totalMap'] = {};

      if (state.selectedRows.length) {
        Object.keys(totalColumnMap).forEach((key) => {
          const { column } = totalColumnMap[key];
          const summaryKey = column.summaryKey ?? column.key;
          const totalCalc = customTotalCalc?.(summaryKey);
          map[summaryKey] = {
            total: totalCalc
              ? totalCalc(state.selectedRows)
              : state.selectedRows.reduce((p, record) => {
                  const text: any = getPathValue(
                    record,
                    column.dataIndex || column.key,
                  );
                  if (isEmpty(text)) return p;
                  return calc(`${p} + ${text} `, EmptyText);
                }, 0),
          };
        });
      } else {
        Object.keys(totalColumnMap).forEach((key) => {
          const { column } = totalColumnMap[key];
          const summaryKey = column.summaryKey ?? column.key;
          map[summaryKey] = state.allTotalMap[summaryKey];
        });
      }
      newState.totalMap = map;

      setState(newState);
    }, [state.selectedRows]);

    const formatTableProps: TableProps = {
      ...props.tableProps,
      ...tableProps,
      loading,
      actionConfig,
      externalQueryDic,
      defaultColumnConfig,
      actions: formatActions,
      columns: formatColumns,
      showFilter,
      filterIsEmpty: formatFilter.length === 0,
      actionRule,
      onActionClick,
      onLinkClick,
      renderTitle,
      renderFilter,
      renderColumn,
      scroll: {
        y: state.tableBodyHeight,
        x: 'max-content',
        scrollToFirstRowOnChange: false,
      },
      pagination:
        props.tableProps?.pagination === false
          ? false
          : {
              ...props.tableProps?.pagination,
              ...tableProps?.pagination,
              hideOnSinglePage: false,
              showSizeChanger: true,
              pageSizeOptions: Array.from(
                new Set(
                  [defaultPageSize, 20, 50, 100, 200].sort((a, b) => a - b),
                ),
              ).map((p) => p + ''),
              onChange: (page, pageSize) => {
                if (onlyCurrentPageSelect) {
                  rowSelectionChange();
                }

                tableProps?.pagination?.onChange?.(page, pageSize);
                if (props.tableProps?.pagination !== false)
                  props.tableProps?.pagination?.onChange?.(page, pageSize);
              },
            },
      rowSelection:
        pTableProps.rowSelection === true
          ? {
              fixed: true,
              preserveSelectedRowKeys: true,
              onChange: rowSelectionChange,
              selectedRowKeys: state.selectedRowKeys,
            }
          : isObject(pTableProps.rowSelection)
          ? {
              fixed: true,
              preserveSelectedRowKeys: true,
              selectedRowKeys: state.selectedRowKeys,
              ...pTableProps.rowSelection,
              onChange: rowSelectionChange,
            }
          : undefined,
    };

    // ============================ ToolBar =============================
    /** 通过筛选标记，重新生成新的 menus */
    const formatMenus = useMemoDeep(() => handleSign(menus, allSign), [
      menus,
      allSign,
    ]) as MenusType;
    const formatButtons = useMemoDeep(() => handleSign(buttons, allSign), [
      allSign,
    ]);
    const renderOption = (
      key: string,
      optionConfig: OptionItemType,
      DefaultOption: React.ReactNode,
    ): React.ReactNode => {
      const node: any = onOptionClick(key, optionConfig, DefaultOption);
      return React.isValidElement(node) ? node : DefaultOption;
    };
    const optionClassName = classnames(
      `${prefixCls}-option`,
      `${prefixCls}-default-option`,
    );
    const renderToolBar = () => (
      <>
        {optionSearch && (
          <Input
            style={{ maxWidth: 165 }}
            placeholder="搜索"
            prefix={<SearchOutlined />}
          />
        )}
        {reload && (
          <Tooltip title="刷新">
            <Button
              icon={<RefreshOutlined />}
              className={optionClassName}
              onClick={onSubmit}
            ></Button>
          </Tooltip>
        )}
        {setting && (
          <ColumnSetting
            className={optionClassName}
            columns={formatColumns}
            checkable
            draggable
          />
        )}
        {formatButtons.map((item, index) => {
          const { name, key, sign, icon, click, ...buttonProps } = item;
          const { type = 'default' } = item;

          return renderOption(
            key || name,
            item,
            <Button
              key={index}
              className={classnames(
                `${prefixCls}-option`,
                type === 'default' && `${prefixCls}-default-option`,
              )}
              icon={typeof icon === 'string' ? <Icon type={icon} /> : icon}
              {...buttonProps}
              onClick={() => {
                if (click) {
                  toRoute(history, externalQueryDic, click);
                } else onOptionClick(key || name, item);
              }}
            >
              {name}
            </Button>,
          );
        })}
      </>
    );
    const handleMenuChange = (key: string, value: any) =>
      setState({ menuParams: { ...state.menuParams, [key]: value } });

    // ============================ Events =============================
    const onReset = () => {
      setState({ oldFilteredValues: {} });
      setFilteredValues({}, {});
      rowSelectionChange();
      setTimeout(() => {
        search.reset();
      }, 0);
    };
    const onFormReset = () => {
      search.reset();
    };
    const onSubmit = () => {
      rowSelectionChange();
      search.submit();
    };
    const onCleanFiltered = () => {
      const newFilteredValues: Record<string, any> = {};
      if (formatFilter instanceof Array) {
        formatFilter.forEach((filter) => {
          const key = filter.name;
          newFilteredValues[key] = undefined;
        });
      }
      setUrlState(newFilteredValues);
      setState({ oldFilteredValues: {} });
      setFilteredValues({}, {});
      onSubmit();
    };

    const render = (wrapperStyle?: React.CSSProperties) => {
      return (
        <div
          id={uniqueKey}
          className={classnames(`${prefixCls}-wrapper `, props.className)}
          style={wrapperStyle}
        >
          {(menus || options || config.title) && (
            <QuickSearch
              menus={formatMenus}
              title={config.title}
              menuParams={state.menuParams}
              statistics={data?.statistics}
              menuBarRender={menuBarRender?.(state.menuParams)}
              toolBarRender={renderToolBar()}
              onMenuChange={handleMenuChange}
            />
          )}

          <Table
            ref={tableRef}
            {...formatTableProps}
            totalMap={state.totalMap}
            totalFail={state.totalFail}
            totalLoading={totalReq.loading}
            onTotalReload={totalReq.refresh}
            filteredValues={filteredValues}
            onCleanFiltered={onCleanFiltered}
            onFilteredChange={(key, value) => {
              const newFilteredValues = { ...filteredValues, [key]: value };
              setState({ oldFilteredValues: newFilteredValues });
              setFilteredValues(newFilteredValues, { [key]: value });
              if (!value) {
                setUrlState({ [key]: value });
              }
              // TODO 临时做处理，查询值变化事件不应该需要手动触发更新
              setTimeout(() => {
                onSubmit();
              }, 0);
            }}
          />
        </div>
      );
    };

    if (formatFormFilter.length) {
      return (
        <WhiteBoard style={style}>
          <WhiteBoard.TopBoard>
            {props.children}
            <FormFilter
              form={form}
              loading={loading}
              filter={formatFormFilter}
              onReset={onFormReset}
              onSubmit={onSubmit}
            />
          </WhiteBoard.TopBoard>
          <WhiteBoard.BottomBoard>{render()}</WhiteBoard.BottomBoard>
        </WhiteBoard>
      );
    }

    return render(style);
  },
);

CommonTable.defaultProps = {
  antPrefixCls: 'ant',
  defaultPageSize: 20,
  customParams: (params) => params,
};

export default CommonTable;
