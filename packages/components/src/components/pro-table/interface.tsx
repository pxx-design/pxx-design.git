import type { ButtonProps } from '../button';
import { TableRowSelection } from 'antd/es/table/interface';
import type { Key, FC, ReactNode, CSSProperties } from 'react';
import type { IconFontProps } from '@ant-design/icons/lib/components/IconFont';
import type { TableProps, ColumnType, ActionType } from '../table';
import type {
  ColumnDataType,
  ColumnFilterItem,
  CommTableProps,
  TableTotalInfo,
} from '../table/interface';
import type {
  HistoryPushParams,
  HistoryProps,
  EventEmitterProps,
} from '@parallel-line/common/es/utils';

export interface ProColumnType
  extends Omit<ColumnType, 'key' | 'hideInSearch' | 'children'> {
  key?: string;
  /**
   * @description 筛选标识，和 Table.externalSign 一起使用
   */
  sign?: Record<string, any>[];
  /**
   * 查询项的key，默认和 dataIndex 一致
   */
  searchKey?: string | string[];
  /**
   * 在查询表单上中不展示此列
   */
  hideInForm?:
    | boolean
    | {
        /**
         * @description 筛选标识，和 Table.externalSign 一起使用
         */
        sign?: Record<string, any>[];
      };
  /**
   * 在 Table 中不展示此列
   */
  hideInTable?:
    | boolean
    | {
        /**
         * @description 筛选标识，和 Table.externalSign 一起使用
         */
        sign?: Record<string, any>[];
      };
  /**
   * 在查询项中不展示此列
   */
  hideInSearch?:
    | true
    | {
        /**
         * @description 筛选标识，和 Table.externalSign 一起使用
         */
        sign?: Record<string, any>[];
      };

  /**
   * @deprecated 不可使用子列
   */
  children?: ProColumnType[];
}

export interface ProTableRequest {
  /** 权限码, 请在运营端/菜单管理中获取 */
  code?: string;
  url: string;
  totalUrl?: string;
  /**
   * @description 默认的查询条件
   */
  query?: Record<string, any>;
  /**
   * @description 筛选标识，和 Table.externalSign 一起使用
   */
  sign?: Record<string, any>[];
}

export interface RequestResult {
  data: {
    rowTotal: number;
    rows: any[];
    statistics?: { 1: Record<string, number> };
  };
}

export type RequestFn = (
  url: string,
  data: Record<string, any>,
) => Promise<RequestResult>;

export interface ProTableState {
  total: number;
  tableBodyHeight: number;
  selectedRows: any[];
  selectedRowKeys: Key[];
  totalFail?: boolean;
  totalMap: Record<string, TableTotalInfo>;
  allTotalMap: Record<string, TableTotalInfo>;
  menuParams: Record<string, any>;
  oldFilteredValues: Record<string, any>;
}

export interface ProTableProps
  extends CommTableProps,
    HistoryProps,
    EventEmitterProps {
  prefixCls?: string;
  antPrefixCls?: string;
  className?: string;
  style?: CSSProperties;
  /**
   * - 默认 false。 即在初始化时自动执行 service。
   * - 如果设置为 true，则需要手动调用 run 触发执行。
   */
  manual?: boolean;
  /**
   * @description 只能单页选择，一旦翻页自动清空已有选择项。
   */
  onlyCurrentPageSelect?: boolean;
  /**
   * @description 自定义请求函数， 默认使用 utils.request
   */
  request?: RequestFn;
  /**
   *  @description 指定 图标组件，默认 global.Icon
   */
  Icon?: FC<IconFontProps>;
  /**
   * @description 筛选标识，根据标识重新生成columns、tableRequest等
   */
  externalSign?: Record<string, any>;
  /**
   * @description 表格基础配置
   */
  config: ProTableConfig;
  /**
   * @description 表格列的配置描述
   */
  columns?: ProColumnType[];
  // columns?: () => Promise<ProColumnType[]>;
  /**
   * @description 表格中的Table配置
   */
  tableProps?: Omit<TableProps, 'columns' | 'getDic' | 'rowSelection'> & {
    rowSelection?: TableRowSelection<any> | true;
  };
  /**
   * @description 筛选值集合
   */
  filteredValues?: Record<string, any>;
  defaultFilteredValues?: Record<string, any>;
  /**
   * @description 筛选值改变事件
   */
  onFilteredValuesChange?(
    allValues: Record<string, any>,
    changedValues: Record<string, any>,
  ): void;
  /**
   * @description 默认每页行数
   */
  defaultPageSize?: number;
  /**
   * @description 在请求前转换参数
   */
  customParams?: (params: Record<string, any>) => Record<string, any>;
  /**
   * @description 自定义统计计算
   */
  customTotalCalc?: (
    key: string,
  ) => ((selectedRows: any[]) => number | string) | undefined;
  /**
   * @description 在统计请求后格式化结果
   */
  formatTotalResult?: (response: any) => any;
  /**
   * 自定义二级菜单区域
   */
  menuBarRender?: (val: any) => React.ReactNode;
  /**
   * ToolBar 上面的菜单改变事件
   */
  onMenuChange?: (val: any) => void;
  /**
   * Option 的点击事件
   *
   * 当返回值为 ReactNode 时，表示为 替换 Option，可用于二次封装 Option
   */
  onOptionClick?: (
    key: string,
    config: OptionItemType,
    DefaultOption?: ReactNode,
  ) => ReactNode | void;
  /**
   * @description 数据源请求成功的回调
   */
  onSuccess?: (response: any) => void;
  /**
   * @description 数据源请求失败的回调
   */
  onError?: (error: any) => void;
}

export interface ProTableFilterType {
  key?: string;
  name: string;
  type: ColumnDataType | 'Enum' | 'Text';
  label: string;
  dicKey?: string;
  options?: ColumnFilterItem[];
  props?: Record<string, any>;
  isPath?: boolean;
}

export interface MenuItemType extends Record<string, any> {
  /**
   * 标题，当该值存在时，将只显示标题文本
   */
  title?: string;
  /**
   * 菜单默认选中的值，默认 __ALL__ ，若 hideAll 为 true 则选中第一个
   */
  value?: '__ALL__' | string;
  /**
   * 是否启用统计功能，默认为false
   */
  statistics?: boolean;
  allPosition?: 'left' | 'right';
  /**
   * @description 隐藏 `全部` 子项
   */
  hideAll?: boolean;
  /**
   * @description 枚举类型在全局字典对应的路径，和getDic搭配使用，获取字典可选项生成 options
   */
  dicKey?: string;
  /**
   * @description 静态可选项，dicKey 和 options 二选一
   */
  options?: Record<string, any>[];
}

export interface MenuFilterType extends MenuItemType {
  type?: 'Filter';
}

export type MenuType = Record<string, MenuItemType | undefined> & {
  /**
   * @description 筛选标识，和 Table.externalSign 一起使用
   */
  sign?: Record<string, any>[];
};

export type MenusType = MenuType[];

export interface OptionItemType
  extends Omit<
    ButtonProps,
    'block' | 'icon' | 'loading' | 'className' | 'onClick'
  > {
  /**
   * 唯一标识，不定义时默认为 name 值
   */
  key?: string;
  name: string;
  icon?: ReactNode;
  click?: HistoryPushParams;
  /**
   * @description 筛选标识，和 Table.externalSign 一起使用
   */
  sign?: Record<string, any>[];
}

export interface OptionsType {
  reload?: boolean;
  setting?: boolean;
  search?: boolean;
  buttons?: OptionItemType[];
}

export interface ProTableConfig {
  actionConfig?: {
    majorNumber?: number;
    majorWidth?: number;
  };
  /**
   * @description 表格操作列配置项
   */
  actions?: ActionType[];
  /**
   * @description 快捷筛选配置项
   */
  menus?: MenusType;
  title?: string;
  /**
   * @description 快捷操作配置项
   */
  options?: OptionsType;
  tableRequest: ProTableRequest | ProTableRequest[];
}

export interface ProTableInstance {
  changeType: () => void;
  submit: () => void;
  reset: () => void;
  getDataSource: () => any[];
  getSelectedRows: () => any[];
  getDataTotal: () => number;
  createReal: () => {
    onReset: () => void;
    onSubmit: () => void;
    state: ProTableState;
  };
}
