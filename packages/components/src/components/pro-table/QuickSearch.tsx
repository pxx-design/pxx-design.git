/* eslint-disable func-names */
import React from 'react';
import { omit } from 'lodash-es';
import classNames from 'classnames';
import { Menu } from 'antd';
import { getDic, TagInfo } from '@parallel-line/utils';

import { MenusType, MenuType } from './interface';

interface QuickSearchProps {
  prefixCls?: string;
  title?: string;
  menus: MenusType;
  menuParams: Record<string, any>;
  statistics?: { 1: Record<string, number> };
  toolBarRender?: React.ReactNode;
  menuBarRender?: React.ReactNode;
  onMenuChange?: (name: string, value: any) => void;
}

const QuickSearch: React.FC<QuickSearchProps> = (props) => {
  const prefixCls = `${props.prefixCls}-pro-table-search`;
  const {
    menus,
    title,
    menuParams = {},
    statistics,
    menuBarRender,
    onMenuChange = function () {},
  } = props;

  if (!menus && !props.toolBarRender) return null;

  const toolBar = (
    <div className={`${prefixCls}-toolbar`}>{props.toolBarRender}</div>
  );

  const renderTitle = () =>
    title ? <div className={`${prefixCls}-title`}>{title}</div> : null;

  const renderMenu = (
    className: string,
    data: Omit<MenuType, 'sign'> | string,
  ) => {
    if (typeof data === 'string') return renderTitle();

    const [name, value] = Object.entries(data)[0];
    const selectedKeys = menuParams[name];

    if (value?.title) return value?.title;

    let list: TagInfo[] = [];

    if (getDic && value?.dicKey) {
      list = [...getDic(value?.dicKey)].map((dic) => ({
        ...dic,
        key: dic.key + '',
      }));
    } else if (value?.options) {
      list = value?.options.map((v) => {
        // eslint-disable-next-line @typescript-eslint/no-shadow
        const [key, value] = Object.entries(v)[0];
        return { key: key + '', value };
      });
    }

    if (list.length > 0 && !value?.hideAll) {
      if (value?.allPosition === 'right') {
        list.push({ key: '__ALL__', value: '全部' });
      } else {
        list.unshift({ key: '__ALL__', value: '全部' });
      }
    }

    return (
      <Menu
        mode="horizontal"
        className={className}
        selectedKeys={[selectedKeys || list[0].key]}
        onClick={(e) => onMenuChange(name, e.key)}
        style={{ marginBottom: 1 }}
      >
        {list.map((v) => (
          <Menu.Item key={v.key}>
            {v.value}
            {value?.statistics
              ? `（${statistics ? statistics[1][v.key] || 0 : 0}）`
              : null}
          </Menu.Item>
        ))}
      </Menu>
    );
  };

  const render = () => {
    if (!menus || (menus as any[]).length === 0)
      return (
        <>
          <div className={`${prefixCls}-menu-wrap`}>
            {renderTitle()}
            {toolBar}
          </div>
          <div className={`${prefixCls}-filter-wrap`} />
        </>
      );

    const [m1, m2] = menus;
    const menuM = m1 ? omit(m1, ['sign']) : title;
    const filterM = m2 ? omit(m2, ['sign']) : null;

    return (
      <>
        <div
          className={classNames(
            `${prefixCls}-menu-wrap`,
            !menuM && `${prefixCls}-menu-space`,
          )}
        >
          {menuM
            ? renderMenu(`${prefixCls}-menu`, menuM)
            : filterM && renderMenu(`${prefixCls}-filter`, filterM)}
          {toolBar}
        </div>
        <div className={`${prefixCls}-filter-wrap`}>
          {filterM && menuM && renderMenu(`${prefixCls}-filter`, filterM)}
        </div>
      </>
    );
  };

  return (
    <div className={prefixCls}>
      {render()}
      {menuBarRender}
    </div>
  );
};

QuickSearch.defaultProps = {
  prefixCls: 'pxx',
};

export default QuickSearch;
