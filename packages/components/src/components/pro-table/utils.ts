import { forEach, isEqual, every } from 'lodash-es';

export function handleSign<T>(data?: T[], signData?: Record<string, any>): T[];
export function handleSign(
  data: any[] = [],
  signData: Record<string, any> = {},
) {
  return data.reduce<any[]>((pre, item) => {
    if (item == null) {
      pre.push(item);
      return pre;
    }
    if (item.sign) {
      forEach(item.sign, (sign) => {
        const filterKeys = Object.keys(sign);
        const flag = every(filterKeys, (v, index) =>
          isEqual(sign[filterKeys[index]], signData[v]),
        );
        if (flag) {
          pre.push(item);
        }
        return !flag;
      });
      return pre;
    }
    if (item.children) {
      item.children = handleSign(item.children, signData);
    }
    pre.push(item);
    return pre;
  }, []);
}

interface TreeArray<T> {
  children?: T[];
  [k: string]: any;
}

export function treeToArray<T extends TreeArray<T>>(nodes: T[]) {
  let r: T[] = [];
  if (Array.isArray(nodes)) {
    for (let i = 0, l = nodes.length; i < l; i++) {
      r.push(nodes[i]); // 取每项数据放入一个新数组
      if (
        nodes[i].children instanceof Array &&
        (nodes[i].children?.length || 0) > 0
      ) {
        // 若存在children则递归调用，把数据拼接到新数组中，并且删除该children
        r = r.concat(treeToArray(nodes[i].children || []));
        delete nodes[i].children;
      }
    }
  }
  return r;
}
