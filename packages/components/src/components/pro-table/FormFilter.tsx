import React from 'react';
import { Form, FormInstance } from 'antd';
import { useMemoDeep } from '@parallel-line/hooks';
import { ProTableFilterType } from './interface';

import Input from '../input';
import Button from '../button';
import Select from '../select';
import CitySelect from '../city-select';
import InputNumber from '../input-number';
import DateRangePicker from '../table/DateRangePicker';

interface FormFilterProps {
  prefixCls?: string;
  form?: FormInstance<any>;
  loading?: boolean;
  filter: ProTableFilterType[];
  onSubmit?: () => void;
  onReset?: () => void;
}

const FormFilter: React.FC<FormFilterProps> = (props) => {
  const { form, filter, loading, onSubmit, onReset } = props;
  const prefixCls = `${props.prefixCls}-pro-table-form`;
  const renderFormItem = useMemoDeep(() => {
    const componentMap: Record<
      ProTableFilterType['type'],
      React.ComponentType<any>
    > = {
      Text: Input,
      Enum: Select,
      Tag: Input,
      Date: DateRangePicker,
      Link: Input,
      City: CitySelect,
      Number: Input,
      Numbering: InputNumber,
    };
    const formItem = filter.map((item) => {
      const Component = componentMap[item.type];
      if (Component == null) return null;
      return (
        <Form.Item key={item.name} name={item.name} label={item.label}>
          <Component {...item.props} />
        </Form.Item>
      );
    });
    return formItem;
  }, [filter]);
  return (
    <Form colon={false} layout="inline" form={form} className={prefixCls}>
      <div className={`${prefixCls}-items`}>{renderFormItem}</div>

      <div className={`${prefixCls}-buttons`}>
        <Button onClick={onReset} loading={loading}>
          重置
        </Button>
        <Button type="primary" loading={loading} onClick={onSubmit}>
          查询
        </Button>
      </div>
    </Form>
  );
};
FormFilter.defaultProps = {
  prefixCls: 'pxx',
  filter: [],
};
export default FormFilter;
