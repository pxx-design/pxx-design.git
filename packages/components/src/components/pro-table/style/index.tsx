import 'antd/es/divider/style';

// style dependencies
import '../../table/style';
import '../../button/style';
import '../../config-provider/style';
import '../../white-board/style';

import './index.less';
