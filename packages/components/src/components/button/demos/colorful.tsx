/**
 * title: 多彩按钮
 * desc: 我们添加了多种预设色彩的按钮样式，用作不同场景使用。
 */

import React from 'react';
import { Space } from 'antd';
import { Button } from '@parallel-line/components';

export default () => (
  <Space wrap size="middle" style={{ background: '#fff', padding: 16 }}>
    <Button color="blue">Blue Button</Button>
    <Button color="gray">Gray Button</Button>
  </Space>
);
