import * as React from 'react';
import classNames from 'classnames';
import ConfigProvider from '../config-provider';
import Button, {
  ButtonSize,
  ButtonShape,
  ButtonType,
  ButtonProps as AntButtonProps,
} from 'antd/es/button';
import { Access } from '@parallel-line/common';

export interface ButtonGroupProps {
  /** 设置按钮大小 */
  size?: ButtonSize;
  style?: React.CSSProperties;
  className?: string;
  prefixCls?: string;
}
export interface ButtonProps extends Omit<AntButtonProps, 'inputMode' | 'is'> {
  /** 权限码, 请在运营端/菜单管理中获取 */
  code?: string;
  /** 将按钮宽度调整为其父宽度的选项 */
  block?: boolean;
  /** 设置按钮大小 */
  size?: ButtonSize;
  /** 按钮色 */
  color?: 'blue' | 'gray';
  /** 设置按钮类型 */
  type?: ButtonType;
  /** 设置按钮的图标组件 */
  icon?: React.ReactNode;
  /** 设置按钮形状 */
  shape?: ButtonShape;
  /** 设置按钮载入状态 */
  loading?:
    | boolean
    | {
        delay?: number;
      };
  /** 幽灵属性，使按钮背景透明 */
  ghost?: boolean;
  /** 设置危险按钮 */
  danger?: boolean;
  inputMode?:
    | 'none'
    | 'text'
    | 'tel'
    | 'url'
    | 'email'
    | 'numeric'
    | 'decimal'
    | 'search';
}

const InlineButton: React.FC<ButtonProps> = (
  props: React.PropsWithChildren<ButtonProps>,
) => {
  const { getPrefixCls } = React.useContext(ConfigProvider.Context);
  const {
    prefixCls: customizePrefixCls,
    className,
    color,
    code,
    ...otherProps
  } = props;
  const prefixCls = getPrefixCls('button', customizePrefixCls);

  return (
    <Access code={code}>
      <Button
        {...otherProps}
        className={classNames(className, color && `${prefixCls}-${color}`)}
        prefixCls={prefixCls}
      />
    </Access>
  );
};

InlineButton.defaultProps = {
  type: 'default',
  size: 'middle',
  loading: false,
  disabled: false,
  danger: false,
  block: false,
  ghost: false,
};

export const Group: React.FC<ButtonGroupProps> = (props) => {
  return (
    <Button.Group {...props} prefixCls={`${props.prefixCls}-button-group`} />
  );
};

Group.defaultProps = {
  prefixCls: 'pxx',
};

export default InlineButton;
