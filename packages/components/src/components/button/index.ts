import InlineButton, { Group, ButtonProps } from './button';

type InternalButtonType = typeof InlineButton;

interface ButtonInterface extends InternalButtonType {
  Group: typeof Group;
}

const Button = InlineButton as ButtonInterface;

Button.Group = Group;

export type { ButtonProps };

export default Button;
