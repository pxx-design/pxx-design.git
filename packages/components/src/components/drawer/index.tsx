import React, {
  useEffect,
  useContext,
  forwardRef,
  useImperativeHandle,
} from 'react';
import { Drawer, DrawerProps as AntDrawerProps } from 'antd';
import { isFunction } from 'lodash-es';
import { useSetState } from 'ahooks';
import classnames from 'classnames';
import ConfigProvider from '../config-provider';

type getContainerFunc = () => HTMLElement;

export interface DrawerInstance {
  close: () => void;
}

export interface DrawerProps
  extends Omit<AntDrawerProps, 'footer' | 'visible'> {
  prefixCls?: string;
  title?: string;
  visible?: boolean;
  className?: string;
  uniqueKey?: string;
  width?: string | number;
  content?: React.ReactNode;
  children?: React.ReactNode;
  destroyOnClose?: boolean;
  onClose?: () => void;
  afterClose?: () => void;
  maskClosable?: boolean;
  getContainer?: string | HTMLElement | getContainerFunc | false;
}

const InlineDrawer = forwardRef<DrawerInstance, DrawerProps>((props, ref) => {
  const {
    uniqueKey,
    prefixCls: customizePrefixCls,
    afterClose,
    content,
    children,
    ...restProps
  } = props;
  const { getPrefixCls } = useContext(ConfigProvider.Context);
  const prefixCls = getPrefixCls('drawer', customizePrefixCls);
  const Component: any = children || content;
  const [state, setState] = useSetState({ _visible: props.visible });

  const batchUpdateDrawersStyle = (
    operateClassName: string,
    operateType: 'open' | 'close',
  ) => {
    const nodeList = Array.from(
      document.getElementsByClassName(operateClassName),
    ) as HTMLElement[];
    const len = nodeList.length;
    nodeList.forEach((node: HTMLElement, index) => {
      let steps = len - index;
      if (operateType === 'close') steps = len - index - 1;
      node.style.transform = `translateX(-${180 * steps}px)`;
    });
  };

  useEffect(() => {
    if (state._visible) {
      // 展开,将前面同一层级下的所有抽屉往左移动180px
      batchUpdateDrawersStyle('ant-drawer-open', 'open');
    } else {
      // 收起，移回上次位置
      batchUpdateDrawersStyle('ant-drawer-open', 'close');
    }
  }, [state._visible]);
  const onClose = () => {
    setState({ _visible: false });
    props.onClose?.();
  };
  const afterVisibleChange = (visible: boolean) => {
    if (!visible) afterClose?.();
    props.afterVisibleChange?.(visible);
  };

  useImperativeHandle(ref, () => ({ close: onClose }));

  return (
    <Drawer
      {...restProps}
      onClose={onClose}
      visible={state._visible}
      afterVisibleChange={afterVisibleChange}
      className={classnames(prefixCls, props.className)}
    >
      {React.Children.map(Component, (child) => {
        if (isFunction(child.type)) {
          return React.cloneElement(child, {
            onClose: (...arg: any[]) => {
              onClose();
              child?.props?.onClose?.(...arg);
            },
          });
        }
        return child;
      })}
    </Drawer>
  );
});

InlineDrawer.defaultProps = {
  width: 448,
  visible: false,
  getContainer: false,
};

export default InlineDrawer;
