import React from 'react';
import { DatePicker } from 'antd';
import { IntervalOutlined } from '@parallel-line/icons';
import { RangePickerProps } from 'antd/es/date-picker';

function DateRangePicker(props: RangePickerProps) {
  return (
    <DatePicker.RangePicker
      allowClear
      separator={
        <span aria-label="to" className="ant-picker-separator">
          <IntervalOutlined />
        </span>
      }
      {...props}
    />
  );
}

export default DateRangePicker;
