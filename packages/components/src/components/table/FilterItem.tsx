import * as React from 'react';
import { Form } from 'antd';
import { isEmpty } from 'lodash-es';
import classNames from 'classnames';
import { useBoolean } from 'ahooks';

interface FilterItemProps<CP> {
  prefixCls: string;
  name?: string;
  children: React.ReactElement<CP>;
}

interface FilterItemChildrenProps<E = Element> extends Record<string, any> {
  onFocus?: React.FocusEventHandler<E>;
  onBlur?: React.FocusEventHandler<E>;
}

function FilterItem<E, CP extends FilterItemChildrenProps<E>>(
  props: FilterItemProps<CP>,
) {
  const { name, children } = props;
  const [focus, { toggle }] = useBoolean();
  const [hasVal, { toggle: setHasVal }] = useBoolean();
  const [hover, { setFalse, setTrue }] = useBoolean();
  const onMouseEnter = () => {
    setTrue();
  };
  const onMouseLeave = () => {
    setFalse();
  };
  const onChange = (value?: any, ...args: any[]) => {
    setHasVal(!isEmpty(value));
    children.props?.onChange?.(value, ...args);
  };
  const onFocus: React.FocusEventHandler<E> = (event) => {
    toggle(true);
    children.props?.onFocus?.(event);
  };
  const onBlur: React.FocusEventHandler<E> = (event) => {
    toggle(false);
    children.props?.onBlur?.(event);
  };
  return (
    <div
      className={classNames(`${props.prefixCls}-filter-item`, {
        [`${props.prefixCls}-filter-item-hover`]: hover,
        [`${props.prefixCls}-filter-item-focus`]: focus,
        [`${props.prefixCls}-filter-item-has-value`]: hasVal,
      })}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
    >
      <Form.Item name={name} style={{ marginBottom: 0 }}>
        {React.cloneElement(children, ({
          onChange,
          onFocus,
          onBlur,
        } as unknown) as Partial<CP>)}
      </Form.Item>
    </div>
  );
}
export default FilterItem;
