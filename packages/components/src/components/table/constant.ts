/**
 * 列宽
 */
export const DefaultColWidth = 160;

/**
 * 列宽
 */
 export const ActionColWidth = 100;

/**
 * 时间列宽
 */
export const DateTimeColWidth = 220;
/**
 * 城市列宽
 */
export const CityColWidth = 220;

/**
 * 空值时显示的文本
 */
export const EmptyText = '--';
