import {
  TableProps as AntdTableProps,
  ColumnType as AntdColumnType,
} from 'antd/es/table';
import { PayloadType } from '@parallel-line/utils';
import { PresetStatusColorType } from 'antd/es/_util/colors';
import {
  ColumnFilterItem as FilterItem,
  ColumnTitleProps,
} from 'antd/es/table/interface';
import { RenderedCell } from 'rc-table/lib/interface';
import { Key, ReactNode } from 'react';
import type {
  HistoryPushParams,
  HistoryProps,
} from '@parallel-line/common/es/utils';
import type { UnitTransformKey } from '@parallel-line/utils';

export type ColumnSummaryType = 'Total';
export type ColumnDataType =
  | 'Tag'
  | 'Date'
  | 'Link'
  | 'Number'
  | 'City'
  | 'Numbering';
export type UnitType = string;

export interface ColumnFilterItem extends Omit<FilterItem, ''> {
  value: Key;
  color?: string;
  status?: PresetStatusColorType;
}

export interface CommColumnType {
  /**
   * 单位是否呈现到表头，默认为true
   */
  unitInTitle?: boolean;
  /**
   * 在查询项中不展示此项
   */
  hideInSearch?: boolean;
}

export interface ColumnType
  extends CommColumnType,
    Omit<
      AntdColumnType<any>,
      'title' | 'width' | 'render' | 'summary' | 'dataIndex'
    > {
  key: string;
  /**
   * 列标题
   */
  title: string;
  /**
   * 列宽度
   */
  width?: number;
  /**
   * 列数据的类型
   */
  type?: ColumnDataType;
  /**
   * 链接的点击事件配置版
   */
  linkClick?: HistoryPushParams;
  /**
   * 列数据的值类型, 支持的值请查看文档：https://supermoonlmy.gitee.io/parallel-line/utils/unit-transform
   *
   * 当为数组时代表连续调用
   */
  valueType?: UnitTransformKey | UnitTransformKey[];
  datePayload?: PayloadType<any>;
  /**
   * 自定义类型的单位
   */
  unit?: UnitType;
  /**
   * @description 列数据在数据项中对应的路径，支持通过数组查询嵌套路径
   */
  dataIndex: string | string[];
  /**
   * @description 枚举类型在全局字典对应的路径，和getDic搭配使用，会自动转化把值当成 key 来取出要显示的内容
   */
  dicKey?: string;
  /**
   * @description 枚举类型的静态可选项，dicKey 和 options 二选一
   */
  options?: ColumnFilterItem[];
  /**
   * 查询表单的 props，会透传给表单项,如果渲染出来是 Input,就支持 input 的所有 props，同理如果是 select，也支持 select 的所有 props。
   */
  fieldProps?: Record<string, any>;
  /**
   * @description 子列
   */
  children?: ColumnType[];
  /**
   * 该列是否启用总结栏
   */
  summary?: true | ColumnSummaryType[];
  /**
   * @description 统计项的key，默认和 dataIndex 一致
   */
  summaryKey?: string;
}

interface ColumnRenderParams {
  text: ReactNode;
  record: any;
  index: number;
  dataSource?: readonly any[];
}

export type RenderColumnFn = (
  params: ColumnRenderParams,
  key: string,
  col: ColumnType,
) => ReactNode | RenderedCell<any>;

export type RenderFilterFn = (params: {
  key: string;
  col: ColumnType;
  node?: ReactNode;
  dataSource?: readonly any[];
  handleFilteredChange: () => void;
}) => ReactNode;

export type RenderColumnTitleFn = (
  props: ColumnTitleProps<any>,
  text: ReactNode,
  key: string,
  col: ColumnType,
) => React.ReactNode;

export interface ActionType {
  /**
   * 唯一标识，不定义时默认为 name 值
   */
  key?: string;
  name: string;
  /** 权限码, 请在运营端/菜单管理中获取 */
  code?: string;
  major?: string | boolean;
  hide?: string | boolean;
  click?: HistoryPushParams;
  /**
   * @description 筛选标识，和 Table.externalSign 一起使用
   */
  sign?: Record<string, any>[];
}

export interface TableInstance {
  container: HTMLDivElement | null;
}

export interface CommTableProps {
  /**
   * @deprecated
   */
  showFilter?: boolean;
  /**
   * @description 设置列默认配置
   */
  defaultColumnConfig?: CommColumnType;
  /**
   * 替换 Column，用于二次封装单元格
   */
  renderColumn?: RenderColumnFn;
  /**
   * 替换 ColumnTitle，用于二次封装单元格表头标题
   */
  renderTitle?: RenderColumnTitleFn;
  renderFilter?: RenderFilterFn;
  /**
   * Column 的 Link 类型的点击事件
   */
  onLinkClick?: (text: ReactNode, record: any, key: string) => void;
  /**
   * Action 的点击事件
   *
   * 当返回值为 ReactNode 时，表示为 替换 Action，可用于二次封装 Action
   *
   * 当 DefaultAction 为 undefined 时为点击事件
   */
  onActionClick?: (
    key: string,
    record: any,
    DefaultAction?: ReactNode,
  ) => ReactNode | void;
  /**
   * Action 的 隐藏时机规则 和 是否是重要操作规则
   *
   * 当 sign 不能够满足所需时，可通过该方法设置 Action 的呈现，若还不满足可通过 onActionClick 替换 Action
   *
   * @returns [隐藏, 重要] 返回一个长度为2 的布尔数组，第一个返回true表示为隐藏 Action，第二个返回true表示为重要操作
   */
  actionRule?: (key: string, record: any) => [boolean, boolean];
}

export interface TableTotalInfo {
  total: string | number;
}

export interface TableProps
  extends CommTableProps,
    Omit<HistoryProps, 'listenRoute'>,
    Omit<AntdTableProps<any>, 'size' | 'tableLayout'> {
  size?: 'small';
  filterIsEmpty?: boolean;
  hideAllFilter?: boolean;
  /**
   * @description 表格常规列配置项
   */
  columns?: ColumnType[];
  /**
   * @description 表格操作列配置项
   */
  actions?: ActionType[];
  actionConfig?: {
    majorNumber?: number;
    majorWidth?: number;
  };
  filteredValues?: Record<string, any>;
  onFilteredChange?: (key: string, value: any) => void;
  onCleanFiltered?: () => void;

  totalFail?: boolean;
  totalMap?: Record<string, TableTotalInfo>;
  totalLoading?: boolean;
  onTotalReload?: () => void;
}
