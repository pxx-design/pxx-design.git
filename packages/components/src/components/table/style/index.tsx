// style dependencies
import '../../input/style';
import '../../button/style';
import '../../select/style';
import '../../input-number/style';
import '../../city-select/style';
import '../../config-provider/style';

import './index.less';
