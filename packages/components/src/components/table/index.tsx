/* eslint-disable func-names */
/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-nested-ternary */
import React, { Key, useEffect, useState, useRef } from 'react';
import Table from 'antd/es/table';
import classnames from 'classnames';
import { useHistory, useMemoDeep } from '@parallel-line/hooks';
import { Form, Badge, Tooltip, Empty } from 'antd';
import { forwardRef, useImperativeHandle } from 'react';
import { Tag } from '@parallel-line/common';
import { isNumber, isNil, isFunction, isObject, uniqueId } from 'lodash-es';
import { calc, get, getDic, unitTransform, toTime } from '@parallel-line/utils';
import {
  SearchOutlined,
  DownOutlined,
  EllipsisOutlined,
} from '@parallel-line/icons';
import type { TableColumnProps } from 'antd';
import { RenderedCell } from 'rc-table/lib/interface';

import Input from '../input';
import Action from './Action';
import Button from '../button';
import Select from '../select';
import FilterItem from './FilterItem';
import CitySelect from '../city-select';
import InputNumber from '../input-number';
import DateRangePicker from './DateRangePicker';
import {
  devWarning,
  getPathValue,
  toRoute,
} from '@parallel-line/common/es/utils';
import {
  DateTimeColWidth,
  CityColWidth,
  ActionColWidth,
  EmptyText,
} from './constant';
import type {
  ColumnFilterItem,
  TableInstance,
  TableProps,
  ColumnType,
  ActionType,
} from './interface';

export type { TableInstance, TableProps, ColumnType, ActionType };

const { ColumnGroup, Column } = Table;

/**
 * 判断是否空数据
 * @param any
 * @returns
 */
const isEmpty = (any: any) => {
  try {
    if (isNumber(any)) return false;
    return isNil(any) || any === '';
  } catch (e) {
    return true;
  }
};

const InlineTable = forwardRef<TableInstance, TableProps>((props, ref) => {
  const {
    size,
    rowKey,
    rowSelection,
    totalMap = {},
    totalFail,
    totalLoading,
    dataSource = [],
    externalQueryDic = {},
    actions = [],
    columns = [],
    actionConfig = {},
    className,
    filterIsEmpty,
    hideAllFilter,
    prefixCls: pCls,
    filteredValues = {},
    defaultColumnConfig = {},
    renderColumn: renderColumnEvent,
    renderTitle,
    renderFilter,
    onFilteredChange = function () {},
    onCleanFiltered = function () {},
    onLinkClick = function () {},
    onActionClick = function () {},
    actionRule = () => [false, false],
    ...tableProps
  } = props;
  const prefixCls = `${pCls}-table`;
  const [form] = Form.useForm();
  const history = useHistory();
  const tableRef = useRef<HTMLDivElement>(null);
  // const emptyRef = useRef<HTMLDivElement>(null);
  const noData = useMemoDeep(() => !dataSource.length, [dataSource]);
  const firstColumnKey = useMemoDeep(() => columns[0]?.key, [columns]);
  const rowKeyFn = (record: any): Key => {
    let _key: Key = '';
    if (typeof rowKey === 'function') {
      try {
        _key = rowKey(record);
      } catch (error) {
        console.error(error);
        _key = uniqueId();
      }
    }

    if (typeof rowKey === 'string') {
      _key = record[rowKey];
    }
    if (!_key) {
      _key = uniqueId();
    }
    return _key;
  };

  if (defaultColumnConfig.unitInTitle == null) {
    defaultColumnConfig.unitInTitle = true;
  }
  if (tableProps.pagination) {
    tableProps.pagination.prefixCls = `${prefixCls}-pagination`;
    if (!tableProps.pagination?.simple) {
      const { current, pageSize } = tableProps.pagination;
      tableProps.pagination.size = 'default';
      tableProps.pagination.showTotal = (total) => {
        if (noData) return '共0条记录';
        return `共${total}条记录 第${current} / ${Math.ceil(
          calc(`${total} / ${pageSize}`),
        )}页`;
      };
    }
  }

  useImperativeHandle(ref, () => ({ container: tableRef.current }));

  useEffect(() => {
    if (filteredValues) {
      form.setFieldsValue(filteredValues);
    }
  }, [filteredValues]);
  // TODO 针对 CitySelect 没有清空事件的处理方式
  /** 城市筛选字段集合 */
  const filteredCitys = useMemoDeep(
    () =>
      columns
        .filter((column) => column.type === 'City')
        .map((column) => column.key),
    [columns],
  );
  const onValuesChange = (changeValues: any) => {
    Object.entries(changeValues).forEach(([key, value]) => {
      if (filteredCitys.includes(key) && !value) {
        onFilteredChange(key, value);
      }
    });
  };

  const renderColumn = ({ children, ...col }: ColumnType, index: number) => {
    const {
      unit,
      type,
      dicKey,
      sorter,
      fieldProps = {},
      hideInSearch = defaultColumnConfig.hideInSearch,
      unitInTitle = defaultColumnConfig.unitInTitle,
    } = col;
    let SearchTitle: React.ReactElement | null = null;
    const placeholder = `搜索${col.title}`;
    let title = col.title;
    const columnProps: TableColumnProps<any> = { ...col };
    columnProps.width = col.width;
    columnProps.fixed = col.fixed;

    if (noData) {
      columnProps.ellipsis = false;
    }

    if (unit && unitInTitle) title = `${title} (${unit})`;
    if (renderTitle) {
      columnProps.title = (titleProps) =>
        renderTitle(titleProps, title, col.key, col);
    } else columnProps.title = title;

    if (sorter) {
      columnProps.sorter = { multiple: index };
    }

    if (type === 'Date') {
      columnProps.width = columnProps.width || DateTimeColWidth;
    } else if (type === 'City') {
      columnProps.width = columnProps.width || CityColWidth;
    }

    if (!hideInSearch) {
      const handleFilteredChange = () => {
        const value = form.getFieldValue(col.key);
        onFilteredChange(col.key, value);
      };
      if (dicKey || col.options) {
        let list = col.options || [];
        if (dicKey) {
          list = getDic(dicKey).map(({ value, key, ...rest }) => ({
            text: value,
            value: key,
            ...rest,
          }));
        }
        SearchTitle = (
          <Select
            noWrap
            allowClear
            placeholder={placeholder}
            bordered={false}
            style={{ width: '100%' }}
            onChange={handleFilteredChange}
            filterOption={(inputValue, option) => {
              if (typeof option?.children === 'string') {
                return option?.children?.search(inputValue) >= 0;
              }
              return false;
            }}
            {...fieldProps}
          >
            {list.map((item) => (
              <Select.Option key={item.value} value={item.value}>
                {item.text}
              </Select.Option>
            ))}
          </Select>
        );
      } else {
        switch (type) {
          case 'Number':
            // TODO: 数值区间筛选
            break;
          case 'Numbering':
            SearchTitle = (
              <InputNumber
                hideStep
                selectInFocus
                allowClear={false}
                bordered={false}
                placeholder={placeholder}
                suffix={<SearchOutlined onClick={handleFilteredChange} />}
                onClose={handleFilteredChange}
                onPressEnter={handleFilteredChange}
                {...fieldProps}
              />
            );
            break;
          case 'City':
            SearchTitle = (
              <CitySelect
                placeholder={placeholder}
                style={{ width: '100%' }}
                bordered={false}
                allowClear
                suffixIcon={<DownOutlined />}
                onPopupVisibleChange={(popupVisible) =>
                  !popupVisible && handleFilteredChange()
                }
                {...fieldProps}
              />
            );
            break;
          case 'Date':
            SearchTitle = (
              <DateRangePicker
                bordered={false}
                onChange={() =>
                  onFilteredChange(col.key, form.getFieldValue(col.key) || [])
                }
                {...fieldProps}
              />
            );
            break;
          default:
            SearchTitle = (
              <Input
                selectInFocus
                bordered={false}
                allowClear={false}
                placeholder={placeholder}
                suffix={<SearchOutlined onClick={handleFilteredChange} />}
                onClose={handleFilteredChange}
                onPressEnter={handleFilteredChange}
                {...fieldProps}
              />
            );
            break;
        }
      }

      if (isFunction(renderFilter)) {
        const node = renderFilter({
          key: col.key,
          col,
          dataSource,
          node: SearchTitle,
          handleFilteredChange,
        });
        SearchTitle = React.isValidElement(node) ? node : SearchTitle;
      }
    }

    if (SearchTitle) {
      SearchTitle = (
        <FilterItem prefixCls={prefixCls} name={col.key}>
          {SearchTitle}
        </FilterItem>
      );
    } else {
      SearchTitle = <div className={`${prefixCls}-filter-empty-item`}></div>;
    }

    if (hideAllFilter || filterIsEmpty)
      return (
        <Column
          {...columnProps}
          key={col.key}
          render={(text, record, inx) => columnRender(record, inx, col)}
        />
      );

    return (
      <ColumnGroup {...columnProps} key={col.key}>
        <Column
          {...columnProps}
          key={col.key}
          title={SearchTitle}
          sorter={false}
          filtered={false}
          className={`${prefixCls}-search`}
          onCell={() => {
            return {
              style: {
                backgroundColor: `var(--cell-background-${index}, var(--cell-background))`,
              },
            };
          }}
          render={(text, record, inx) => columnRender(record, inx, col)}
        />
      </ColumnGroup>
    );
  };

  const columnRender = (
    record: any,
    index: number,
    col: ColumnType,
  ): React.ReactNode | RenderedCell<any> => {
    if (noData) {
      if (col.key === firstColumnKey) {
        let colSpan = columns.length + 1;

        if (!filterIsEmpty || actions.length > 0) colSpan += 1;
        // TODO 具备选中行时需考虑怎么呈现

        return {
          props: { colSpan, className },
          children: (
            <div
              className="ant-table-expanded-row-fixed"
              style={{
                width: tableRef.current?.clientWidth,
                position: 'sticky',
                left: 0,
                overflow: 'hidden',
              }}
            >
              {props.locale?.emptyText || (
                <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
              )}
            </div>
          ),
        };
      }
      return { props: { colSpan: 0 } };
    }
    const key = col.dataIndex || col.key;
    let text: any = getPathValue(record, key);

    text = columnTextRender(text, col);

    if (text === EmptyText) {
      text = columnEllipsisRender(text, col);
      if (isFunction(renderColumnEvent)) {
        return renderColumnEvent(
          { text, record, index, dataSource },
          col.key,
          col,
        );
      }
      return text;
    }

    if (col.type === 'Link') {
      return (
        <Button
          type="link"
          size="small"
          style={{ padding: 0 }}
          onClick={() => {
            if (col.linkClick) {
              const mergedQueryDic = { ...externalQueryDic, record };
              toRoute(history, mergedQueryDic, col.linkClick);
            } else onLinkClick(text, record, col.key);
          }}
        >
          {text}
        </Button>
      );
    }

    if (isFunction(renderColumnEvent)) {
      text = renderColumnEvent(
        { text, record, index, dataSource },
        col.key,
        col,
      );

      // TODO 临时判断是否是 RenderedCell 类型
      if (
        text &&
        isObject(text) &&
        (text as any).props &&
        !(text as any).type
      ) {
        return {
          props: (text as any).props,
          children: columnEllipsisRender((text as any).children, col),
        };
      }
    }

    return columnEllipsisRender(text, col);
  };

  const columnEllipsisRender = (
    text: any,
    col: ColumnType,
  ): React.ReactNode | RenderedCell<any> => {
    const width = col.width
      ? typeof col.width === 'number'
        ? col.width + 'px'
        : col.width
      : undefined;
    const style: React.CSSProperties = {
      width: col.width ? `calc(${width} - 24px)` : undefined,
      maxWidth: width,
      color: 'inherit',
      padding: 0,
      margin: 0,
      textAlign: col.align || 'left',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    };
    // if (col.ellipsis) {
    //   if (['number', 'string'].includes(typeof text)) {
    //     text = (
    //       <Typography.Paragraph
    //         style={style}
    //         ellipsis={{ rows: 1, tooltip: true }}
    //       >
    //         {`${text}`}
    //       </Typography.Paragraph>
    //     );
    //   }
    // }
    if (col.ellipsis && text !== EmptyText) {
      if (['number', 'string'].includes(typeof text)) {
        return (
          <Tooltip title={text}>
            <div style={style}>{text}</div>
          </Tooltip>
        );
      }
    }

    return <div style={style}>{text}</div>;
  };

  const columnTextRender = (
    text: any,
    col: ColumnType,
    ellipsisFn?: (
      text: any,
      col: ColumnType,
    ) => React.ReactNode | RenderedCell<any>,
  ): React.ReactNode | RenderedCell<any> => {
    const { type, valueType, unit, unitInTitle = true } = col;
    let dicItem: ColumnFilterItem | undefined;

    if (isEmpty(text)) {
      return EmptyText;
    }

    if (getDic && col.dicKey) {
      const D = getDic(col.dicKey, text);
      dicItem = { ...D, text: D.value, value: D.key };
      text = dicItem?.text;
    } else if (col.options && col.options.length > 0) {
      dicItem = col.options.find((option) => option.value === text);
      if (dicItem) text = dicItem.text;
    }

    if (valueType) {
      if (valueType instanceof Array) {
        valueType.forEach((key) => {
          text = unitTransform[key]
            ? unitTransform[key]?.(text, { errorResult: EmptyText })
            : text;
        });
      } else {
        text = unitTransform[valueType]
          ? unitTransform[valueType]?.(text, { errorResult: EmptyText })
          : text;
      }
    } else if (type === 'Date') {
      text = toTime(text, col.datePayload);
    }

    if (unit && !unitInTitle) {
      text = `${text} ${unit}`;
    }

    if (dicItem) {
      if (dicItem.status || dicItem.color) {
        text =
          type === 'Tag' ? (
            <Tag color={dicItem.status || dicItem.color}>{text}</Tag>
          ) : (
            <Badge
              status={dicItem.status}
              color={dicItem.color}
              text={text}
            ></Badge>
          );
        return text;
      }
    }

    if (ellipsisFn) text = ellipsisFn(text, col);

    // TODO： Highlighter 和 Paragraph 混合会导致 省略号异常
    // text =
    //   state.searchedColumn === col.key ? (
    //     <Highlighter
    //       highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
    //       searchWords={[state.searchText]}
    //       autoEscape
    //       textToHighlight={text ? text.toString() : ''}
    //     />
    //   ) : (
    //     text
    //   );

    return text;
  };

  const actionColumnProps: Omit<ColumnType, 'children'> = {
    title: '操作',
    key: '_action',
    dataIndex: '_action',
    fixed: 'right',
    width: noData ? ActionColWidth : undefined,
  };
  const renderActionCol = (_: any, record: any, index: number) => {
    if (noData) return { props: { colSpan: 0 } };
    const text = (
      <Action
        prefixCls={prefixCls}
        record={record}
        actions={actions}
        {...actionConfig}
        queryDic={{ ...externalQueryDic, record }}
        actionRule={actionRule}
        onActionClick={onActionClick}
      />
    );
    if (renderColumnEvent) {
      if (isFunction(renderColumnEvent)) {
        return renderColumnEvent(
          { text, record, index, dataSource },
          actionColumnProps.key,
          actionColumnProps,
        );
      }
    }
    return text;
  };

  const onReset = () => {
    form.resetFields();
    onCleanFiltered();
  };
  const renderReSetButton = () => (
    <Button type="link" size="small" style={{ padding: 0 }} onClick={onReset}>
      重置筛选
    </Button>
  );

  const renderActionColumn = () => {
    if (props.showFilter === false || filterIsEmpty)
      return actions.length ? (
        <Column {...actionColumnProps} render={renderActionCol} />
      ) : null;
    return (
      <ColumnGroup {...actionColumnProps}>
        <Column
          {...actionColumnProps}
          title={renderReSetButton}
          render={renderActionCol}
          className={`${prefixCls}-search-right`}
        />
      </ColumnGroup>
    );
  };

  // TODO 后面需要调整渲染调用频率
  const render = () => {
    const tmpColumns = [...columns];
    const existSummary = tmpColumns.find((column) => !!column.summary);
    if (existSummary) {
      tmpColumns[0].fixed = true;
    }

    return (
      <>
        {tmpColumns.map(renderColumn)}
        {!hideAllFilter && renderActionColumn()}
      </>
    );
  };

  const renderSummary = useMemoDeep(() => {
    const [first, ...restColumns] = columns;
    devWarning(!first?.summary, 'Table', '第一列不能设置为统计栏');
    const summaryColumns = restColumns.filter((column) => !!column.summary);
    if (!summaryColumns.length) return undefined;
    if (noData) return undefined;
    return (_: readonly any[]) => {
      const hasSelection = !!rowSelection;
      const carry = hasSelection ? 2 : 1;

      return (
        <Table.Summary fixed>
          <Table.Summary.Row>
            {hasSelection && (
              <Table.Summary.Cell index={0}></Table.Summary.Cell>
            )}
            <Table.Summary.Cell index={hasSelection ? 1 : 0}>
              <div>
                {rowSelection?.selectedRowKeys?.length
                  ? `${rowSelection?.selectedRowKeys?.length}条数据合计`
                  : '总计'}
              </div>
            </Table.Summary.Cell>
            {totalFail ? (
              <Table.Summary.Cell
                index={hasSelection ? 2 : 1}
                colSpan={restColumns.length}
              >
                <div>
                  统计数据加载失败，
                  <a onClick={props.onTotalReload}>重新加载</a>
                </div>
              </Table.Summary.Cell>
            ) : (
              restColumns.map((column, index) => {
                const inx = index + carry;
                const colInx = index + 1;
                const key = column.summaryKey ?? column.key;
                const cellProps: any = { index: inx, key: inx };
                const cellDivProps: any = {};
                const hasTotal = !isEmpty(totalMap[key]);
                let children: React.ReactNode = null;
                if (totalLoading) {
                  children = <EllipsisOutlined />;
                } else if (hasTotal) {
                  children = columnTextRender(
                    totalMap[key].total,
                    column,
                    columnEllipsisRender,
                  );
                  cellProps.className = `${prefixCls}-total-cell`;
                  cellDivProps.onMouseOut = (
                    e: React.MouseEvent<HTMLDivElement, MouseEvent>,
                  ) => {
                    e.stopPropagation();
                    if (tableRef.current) {
                      tableRef.current.style.setProperty(
                        `--cell-background-${colInx}`,
                        'unset',
                      );
                    }
                  };
                  cellDivProps.onMouseOver = (
                    e: React.MouseEvent<HTMLDivElement, MouseEvent>,
                  ) => {
                    e.stopPropagation();
                    if (tableRef.current) {
                      tableRef.current.style.setProperty(
                        `--cell-background-${colInx}`,
                        'var(--cell-hover-background)',
                      );
                    }
                  };
                }
                return (
                  <Table.Summary.Cell {...cellProps}>
                    <div {...cellDivProps}>{children}</div>
                  </Table.Summary.Cell>
                );
              })
            )}
            {actions.length !== 0 && (
              <Table.Summary.Cell index={restColumns.length + carry} />
            )}
          </Table.Summary.Row>
        </Table.Summary>
      );
    };
    // TODO： 需要注意 columnTextRender 方法所引入的变量。
  }, [columns, noData, rowSelection, totalMap, totalFail, totalLoading]);

  return (
    <Form
      form={form}
      style={{ height: '100%' }}
      autoComplete="off"
      onValuesChange={onValuesChange}
    >
      <div
        ref={tableRef}
        className={classnames(
          `${prefixCls}-wrapper`,
          size && `${prefixCls}-${size}-wrapper`,
          { [`${prefixCls}-wrapper-has-filter`]: !filterIsEmpty },
        )}
      >
        <Table
          prefixCls={`${pCls}-inline-table`}
          className={classnames(prefixCls, className)}
          summary={renderSummary}
          tableLayout="auto"
          // TODO 通过在空数据下添加长度为1的数组消除表头异常
          // TODO 自定义空数据会出现改问题：ID1001322。
          // dataSource={noData ? [{ id: '0' }] : dataSource}
          dataSource={dataSource}
          rowSelection={noData ? undefined : rowSelection}
          {...tableProps}
          expandable={noData ? undefined : tableProps.expandable}
          rowKey={rowKeyFn}
          size="small"
        >
          {render()}
        </Table>
      </div>
    </Form>
  );
});

InlineTable.defaultProps = {
  prefixCls: 'pxx',
  rowKey: 'id',
  columns: [],
  dataSource: [],
  onChange: () => {},
};

export default InlineTable;
