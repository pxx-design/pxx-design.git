/* eslint-disable no-new-func */
import React from 'react';
import type { ReactNode } from 'react';
import classNames from 'classnames';
import { DownFill } from '@parallel-line/icons';
import { Menu, Dropdown } from 'antd';
import { useHistory } from '@parallel-line/hooks';
import { toRoute } from '@parallel-line/common/es/utils';
import { useAccess as getAccess } from '@parallel-line/hooks';
import Button from '../button';
import type { ActionType } from './interface';

interface ActionProps {
  prefixCls: string;
  majorNumber?: number;
  majorWidth?: number;
  actions: ActionType[];
  record: any;
  queryDic: Record<string, any>;
  actionRule: (key: string, record: any) => [boolean, boolean];
  onActionClick: (
    key: string,
    record: any,
    DefaultAction?: ReactNode,
  ) => ReactNode | void;
}

const Action: React.FC<ActionProps> = ({
  prefixCls,
  actions,
  queryDic,
  record,
  majorWidth,
  majorNumber = 4,
  actionRule,
  onActionClick,
}) => {
  let majorActions: ActionType[] = [];
  let notMajorActions: ActionType[] = [];
  const history = useHistory();

  const handleActionClick = (action: ActionType) => {
    if (action.click) {
      toRoute(history, queryDic, action.click);
    } else onActionClick(action.key || action.name, record);
  };
  const renderActionEvent = (
    action: ActionType,
    DefaultAction: React.ReactNode,
  ): React.ReactNode => {
    const node: any = onActionClick(
      action.key || action.name,
      record,
      DefaultAction,
    );
    return React.isValidElement(node) ? node : DefaultAction;
  };
  const internalActionRule = (action: ActionType) => {
    let hide: boolean | undefined;
    let major: boolean | undefined;
    const key = action.key || action.name;

    const accessible = getAccess(action.code);
    if (!accessible) return;

    if (action.hide == null && action.major == null) {
      [hide, major] = actionRule(key, record);
    } else {
      if (action.hide != null) {
        if (typeof action.hide === 'string') {
          const fn = new Function(
            'record',
            action.hide.trim().indexOf('return') === 0
              ? action.hide
              : 'return ' + action.hide,
          );

          hide = fn(record);
        } else {
          hide = action.hide;
        }
      }
      // if (action.major != null) {
      //   if (typeof action.major === 'string') {
      //     const fn = new Function(
      //       'record',
      //       action.major.trim().indexOf('return') === 0
      //         ? action.major
      //         : 'return ' + action.major,
      //     );

      //     major = fn(record);
      //   } else {
      //     major = action.major;
      //   }
      // }
    }

    if (hide) return;

    // if (major) {
    //   majorActions.push(action);
    // } else {
    //   notMajorActions.push(action);
    // }
    majorActions.push(action);
  };

  actions.forEach(internalActionRule);

  if (majorActions.length > 3) {
    notMajorActions = majorActions.splice(2);
  }

  if ([...majorActions, ...notMajorActions].length === 0)
    return <div className={`${prefixCls}-actions`} />;

  const renderAction = (action: ActionType, key: number) =>
    renderActionEvent(
      action,
      <Button
        type="link"
        size="small"
        style={{ padding: 0 }}
        key={key}
        onClick={() => handleActionClick(action)}
      >
        {action.name}
      </Button>,
    );

  if (majorActions.length === 0) {
    const [first, ...restActions] = notMajorActions;
    majorActions = [first];
    notMajorActions = [...restActions];
  }

  const majorClassName = classNames(`${prefixCls}-actions-major`, {
    [`${prefixCls}-actions-major-${majorNumber}`]: !majorWidth,
  });

  if (notMajorActions.length === 0)
    return (
      <div className={`${prefixCls}-actions`}>
        <div className={majorClassName} style={{ width: majorWidth }}>
          {majorActions.map(renderAction)}
        </div>
      </div>
    );

  const menu = (
    <Menu>
      {notMajorActions
        .map((action: ActionType, key: number) => {
          return renderActionEvent(
            action,
            <Menu.Item key={key} onClick={() => handleActionClick(action)}>
              <Button type="link" size="small">
                {action.name}
              </Button>
            </Menu.Item>,
          );
        })
        .filter((a) => !!a)}
    </Menu>
  );
  return (
    <div className={`${prefixCls}-actions`}>
      <div className={majorClassName}>{majorActions.map(renderAction)}</div>
      <Dropdown overlay={menu}>
        <Button
          className={`${prefixCls}-actions-more`}
          type="link"
          size="small"
        >
          更多
          <DownFill />
        </Button>
      </Dropdown>
    </div>
  );
};
export default Action;
