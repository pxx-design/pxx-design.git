/**
 * title: 基本使用
 * desc: 基本使用。
 */

import React from 'react';
import { CitySelect } from '@parallel-line/components';

export default () => (
  <>
    <CitySelect placeholder='Basic usage' />
  </>
);
