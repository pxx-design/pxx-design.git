---
title: CitySelect 城市选择器
group:
  title: CitySelect 城市选择器
  order: 14
---

# CitySelect 城市选择器

城市下拉选择器。

## 何时使用

- 弹出一个下拉菜单给用户选择城市操作。

## 代码演示

### 基本使用

<code src="./demos/basic.tsx"></code>

<API></API>