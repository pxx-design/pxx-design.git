import React, {
  useState,
  useEffect,
  useMemo,
  useRef,
  useImperativeHandle,
} from 'react';
import { Cascader } from 'antd';
import { isArray, last } from 'lodash-es';
import { getCitysCode, global } from '@parallel-line/utils';
import type {
  CascaderValueType,
  CascaderOptionType,
  CascaderProps,
} from 'antd/es/cascader';

export interface CitySelectProps
  extends Omit<CascaderProps, 'options' | 'value' | 'onChange'> {
  level?: 'province' | 'city' | 'district';
  value?: React.Key;
  onChange?: (value?: React.Key) => void;
  /**
   * 可选城市列表数据源，默认 global.countryTree
   */
  options?: CascaderOptionType[];
}

interface CitySelectInstance {
  focus(): void;
  blur(): void;
}

const levelMap = ['province', 'city', 'district'];

const CitySelect = React.forwardRef<CitySelectInstance, CitySelectProps>(
  (
    {
      options = global.countryTree,
      fieldNames,
      value: val,
      level = 'district',
      onChange,
      ...props
    },
    ref,
  ) => {
    const cascaderRef = useRef<Cascader>(null);
    const { children: key = 'districts', label = 'name', value: v = 'adcode' } =
      fieldNames || {};
    const [value, setValue] = useState<CascaderValueType>(getCitysCode(val));

    useEffect(() => {
      setValue(getCitysCode(val));
    }, [val]);

    function filter(inputValue: string, path: any[]) {
      return path.some(
        (option) =>
          option?.[label]
            ?.toLowerCase?.()
            .indexOf?.(inputValue?.toLowerCase?.()) > -1,
      );
    }

    const optionsFormat = useMemo(() => {
      if (!level || level === 'district') return options;
      const citycodes = ["021", "023", "022", "010"];
      const index = levelMap.indexOf(level);
      const levelArray = levelMap.slice(0, index);
      const loop = (data: any) =>
        data.map((t: any) => {
          if (!levelArray.includes(t.level))
            return {
              ...t,
              [key]: undefined,
            };
          return {
            ...t,
            [key]:
              isArray(t[key]) && t[key].length > 0 && !citycodes.includes(t.citycode) ? loop(t[key]) : undefined,
          };
        });
      return loop(options);
    }, [options, level]);

    useImperativeHandle(ref, () => ({
      focus: () => {
        cascaderRef.current?.focus();
      },
      blur: () => {
        cascaderRef.current?.blur();
      },
    }));

    const handleChange = (value: CascaderValueType) => {
      setValue(value);

      onChange?.(last(value));
    };

    return (
      <Cascader
        ref={cascaderRef}
        changeOnSelect
        value={value}
        onChange={handleChange}
        showSearch={{ filter }}
        fieldNames={{ children: key, value: v, label }}
        options={optionsFormat}
        {...props}
      />
    );
  },
);

export default CitySelect;
