import { Key } from 'react';

/**
 * 根据adcode规律返回省市区数组
 * @param adcode
 */
export const getCitysCode = (adcode?: string | number): Key[] => {
  if (!adcode) return [];
  const val = String(adcode);
  if (+val.slice(2, 6) === 0) {
    // 当前是省级
    return [adcode];
  }
  if (+val.slice(4, 6) === 0) {
    // 当前是市级
    return [val.slice(0, 2).padEnd(6, '0'), adcode];
  }
  const slice2 = +val.slice(0, 2);
  if (slice2 === 11 || slice2 === 12 || slice2 === 50 || slice2 === 31) {
    // 直辖市
    return [val.slice(0, 2).padEnd(6, '0'), adcode];
  }
  return [
    val.slice(0, 2).padEnd(6, '0'),
    val.slice(0, 4).padEnd(6, '0'),
    adcode,
  ];
};
