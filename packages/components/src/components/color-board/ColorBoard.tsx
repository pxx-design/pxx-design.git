import React from 'react';
import { Dropdown, Popover } from 'antd';
import { SketchPicker, SketchPickerProps } from 'react-color';
import { useLocalStorageState, useBoolean, useControllableValue } from 'ahooks';
import { MoreArrowOutlined } from '@parallel-line/icons';
import { useNamespace } from '@parallel-line/common/es/hooks';
import Button from '../button';

export interface ColorBoardProps {
  prefixCls?: string;
  defaultColor?: string;
  currentColor?: string;
  onChange?(currentColor: string): void;
}

export interface ColorBoardPickerProps extends ColorBoardProps {
  visible?: boolean;
  onVisibleChange?: ((visible: boolean) => void) | undefined;
}

interface ColorPickerProps extends Omit<SketchPickerProps, 'presetColors'> {
  className?: string;
  onComplete?: (
    color: string,
    event: React.MouseEvent<HTMLElement, MouseEvent>,
  ) => void;
}

const DEFAULTCOLOR = 'rgb(64, 64, 64)';
const defaultAlpha = 1;

interface ColorBoardItem {
  title: string;
  color: string;
  border?: boolean;
}

const decimalToHex = (alpha: number) => {
  let aHex = Math.round(255 * alpha).toString(16);
  return alpha === 0 ? '00' : aHex.length < 2 ? `0${aHex}` : aHex;
};

// TODO react-color 固定版本号 2.17.2
// https://github.com/casesandberg/react-color/pull/611 因为这个后续版本直接删除了 e.preventDefault() 导致失焦
// 后续应该提个PR，把 e.preventDefault() 设置为可选
function ColorPicker(props: ColorPickerProps) {
  const [moreColor, setMoreColor] = React.useState('');
  const [color, onChange] = useControllableValue(props, {
    valuePropName: 'color',
    trigger: 'onChange',
  });
  const handleChange: SketchPickerProps['onChange'] = (color) => {
    const hexCode = `${color.hex}${decimalToHex(
      color?.rgb?.a ?? defaultAlpha,
    )}`;
    onChange(hexCode);
  };
  const handleColorComplete: SketchPickerProps['onChangeComplete'] = (
    color,
  ) => {
    const { r, g, b, a } = color.rgb;
    setMoreColor(`rgba(${r},${g},${b},${a})`);
  };
  const handleComplete = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    event.preventDefault();
    props.onComplete?.(moreColor, event);
  };
  return (
    <div className={props.className}>
      <SketchPicker
        width="250px"
        {...props}
        presetColors={[]}
        color={color}
        onChange={handleChange}
        onChangeComplete={handleColorComplete}
      />
      <Button size="small" type="primary" onMouseDown={handleComplete}>
        确定
      </Button>
    </div>
  );
}

function ColorBoard(props: ColorBoardProps) {
  const { defaultColor = DEFAULTCOLOR } = props;
  const [, bem] = useNamespace('color-board', props.prefixCls);
  const [visible, { toggle }] = useBoolean();
  const [currentColor, onChange] = useControllableValue(props, {
    defaultValuePropName: 'defaultColor',
    valuePropName: 'currentColor',
    trigger: 'onChange',
  });
  const [colors, setColors] = useLocalStorageState<string[]>(
    bem('history'),
    [],
  );

  const handleColorComplete: ColorPickerProps['onComplete'] = (color) => {
    setTimeout(() => {
      toggle(false);
      onChange(color);
    }, 0);
    if (!color) return;
    if (colors.includes(color)) return;
    const newColors = [color, ...colors];
    if (newColors.length > 10) newColors.splice(10);
    setColors(newColors);
  };
  const handleChange = (
    currentColor: string,
    e: React.MouseEvent<HTMLSpanElement, MouseEvent>,
  ) => {
    e.preventDefault();
    onChange(currentColor);
  };

  const renderItem = ({ border, color }: ColorBoardItem, index: number) => {
    return (
      <span
        key={index}
        className={bem('group-item', { border })}
        onMouseDown={(e) => handleChange(color, e)}
      >
        <span style={{ backgroundColor: color }}>
          <Icon show={color === currentColor} />
        </span>
      </span>
    );
  };
  return (
    <div className={bem()}>
      <div
        className={bem('default')}
        onMouseDown={(e) => handleChange(defaultColor, e)}
      >
        <span className={bem('group-item')}>
          <span style={{ backgroundColor: defaultColor }}>
            <Icon />
          </span>
        </span>
        <span style={{ marginLeft: 8, lineHeight: 2 }}>默认</span>
      </div>
      {ColorBoardData.map((list, inx) => {
        return (
          <span key={inx} className={bem('group')}>
            {list.map(renderItem)}
          </span>
        );
      })}
      <div className={bem('history')}>最近使用自定义颜色</div>
      {colors.length === 0 ? (
        <div className={bem('none-custom-color')}>暂无</div>
      ) : (
        <span className={bem('group')}>
          {colors
            .map((color) => ({ title: color, color } as ColorBoardItem))
            .map(renderItem)}
        </span>
      )}

      <Popover
        placement="rightBottom"
        trigger={['click']}
        visible={visible}
        onVisibleChange={toggle}
        content={
          <ColorPicker
            className={bem('color-picker')}
            onComplete={handleColorComplete}
          />
        }
        overlayClassName={bem('more-color--popover')}
        getPopupContainer={(triggerNode) =>
          triggerNode.parentElement || document.body
        }
      >
        <div
          className={bem('more-color')}
          onClick={(e) => e.preventDefault()}
          onMouseDown={(e) => e.preventDefault()}
        >
          <div className={bem('more-color--container')}>
            <div className={bem('more-color--preview')}></div>
            <span>更多颜色</span>
            <MoreArrowOutlined className={bem('more-color--icon')} />
          </div>
        </div>
      </Popover>
    </div>
  );
}

ColorBoard.defaultProps = {
  defaultColor: DEFAULTCOLOR,
};

export function ColorBoardPicker(
  props: React.PropsWithChildren<ColorBoardPickerProps>,
) {
  const { ...colorBoardProps } = props;
  const [, bem] = useNamespace('color-board', props.prefixCls);
  const [visible, toggle] = useControllableValue<boolean>(props, {
    defaultValuePropName: 'defaultVisible',
    valuePropName: 'visible',
    trigger: 'onVisibleChange',
  });
  const [currentColor, onChange] = useControllableValue<string>(props, {
    defaultValuePropName: 'defaultColor',
    valuePropName: 'currentColor',
    trigger: 'onChange',
  });
  const handleChange = (currentColor: string) => {
    onChange(currentColor);
    toggle(false);
  };
  return (
    <Dropdown
      placement="bottomLeft"
      trigger={['click']}
      visible={visible}
      onVisibleChange={toggle}
      overlay={
        <div className={bem('overlay')}>
          <ColorBoard
            {...colorBoardProps}
            currentColor={currentColor}
            onChange={handleChange}
          />
        </div>
      }
    >
      {props.children}
    </Dropdown>
  );
}

ColorBoardPicker.defaultProps = {
  defaultColor: DEFAULTCOLOR,
};

function Icon({ show }: { show?: boolean }) {
  return (
    <svg
      viewBox="0 0 18 18"
      style={{ fill: 'rgb(255, 255, 255)', display: show ? 'block' : 'none' }}
    >
      <path d="M21,7L9,19L3.5,13.5L4.91,12.09L9,16.17L19.59,5.59L21,7Z"></path>
    </svg>
  );
}

const ColorBoardData: ColorBoardItem[][] = [
  [
    { title: '黑色', color: 'rgb(0, 0, 0)' },
    { title: '深灰 3', color: 'rgb(38, 38, 38)' },
    { title: '深灰 2', color: 'rgb(89, 89, 89)' },
    { title: '深灰 1', color: 'rgb(140, 140, 140)' },
    { title: '灰色', color: 'rgb(191, 191, 191)' },
    { title: '浅灰 4', color: 'rgb(217,217,217)' },
    { title: '浅灰 3', color: 'rgb(233, 233, 233)' },
    { title: '浅灰 2', color: 'rgb(245, 245, 245)' },
    { title: '浅灰 1', color: 'rgb(250, 250, 250)', border: true },
    { title: '白色', color: 'rgb(255, 255, 255)', border: true },
  ],
  [
    { title: '红色', color: 'rgb(245, 34, 45)' },
    { title: '朱红', color: 'rgb(250, 84, 28)' },
    { title: '橙色', color: 'rgb(250, 140, 22)' },
    { title: '黄色', color: 'rgb(250, 219, 20)' },
    { title: '绿色', color: 'rgb(82, 196, 26)' },
    { title: '青色', color: 'rgb(19, 194, 194)' },
    { title: '浅蓝', color: 'rgb(24, 144, 255)' },
    { title: '蓝色', color: 'rgb(47, 84, 235)' },
    { title: '紫色', color: 'rgb(114, 46, 209)' },
    { title: '玫红', color: 'rgb(235, 47, 150)' },
  ],
  [
    { title: '红色 1', color: 'rgb(255, 232, 230)' },
    { title: '朱红 1', color: 'rgb(255, 236, 224)' },
    { title: '橙色 1', color: 'rgb(255, 239, 209)' },
    { title: '黄色 1', color: 'rgb(252, 252, 202)' },
    { title: '绿色 1', color: 'rgb(228, 247, 210)' },
    { title: '青色 1', color: 'rgb(211, 245, 240)' },
    { title: '浅蓝 1', color: 'rgb(212, 238, 252)' },
    { title: '蓝色 1', color: 'rgb(222, 232, 252)' },
    { title: '紫色 1', color: 'rgb(239, 225, 250)' },
    { title: '玫红 1', color: 'rgb(250, 225, 235)' },
  ],
  [
    { title: '红色 2', color: 'rgb(255, 163, 158)' },
    { title: '朱红 2', color: 'rgb(255, 187, 150)' },
    { title: '橙色 2', color: 'rgb(255, 213, 145)' },
    { title: '黄色 2', color: 'rgb(255, 251, 143)' },
    { title: '绿色 2', color: 'rgb(183, 235, 143)' },
    { title: '青色 2', color: 'rgb(135, 232, 222)' },
    { title: '浅蓝 2', color: 'rgb(145, 213, 255)' },
    { title: '蓝色 2', color: 'rgb(173, 198, 255)' },
    { title: '紫色 2', color: 'rgb(211, 173, 247)' },
    { title: '玫红 2', color: 'rgb(255, 173, 210)' },
  ],
  [
    { title: '红色 3', color: 'rgb(255, 77, 79)' },
    { title: '朱红 3', color: 'rgb(255, 122, 69)' },
    { title: '橙色 3', color: 'rgb(255, 169, 64)' },
    { title: '黄色 3', color: 'rgb(255, 236, 61)' },
    { title: '绿色 3', color: 'rgb(115, 209, 61)' },
    { title: '青色 3', color: 'rgb(54, 207, 201)' },
    { title: '浅蓝 3', color: 'rgb(64, 169, 255)' },
    { title: '蓝色 3', color: 'rgb(89, 126, 247)' },
    { title: '紫色 3', color: 'rgb(146, 84, 222)' },
    { title: '玫红 3', color: 'rgb(247, 89, 171)' },
  ],
  [
    { title: '红色 4', color: 'rgb(207, 19, 34)' },
    { title: '朱红 4', color: 'rgb(212, 56, 13)' },
    { title: '橙色 4', color: 'rgb(212, 107, 8)' },
    { title: '黄色 4', color: 'rgb(212, 177, 6)' },
    { title: '绿色 4', color: 'rgb(56, 158, 13)' },
    { title: '青色 4', color: 'rgb(8, 151, 156)' },
    { title: '浅蓝 4', color: 'rgb(9, 109, 217)' },
    { title: '蓝色 4', color: 'rgb(29, 57, 196)' },
    { title: '紫色 4', color: 'rgb(83, 29, 171)' },
    { title: '玫红 4', color: 'rgb(196, 29, 127)' },
  ],
  [
    { title: '红色 5', color: 'rgb(130, 0, 20)' },
    { title: '朱红 5', color: 'rgb(135, 20, 0)' },
    { title: '橙色 5', color: 'rgb(135, 56, 0)' },
    { title: '黄色 5', color: 'rgb(97, 71, 0)' },
    { title: '绿色 5', color: 'rgb(19, 82, 0)' },
    { title: '青色 5', color: 'rgb(0, 71, 79)' },
    { title: '浅蓝 5', color: 'rgb(0, 58, 140)' },
    { title: '蓝色 5', color: 'rgb(6, 17, 120)' },
    { title: '紫色 5', color: 'rgb(34, 7, 94)' },
    { title: '玫红 5', color: 'rgb(120, 6, 80)' },
  ],
];

export default ColorBoard;
