import InlineColorBoard, {
  ColorBoardPicker,
  ColorBoardPickerProps,
  ColorBoardProps,
} from './ColorBoard';

export type { ColorBoardPickerProps, ColorBoardProps };

type ColorBoardType = typeof InlineColorBoard;

interface ColorBoardInterface extends ColorBoardType {
  Picker: typeof ColorBoardPicker;
}

const ColorBoard = InlineColorBoard as ColorBoardInterface;
ColorBoard.Picker = ColorBoardPicker;

export default ColorBoard;
