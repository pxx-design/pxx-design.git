import React from 'react';
import classNames from 'classnames';
import ConfigProvider from '../config-provider';

interface WhiteBoardProps {
  prefixCls?: string;
  className?: string;
  style?: React.CSSProperties;
  children: React.ReactNode;
}

const WhiteBoard: React.FC<WhiteBoardProps> & {
  TopBoard: React.FC<TopBoardProps>;
  BottomBoard: React.FC<BottomBoardProps>;
} = (props) => {
  const { getPrefixCls } = React.useContext(ConfigProvider.Context);
  const prefixCls = getPrefixCls('whiteboard', props.prefixCls);
  return (
    <div className={classNames(prefixCls, props.className)} style={props.style}>
      {props.children}
    </div>
  );
};

interface TopBoardProps {
  prefixCls?: string;
  className?: string;
  style?: React.CSSProperties;
  children: React.ReactNode;
}

const TopBoard: React.FC<TopBoardProps> = (props) => {
  const { getPrefixCls } = React.useContext(ConfigProvider.Context);
  const prefixCls = getPrefixCls('whiteboard', props.prefixCls);
  return (
    <div
      className={classNames(`${prefixCls}-top`, props.className)}
      style={props.style}
    >
      {props.children}
    </div>
  );
};

interface BottomBoardProps {
  prefixCls?: string;
  className?: string;
  style?: React.CSSProperties;
  children: React.ReactNode;
}

const BottomBoard: React.FC<BottomBoardProps> = (props) => {
  const { getPrefixCls } = React.useContext(ConfigProvider.Context);
  const prefixCls = getPrefixCls('whiteboard', props.prefixCls);
  return (
    <div
      className={classNames(`${prefixCls}-bottom`, props.className)}
      style={props.style}
    >
      {props.children}
    </div>
  );
};

WhiteBoard.TopBoard = TopBoard;
WhiteBoard.BottomBoard = BottomBoard;

export default WhiteBoard;
