import InlineRadio, { Group, Button } from './radio';

type InternalRadioType = typeof InlineRadio;

interface RadioInterface extends InternalRadioType {
  Group: typeof Group;
  Button: typeof Button;
}

const Radio = InlineRadio as RadioInterface;

Radio.Group = Group;
Radio.Button = Button;

export type { RadioProps } from './radio';

export default Radio;
