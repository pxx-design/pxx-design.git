/**
 * title: 单选组合
 * desc: 一组互斥的Radio配合使用
 */

import React from 'react';
import { Radio } from '@parallel-line/components';

const RadioGroup = () => {
  const [value, setValue] = React.useState(1);

  const onChange = (e) => {
    setValue(e.target.value);
  };

  return (
    <Radio.Group onChange={onChange} value={value}>
      <Radio value={1}>A</Radio>
      <Radio value={2}>B</Radio>
      <Radio value={3}>C</Radio>
      <Radio value={4}>D</Radio>
    </Radio.Group>
  );
};

export default RadioGroup;
