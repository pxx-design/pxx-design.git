/**
 * title: 基本
 * desc: 最简单的用法
 */

import React from 'react';
import { Radio } from '@parallel-line/components';

const Basic = () => {
  return <Radio>Radio</Radio>;
};

export default Basic;
