/**
 * title: parallel定制样式
 * desc: stereoscopic类型对应的定制样式
 */

import React from 'react';
import { Radio } from '@parallel-line/components';

const options1 = [
  { label: 'Apple', value: 'Apple' },
  { label: 'Pear', value: 'Pear' },
  { label: 'Orange', value: 'Orange' },
];

const options2 = [
  { label: 'bus', value: 'bus' },
  { label: 'car', value: 'car' },
  { label: 'texi', value: 'texi' },
  { label: 'train', value: 'train' },
  { label: 'subway', value: 'subway' },
];

const StereoscopicStyle = () => {
  const [value1, setValue1] = React.useState('Apple');
  const [value2, setValue2] = React.useState('bus');

  const onChange1 = (e) => {
    setValue1(e.target.value);
    console.log(e.target.value);
  };

  const onChange2 = (e) => {
    setValue2(e.target.value);
    console.log(e.target.value);
  };

  return (
    <div>
      <Radio.Group
        options={options1}
        onChange={onChange1}
        value={value1}
        optionType="button"
        buttonStyle="solid"
      />
      <div style={{ margin: '16px' }} />
      <div style={{ padding: '16px', backgroundColor: '#fff' }}>
        <Radio.Group
          className="stereoscopic"
          options={options2}
          onChange={onChange2}
          value={value2}
          optionType="button"
          buttonStyle="stereoscopic"
        />
      </div>
    </div>
  );
};

export default StereoscopicStyle;
