import React from 'react';
import Radio, {
  RadioProps as AntRadioProps,
  RadioGroupProps as AntRadioGroupProps,
} from 'antd/es/radio';

export interface RadioProps extends AntRadioProps {
  className?: string;
  prefixCls?: string;
}

interface RadioGroupProps extends Omit<AntRadioGroupProps, 'buttonStyle'> {
  /**
   * @description RadioButton 的风格样式, 目前有描边,填色和stereoscopic三种风格
   */
  buttonStyle?: 'outline' | 'solid' | 'stereoscopic';
  prefixCls?: string;
}

const InlineRadio = React.forwardRef<HTMLElement, RadioProps>((props, ref) => (
  <Radio ref={ref} {...props} prefixCls={`${props.prefixCls}-radio`} />
));

InlineRadio.defaultProps = {
  autoFocus: false,
  prefixCls: 'pxx',
};

export const Group: React.FC<RadioGroupProps> = (props: any) => {
  const radioGroupPrefixCls = `${props.prefixCls}-radio`;

  return <Radio.Group {...props} prefixCls={radioGroupPrefixCls} />;
};

Group.defaultProps = {
  prefixCls: 'pxx',
  buttonStyle: 'outline',
  disabled: false,
  optionType: 'default',
};

export const Button: React.FC<RadioProps> = (props: any) => (
  <Radio.Button {...props} prefixCls={`${props.prefixCls}-radio-group`} />
);

Button.defaultProps = {
  prefixCls: 'pxx',
};

export type { RadioGroupProps };
export default InlineRadio;
