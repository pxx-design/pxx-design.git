/**
 * title: 基本使用
 * desc: 基本使用。
 */

import React from 'react';
import { DatePicker } from '@parallel-line/components';

export default () => <div style={{ width: 200 }}>
  <DatePicker/>
</div>;
