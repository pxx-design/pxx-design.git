import React, { useRef, useEffect } from 'react';
import moment from 'moment';
import { isNull } from 'lodash-es';
import { useSetState } from 'ahooks';
import { isEmpty } from '@parallel-line/utils';
import { DatePicker as AntDatePicker, Radio } from 'antd';

import './style/index';

const formatMap: any = {
  time: 'YYYY-MM-DD HH:mm',
  date: 'YYYY-MM-DD',
};

const DatePicker = (props: any) => {
  const switchRef = useRef(false);
  const pCls = `${props.prefixCls}-date-picker`;
  const pClsDropdown = `${pCls}-dropdown`;
  const [state, setState] = useSetState<any>({
    type: 'time',
    open: false,
    value: undefined,
  });
  useEffect(() => {
    if (isEmpty(props.value)) return;
    let _moment = props.value;
    if (!moment.isMoment(props.value)) {
      _moment = moment(props.value, 'YYYY-MM-DDTHH:mm:ss');
    }
    const second = _moment.get('second');
    const type = second === 59 ? 'date' : 'time';
    setState({ value: _moment, type });
  }, [props.value]);
  const handleSwitch = (e: any) => {
    const { value } = e.target;
    switchRef.current = value === 'time';
    setState({ type: value });
  };
  useEffect(()=>{
    console.log(state.value)
  },[state.type])
  const renderExtraFooter = () => {
    return (
      <div className={`${pClsDropdown}-switch`}>
        <Radio.Group value={state.type} onChange={handleSwitch}>
          <Radio.Button value="time">时间</Radio.Button>
          <Radio.Button value="date">日期</Radio.Button>
        </Radio.Group>
      </div>
    );
  };
  const handleChange = (value: any) => {
    if (isNull(value)) {
      setState({ value: undefined });
      props.onChange?.();
      return;
    }
    const _moment = value.clone();
    if (state.type === 'time') {
      _moment.set({ second: 0 });
      props.onChange?.(_moment);
      return;
    }
    _moment.set({ hour: 23, minute: 59, second: 59 });
    props.onChange?.(_moment);
  };
  const handleOpenChange = (open: boolean) => {
    setTimeout(() => {
      if (switchRef.current && !open) {
        switchRef.current = false;
        return;
      }
      setState({ open });
    }, 1);
  };

  return (
    <AntDatePicker
      className={pCls}
      open={state.open}
      value={state.value}
      style={props.style}
      onChange={handleChange}
      disabled={props.disabled}
      format={formatMap[state.type]}
      onOpenChange={handleOpenChange}
      placeholder={props.placeholder}
      showTime={state.type === 'time'}
      dropdownClassName={pClsDropdown}
      renderExtraFooter={renderExtraFooter}
      getPopupContainer={props.getPopupContainer}
    />
  );
};

DatePicker.defaultProps = {
  prefixCls: 'pxx',
};

export default DatePicker;
