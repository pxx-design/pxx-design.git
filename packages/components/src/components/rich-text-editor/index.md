---
title: RichTextEditor 富文本框
---

# RichTextEditor 富文本框

富文本编辑器，用于编辑富文本信息。

## 代码演示

<code src="./demos/basic.tsx"></code>

<API src="./RichTextEditor.tsx" exports='["default", "RichTextBox"]'></API>

## FQA

### RichTextBox 性能问题解决方案

- 不使用 `RichTextBox` 而是使用 `RichTextEditor`
- 提供 `serializeHTML` 和 `deserializeHTML` 函数
- 从后端获取数据后调用 `RichTextEditor.deserializeHTML`
- 在提交时调用 `RichTextEditor.serializeHTML`
