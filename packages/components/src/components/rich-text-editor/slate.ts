import { BaseEditor } from 'slate';
import { ReactEditor } from 'slate-react';
import { HistoryEditor } from 'slate-history';

export type CustomEditor = BaseEditor & ReactEditor & HistoryEditor;

export const ElementTypes = [
  'paragraph',
  'block-quote',
  'bulleted-list',
  'heading-one',
  'heading-two',
  'list-item',
  'numbered-list',
] as const;

export const TextWrappers = ['paragraph', 'heading-one', 'heading-two'];

export type ElementType = typeof ElementTypes[number];

export type ElementTextAlignType = 'left' | 'right' | 'center';
export interface CustomElement {
  type: ElementType;
  children: CustomText[];
  textAlign?: ElementTextAlignType; // textWrapper的属性
}

export interface FormattedText {
  text: string;
  bold?: true;
  italic?: true;
  underline?: true;
  color?: string;
}

export type FormattedKey = keyof Omit<FormattedText, 'text'>;

export type CustomText = FormattedText;

export * from 'slate';
export * from 'slate-react';
export * from 'slate-history';

declare module 'slate' {
  interface CustomTypes {
    Editor: CustomEditor;
    Element: CustomElement;
    Text: CustomText;
  }
}
