import React, { useCallback, useMemo } from 'react';
import isHotkey from 'is-hotkey';
import { useControllableValue, useUpdateEffect, useBoolean } from 'ahooks';
import { useNamespace } from '@parallel-line/common/es/hooks';
import {
  serializeElement,
  serializeText,
  serializeHTML,
  deserializeHTML,
  EMPTY_DESCENDANT,
  serializeLeaf,
} from './utils';
import { MarkButton, ToolBar } from './components';
import {
  withHistory,
  Editable,
  withReact,
  Slate,
  createEditor,
  Descendant,
  FormattedKey,
  RenderLeafProps,
  RenderElementProps,
} from './slate';

export interface RichTextEditorProps {
  prefixCls?: string;
  // /**
  //  * @description 是否展示字数
  //  */
  // showCount?: boolean;
  // /**
  //  * @description 内容最大长度
  //  */
  // maxLength?: number;
  /**
   * @description 是否禁用
   */
  disabled?: boolean;
  /**
   * @description 规定可描述输入字段预期值的简短的提示信息。
   */
  placeholder?: string;
  /**
   * @description 输入框内容
   */
  value?: Descendant[];
  /**
   * @description 输入框默认内容
   */
  defaultValue?: Descendant[];
  /**
   * @description 输入框内容变化时的回调
   */
  onChange?: (value: Descendant[]) => void;
}

export interface RichTextBoxProps
  extends Omit<RichTextEditorProps, 'value' | 'defaultValue' | 'onChange'> {
  /**
   * @description 输入框内容
   */
  value?: string;
  /**
   * @description 输入框默认内容
   */
  defaultValue?: string;
  /**
   * @description 输入框内容变化时的回调
   */
  onChange?: (value: string | undefined) => void;
}

const HOTKEYS: Record<string, FormattedKey> = {
  'mod+b': 'bold',
  'mod+i': 'italic',
  'mod+u': 'underline',
};

export function RichTextEditor(props: RichTextEditorProps) {
  const {
    // showCount,
    // maxLength = -1,
    disabled,
    placeholder = '请输入',
    value = EMPTY_DESCENDANT,
    defaultValue,
  } = props;
  const [, bem] = useNamespace('rich-text-box', props.prefixCls);
  const [state, setState] = React.useState(defaultValue || value);
  // const [count, setCount] = React.useState(0);
  const [hasFocus, { setTrue: handleFocus, setFalse }] = useBoolean();
  const renderElement = useCallback(
    ({ attributes, children, element }: RenderElementProps) => {
      const { tag, css } = serializeElement(element);
      return React.createElement(tag, { ...attributes, style: css }, children);
    },
    [],
  );
  const renderLeaf = useCallback(
    ({ attributes, children, leaf }: RenderLeafProps) => {
      const { tag, css } = serializeLeaf(leaf);
      return React.createElement(tag, { ...attributes, style: css }, children);
    },
    [],
  );
  const editor = useMemo(() => withHistory(withReact(createEditor())), []);

  useUpdateEffect(() => {
    setState(value);
  }, [value]);

  const handleKeyDown: React.KeyboardEventHandler<HTMLDivElement> = (event) => {
    for (const hotkey in HOTKEYS) {
      if (isHotkey(hotkey, event)) {
        event.preventDefault();
        const mark = HOTKEYS[hotkey];
        MarkButton.toggleMark(editor, mark);
      }
    }
  };
  const handleBlur = () => {
    setTimeout(setFalse, 300);
  };
  const handleChange = (value: Descendant[]) => {
    if (serializeText(value).length) {
      setState(value);
      props.onChange?.(value);
      return;
    }
    setState(EMPTY_DESCENDANT);
    props.onChange?.(EMPTY_DESCENDANT);
    // if (maxLength > 0) {
    //   const newCount = serializeText(value).length;
    //   setCount(newCount);
    // }
  };

  return (
    <Slate editor={editor} value={state} onChange={handleChange}>
      <div
        className={bem('wrapper', { disabled })}
        // className={bem('wrapper', { 'show-count': showCount })}
        // data-count={maxLength > 0 ? `${count} / ${maxLength}` : undefined}
      >
        <ToolBar
          disabled={disabled ? disabled : !hasFocus}
          className={bem('tool-bar')}
          buttonPrefixCls={bem('button')}
        />
        <Editable
          spellCheck
          className={bem()}
          readOnly={disabled}
          renderLeaf={renderLeaf}
          renderElement={renderElement}
          placeholder={placeholder}
          onKeyDown={handleKeyDown}
          onFocus={handleFocus}
          onBlur={handleBlur}
        />
      </div>
    </Slate>
  );
}

export function RichTextBox(props: RichTextBoxProps) {
  const {
    placeholder = '请输入',
    defaultValue,
    ...richTextEditorProps
  } = props;
  const [state, setState] = useControllableValue<string>(props);

  const handleChange: RichTextEditorProps['onChange'] = (value) => {
    setState(serializeHTML(value));
  };

  return (
    <RichTextEditor
      {...richTextEditorProps}
      placeholder={placeholder}
      value={deserializeHTML(state)}
      onChange={handleChange}
    />
  );
}

export default RichTextEditor;
