import { jsx } from 'slate-hyperscript';
import { CSSProperties } from 'react';
import escapeHtml from 'escape-html';
import { string } from 'to-style';
import {
  Text,
  Node,
  Element,
  Descendant,
  ElementType,
  CustomElement,
  TextWrappers,
  FormattedText,
} from './slate';

interface NodeInfo {
  tag: string;
  css: CSSProperties;
}

export const DEFAULTCOLOR = 'rgb(26, 43, 78)';
// TODO 临时通过始终有一个空格来修复，在空文本-中文输入下失焦的问题
export const EMPTY_DESCENDANT: Descendant[] = [
  { type: 'paragraph', children: [{ text: ' ' }] },
];
const ElementMap: Record<ElementType, NodeInfo> = {
  'block-quote': {
    tag: 'blockquote',
    css: {
      fontSize: '14px',
      borderLeft: '2px solid #ddd',
      marginLeft: '0',
      marginRight: '0',
      paddingLeft: '10px',
      color: '#aaaaaa',
      fontStyle: 'italic',
    },
  },
  'bulleted-list': { tag: 'ul', css: { fontSize: '14px' } },
  'heading-one': {
    tag: 'h1',
    css: { fontSize: '18px', marginBottom: '8px', color: DEFAULTCOLOR },
  },
  'heading-two': {
    tag: 'h2',
    css: { fontSize: '16px', marginBottom: '8px', color: DEFAULTCOLOR },
  },
  'list-item': { tag: 'li', css: { fontSize: '14px' } },
  'numbered-list': { tag: 'ol', css: { fontSize: '14px' } },
  paragraph: {
    tag: 'p',
    css: {
      fontSize: '14px',
      color: 'rgba(26,43,78,0.80)',
      marginBottom: '8px',
    },
  },
};

export function isTextWrapper(node: Node) {
  return Element.isElement(node) && TextWrappers.includes(node.type);
}

export function toStyleString(css: CSSProperties): string {
  return string(css);
}

export function serializeElement(element: CustomElement) {
  const info = ElementMap[element.type];
  if (isTextWrapper(element)) {
    info.css = { ...info.css, textAlign: element.textAlign || 'left' };
  }
  return info;
}

export function serializeLeaf(leaf: FormattedText): NodeInfo {
  const style: React.CSSProperties = { color: leaf.color || DEFAULTCOLOR };
  if (style.color === DEFAULTCOLOR) {
    style.color = 'currentcolor';
  }

  if (leaf.bold) {
    style.fontWeight = 'bolder';
  }

  if (leaf.italic) {
    style.fontStyle = 'italic';
  }

  if (leaf.underline) {
    style.textDecoration = 'underline';
  }

  return { tag: 'span', css: style };
}

function serialize(node: Descendant): string {
  if (Text.isText(node)) {
    const children = escapeHtml(node.text);
    const { tag, css } = serializeLeaf(node);
    return `<${tag} style="${toStyleString(css)}"}>${children}</${tag}>`;
  }

  const children = node.children.map((n) => serialize(n)).join('');
  const { tag, css } = serializeElement(node);
  return `<${tag} style="${toStyleString(css)}">${children}</${tag}>`;
}

function deserialize(el: HTMLElement): any {
  if (el.nodeType === 3) {
    return el.textContent;
  } else if (el.nodeType !== 1) {
    return null;
  }

  let children = (Array.from(el.childNodes) as HTMLElement[]).map(deserialize);

  if (children.length === 0) {
    children = [{ text: '' }];
  }
  const elProps: Omit<FormattedText, 'text'> = {};

  switch (el.nodeName) {
    case 'BODY':
      return jsx('fragment', {}, children);
    case 'BR':
      return '\n';
    case 'BLOCKQUOTE':
      return jsx('element', { type: 'block-quote' }, children);
    case 'H1':
      return jsx(
        'element',
        { type: 'heading-one', textAlign: el.style.textAlign || 'left' },
        children,
      );
    case 'H2':
      return jsx(
        'element',
        { type: 'heading-two', textAlign: el.style.textAlign || 'left' },
        children,
      );
    case 'LI':
      return jsx('element', { type: 'list-item' }, children);
    case 'UL':
      return jsx('element', { type: 'bulleted-list' }, children);
    case 'OL':
      return jsx('element', { type: 'numbered-list' }, children);
    case 'P':
      return jsx(
        'element',
        { type: 'paragraph', textAlign: el.style.textAlign || 'left' },
        children,
      );
    case 'SPAN':
      if (el.style.fontWeight === 'bolder') {
        elProps.bold = true;
      }
      if (el.style.fontStyle === 'italic') {
        elProps.italic = true;
      }
      if (el.style.textDecoration === 'underline') {
        elProps.underline = true;
      }
      elProps.color = el.style.color || DEFAULTCOLOR;
      return jsx('text', elProps, children);

    default:
      return el.textContent;
  }
}

export function serializeText(nodes: Descendant[]) {
  return nodes.map((n) => Node.string(n)).join('\n').trim();
}

export function serializeHTML(nodes: Descendant[]) {
  if (!serializeText(nodes)) return '';
  return nodes.map(serialize).join('');
}

export function deserializeHTML(html: string): Descendant[] {
  if (!html) return EMPTY_DESCENDANT;
  const document = new DOMParser().parseFromString(html, 'text/html');
  return deserialize(document.body);
}

const catchSlateFragment = /data-slate-fragment="(.+?)"/m;
export const getSlateFragmentAttribute = (
  dataTransfer: DataTransfer,
): string | void => {
  const htmlData = dataTransfer.getData('text/html');
  const [, fragment] = htmlData.match(catchSlateFragment) || [];
  return fragment;
};
