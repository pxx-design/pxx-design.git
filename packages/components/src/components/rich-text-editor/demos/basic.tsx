/**
 * title: 基本使用
 * desc: 基本使用。
 */

import React, { useState } from 'react';
import { useBoolean } from 'ahooks';
import { Switch, Form } from 'antd';
import { RichTextEditor } from '@parallel-line/components';

export default function () {
  const [value, setValue] = useState<string>('');
  const [disabled, { toggle }] = useBoolean();
  return (
    <>
      <Form layout="inline" style={{ marginBottom: 16 }}>
        <Form.Item label="是否禁用">
          <Switch checked={disabled} onChange={toggle} />
        </Form.Item>
      </Form>
      <RichTextEditor
        disabled={disabled}
        defaultValue={RichTextEditor.deserializeHTML(
          `<p style="font-size: 14px; color: rgba(26,43,78,0.80); margin-bottom: 8px; text-align: left"><span style="color: currentcolor"}>This is editable </span><span style="color: currentcolor; font-weight: bolder"}>rich</span><span style="color: currentcolor"}> text, </span><span style="color: currentcolor; font-style: italic"}>much</span><span style="color: currentcolor"}> better than a &lt;textarea&gt;!</span></p><p style="font-size: 14px; color: rgba(26,43,78,0.80); margin-bottom: 8px; text-align: left"><span style="color: currentcolor"}>Since it&#39;s </span><span style="color: rgb(245, 34, 45)"}>rich</span><span style="color: currentcolor"}> text, you can do things like turn a selection of text </span><span style="color: currentcolor; font-weight: bolder"}>bold</span><span style="color: currentcolor"}>, or add a semantically rendered block quote in the middle of the page, like this:</span></p><blockquote style="font-size: 14px; border-left: 2px solid #ddd; margin-left: 0px; margin-right: 0px; padding-left: 10px; color: #aaaaaa; font-style: italic"><span style="color: currentcolor"}>A wise quote.</span></blockquote><p style="font-size: 14px; color: rgba(26,43,78,0.80); margin-bottom: 8px; text-align: left"><span style="color: currentcolor; text-decoration: underline"}>Try it out for yourself!</span></p>`,
        )}
        onChange={(v) => setValue(RichTextEditor.serializeHTML(v))}
      />
      output-string: {value}
      <br />
      output-html: <div dangerouslySetInnerHTML={{ __html: value }}></div>
    </>
  );
}
