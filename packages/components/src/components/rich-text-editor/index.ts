import InlineRichTextEditor, {
  RichTextBox,
  RichTextEditorProps,
  RichTextBoxProps,
} from './RichTextEditor';
import { serializeText, serializeHTML, deserializeHTML } from './utils';
import { Descendant } from './slate';

type RichTextEditorType = typeof InlineRichTextEditor;

interface RichTextEditorInterface extends RichTextEditorType {
  /**
   * Html版本, 实时转化value为html
   *
   * - **存在数据越多越卡顿的问题**
   * - **存在中文输入光标异常问题**
   */
  RichTextBox: typeof RichTextBox;
  serializeText: typeof serializeText;
  serializeHTML: typeof serializeHTML;
  deserializeHTML: typeof deserializeHTML;
}

const RichTextEditor = InlineRichTextEditor as RichTextEditorInterface;
RichTextEditor.RichTextBox = RichTextBox;
RichTextEditor.serializeText = serializeText;
RichTextEditor.serializeHTML = serializeHTML;
RichTextEditor.deserializeHTML = deserializeHTML;

export type { Descendant, RichTextBoxProps, RichTextEditorProps };
export default RichTextEditor;
