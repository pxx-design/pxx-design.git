import React from 'react';
import { Button, BaseButtonProps } from './Button';
import { useSlate, Editor, CustomEditor, FormattedKey } from '../slate';

interface MarkButtonProps extends BaseButtonProps {
  format: FormattedKey;
}

const isMarkActive = (editor: CustomEditor, format: FormattedKey) => {
  const marks = Editor.marks(editor);
  return marks ? marks[format] === true : false;
};

const toggleMark = (editor: CustomEditor, format: FormattedKey) => {
  const isActive = isMarkActive(editor, format);

  if (isActive) {
    Editor.removeMark(editor, format);
  } else {
    Editor.addMark(editor, format, true);
  }
};

export const MarkButton = ({
  format,
  icon,
  title,
  ...btnProps
}: MarkButtonProps) => {
  const editor = useSlate();
  return (
    <Button
      {...btnProps}
      title={title}
      active={isMarkActive(editor, format)}
      onMouseDown={(event) => {
        event.preventDefault();
        toggleMark(editor, format);
      }}
    >
      {icon}
    </Button>
  );
};

MarkButton.toggleMark = toggleMark;
