import React from 'react';
import { FontColorsOutlined } from '@parallel-line/icons';
import { Button } from './Button';
import { DEFAULTCOLOR } from '../utils';
import { useSlate, Editor } from '../slate';
import ColorBoard from '../../color-board';

interface FontColorButtonProps {
  prefixCls: string;
  disabled?: boolean;
}

export const FontColorButton = ({
  prefixCls,
  disabled,
}: FontColorButtonProps) => {
  const editor = useSlate();
  const marks = Editor.marks(editor);
  const [currentColor, setColor] = React.useState(DEFAULTCOLOR);
  const style = React.useMemo(() => {
    const style: React.CSSProperties = {};
    if (currentColor === DEFAULTCOLOR || currentColor === 'currentcolor') {
      return style;
    }
    style.color = currentColor;
    return style;
  }, [currentColor]);

  React.useEffect(() => {
    setColor(marks?.color || DEFAULTCOLOR);
  }, [marks?.color]);

  const handleChange = (currentColor: string) => {
    setColor(currentColor);
    Editor.addMark(editor, 'color', currentColor);
  };

  const render = () => (
    <Button
      title="字体颜色"
      disabled={disabled}
      prefixCls={prefixCls}
      style={style}
      onMouseDown={(e) => {
        e.preventDefault();
      }}
    >
      <FontColorsOutlined />
    </Button>
  );
  if (disabled) return render();
  return (
    <ColorBoard.Picker
      defaultColor={DEFAULTCOLOR}
      currentColor={currentColor}
      onChange={handleChange}
    >
      {render()}
    </ColorBoard.Picker>
  );
};
