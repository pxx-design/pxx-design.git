import React from 'react';
import { Button, BaseButtonProps } from './Button';
import {
  useSlate,
  Editor,
  Element,
  Transforms,
  CustomEditor,
  CustomElement,
  ElementType,
} from '../slate';

interface BlockButtonProps extends BaseButtonProps {
  format: ElementType;
}

const LIST_TYPES = ['numbered-list', 'bulleted-list'];

const isBlockActive = (editor: CustomEditor, format: string) => {
  const [match] = Editor.nodes(editor, {
    match: (n) =>
      !Editor.isEditor(n) && Element.isElement(n) && n.type === format,
  });

  return !!match;
};

const toggleBlock = (editor: CustomEditor, format: ElementType) => {
  const isActive = isBlockActive(editor, format);
  const isList = LIST_TYPES.includes(format);

  Transforms.unwrapNodes(editor, {
    match: (n) =>
      LIST_TYPES.includes(
        !Editor.isEditor(n) && Element.isElement(n) ? n.type : '',
      ),
    split: true,
  });
  const newProperties: Partial<Element> = {
    type: isActive ? 'paragraph' : isList ? 'list-item' : format,
  };
  Transforms.setNodes(editor, newProperties);

  if (!isActive && isList) {
    const block: CustomElement = { type: format, children: [] };
    Transforms.wrapNodes(editor, block);
  }
};

export const BlockButton = ({
  format,
  icon,
  title,
  ...btnProps
}: BlockButtonProps) => {
  const editor = useSlate();
  return (
    <Button
      {...btnProps}
      title={title}
      active={isBlockActive(editor, format)}
      onMouseDown={(event) => {
        event.preventDefault();
        toggleBlock(editor, format);
      }}
    >
      {icon}
    </Button>
  );
};
