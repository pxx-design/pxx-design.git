import React from 'react';
import { Tooltip } from 'antd';
import classNames from 'classnames';
import { useNamespace } from '@parallel-line/common/es/hooks';

export interface BaseButtonProps {
  prefixCls: string;
  title: React.ReactNode;
  icon: React.ReactNode;
  disabled?: boolean;
}

interface ButtonProps
  extends Omit<React.HTMLAttributes<HTMLSpanElement>, 'title'> {
  prefixCls: string;
  title: React.ReactNode;
  active?: boolean;
  reversed?: boolean;
  disabled?: boolean;
}

export const Button = React.forwardRef<HTMLSpanElement, ButtonProps>(
  (
    { className, title, active, reversed, disabled, prefixCls, ...props },
    ref,
  ) => {
    const [, bem] = useNamespace('', prefixCls);

    const render = () => (
      <span
        {...props}
        ref={ref}
        className={classNames(className, bem({ active, reversed, disabled }))}
      />
    );
    if (disabled) return render();
    return (
      <Tooltip title={title} placement="bottom">
        {render()}
      </Tooltip>
    );
  },
);
