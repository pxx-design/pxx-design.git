import React from 'react';
import { Button, BaseButtonProps } from './Button';
import {
  useSlate,
  Editor,
  Element,
  Transforms,
  CustomEditor,
  ElementTextAlignType,
} from '../slate';
import { isTextWrapper } from '../utils';

interface TextAlignButtonProps extends BaseButtonProps {
  format: ElementTextAlignType;
}

const toggleTextAlign = (
  editor: CustomEditor,
  format: ElementTextAlignType,
) => {
  if (!editor.selection) return;
  Transforms.setNodes(
    editor,
    { textAlign: format },
    {
      mode: 'lowest',
      match(n) {
        return isTextWrapper(n);
      },
    },
  );
};

const isTextAlignActive = (
  editor: CustomEditor,
  format: ElementTextAlignType,
) => {
  const [node] = Editor.nodes(editor, {
    match(n) {
      return isTextWrapper(n);
    },
  });
  if (!node) return format === 'left';
  const textAlign = Element.isElement(node[0]) && node[0].textAlign;
  return (
    (textAlign === false || textAlign == null ? 'left' : textAlign) === format
  );
};

export const TextAlignButton = ({
  format,
  icon,
  title,
  ...btnProps
}: TextAlignButtonProps) => {
  const editor = useSlate();
  return (
    <Button
      {...btnProps}
      title={title}
      active={isTextAlignActive(editor, format)}
      onMouseDown={(event) => {
        event.preventDefault();
        toggleTextAlign(editor, format);
      }}
    >
      {icon}
    </Button>
  );
};
