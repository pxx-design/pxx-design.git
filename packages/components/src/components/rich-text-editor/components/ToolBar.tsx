import React from 'react';
import {
  AlignCenterOutlined,
  AlignLeftOutlined,
  AlignRightOutlined,
  BoldOutlined,
  HeadingOneOutlined,
  HeadingTwoOutlined,
  ItalicOutlined,
  OrderedListOutlined,
  UnderlineOutlined,
  UnorderedListOutlined,
} from '@parallel-line/icons';
import { MarkButton } from './MarkButton';
import { BlockButton } from './BlockButton';
import { FontColorButton } from './FontColorButton';
import { TextAlignButton } from './TextAlignButton';

interface ToolBarProps {
  className?: string;
  buttonPrefixCls: string;
  disabled?: boolean;
}

export function ToolBar(props: ToolBarProps) {
  const { className, disabled, buttonPrefixCls } = props;
  return (
    <div className={className}>
      <MarkButton
        disabled={disabled}
        prefixCls={buttonPrefixCls}
        format="bold"
        title="粗体"
        icon={<BoldOutlined />}
      />
      <MarkButton
        disabled={disabled}
        prefixCls={buttonPrefixCls}
        format="italic"
        title="斜体"
        icon={<ItalicOutlined />}
      />
      <MarkButton
        disabled={disabled}
        prefixCls={buttonPrefixCls}
        format="underline"
        title="下划线"
        icon={<UnderlineOutlined />}
      />
      <FontColorButton disabled={disabled} prefixCls={buttonPrefixCls} />
      <BlockButton
        disabled={disabled}
        prefixCls={buttonPrefixCls}
        format="heading-one"
        title="标题1"
        icon={<HeadingOneOutlined />}
      />
      <BlockButton
        disabled={disabled}
        prefixCls={buttonPrefixCls}
        format="heading-two"
        title="标题2"
        icon={<HeadingTwoOutlined />}
      />
      <BlockButton
        disabled={disabled}
        prefixCls={buttonPrefixCls}
        format="bulleted-list"
        title="无序列表"
        icon={<UnorderedListOutlined />}
      />
      <BlockButton
        disabled={disabled}
        prefixCls={buttonPrefixCls}
        format="numbered-list"
        title="有序列表"
        icon={<OrderedListOutlined />}
      />
      <TextAlignButton
        disabled={disabled}
        prefixCls={buttonPrefixCls}
        format="left"
        title="左对齐"
        icon={<AlignLeftOutlined />}
      />
      <TextAlignButton
        disabled={disabled}
        prefixCls={buttonPrefixCls}
        format="center"
        title="居中对齐"
        icon={<AlignCenterOutlined />}
      />
      <TextAlignButton
        disabled={disabled}
        prefixCls={buttonPrefixCls}
        format="right"
        title="右对齐"
        icon={<AlignRightOutlined />}
      />
    </div>
  );
}
