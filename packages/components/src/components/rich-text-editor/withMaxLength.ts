import { serializeText, getSlateFragmentAttribute } from './utils';
import { Transforms, Descendant, CustomEditor } from './slate';

export function withMaxLength(editor: CustomEditor, maxLength: number = -1) {
  const e = editor as CustomEditor;

  // 劫持粘贴方法，判断添加新粘贴的文本后是否超过 maxLength 并做相应处理
  e.insertData = (data) => {
    if (maxLength <= 0) {
      e.insertData(data);
      return;
    }
    const all = e.children;
    const surplusCount = maxLength - serializeText(all).length;

    /**
     * Checking copied fragment from application/x-slate-fragment or data-slate-fragment
     */
    const fragment =
      data.getData('application/x-slate-fragment') ||
      getSlateFragmentAttribute(data);

    if (fragment) {
      const decoded = decodeURIComponent(window.atob(fragment));
      const parsed = JSON.parse(decoded) as Descendant[];
      // TODO fragment 的字数限制待实现
      console.log('insertData:fragment', decoded);
      e.insertFragment(parsed);
      return;
    }

    let text = data.getData('text/plain');

    if (text) {
      const count = text.length;
      if (count > surplusCount) {
        text = text.substring(0, surplusCount);
      }
      const lines = text.split(/\r\n|\r|\n/);
      let split = false;

      for (const line of lines) {
        if (split) {
          Transforms.splitNodes(e, { always: true });
        }

        e.insertText(line);
        split = true;
      }
    }
  };
  return e;
}
