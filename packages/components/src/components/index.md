---
title: 组件总览
order: 1
nav:
  title: WEB组件
  order: 4
---

> 平行线前端WEB组件库。

## 快速上手

## 📦 安装

```bash
npm install @parallel-line/components --save
```

```bash
yarn add @parallel-line/components
```

### 在 Umi 里配置

如果你在使用 [Umi](https://umijs.org/zh-CN/config#theme)，那么可以很方便地在项目根目录的 `.umirc.ts` 或 [config/config.ts](https://github.com/ant-design/ant-design-pro/blob/v5/config/config.ts) 文件中 [theme](https://umijs.org/zh-CN/config#theme) 字段进行主题配置。`theme` 可以配置为一个对象或文件路径。

```js
"theme": {
  "@hd": "2px",
},
```

## 按需加载

### 安装

``` sh
yarn add -D babel-plugin-pxx-import
```

### 配置

```js
import { defineConfig } from 'dumi';

export default defineConfig({
  extraBabelPlugins: [
    [
      'pxx-import',
      {
        libraryName: '@parallel-line/components',
        libraryDirectory: 'es/components',
        style: true,
      },
      'parallel-line',
    ],
  ],
});
```
