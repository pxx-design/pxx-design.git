import * as React from 'react';

export interface ConfigConsumerProps {
  hd: (px: number) => number;
}

export const ConfigContext = React.createContext<ConfigConsumerProps>({
  hd: (px) => px,
});

export const ConfigConsumer = ConfigContext.Consumer;
