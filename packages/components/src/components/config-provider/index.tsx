import React from 'react';
import { ConfigProvider as AntConfigProvider } from 'antd';
import { ConfigProviderProps as ProviderProps } from 'antd/es/config-provider';
import locale from 'antd/es/locale/zh_CN';
import {
  ConfigProvider,
  ConfigConsumer,
  ConfigProviderProps,
  ConfigConsumerProps,
} from '@parallel-line/common/es/components';
import moment from 'moment';
import 'moment/locale/zh-cn';

moment.locale('zh-cn');

const ConfigContext = ConfigProvider.Context;
const SizeContext = ConfigProvider.SizeContext;

const InlineConfigProvider: React.FC<
  ConfigProviderProps & { antProviderProps?: ProviderProps }
> & {
  Context: typeof ConfigContext;
  SizeContext: typeof SizeContext;
} = ({ antProviderProps, ...props }) => (
  <AntConfigProvider locale={locale} {...antProviderProps}>
    <ConfigProvider {...props} />
  </AntConfigProvider>
);

InlineConfigProvider.defaultProps = {
  prefixCls: 'pxx',
};

InlineConfigProvider.Context = ConfigContext;
InlineConfigProvider.SizeContext = SizeContext;

export { ConfigConsumer, ConfigContext };

export type { ConfigProviderProps, ConfigConsumerProps };

export default InlineConfigProvider;
