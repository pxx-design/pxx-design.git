import React from 'react';
import { PageLoading } from '@parallel-line/components';

const Basic = () => {
  return (
    <div style={{ height: 500, display: 'flex' }}>
      <PageLoading />
    </div>
  );
};

export default Basic;
