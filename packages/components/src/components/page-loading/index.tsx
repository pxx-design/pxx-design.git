import React, { useState, useMemo } from 'react';
import { Result, Button } from 'antd';
import Lottie from 'lottie-react-web';
import { useSize, useInterval } from 'ahooks';
import useAntdMediaQuery from 'use-media-antd-query';

import animationData from './loading.json';

interface PageLoadingProps {
  prefixCls?: string;
}

const PageLoading: React.FC<PageLoadingProps> = (props) => {
  const cls = `${props.prefixCls}-page-loading`;
  const { width = 0 } = useSize(document.body);
  const colSize = useAntdMediaQuery();
  const isMobile = useMemo(() => colSize === 'sm' || colSize === 'xs', [
    colSize,
  ]);
  const size = isMobile ? 120 : width / 21;
  const [count, setCount] = useState(30);
  const [interval, setInterval] = useState<number | null>(1000);
  useInterval(() => {
    setCount(count - 1);
    if (count === 0) {
      // 清除定时
      setInterval(null);
    }
  }, interval);

  const reload = () => window.location.reload();

  const renderWarn = () => {
    return (
      <Result
        status="warning"
        title="文件加载遇到问题"
        extra={
          <Button type="primary" onClick={reload}>
            刷新一下
          </Button>
        }
      />
    );
  };

  return (
    <div className={cls}>
      {count <= 0 ? (
        renderWarn()
      ) : (
        <Lottie
          width={size}
          height={size}
          options={{
            loop: true,
            animationData,
            autoplay: true,
          }}
        />
      )}
    </div>
  );
};
PageLoading.defaultProps = {
  prefixCls: 'pxx',
};
export default PageLoading;
