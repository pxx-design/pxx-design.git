import React, { useMemo } from 'react';
import Text from '../text';
import Input from '../input';
import classnames from 'classnames';
import { setFormData } from '@parallel-line/utils';
import { isUndefined, get, isFunction } from 'lodash-es';
import { Form as AntForm, Button, Space, Cascader } from 'antd';

interface FormProps {
  form?: any;
  prefixCls?: string;
  className?: string;
  onCancel?: () => void;
  loading?: boolean;
  children: React.ReactNode;
  onFinish: (params: any) => any;
}

const Form = (props: FormProps) => {
  const onFinish = (formData: any = {}) => {
    const params = setFormData(formData);
    props.onFinish(params);
  };
  const prefixCls = `${props.prefixCls}-form`;
  return (
    <AntForm
      colon={false}
      form={props.form}
      onFinish={onFinish}
      className={classnames(prefixCls, props.className)}
    >
      {props.children}
      <AntForm.Item className={`${prefixCls}-drawer-footer`}>
        <Space>
          <Button onClick={props.onCancel}>取消</Button>
          <Button type="primary" htmlType="submit" loading={props.loading}>
            确定
          </Button>
        </Space>
      </AntForm.Item>
    </AntForm>
  );
};

Form.defaultProps = {
  prefixCls: 'pxx',
  onFinish: () => {},
  onCancel: () => {},
};

interface Item {
  props?: any;
  type?: string;
  rules?: any[];
  name: string;
  hidden?: boolean;
  noStyle?: boolean;
  required?: boolean;
  initialValue?: any;
  label?: React.ReactNode;
  render?: (
    item: Item,
    initialValue: any,
    initialValues: any,
  ) => React.ReactNode;
}

interface ItemsProps {
  prefixCls?: string;
  list: Item[];
  initialValues?: any;
}

const Items = (props: ItemsProps) => {
  const render = useMemo(() => {
    const componentMap: any = {
      Cascader,
      Input,
    };
    return props.list?.map((t: Item, key: number) => {
      const Component = componentMap[t.type || 'Input'];
      const initialValue = isUndefined(t.initialValue)
        ? get(props.initialValues, t.name)
        : t.initialValue;
      const formItemProps: any = {
        initialValue,
        name: t.name,
        rules: t.rules,
        label: t.label,
        hidden: t.hidden,
        noStyle: t.noStyle,
        key: t.name || key,
      };
      if (t.required) formItemProps['required'] = t.required;
      return (
        <AntForm.Item {...formItemProps}>
          {isFunction(t.render) ? (
            t.render(t, initialValue, props.initialValues)
          ) : (
            <Component {...t.props} />
          )}
        </AntForm.Item>
      );
    });
  }, [props.list]);
  return <>{render}</>;
};

Items.defaultProps = {
  prefixCls: 'pxx',
  list: [],
};

interface CardProps extends ItemsProps {
  title?: string;
}

const Card = (props: CardProps) => {
  const prefixCls = `${props.prefixCls}-form`;
  return (
    <div className={`${prefixCls}-card`}>
      {!!props.title && (
        <Text block fs={16} fw={700} mb={24} ml={16}>
          {props.title}
        </Text>
      )}
      <Items list={props.list} initialValues={props.initialValues} />
    </div>
  );
};

Card.defaultProps = {
  prefixCls: 'pxx',
};

Form.Card = Card;
Form.Items = Items;

export default Form;
