/**
 * title: 基本使用
 * desc: 基本使用。
 */

import React from 'react';
import { Text } from '@parallel-line/components';

export default () => (
  <>
    <Text>default</Text>

    <Text mt={10}>marginTop: 10</Text>

    <Text ml={10}>marginLeft: 10</Text>

    <Text pl={10} pt={10}>
      paddingLeft: 10; paddingTop: 10
    </Text>

    <Text fs={20} fw="bold">
      fontSize: 20; fontWeight: bold
    </Text>
  </>
);
