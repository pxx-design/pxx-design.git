import React from 'react';
import { Property } from 'csstype';
import classnames from 'classnames';

interface TextProps {
  className?: string;
  block?: boolean;
  color?: Property.Color;
  inlineBlock?: boolean;
  opacity?: number;
  /**
   * @description fontSize
   */
  fs?: number;
  /**
   * @description fontWeight
   */
  fw?: Property.FontWeight;
  /**
   * @description marginTop
   */
  mt?: number | string;
  /**
   * @description marginBottom
   */
  mb?: number | string;
  /**
   * @description marginLeft
   */
  ml?: number | string;
  /**
   * @description marginRight
   */
  mr?: number | string;
  /**
   * @description paddingTop
   */
  pt?: number | string;
  /**
   * @description paddingBottom
   */
  pb?: number | string;
  /**
   * @description paddingLeft
   */
  pl?: number | string;
  /**
   * @description paddingRight
   */
  pr?: number | string;
  children: React.ReactNode;
}

const Text: React.FC<TextProps> = (props) => {
  const style: React.CSSProperties = {
    fontSize: props.fs,
    lineHeight: props.fs ? `${props.fs + 8}px` : undefined,
    fontWeight: props.fw,
    color: props.color || `rgba(0,0,0,.${props.opacity})`,
    marginTop: props.mt,
    marginBottom: props.mb,
    marginLeft: props.ml,
    marginRight: props.mr,
    paddingTop: props.pt,
    paddingBottom: props.pb,
    paddingLeft: props.pl,
    paddingRight: props.pr,
    display: classnames({ block: props.block, inlineBlock: props.inlineBlock }),
  };
  return (
    <div className={classnames(props.className)} style={style}>
      {props.children}
    </div>
  );
};

Text.defaultProps = {
  mt: 0,
  ml: 0,
  mb: 0,
  mr: 0,
  pt: 0,
  pl: 0,
  pb: 0,
  pr: 0,
  fs: 14,
  fw: 400,
  opacity: 85,
};

export default Text;
