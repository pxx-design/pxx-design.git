---
title: Input 输入框
group:
  title: Input 输入框
  order: 14
---

# Input 输入框

通过鼠标或键盘输入内容，是最基础的表单域的包装。

## 代码演示

<Space>
  <code src="./demos/basic.tsx" ></code>
  <code src="./demos/addon.tsx" ></code>
  <code src="./demos/search.tsx" ></code>
  <code src="./demos/textarea.tsx" ></code>
  <code src="./demos/password.tsx" ></code>
  <code src="./demos/group.tsx" ></code>
</Space>

<API src="./input.tsx"></API>
