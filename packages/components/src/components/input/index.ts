import InlineInput, { Group, Search, TextArea, Password } from './input';

type InternalInputType = typeof InlineInput;

interface InputInterface extends InternalInputType {
  Group: typeof Group;
  Search: typeof Search;
  TextArea: typeof TextArea;
  Password: typeof Password;
}

const Input = InlineInput as InputInterface;

Input.Group = Group;
Input.Search = Search;
Input.TextArea = TextArea;
Input.Password = Password;

export type { InputProps } from './input';

export default Input;
