/**
 * title: 文本域
 * desc: 用于多行输入。
 */

import React from 'react';
import { Input } from '@parallel-line/components';

export default () => <Input.TextArea rows={4} />;
