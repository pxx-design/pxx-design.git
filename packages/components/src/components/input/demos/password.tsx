/**
 * title: 密码框
 * desc: 密码框。
 */

import React from 'react';
import { Space } from 'antd';
import { Input } from '@parallel-line/components';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';

export default () => (
  <>
  <Space direction="vertical">
    <Input.Password placeholder="input password" />
    <Input.Password
      placeholder="input password"
      iconRender={(visible) =>
        visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
      }
    />
  </Space>
  </>
);
