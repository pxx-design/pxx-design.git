/**
 * title: 基本使用
 * desc: 基本使用。
 */

import React from 'react';
import { Input } from '@parallel-line/components';

export default () => (
  <>
    <Input placeholder="Basic usage" />
  </>
);
