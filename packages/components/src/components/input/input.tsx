import * as React from 'react';
import { trim } from 'lodash-es';
import classNames from 'classnames';
import { useUpdateEffect } from 'ahooks';
import Input, {
  InputProps as AntInputProps,
  GroupProps,
  SearchProps,
  PasswordProps,
} from 'antd/es/input';
import { TextAreaProps, TextAreaRef } from 'antd/es/input/TextArea';
import { LiteralUnion } from 'antd/es/_util/type';
import { CloseCircleFilled } from '@ant-design/icons';

export interface InputProps
  extends Omit<
    AntInputProps,
    'defaultValue' | 'value' | 'inputMode' | 'is' | 'onChange'
  > {
  /**
   * @description 带标签的 input，设置前置标签
   */
  addonAfter?: React.ReactNode;
  /**
   * @description 带标签的 input，设置后置标签
   */
  addonBefore?: React.ReactNode;
  /**
   * @description 可以点击清除图标删除内容
   */
  allowClear?: boolean;
  /**
   * @description 是否有边框
   */
  bordered?: boolean;
  /**
   * @description 聚焦时自动全选
   */
  selectInFocus?: boolean;
  /**
   * @description 输入框默认内容
   */
  defaultValue?: string;
  /**
   * @description 是否禁用状态，默认为 false
   */
  disabled?: boolean;
  /**
   * @description 输入框的 id
   */
  id?: string;
  /**
   * @description 最大长度
   */
  maxLength?: number;
  /**
   * @description 带有前缀图标的 input
   */
  prefix?: React.ReactNode;
  /**
   * @description 控件大小。注：标准表单内的输入框大小限制为 large
   */
  size?: 'small' | 'middle' | 'large';
  /**
   * @description 带有后缀图标的 input
   */
  suffix?: React.ReactNode;
  /**
   * @description 声明 input 类型，同原生 input 标签的 type 属性 (请直接使用 Input.TextArea 代替 type="textarea")
   */
  type?: LiteralUnion<
    | 'button'
    | 'checkbox'
    | 'color'
    | 'date'
    | 'datetime-local'
    | 'email'
    | 'file'
    | 'hidden'
    | 'image'
    | 'month'
    | 'number'
    | 'password'
    | 'radio'
    | 'range'
    | 'reset'
    | 'search'
    | 'submit'
    | 'tel'
    | 'text'
    | 'time'
    | 'url'
    | 'week',
    string
  >;
  /**
   * @description 输入框内容
   */
  value?: string;
  /**
   * @description 输入框内容变化时的回调
   */
  onChange?: (value: string | undefined) => void;
  /**
   * @description allowClear 为true时，点击close图标的回调
   */
  onClose?: () => void;
  /**
   * @description 按下回车的回调
   */
  onPressEnter?: React.KeyboardEventHandler<HTMLInputElement>;
}

const InlineInput = React.forwardRef<Input, InputProps>((props, ref) => {
  const {
    suffix,
    allowClear,
    selectInFocus,
    prefixCls: customizePrefixCls,
    onChange,
    onClose,
    ...inputProps
  } = props;
  const inputRef = React.useRef<Input>(null);
  const [value, setValue] = React.useState(props.value || props.defaultValue);
  const pxxPrefixCls = `${customizePrefixCls}-input`;

  React.useImperativeHandle(ref, () => inputRef.current!);

  useUpdateEffect(() => {
    setValue(props.value);
  }, [props.value]);

  const handleReset = () => {
    setValue('');
    onChange?.('');
    onClose?.();
  };
  const renderClearIcon = (prefixCls: string) => {
    const { disabled, readOnly } = props;
    if (!allowClear) {
      return null;
    }
    const needClear = !disabled && !readOnly && value;
    const closeClassName = `${prefixCls}-clear-icon`;
    return (
      <CloseCircleFilled
        onClick={handleReset}
        className={classNames(
          {
            [`${closeClassName}-hidden`]: !needClear,
          },
          closeClassName,
        )}
        role="button"
      />
    );
  };
  const renderSuffix = (prefixCls: string) => {
    if (suffix || allowClear) {
      return (
        <>
          {renderClearIcon(prefixCls)}
          {suffix}
        </>
      );
    }
    return null;
  };

  const handleChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    const trimValue = trim(e.target.value);
    const _value = trimValue === '' ? undefined : trimValue;
    setValue(_value);
    onChange?.(_value);
  };
  return (
    <Input
      ref={inputRef}
      suffix={renderSuffix(pxxPrefixCls)}
      value={value}
      onChange={handleChange}
      prefixCls={pxxPrefixCls}
      {...inputProps}
      onFocus={(event) => {
        if (selectInFocus) inputRef.current?.select();
        inputProps.onFocus?.(event);
      }}
    />
  );
});

InlineInput.defaultProps = {
  prefixCls: 'pxx',
  placeholder: '请输入',
  allowClear: true,
};

export const Group: React.FC<GroupProps> = (props) => (
  <Input.Group {...props} prefixCls={`${props.prefixCls}-input-group`} />
);

Group.defaultProps = {
  prefixCls: 'pxx',
};

export const Search = React.forwardRef<Input, SearchProps>((props, ref) => (
  <Input.Search
    ref={ref}
    {...props}
    prefixCls={`${props.prefixCls}-input-search`}
    inputPrefixCls={`${props.prefixCls}-input`}
  />
));

Search.defaultProps = {
  prefixCls: 'pxx',
};

export const TextArea = React.forwardRef<TextAreaRef, TextAreaProps>(
  (props, ref) => (
    <Input.TextArea
      ref={ref}
      {...props}
      prefixCls={`${props.prefixCls}-input`}
    />
  ),
);

TextArea.defaultProps = {
  prefixCls: 'pxx',
};

export const Password = React.forwardRef<Input, PasswordProps>((props, ref) => (
  <Input.Password
    ref={ref}
    {...props}
    prefixCls={`${props.prefixCls}-input-password`}
    inputPrefixCls={`${props.prefixCls}-input`}
  />
));

Password.defaultProps = {
  prefixCls: 'pxx',
};

export type { SearchProps, GroupProps };
export default InlineInput;
