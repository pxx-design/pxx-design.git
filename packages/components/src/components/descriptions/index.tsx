import React, {ReactNode} from 'react'
import classNames from 'classnames'


interface DescriptionsProps {
  /**
   * @description 是否水平排列
   * @default false
   */
  isHorizontal?: boolean,
  children: React.ReactNode;
  style?: Record<string, string | number>;
}

const prefixCls = 'pxx-descriptions'

const Descriptions = (props: DescriptionsProps) => {
  const {isHorizontal = false, style} = props;
  return <div style={style} className={classNames(`${prefixCls}-wrapper`, {
    [`${prefixCls}-descriptions-horizontal`]: isHorizontal,
    [`${prefixCls}-descriptions-vertical`]: !isHorizontal
  })}>
    {
      props.children
    }
  </div>
};

interface DescriptionsItemProps {
  /**
   * @description 内容描述
   * @default -
   */
  label?: ReactNode;
  children: React.ReactNode;
}

export const Item = (props: DescriptionsItemProps) => {
  const {label, children} = props

  return <div className={`${prefixCls}-description`}>
    <div className={`${prefixCls}-label`}>{label}：</div>
    <div className={`${prefixCls}-value`}>{children}</div>
  </div>
}

Descriptions.Item = Item
// @ts-ignore
export default Descriptions;
