import React from 'react';
import {Descriptions} from '@parallel-line/components'

const Demo = () => {
  return <Descriptions isHorizontal={true}>
    <Descriptions.Item label={'姓名'}>李斯</Descriptions.Item>
    <Descriptions.Item label={'年龄'}>23</Descriptions.Item>
    <Descriptions.Item label={'性别'}>男</Descriptions.Item>
    <Descriptions.Item label={'出生年月'}>1999/09/21</Descriptions.Item>
    <Descriptions.Item label={'籍贯'}>沈阳</Descriptions.Item>
  </Descriptions>
}

export default Demo
