import React from 'react';
import {Descriptions} from '@parallel-line/components'

const Demo = () => {
  return <Descriptions>
    <Descriptions.Item label={'姓名'}>张三</Descriptions.Item>
    <Descriptions.Item label={'年龄'}>33</Descriptions.Item>
    <Descriptions.Item label={'性别'}>女</Descriptions.Item>
    <Descriptions.Item label={'出生年月'}>--</Descriptions.Item>
    <Descriptions.Item label={'籍贯'}>大连</Descriptions.Item>
  </Descriptions>
}

export default Demo
