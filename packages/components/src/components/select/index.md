---
title: Select 选择框
group:
  title: Select 选择框
  order: 14
---

# Select 选择框

基于 antd Select
### 基本使用
<code src="./demos/index1.tsx"></code>

使用 fieldNames 处理 options 数据中字段与 select 所需字段不统一问题
<code src="./demos/index2.tsx"></code>

配合字典使用
<code src="./demos/index3.tsx"></code>

<API></API>
<span style="font-size: 12px">
更多属性参考<a href="https://ant.design/components/select-cn/" target="_blank">antd select</a>
</span>
