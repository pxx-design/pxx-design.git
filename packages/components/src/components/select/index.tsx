import React, { useRef, forwardRef } from 'react';
import { Select } from 'antd';
import classNames from 'classnames';
import { isEmpty } from 'lodash-es';
import { useSize, useBoolean } from 'ahooks';
import { MoreOutlined, DownOutlined } from '@parallel-line/icons';

import type {
  SelectValue,
  RefSelectProps,
  SelectProps as AntdSelectProps,
} from 'antd/es/select';

interface SelectProps<VT = SelectValue>
  extends Omit<
    AntdSelectProps<VT>,
    'maxLength' | 'filterOption' | 'internalProps'
  > {
  noWrap?: boolean;
  /**
   * @description option中value和label在数据中相对应的字段
   * @default {label: 'value'; value: 'key';}
   */
  fieldNames?: {
    label: string;
    value: string;
  };
  value?: any;
  maxLength?: number;
  filterOption?:
    | boolean
    | ((inputValue: string, option?: Record<string, any>) => boolean);
}

const InlineSelect = forwardRef<RefSelectProps, SelectProps>((props, ref) => {
  const {
    prefixCls = 'pxx-select',
    fieldNames = { label: 'value', value: 'key' },
    labelInValue,
    noWrap = true,
    children,
    onChange,
    options,
    allowClear = true,
    showSearch = true,
    placeholder = '请选择',
    suffixIcon = <DownOutlined className="ant-select-suffix" />,
    ...selectProps
  } = props;
  const wrapperRef = useRef<HTMLDivElement>(null);
  const [isMore, { toggle }] = useBoolean();
  const [hasVal, { toggle: setHasVal }] = useBoolean(
    !isEmpty(props.value) || !isEmpty(props.defaultValue),
  );
  const { width: wrapperWidth = 0 } = useSize(wrapperRef);

  const handleOptions = () => {
    const { label, value } = fieldNames;
    return options?.map((item: Record<string, any>) => {
      const { [label]: l, [value]: v } = item;
      return {
        ...item,
        label: l,
        value: v,
      };
    });
  };

  const handleChange = (value: any, option: any) => {
    setHasVal(!isEmpty(value));
    onChange?.(value, option);
    setTimeout(() => {
      if (wrapperRef.current) {
        const overflowDom = wrapperRef.current.getElementsByClassName(
          'ant-select-selection-overflow',
        )[0];
        if (overflowDom) {
          let overflowWidth = 24;
          overflowDom.childNodes?.forEach((child, inx) => {
            if (inx === overflowDom.childNodes.length - 1) return;
            overflowWidth += (child as Element).clientWidth;
          });
          toggle(overflowWidth > wrapperWidth);
        }
        console.log(overflowDom);
      }
    }, 66);
  };

  return (
    <div
      className={classNames(`${prefixCls}-wrapper`, {
        [`${prefixCls}-multiple`]: selectProps.mode === 'multiple',
        [`${prefixCls}-multiple-more`]: isMore,
        [`${prefixCls}-no-wrap`]: noWrap,
        [`${prefixCls}-has-value`]: hasVal,
      })}
      ref={wrapperRef}
      style={selectProps.style}
    >
      <Select
        {...selectProps}
        ref={ref}
        suffixIcon={suffixIcon}
        showSearch={showSearch}
        allowClear={allowClear}
        placeholder={placeholder}
        value={props.value}
        options={handleOptions()}
        onChange={handleChange}
      >
        {children}
      </Select>
      {selectProps.mode === 'multiple' &&
        noWrap &&
        (isMore ? (
          <div className={`${prefixCls}-more`}>
            <MoreOutlined />
          </div>
        ) : (
          <div className={`${prefixCls}-suffix`}>
            <DownOutlined />
          </div>
        ))}
      {isMore && selectProps.mode === 'multiple' && (
        <div className={`${prefixCls}-more`}>
          <MoreOutlined />
        </div>
      )}
    </div>
  );
});
type SelectType = typeof InlineSelect;

interface SelectInstance extends SelectType {
  Option: typeof Select.Option;
  OptGroup: typeof Select.OptGroup;
}

const InternalSelect = InlineSelect as SelectInstance;
InternalSelect.Option = Select.Option;
InternalSelect.OptGroup = Select.OptGroup;

export default InternalSelect;
