import React from 'react';
import { Select } from '@parallel-line/components';
import { obtainDic } from '@parallel-line/utils';

import init from './init';

init();

export default () => {
  const options: any[] = obtainDic({
    dicKey: 'common.common.gender'
  })

  return <Select
    style={{ width: 300 }}
    options={options}
  />;
};
