import React from 'react';
import { Select } from '@parallel-line/components';
import init from './init';

init();

const list = [
  { key: 1, value: '北京' },
  { key: 2, value: '上海' },
];

export default () => {
  return (
    <Select
      options={list}
      style={{ width: 300 }}
    />
  );
};
