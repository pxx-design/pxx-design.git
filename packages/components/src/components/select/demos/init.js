import { global } from '@parallel-line/utils';

export default () => {
  global.dictionary = window.dictionary;
};
