import React from 'react';
import { Select } from '@parallel-line/components';

import init from './init';

init();

const options: any[] = [
  {
    name: '张山',
    IdCard: 1,
    age: 15,
    sex: 'man'
  },
  {
    name: '李斯',
    IdCard: 2,
    age: 27,
    sex: 'man'
  },
  {
    name: '王舞',
    IdCard: 3,
    age: 33,
    sex: 'woman'
  },
  {
    name: '陈留',
    IdCard: 4,
    age: 62,
    sex: 'man'
  },
  {
    name: '陈长春',
    IdCard: 5,
    age: 62,
    sex: 'man'
  },
  {
    name: '镇水神兽',
    IdCard: 6,
    age: 62,
    sex: 'man'
  },
  {
    name: '啛啛喳喳',
    IdCard: 7,
    age: 62,
    sex: 'man'
  }
]
const fieldNames: any = {label: 'name', value: 'IdCard'}

export default () => {
  return <Select
    labelInValue
    style={{ width: 300 }}
    options={options}
    mode={'multiple'}
    defaultValue={[2,3,4]}
    onChange={(a,b) => console.log(a,b)}
    fieldNames={fieldNames}
  />;
};
