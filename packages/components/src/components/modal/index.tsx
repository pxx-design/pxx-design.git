import React, { useState, forwardRef, useImperativeHandle } from 'react';
import { Modal, ModalProps } from 'antd';
import classNames from 'classnames';
import { isFunction } from 'lodash-es';

interface ModalFuncProps extends Omit<ModalProps, 'footer' | 'visible'> {
  uniqueKey?: string;
  content: React.ReactNode;
  visible?: boolean;
}
interface ModalInstance {
  close: () => void;
}

const InlineModal = forwardRef<ModalInstance, ModalFuncProps>(
  ({ prefixCls: customizePrefixCls, ...props }, ref) => {
    const prefixCls = `${customizePrefixCls}-modal`;
    const Component: any = props.children || props.content;
    const [visible, setVisible] = useState(props.visible);

    const onClose = () => {
      setVisible(false);
    };

    useImperativeHandle(ref, () => ({ close: onClose }));

    return (
      <Modal
        destroyOnClose
        {...props}
        footer={null}
        visible={visible}
        className={classNames(prefixCls, props.className)}
        onCancel={onClose}
        afterClose={props.afterClose}
      >
        {React.Children.map(Component, (child) => {
          if (isFunction(child.type)) {
            return React.cloneElement(child, {
              onClose: (...arg: any[]) => {
                onClose();
                child?.props?.onClose?.(...arg);
              },
            });
          }
          return child;
        })}
      </Modal>
    );
  },
);

InlineModal.defaultProps = {
  prefixCls: 'pxx',
};

export default InlineModal;
