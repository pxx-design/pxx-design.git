import React from 'react';
import classNames from 'classnames';
import {useNamespace} from "@parallel-line/common/es/hooks";

import {Skeleton} from 'antd';

interface FormProps {
  prefixCls?: string;
  className?: string;
  loading?: boolean;
  children?: React.ReactNode;
  style?: React.CSSProperties;
}

const Form = (props: FormProps) => {
  const [, bem] = useNamespace('whiteboard', props.prefixCls);
  return (
    <div className={classNames(bem('form'), props.className)} style={props.style}>
      <div className={bem('container')}>
        {
          props.loading ?
            <>
              <Skeleton active/>
              <Skeleton active/>
              <Skeleton active/>
            </> :
            props.children
        }
      </div>
    </div>
  );
};

export default Form;
