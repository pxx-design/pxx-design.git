import React from 'react';
import Form from './form';

interface WhiteboardInterface {
  Form: typeof Form;
}

const Whiteboard: WhiteboardInterface = {
  Form
};

export default Whiteboard

