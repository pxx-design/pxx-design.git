/**
 * title: 基本使用
 * desc: 基本使用。
 */

import React from 'react';
import { Divider, Space } from 'antd';
import styled from 'styled-components';
import { Upload } from '@parallel-line/components';

const NSpace = styled(Space)`
  background-color: white;
  width: 100%;
  min-height: 100px;
  padding: 16px;
`;

function UploadDemo() {
  const [value, onChange] = React.useState(['10806']);
  return (
    <NSpace direction="vertical">
      <Upload />

      <Divider orientation="left">限制可选择的文件格式</Divider>
      <Upload accept=".doc,.docx,.pdf" />

      <Divider orientation="left">限制可选择的文件数量</Divider>
      <Upload maxCount={2} />

      <Divider orientation="left">具备默认的初始化值</Divider>
      <Upload
        value={value}
        defaultValue={['7c9d00df655c427a87de5fb6501ab700.png']}
        onChange={onChange}
      />

      <Divider orientation="left">禁用状态</Divider>
      <Upload
        disabled
        value={value}
        defaultValue={['7c9d00df655c427a87de5fb6501ab700.png']}
        onChange={onChange}
      />
    </NSpace>
  );
}

export default UploadDemo;
