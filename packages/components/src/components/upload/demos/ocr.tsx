/**
 * title: OCR识别
 * desc: OCR识别。
 */

import React from 'react';
import { Upload } from '@parallel-line/components';

function UploadDemo() {
  return <Upload.OCR fileType="1001" />;
}

export default UploadDemo;
