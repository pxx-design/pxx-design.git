/**
 * title: 照片墙
 * desc: 用户可以上传图片并在列表中显示缩略图。当上传照片数到达限制后，上传按钮消失。
 */

import React from 'react';
import { useBoolean } from 'ahooks';
import styled from 'styled-components';
import { Divider, Space, Form, Switch } from 'antd';
import { Upload } from '@parallel-line/components';

const NSpace = styled(Space)`
  background-color: white;
  width: 100%;
  min-height: 100px;
  padding: 16px;
`;

function UploadDemo() {
  const [value, onChange] = React.useState(['10806']);
  const [disabled, { toggle }] = useBoolean();
  return (
    <NSpace direction="vertical">
      <Form layout="inline" style={{ marginBottom: 16 }}>
        <Form.Item label="是否禁用">
          <Switch checked={disabled} onChange={toggle} />
        </Form.Item>
      </Form>
      <Upload
        multiple
        maxCount={8}
        listType="picture-list"
        disabled={disabled}
      />

      <Divider orientation="left">具备默认的初始化值</Divider>
      <Upload
        multiple
        maxCount={8}
        disabled={disabled}
        listType="picture-list"
        value={value}
        defaultValue={[
          'https://dev-1302801272.cos.ap-guangzhou.myqcloud.com/undefined/undefined/302129010824/7c9d00df655c427a87de5fb6501ab700.png',
        ]}
        onChange={onChange}
      />
      <div>Upload-value: {value.join(' | ')}</div>
    </NSpace>
  );
}

export default UploadDemo;
