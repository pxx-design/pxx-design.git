import * as React from 'react';
import Image, { ImageProps } from '../image';
import { useBoolean } from 'ahooks';
import { isFunction } from 'lodash-es';
import { global } from '@parallel-line/utils';
import { imageMap, ImageType } from './imageMap';
import InternalUpload, { UploadProps } from './Upload';

interface OCRResponse {
  success?: boolean;
  msg?: string;
}

export interface OCRProps<T = unknown>
  extends Omit<UploadProps, 'type' | 'value' | 'defaultValue' | 'onChange'> {
  fileType: ImageType;
  value?: T & OCRResponse;
  defaultValue?: string[];
  onChange?: (value: T, urls: string[]) => void;
}

function validator<T>(_: T, value: OCRResponse) {
  if (value?.success === false) {
    return Promise.reject(value.msg);
  }

  return Promise.resolve(true);
}

const InternalOCR = <T extends Record<string, any> = any>(
  props: OCRProps<T>,
  ref: React.Ref<unknown>,
) => {
  const {
    data,
    fileType,
    disabled,
    serviceName,
    moduleName,
    fileSource = 12,
    businessId = global?.companyInfo?.id ?? localStorage.getItem('pxx_userId'),
    value,
    defaultValue,
    onChange,
    ...restProps
  } = props;

  const [loading, { toggle }] = useBoolean(props.loading);
  const [imageProps, setImageProps] = React.useState<ImageProps>({
    ...imageMap[fileType],
    ...props.imageProps,
    onDelete: () => {
      const newImageProps = {
        ...imageProps,
        src: imageMap[fileType].src,
        preview: false,
        delete: false,
      };
      setImageProps(newImageProps);
      props.onImageProps?.(newImageProps);
    },
  });

  const ocrProps: UploadProps = {
    action: '/host/ocr',
    showUploadList: false,
    disabled: disabled || loading,
    data: (file) => {
      return {
        waterMark: 0,
        serviceName,
        moduleName,
        businessId,
        fileSource,
        fileType,
        ...(isFunction(data) ? data(file) : data),
      };
    },
    ...restProps,
    listType: 'picture',
    placeholder: <Image {...imageProps} />,
    imageProps,
    onImageProps: (imageProps) => {
      const newImageProps = { ...imageMap[fileType], ...imageProps };
      setImageProps(newImageProps);
      props.onImageProps?.(newImageProps);
    },

    loading,
    onLoading: (loading) => {
      toggle(loading);
      props.onLoading?.(loading);
    },
  };
  return <InternalUpload ref={ref} {...ocrProps} />;
};

interface OCRInterface {
  <T extends Record<string, any> = any>(
    props: OCRProps<T> & { ref?: React.Ref<unknown> },
  ): React.ReactElement;
  displayName: string;
  validator: typeof validator;
}

const OCR: OCRInterface = React.forwardRef(InternalOCR) as any;
OCR.validator = validator;
OCR.displayName = 'OCR';

export { OCR };
