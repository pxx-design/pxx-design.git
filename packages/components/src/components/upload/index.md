---
title: Upload 上传
---

# Upload 上传

文件选择上传和拖拽上传控件。

## 何时使用
上传是将信息（网页、文字、图片、视频等）通过网页或者上传工具发布到远程服务器上的过程。

- 当需要上传一个或一些文件时。
- 当需要展现上传的进度时。
- 当需要使用拖拽交互时。

⚠️注意： 目前线上环境无法对`proxy`生效，所以`demo`需要在开发环境下查看。

## 代码演示

<!-- <AccessToken></AccessToken> -->

<code src="./demos/basic.tsx"></code>
<code src="./demos/avatar.tsx"></code>
<code src="./demos/list.tsx"></code>
<code src="./demos/ocr.tsx"></code>


<API src="./Upload.tsx" exports='["default"]'></API>
<!-- <API src="./OCR.tsx" exports='["OCR"]'></API> -->
