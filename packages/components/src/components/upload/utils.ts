interface OCRResponse {
  success?: boolean;
  msg?: string;
}

export const imgAccept = ['image/jpeg', 'image/png', 'image/jpg'];
export const accept = imgAccept.join(',');

export function validator<T>(_: T, value: OCRResponse) {
  if (value?.success === false) {
    return Promise.reject(value.msg);
  }

  return Promise.resolve(true);
}
