import { OCR, OCRProps } from './OCR';
import InternalUpload, { Dragger, DraggerProps, UploadProps } from './Upload';

export type { OCRProps, DraggerProps, UploadProps };

type UploadType = typeof InternalUpload;

interface UploadInterface extends UploadType {
  OCR: typeof OCR;
  Dragger: typeof Dragger;
}

export const Upload = InternalUpload as UploadInterface;

Upload.OCR = OCR;
Upload.Dragger = Dragger;

export default Upload;
