import { IDCard1 } from './IDCard1';
import { IDCard2 } from './IDCard2';
import { business2 } from './business2';
import { licence } from './licence';

interface ImageInfo {
  src: string;
  width: number;
  height: number;
  preview: boolean;
}

export type ImageType = '1001' | '1003' | '1002' | '1004' | '1005' | '1006';

export const imageMap: Record<ImageType, ImageInfo> = {
  1001: {
    src: IDCard1,
    width: 278,
    height: 178,
    preview: false,
  },
  1003: {
    src: IDCard1,
    width: 278,
    height: 178,
    preview: false,
  },
  1002: {
    src: IDCard2,
    width: 278,
    height: 178,
    preview: false,
  },
  1004: {
    src: IDCard2,
    width: 278,
    height: 178,
    preview: false,
  },
  1005: {
    src: business2,
    width: 188,
    height: 267,
    preview: false,
  },
  1006: {
    src: licence,
    width: 278,
    height: 178,
    preview: false,
  },
};
