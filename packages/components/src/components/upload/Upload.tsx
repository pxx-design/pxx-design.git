import * as React from 'react';
import classNames from 'classnames';
import { omit, isFunction } from 'lodash-es';
import { useBoolean, useUpdateEffect } from 'ahooks';
import { PaperClipOutlined } from '@ant-design/icons';
import { message, Upload as RcUpload, Popconfirm } from 'antd';
import { Spin } from '@parallel-line/common';
import { global, calc } from '@parallel-line/utils';
import { useNamespace } from '@parallel-line/common/es/hooks';
import {
  DeleteOutlined,
  ExcelOutlined,
  LoadingOutlined,
  PdfOutlined,
  PlusCircleFill,
  PlusOutlined,
  PptOutlined,
  WordOutlined,
} from '@parallel-line/icons';
import { DraggerProps, UploadProps as RcUploadProps } from 'antd/es/upload';
import { UploadFile } from 'antd/es/upload/interface';
import Image, { ImageProps } from '../image';

declare module 'antd/es/upload/interface' {
  interface UploadFile {
    delete?: boolean;
  }
}

export type UploadListType = 'text' | 'picture' | 'picture-list';

const MIMETypeMap: Record<string, string> = {
  '.pdf': 'application/pdf',
  '.png': 'image/png',
  '.jpg': 'image/jpeg',
  '.jpeg': 'image/jpeg',
  '.doc': 'application/msword',
  '.docx':
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  '.xls': 'application/vnd.ms-excel',
  '.xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
};

interface UploadProps
  extends Omit<
    RcUploadProps,
    'type' | 'listType' | 'transformFile' | 'onChange'
  > {
  action?: string;
  /**
   * 上传列表的内建样式，支持三种基本样式 `text`, `picture` 和 `picture-list`
   */
  listType?: UploadListType;
  children?: React.ReactNode;
  placeholder?: React.ReactNode;
  placeholderProps?: UploadPlaceholderProps;
  accessTokenKey?: string;

  /** 可上传文件最大大小，单位为M */
  maxSize?: number;
  /** 可上传文件最小大小，单位为KB */
  minSize?: number;

  /** 业务ID， 默认为 企业ID ?? 用户ID */
  businessId?: string;
  /** 文件来源 */
  fileSource?: number;
  /** 服务名称 */
  serviceName?: string;
  /** 模块名称 */
  moduleName?: string;

  /**
   * @description 文件ID列表
   */
  value?: string[];
  /**
   * @description 图片URL列表 或 文件名称列表，不通过外部新增文件只需第一次传递即可
   */
  defaultValue?: string[];
  /**
   * @description 文件列表变化时的回调
   */
  onChange?: (value: string[], urls: string[]) => void;

  imageProps?: ImageProps;
  onImageProps?: (imageProps?: ImageProps) => void;

  loading?: boolean;
  onLoading?: (loading?: boolean) => void;
}

export const Dragger = RcUpload.Dragger;

export const imgAccept = ['image/jpeg', 'image/png', 'image/jpg'];

export function getBase64(img: File, callback: (imageUrl: string) => void) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result as string));
  reader.readAsDataURL(img);
}

interface UploadPlaceholderProps {
  prefixCls?: string;
  className?: string;
  disabled?: boolean;
  style?: React.CSSProperties;
  width?: number | string;
  height?: number | string;
  text?: string;
}

const Placeholder: React.FC<UploadPlaceholderProps> = ({
  disabled,
  prefixCls,
  className,
  width,
  height,
  style,
  children,
  text,
}) => {
  const [, bem] = useNamespace('upload', prefixCls);
  return (
    <div
      className={classNames(bem('placeholder', { disabled }), className)}
      style={{ width, height, ...style }}
    >
      <span className={bem('placeholder-icon')}>
        <PlusCircleFill />
      </span>

      <span>{children ?? text}</span>
    </div>
  );
};

function useShamMemo<T>(factory: () => T): T {
  return factory();
}

const Upload = React.forwardRef<unknown, UploadProps>((props, ref) => {
  const {
    headers,
    listType = 'text',
    maxCount = -1,
    placeholderProps,
    accessTokenKey = 'pxx_accessToken',
    action = '/host/uploadFile',
  } = props;
  const {
    data,
    prefixCls,
    serviceName,
    moduleName,
    fileSource = 12,
    value,
    defaultValue,
    disabled,
    onChange,
    businessId = global?.companyInfo?.id ?? localStorage.getItem('pxx_userId'),
    ...restProps
  } = props;

  const [, bem] = useNamespace('upload', prefixCls);
  const [loading, { toggle }] = useBoolean(props.loading);
  const [status, { toggle: setStatus }] = useBoolean(!!value);
  const MIMETypes = React.useMemo(() => {
    if (props.accept) {
      return props.accept.split(',').reduce((p, c) => {
        if (MIMETypeMap[c]) p.push(MIMETypeMap[c]);
        else
          console.log(
            `${c} 不在MIMETypeMap里面，请参考MIME_types扩充: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types`,
          );
        return p;
      }, [] as string[]);
    }
    return undefined;
  }, [props.accept]);
  const [fileList, setFileList] = React.useState<UploadFile[]>(() => {
    return value
      ? value.map((uid, idx) => {
          if (listType === 'text') {
            return { uid, name: defaultValue?.[idx] ?? '', status: 'done' };
          }
          return { uid, name: uid, url: defaultValue?.[idx], status: 'done' };
        })
      : [];
  });

  const [imageProps, setImageProps] = React.useState<ImageProps | undefined>(
    () => {
      if (listType === 'picture') {
        return {
          width: 280,
          height: 180,
          delConfirm: true,
          src: !loading
            ? value
              ? defaultValue?.[0]
              : Image.PLACEHOLDER
            : Image.PLACEHOLDER,
          ...props.imageProps,
          disabled,
          onDelete: () => {
            setStatus(false);
            const newImageProps = {
              ...imageProps,
              src: Image.PLACEHOLDER,
              preview: false,
              delete: false,
            };
            setImageProps(newImageProps);
            props.onImageProps?.(newImageProps);
            onChange?.([], []);
          },
          preview: !!value,
          delete: !!value,
        };
      }

      return {
        width: 104,
        height: 104,
        delete: true,
        delConfirm: true,
        size: 'small',
        ...props.imageProps,
        disabled,
      };
    },
  );
  const {
    placeholder = (
      <Placeholder
        {...{
          text: '上传照片',
          width: imageProps?.width,
          height: imageProps?.height,
          prefixCls,
          disabled,
          ...placeholderProps,
          ...(listType === 'picture-list' ? { width: 122, height: 122 } : {}),
        }}
      />
    ),
  } = props;
  const rcUploadProps = useShamMemo(() => {
    let rcUploadProps: RcUploadProps = {
      ...omit(restProps, [
        'type',
        'prefixCls',
        'listType',
        'headers',
        'imageProps',
        'onImageProps',
        'value',
        'onChange',
        'loading',
        'onLoading',
      ]),
      disabled,
      className: bem(),
      showUploadList: false,
      action,
      headers: {
        Authorization: localStorage.getItem(accessTokenKey)!,
        ...headers,
      },
      listType:
        props.listType == null && props.listType === 'picture-list'
          ? 'picture-card'
          : 'text',
      data: (file) => {
        return {
          waterMark: 0,
          serviceName,
          moduleName,
          businessId,
          fileSource,
          ...(isFunction(data) ? data(file) : data),
        };
      },
      beforeUpload: (file) => {
        if (MIMETypes && !MIMETypes.includes(file.type)) {
          message.warning('不支持该文件格式类型上传，请重新选择');
          return false;
        }
        if (
          props.maxSize &&
          calc(`${file.size} / 1024 / 1024`) >= props.maxSize
        ) {
          message.warning(`上传文件大小不能大于${props.maxSize}M`);
          return false;
        }

        if (props.minSize && calc(`${file.size} / 1024`) <= props.minSize) {
          message.warning(`上传文件大小不能小于${props.minSize}KB`);
          return false;
        }
        return true;
      },
    };

    if (listType === 'picture' || listType === 'picture-list') {
      rcUploadProps = {
        accept: imgAccept.join(','),
        ...rcUploadProps,
      };
    }

    if (listType === 'picture') {
      rcUploadProps.onChange = (info) => {
        if (info.file.status === 'uploading') {
          toggle(true);
          props.onLoading?.(true);
          setStatus(true);
          return;
        }

        if (info.file.status === 'done') {
          if (info.file.originFileObj)
            getBase64(info.file.originFileObj, (imageUrl) => {
              const response = info.file?.response || {};
              const newImageProps = {
                ...imageProps,
                src: imageUrl,
                preview: true,
                delete: true,
              };

              toggle(false);
              setStatus(true);
              props.onLoading?.(false);
              setImageProps(newImageProps);
              props.onImageProps?.(newImageProps);

              if (response?.success && response?.data) {
                onChange?.([response.data], [imageUrl]);
              }
            });
        }
      };
    }

    if (listType === 'picture-list' || listType === 'text') {
      // rcUploadProps.fileList = fileList;
      if (maxCount !== -1 && fileList.length >= maxCount) {
        rcUploadProps.disabled = true;
      }

      rcUploadProps.onChange = (info) => {
        if (info.file.status === 'done') {
          if (info.file.originFileObj)
            getBase64(info.file.originFileObj, (imageUrl) => {
              const response = info.file?.response || {};
              const newFileList = [...fileList];
              const currentileIdx = newFileList.findIndex(
                (f) => f.uid === info.file.uid,
              );
              newFileList[currentileIdx].status = 'done';
              if (listType === 'picture-list') {
                newFileList[currentileIdx].url = imageUrl;
              }
              if (response?.success && response?.data) {
                newFileList[currentileIdx].uid = response.data;
              }
              updateFileList(newFileList);
            });
        } else {
          const newFileList = [...fileList];
          const currentileIdx = newFileList.findIndex(
            (f) => f.uid === info.file.uid,
          );
          if (currentileIdx === -1) {
            newFileList.push(info.file);
          } else {
            newFileList[currentileIdx] = info.file;
          }
          setFileList(newFileList.filter(({ status }) => !!status));
        }
      };
    }
    return rcUploadProps;
  });

  const updateFileList = (fileList: UploadFile<any>[]) => {
    setFileList(fileList);
    const [v, u] = fileList.reduce<[string[], string[]]>(
      (p, c) => {
        const [v, u] = p;
        v.push(c.uid);
        u.push(c.url!);
        return [v, u];
      },
      [[], []],
    );
    onChange?.(v, u);
  };

  useUpdateEffect(() => {
    toggle(props.loading);
  }, [props.loading]);

  useUpdateEffect(() => {
    setImageProps({ ...imageProps, disabled });
  }, [disabled]);

  useUpdateEffect(() => {
    setImageProps({ ...imageProps, ...props.imageProps });
  }, [props.imageProps]);

  const renderPictureList = () => {
    return (
      <div className={bem('list')}>
        {fileList.map((file) => {
          return (
            <Spin
              key={file.uid}
              tip="正在上传中...."
              spinning={file.status === 'uploading'}
            >
              <div className={bem('list-container')}>
                <div
                  className={bem('list-item', {
                    [file.status || 'uploading']: true,
                  })}
                >
                  <Image
                    key={file.uid}
                    {...imageProps}
                    src={file.url}
                    onDelete={() => {
                      const newFileList = [...fileList];
                      const currentileIdx = newFileList.findIndex(
                        (f) => f.uid === file.uid,
                      );
                      newFileList.splice(currentileIdx, 1);
                      updateFileList(newFileList);
                    }}
                  />
                </div>
              </div>
            </Spin>
          );
        })}
        <RcUpload ref={ref} {...rcUploadProps}>
          {fileList.length >= maxCount ? null : placeholder}
        </RcUpload>
      </div>
    );
  };

  const iconRender = (file: UploadFile) => {
    if (file.status === 'uploading')
      return <LoadingOutlined className={bem('text-icon')} />;
    const fileSuffix = file.name.split('.').slice(-1)[0];
    if (['xls', 'xlsx'].includes(fileSuffix)) {
      return <ExcelOutlined />;
    }
    if (['doc', 'docx'].includes(fileSuffix)) {
      return <WordOutlined />;
    }
    if (['ppt', 'pptx'].includes(fileSuffix)) {
      return <PptOutlined />;
    }
    if (['pdf'].includes(fileSuffix)) {
      return <PdfOutlined />;
    }

    return <PaperClipOutlined />;
  };

  const renderFile = (): JSX.Element => {
    return (
      <div className={bem('text')}>
        <RcUpload ref={ref} {...rcUploadProps}>
          <span className={bem('button', { disabled: rcUploadProps.disabled })}>
            <PlusOutlined />
            <span>上传文件</span>
          </span>
        </RcUpload>
        <div className={bem('text-list')}>
          <div className={bem('text-list-container')}>
            {fileList.map((file) => {
              return (
                <div
                  key={file.uid}
                  className={bem('text-list-item', {
                    loading: file.status === 'uploading',
                    delete: file.delete,
                  })}
                >
                  <div className={bem('text-list-item-info')}>
                    <span>
                      <span className={bem('text-list-item-icon')}>
                        {iconRender(file)}
                      </span>
                      <span
                        className={bem('text-list-item-name')}
                        title={file.name}
                      >
                        {file.name}
                      </span>
                      <span className={bem('text-list-item-actions')}>
                        {!disabled ? (
                          <Popconfirm
                            okText="确定"
                            cancelText="取消"
                            placement="left"
                            onVisibleChange={(visible) => {
                              const newFileList = [...fileList];
                              const currentileIdx = newFileList.findIndex(
                                (f) => f.uid === file.uid,
                              );
                              newFileList[currentileIdx].delete = visible;
                              updateFileList(newFileList);
                            }}
                            onConfirm={() => {
                              const newFileList = [...fileList];
                              const currentileIdx = newFileList.findIndex(
                                (f) => f.uid === file.uid,
                              );
                              newFileList.splice(currentileIdx, 1);
                              updateFileList(newFileList);
                            }}
                            title="你确定要删除此文件吗？"
                          >
                            <div className={bem('text-list-item-actions-btn')}>
                              <DeleteOutlined />
                            </div>
                          </Popconfirm>
                        ) : null}
                      </span>
                    </span>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  };

  const renderPicture = (): JSX.Element => {
    const render = (): any => {
      if (!loading && !status) return placeholder;
      if (props.children) return props.children;
      return (
        <Spin tip="正在上传中...." spinning={loading}>
          <div className={bem('picture', { disabled })}>
            <Image {...imageProps} />
          </div>
        </Spin>
      );
    };
    // TODO 临时解决预览图片点击触发上传的问题
    if (!loading && status) return render();

    return (
      <RcUpload ref={ref} {...rcUploadProps}>
        {render()}
      </RcUpload>
    );
  };

  const render = () => {
    switch (listType) {
      case 'picture':
        return renderPicture();
      case 'picture-list':
        return renderPictureList();
      case 'text':
      default:
        return renderFile();
    }
  };

  return render();
});

export type { DraggerProps, UploadProps };
export default Upload;
