/**
 * title: 基本使用
 * desc: 基本使用。
 */

import React from 'react';
import { message } from 'antd';
import { Image } from '@parallel-line/components';

function ImageDemo() {
  return (
    <Image
      width={200}
      delete
      src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
      onDelete={() => message.info('onDelete')}
    />
  );
}

export default ImageDemo;
