/**
 * title: 加载中提示
 * desc: 大图使用 placeholder 进行加载中提示
 */

import React from 'react';
import { Space } from 'antd';
import { Button, Image } from '@parallel-line/components';

function ImageDemo() {
  const [random, setRandom] = React.useState<any>();
  return (
    <Space size={12}>
      <Image
        width={200}
        delete
        src={`https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png?${random}`}
      />
      <Button
        type="primary"
        onClick={() => {
          setRandom(Date.now());
        }}
      >
        Reload
      </Button>
    </Space>
  );
}

export default ImageDemo;
