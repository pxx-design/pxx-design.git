/**
 * title: 容错处理
 * desc: 加载失败显示图像占位符。
 */

import React from 'react';
import { Image } from '@parallel-line/components';

function ImageDemo() {
  return <Image width={200} height={200} delete src="error" />;
}

export default ImageDemo;
