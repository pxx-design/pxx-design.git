import React from 'react';
import { omit } from 'lodash-es';
import { Popconfirm } from 'antd';
import { useBoolean } from 'ahooks';
import classNames from 'classnames';
import { useNamespace } from '@parallel-line/common/es/hooks';
import { DeleteOutlined, EyeFill } from '@parallel-line/icons';
import RcImage, { ImageProps as RcImageProps } from 'antd/es/image';

export interface ImageProps extends Omit<RcImageProps, 'preview' | 'onClick'> {
  preview?: boolean;
  delete?: boolean;
  delConfirm?: boolean;
  disabled?: boolean;
  size?: 'small' | 'default';
  onDelete?: () => void;
}

const placeholderUrl =
  'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHZpZXdCb3g9IjAgMCAxNiAxNiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48cGF0aCBkPSJNMTQuNSAyLjVoLTEzQS41LjUgMCAwIDAgMSAzdjEwYS41LjUgMCAwIDAgLjUuNWgxM2EuNS41IDAgMCAwIC41LS41VjNhLjUuNSAwIDAgMC0uNS0uNXpNNS4yODEgNC43NWExIDEgMCAwIDEgMCAyIDEgMSAwIDAgMSAwLTJ6bTguMDMgNi44M2EuMTI3LjEyNyAwIDAgMS0uMDgxLjAzSDIuNzY5YS4xMjUuMTI1IDAgMCAxLS4wOTYtLjIwN2wyLjY2MS0zLjE1NmEuMTI2LjEyNiAwIDAgMSAuMTc3LS4wMTZsLjAxNi4wMTZMNy4wOCAxMC4wOWwyLjQ3LTIuOTNhLjEyNi4xMjYgMCAwIDEgLjE3Ny0uMDE2bC4wMTUuMDE2IDMuNTg4IDQuMjQ0YS4xMjcuMTI3IDAgMCAxLS4wMi4xNzV6IiBmaWxsPSIjOEM4QzhDIiBmaWxsLXJ1bGU9Im5vbnplcm8iLz48L3N2Zz4=';

const fallbackUrl = `data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/PjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+PHN2ZyB0PSIxNjI2MTU3MzM1MTM5IiBjbGFzcz0iaWNvbiIgdmlld0JveD0iMCAwIDEwMjQgMTAyNCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHAtaWQ9IjM1NDciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCI+PGRlZnM+PHN0eWxlIHR5cGU9InRleHQvY3NzIj48L3N0eWxlPjwvZGVmcz48cGF0aCBkPSJNMzk4LjY3ODAxNiAxNzEuMDA4bC0xOC45NDQgNTYuODg5MzQ0SDExNC4yMzMzNDR2Mzk4LjIyMjMzNmw3My41NTY5OTItNzMuNTU2OTkyYzIwLjUzMjIyNC0yMC41MzMyNDggNTIuNzk1MzkyLTIyLjExMjI1NiA3NS4xMzI5MjgtNC43MzkwNzJsNS4zNjU3NiA0LjczOTA3MiA3My40OTk2NDggNzMuNTU2OTkyIDEwNS4zNTkzNi0xMjYuNDY0TDU2OS4zNDQgNjgzLjAwOGwtNTYuODg5MzQ0IDE3MC42NjcwMDhoLTM5OC4yMjMzNmMtMjkuMTU5NDI0IDAtNTMuMjE3MjgtMjEuOTc1MDQtNTYuNTA1MzQ0LTUwLjI1NzkyTDU3LjM0NCA3OTYuNzg2Njg4VjIyNy44OTczNDRjMC0yOS4yMTI2NzIgMjEuOTc1MDQtNTMuMjI1NDcyIDUwLjI1NzkyLTU2LjUwNzM5Mmw2LjYzMTQyNC0wLjM4MTk1MmgyODQuNDQ0NjcyeiBtNTEyIDBjMjkuMjEyNjcyIDAgNTMuMjI1NDcyIDIxLjkyNTg4OCA1Ni41MDczOTIgNTAuMjQ3NjhsMC4zODE5NTIgNi42NDE2NjR2NTY4Ljg4OTM0NGMwIDI5LjE1OTQyNC0yMS45MjU4ODggNTMuMjE3MjgtNTAuMjQ3NjggNTYuNTA2MzY4bC02LjY0MTY2NCAwLjM4Mjk3Nkg2MjYuMjMzMzQ0TDY4My4xMjI2ODggNjgzLjAwOCA1MTAuMzUwMzM2IDQyMy44MjIzMzZsNzIuODE4Njg4LTg3LjMyNDY3MmMyMS4wMjU3OTItMjUuMjczMzQ0IDU4LjU1NDM2OC0yNy4yMDY2NTYgODIuMDU4MjQtNS42MTI1NDRsNC44MTA3NTIgNC45ODY4OCAyNDAuNjQgMjgwLjc0NzAwOFYyMjcuODk3MzQ0SDQ5My41MTE2OGwxOC45NDQtNTYuODg5MzQ0aDM5OC4yMjMzNnpNMzEzLjM3MjY3MiAyODQuNzYzMTM2YzE1Ljg4MDE5MiAwIDMwLjc1MDcyIDQuMzQ0ODMyIDQzLjQ4OTI4IDExLjkwOTEybC0xNS4wNzMyOCA0NS4wMDI3NTIgNDYuMzY4NzY4IDY5LjUzMzY5NmMtMTQuNTI4NTEyIDI2LjM1NDY4OC00Mi41ODMwNCA0NC4yMjE0NC03NC43ODQ3NjggNDQuMjIxNDQtNDcuMTA0IDAtODUuMzMyOTkyLTM4LjIyODk5Mi04NS4zMzI5OTItODUuMzM0MDE2IDAtNDcuMTA0IDM4LjIyODk5Mi04NS4zMzI5OTIgODUuMzMyOTkyLTg1LjMzMjk5MnoiIHAtaWQ9IjM1NDgiIGZpbGw9IiM4QzhDOEMiPjwvcGF0aD48L3N2Zz4=`;

const InternalImage: React.FC<ImageProps> = (props) => {
  const {
    prefixCls,
    className,
    width,
    height,
    style,
    disabled,
    delConfirm,
    size = 'default',
    preview = true,
    fallback = fallbackUrl,
    onDelete,
  } = props;
  const [visible, { toggle: setVisible }] = useBoolean();
  const [hasError, { toggle }] = useBoolean();
  const [, bem] = useNamespace('image', prefixCls);
  const restProps = {
    ...omit(props, [
      'prefixCls',
      'className',
      'delConfirm',
      'preview',
      'delete',
      'onDelete',
      'onClick',
    ]),
    fallback,
  };
  const placeholderProps: RcImageProps = {
    ...restProps,
    prefixCls: bem(),
    preview: false,
    src: placeholderUrl,
  };
  const imageProps: RcImageProps = {
    ...restProps,
    prefixCls: bem(),
    placeholder: <RcImage {...placeholderProps} />,
    onError: () => {
      toggle(true);
    },
    preview: preview
      ? {
          visible,
          onVisibleChange: (flag) => {
            setVisible(flag);
          },
          // onClick: (event) => event.stopPropagation(),
        }
      : false,
  };

  const renderDelete = (onClick?: React.MouseEventHandler<HTMLDivElement>) => {
    return (
      <div className={bem('delete')} title="删除" onClick={onClick}>
        <DeleteOutlined />
      </div>
    );
  };

  const previewClick: React.MouseEventHandler<HTMLDivElement> = (event) => {
    event.stopPropagation();
    setVisible(true);
  };
  const deleteClick: React.MouseEventHandler<HTMLDivElement> = (event) => {
    event.stopPropagation();
    onDelete?.();
  };

  const dominoRender = () => {
    if (hasError) return null;
    return (
      <>
        {preview && (
          <div className={bem('domino', [size])} onClick={previewClick}>
            <div className={bem('domino-preview')} title="预览">
              <span className={bem('domino-preview-icon')}>
                <EyeFill />
              </span>
              <span>查看大图</span>
            </div>
          </div>
        )}
        {!disabled &&
          props.delete &&
          (delConfirm ? (
            <Popconfirm
              okText="确定"
              cancelText="取消"
              placement="left"
              onConfirm={onDelete}
              title="你确定要删除此图片吗？"
            >
              {renderDelete()}
            </Popconfirm>
          ) : (
            renderDelete(deleteClick)
          ))}
      </>
    );
  };

  return (
    <div
      className={classNames(bem('wrapper'), className)}
      style={{ width, height, ...style }}
    >
      <RcImage {...imageProps} />
      {dominoRender()}
    </div>
  );
};

type ImageType = typeof InternalImage;
interface ImageInterface extends ImageType {
  PLACEHOLDER: string;
  FALLBACK: string;
}

const Image = InternalImage as ImageInterface;
Image.PLACEHOLDER = placeholderUrl;
Image.FALLBACK = fallbackUrl;

export { Image };
export default Image;
