---
title: Image 图片
---

# Image 图片

可预览的图片。

## 代码演示

<Space>
<code src="./demos/basic.tsx"></code>
<code src="./demos/fallback.tsx"></code>
<code src="./demos/placeholder.tsx"></code>
</Space>

