import * as React from 'react';
import classNames from 'classnames';
import { InputNumber, InputNumberProps as AntInputNumberProps } from 'antd';
import { useUpdateEffect } from 'ahooks';
import { CloseCircleFilled } from '@ant-design/icons';

export interface InputNumberProps extends Omit<AntInputNumberProps, ''> {
  /**
   * @description 隐藏步进器
   */
  hideStep?: boolean;
  /**
   * @description 可以点击清除图标删除内容
   */
  allowClear?: boolean;
  /**
   * @description 带有后缀图标的 input-number
   */
  suffix?: React.ReactNode;
  /**
   * @description 聚焦时自动全选
   */
  selectInFocus?: boolean;
  /**
   * @description allowClear 为true时，点击close图标的回调
   */
  onClose?: () => void;
}

const InlineInputNumber = React.forwardRef<HTMLInputElement, InputNumberProps>(
  (props, ref) => {
    const {
      bordered,
      allowClear,
      suffix,
      className,
      hideStep,
      selectInFocus,
      prefixCls: customizePrefixCls,
      onClose,
      onChange,
      ...restProps
    } = props;
    const inputRef = React.useRef<HTMLInputElement>(null);
    const [value, setValue] = React.useState(props.value);
    const pCls = `${customizePrefixCls}-input-number`;

    React.useImperativeHandle(ref, () => inputRef.current!);

    useUpdateEffect(() => {
      setValue(props.value);
      console.log('useUpdateEffect', props.value);
    }, [props.value]);

    const handleReset = () => {
      setValue('');
      onChange?.('');
      onClose?.();
    };
    const renderClearIcon = (prefixCls: string) => {
      const { disabled, readOnly } = props;
      if (!allowClear) {
        return null;
      }
      const needClear = !disabled && !readOnly && props.value;
      const closeClassName = `${prefixCls}-clear-icon`;
      return (
        <CloseCircleFilled
          onClick={handleReset}
          className={classNames(
            {
              [`${closeClassName}-hidden`]: !needClear,
            },
            closeClassName,
          )}
          role="button"
        />
      );
    };
    const renderSuffix = (prefixCls: string) => {
      if (suffix || allowClear) {
        return (
          <span className={`${prefixCls}-suffix`}>
            {renderClearIcon(prefixCls)}
            {suffix}
          </span>
        );
      }
      return null;
    };

    const handleChange = (val: React.Key) => {
      setValue(val);

      console.log('handleChange', val, props.value);
      onChange?.(val);
    };

    return (
      <div
        className={classNames(
          `${pCls}-wrapper`,
          !bordered && `${pCls}-wrapper-borderless`,
        )}
      >
        <InputNumber<React.Key>
          ref={inputRef}
          className={classNames(
            pCls,
            hideStep && `${pCls}-hide-step`,
            className,
          )}
          value={value}
          bordered={bordered}
          onChange={handleChange}
          {...restProps}
          onFocus={(event) => {
            if (selectInFocus) inputRef.current?.select();
            restProps.onFocus?.(event);
          }}
        />
        {renderSuffix(pCls)}
      </div>
    );
  },
);

InlineInputNumber.defaultProps = {
  prefixCls: 'pxx',
  placeholder: '请输入',
  allowClear: true,
};

export default InlineInputNumber;
