---
title: InputNumber 数字输入框
group:
  title: InputNumber 数字输入框
  order: 14
---

# InputNumber 数字输入框

通过鼠标或键盘，输入范围内的数值。

## 何时使用

当需要获取标准数值时。

## 代码演示

### 基本使用

<code src="./demos/basic.tsx" />

<API exports='["default"]'></API>
