/**
 * title: 基本使用
 * desc: 基本使用。
 */

import React from 'react';
import { InputNumber } from '@parallel-line/components';

export default () => (
  <InputNumber
    placeholder="Basic usage"
    style={{ width: 200 }}
    onChange={(v) => {
      console.log(v);
    }}
  />
);
