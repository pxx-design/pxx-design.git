// @ts-ignore
import React from 'react';
import { useSetState } from 'ahooks';
import { Switch, Form } from 'antd';
import { BaseTable, BaseTableProps } from '@parallel-line/components';

const dataSource = Array.from({ length: 10 }, (_, k) => ({
  key: `demo-${k + 1}`,
  name: `胡彦斌${k + 1}`,
  age: 35 + k,
  address: `西湖区湖底公园${k + 1}号`,
}));

const columns: BaseTableProps['columns'] = [
  {
    title: '姓名',
    dataIndex: 'name',
    key: 'name',
    sorter: true,
  },
  {
    title: '年龄',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: '住址',
    dataIndex: 'address',
    key: 'address',
  },
];

// rowSelection object indicates the need for row selection
const rowSelection: BaseTableProps['rowSelection'] = {
  onChange: (selectedRowKeys: React.Key[], selectedRows: any[]) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      'selectedRows: ',
      selectedRows,
    );
  },
  getCheckboxProps: (record: any) => ({
    disabled: record.age > 40, // Column configuration not to be checked
    name: record.name,
  }),
};

const Basic = () => {
  const [state, setState] = useSetState<BaseTableProps>({
    dataSource,
    columns,
  });
  const handleRowSelectionChange = (enable?: boolean) => {
    setState({ rowSelection: enable ? rowSelection : undefined });
  };
  const handleSizeChange = (enable?: boolean) => {
    setState({ size: enable ? 'small' : undefined });
  };

  return (
    <>
      <Form layout="inline" style={{ marginBottom: 16 }}>
        <Form.Item label="Checkbox">
          <Switch
            checked={!!state.rowSelection}
            onChange={handleRowSelectionChange}
          />
        </Form.Item>
        <Form.Item label="Small">
          <Switch checked={!!state.size} onChange={handleSizeChange} />
        </Form.Item>
      </Form>
      <BaseTable rowKey="key" {...state} />
    </>
  );
};

export default Basic;
