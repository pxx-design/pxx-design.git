import React from 'react';
import Table, { TableProps } from '../table';
import classNames from 'classnames';

export interface BaseTableProps extends TableProps {}

function BaseTable(props: BaseTableProps) {
  const { className, prefixCls = 'pxx', ...restProps } = props;
  return (
    <Table
      hideAllFilter
      pagination={false}
      {...restProps}
      className={classNames(`${prefixCls}-base-table`, className)}
    />
  );
}

export default BaseTable;
