const autoExport = require('pxx-plugin-auto-export');

const excludeTemplate = `
export * from '@parallel-line/common';

`;

autoExport({
  libName: 'components',
  excludeTemplate,
});
