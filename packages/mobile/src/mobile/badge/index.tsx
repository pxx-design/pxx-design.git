import React from 'react';
import classNames from 'classnames';
import {useNamespace} from "@parallel-line/common/es/common/hooks";

interface BadgeProps {
  text: string;
  className?: string;
  status: 'success' | 'error' | 'default' | 'processing' | 'warning';
}

const Badge = (props: BadgeProps) => {
  const {text, status, className} = props;
  const [cls, bem] = useNamespace('badge');
  return (
    <div className={classNames(cls,
      {[bem(status)]: !!status},
      className)}>
      <div className={bem('dot')}/>
      {text}
    </div>
  );
};

export default Badge;
