---
title: 组件总览
order: 1
nav:
  title: H5组件
  order: 3
---

> 平行线前端移动端组件库。

## 快速上手

## 📦 安装

```bash
npm install @parallel-line/mobile --save
```

```bash
yarn add @parallel-line/mobile
```

## 移动端

通过`@hd`替代`px`, 通过 `less` 提供的 [modifyVars](http://lesscss.org/usage/#using-less-in-the-browser-modify-variables) 的方式进行覆盖`@hd`变量即可。

### 在 Umi 里配置

如果你在使用 [Umi](https://umijs.org/zh-CN/config#theme)，那么可以很方便地在项目根目录的 `.umirc.ts` 或 [config/config.ts](https://github.com/ant-design/ant-design-pro/blob/v5/config/config.ts) 文件中 [theme](https://umijs.org/zh-CN/config#theme) 字段进行主题配置。`theme` 可以配置为一个对象或文件路径。

```js
"theme": {
  "@hd": "2px",
},
```

## 按需加载

### 安装

``` sh
yarn add -D babel-plugin-pxx-import
```

### 配置
```js
import { defineConfig } from 'dumi';

export default defineConfig({
  extraBabelPlugins: [
    [
      'pxx-import',
      {
        libraryName: '@parallel-line/mobile',
        libraryDirectory: 'es/mobile',
        style: true,
      },
      'parallel-mobile',
    ],
  ],
});
```
