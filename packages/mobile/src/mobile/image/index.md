---
title: Image 图片
mobile: true
---

# Image 图片

增强版的 img 标签，提供多种图片填充模式，支持加载中提示、加载失败提示。

## 代码演示

<code src="./demos/basic.tsx"></code>
<code src="./demos/fit.tsx"></code>
<code src="./demos/round.tsx"></code>
<code src="./demos/loading.tsx"></code>
<code src="./demos/error.tsx"></code>

<API exports='["default"]'></API>

## 待实现功能

- lazyLoad 懒加载
