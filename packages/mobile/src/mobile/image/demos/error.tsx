/**
 * title: 加载失败提示
 * desc: `Image` 组件提供了默认的加载失败提示，支持通过 `error` 自定义内容。
 */

import React from 'react';
import styled from 'styled-components';
import { Image } from '@parallel-line/mobile';
import { Space } from 'antd';

const Item = styled.div`
  display: flex;
  width: 6rem;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export default () => (
  <Space wrap>
    <Item>
      <Image width="6rem" height="6rem" src="x" />
      <div>默认提示</div>
    </Item>

    <Item>
      <Image
        width="6rem"
        height="6rem"
        src="x"
        errorIcon={() => (
          <div style={{ fontSize: 12, color: '#969799' }}>加载失败</div>
        )}
      />
      <div>自定义提示</div>
    </Item>
  </Space>
);
