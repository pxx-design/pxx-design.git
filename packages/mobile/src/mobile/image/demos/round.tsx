/**
 * title: 圆形图片
 * desc: 通过 `round` 属性可以设置图片变圆，注意当图片宽高不相等且 `fit` 为 `contain` 或 `scale-down` 时，将无法填充一个完整的圆形。
 */

import React from 'react';
import styled from 'styled-components';
import { Image } from '@parallel-line/mobile';
import { Space } from 'antd';

const Item = styled.div`
  display: flex;
  width: 6rem;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export default () => (
  <Space wrap>
    <Item>
      <Image
        round
        width="6rem"
        height="6rem"
        fit="contain"
        src="https://img01.yzcdn.cn/vant/cat.jpeg"
      />
      <div>contain</div>
    </Item>

    <Item>
      <Image
        round
        width="6rem"
        height="6rem"
        fit="cover"
        src="https://img01.yzcdn.cn/vant/cat.jpeg"
      />
      <div>cover</div>
    </Item>

    <Item>
      <Image
        round
        width="6rem"
        height="6rem"
        fit="fill"
        src="https://img01.yzcdn.cn/vant/cat.jpeg"
      />
      <div>fill</div>
    </Item>

    <Item>
      <Image
        round
        width="6rem"
        height="6rem"
        fit="none"
        src="https://img01.yzcdn.cn/vant/cat.jpeg"
      />
      <div>none</div>
    </Item>

    <Item>
      <Image
        round
        width="6rem"
        height="6rem"
        fit="scale-down"
        src="https://img01.yzcdn.cn/vant/cat.jpeg"
      />
      <div>scale-down</div>
    </Item>
  </Space>
);
