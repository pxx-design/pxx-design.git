/**
 * title: 加载中提示
 * desc: `Image` 组件提供了默认的加载中提示，支持通过 `loadingIcon` 自定义内容。
 */

import React from 'react';
import styled from 'styled-components';
import { LoadingOutlined } from '@parallel-line/icons';
import { Image } from '@parallel-line/mobile';
import { Space } from 'antd';

const Item = styled.div`
  display: flex;
  width: 6rem;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export default () => (
  <Space wrap>
    <Item>
      <Image width="6rem" height="6rem" />
      <div>默认提示</div>
    </Item>

    <Item>
      <Image
        width="6rem"
        height="6rem"
        loadingIcon={(props) => <LoadingOutlined {...props} />}
      />
      <div>自定义提示</div>
    </Item>
  </Space>
);
