/**
 * title: 填充模式
 * desc: 通过 `fit` 属性可以设置图片填充模式，可选值见下方表格。
 */

import React from 'react';
import styled from 'styled-components';
import { Image } from '@parallel-line/mobile';
import { Space } from 'antd';

const Item = styled.div`
  display: flex;
  width: 6rem;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export default () => (
  <Space wrap>
    <Item>
      <Image
        width="6rem"
        height="6rem"
        fit="contain"
        src="https://img01.yzcdn.cn/vant/cat.jpeg"
      />
      <div>contain</div>
    </Item>

    <Item>
      <Image
        width="6rem"
        height="6rem"
        fit="cover"
        src="https://img01.yzcdn.cn/vant/cat.jpeg"
      />
      <div>cover</div>
    </Item>

    <Item>
      <Image
        width="6rem"
        height="6rem"
        fit="fill"
        src="https://img01.yzcdn.cn/vant/cat.jpeg"
      />
      <div>fill</div>
    </Item>

    <Item>
      <Image
        width="6rem"
        height="6rem"
        fit="none"
        src="https://img01.yzcdn.cn/vant/cat.jpeg"
      />
      <div>none</div>
    </Item>

    <Item>
      <Image
        width="6rem"
        height="6rem"
        fit="scale-down"
        src="https://img01.yzcdn.cn/vant/cat.jpeg"
      />
      <div>scale-down</div>
    </Item>
  </Space>
);
