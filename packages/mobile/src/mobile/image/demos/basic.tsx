/**
 * title: 基础用法
 * desc: 基础用法与原生 img 标签一致，可以设置 `src`、`width`、`height`、`alt` 等原生属性。
 */

import React from 'react';
import { Image } from '@parallel-line/mobile';

export default () => (
  <Image width="100" height="100" src="https://img01.yzcdn.cn/vant/cat.jpeg" />
);
