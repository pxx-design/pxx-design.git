import * as React from 'react';
import { useBoolean } from 'ahooks';
import classNames from 'classnames';
import { PhotoOutlined,PhotoFailOutlined} from '@parallel-line/icons';
import { useNamespace } from '@parallel-line/common/es/hooks';
import ConfigProvider from '../config-provider';
import { isDef, addUnit } from '@parallel-line/common/es/utils';

export type ImageFit = 'contain' | 'cover' | 'fill' | 'none' | 'scale-down';

export interface ImageProps {
  /** 图片链接 */
  src?: string;
  /** 替代文本 */
  alt?: string;
  /** 图片填充模式 */
  fit?: ImageFit;
  className?: string;
  style?: React.CSSProperties;
  /** 宽度，默认单位为px */
  width?: number | string;
  /** 高度，默认单位为px */
  height?: number | string;
  /** 圆角大小，默认单位为px */
  radius?: number | string;
  /** 是否显示为圆形 */
  round?: boolean;
  // lazyLoad?: boolean;
  /** 是否展示图片加载失败提示 */
  showError?: boolean;
  /** 是否展示图片加载中提示 */
  showLoading?: boolean;
  /** 自定义失败时提示的内容 */
  errorIcon?: (props: { className: string }) => React.ReactNode;
  /** 自定义加载时提示的内容 */
  loadingIcon?: (props: { className: string }) => React.ReactNode;

  /** 点击图片时触发 */
  onClick?: React.MouseEventHandler<HTMLDivElement>;
  /** 图片加载完毕时触发 */
  onLoad?(event?: Event): void;
  /** 图片加载失败时触发 */
  onError?(event?: Event): void;
}

const Image: React.FC<ImageProps> = (props) => {
  const [, bem] = useNamespace('image');
  const { hd } = React.useContext(ConfigProvider.Context);
  const [error, { toggle: setError }] = useBoolean(false);
  const [loading, { toggle }] = useBoolean(true);
  const imageRef = React.useRef<HTMLElement>();

  const style = React.useMemo(() => {
    const style: React.CSSProperties = { ...props.style };

    if (isDef(props.width)) {
      style.width = addUnit(props.width, hd);
    }

    if (isDef(props.height)) {
      style.height = addUnit(props.height, hd);
    }

    if (isDef(props.radius)) {
      style.overflow = 'hidden';
      style.borderRadius = addUnit(props.radius, hd);
    }

    return style;
  }, [props.style, props.width, props.height, props.radius]);

  React.useEffect(() => {
    setError(false);
    toggle(true);
  }, [props.src]);

  const onLoad: any = (event?: Event) => {
    toggle(false);
    props.onLoad?.(event);
  };

  const onError: any = (event?: Event) => {
    setError(true);
    toggle(false);
    props.onError?.(event);
  };

  const renderLoadingIcon = () => {
    const loadingIconProps = {
      className: bem('loading-icon'),
    };
    if (props.loadingIcon) {
      return props.loadingIcon(loadingIconProps);
    }

    return <PhotoOutlined {...loadingIconProps} />;
  };

  const renderErrorIcon = () => {
    const errorIconProps = {
      className: bem('error-icon'),
    };
    if (props.errorIcon) {
      return props.errorIcon(errorIconProps);
    }

    return <PhotoFailOutlined {...errorIconProps} />;
  };

  const renderPlaceholder = () => {
    if (loading && props.showLoading) {
      return <div className={bem('loading')}>{renderLoadingIcon()}</div>;
    }
    if (error && props.showError) {
      return <div className={bem('error')}>{renderErrorIcon()}</div>;
    }
  };

  const renderImage = () => {
    if (error || !props.src) {
      return;
    }

    const attrs = {
      alt: props.alt,
      className: bem('img'),
      style: {
        objectFit: props.fit,
      } as React.CSSProperties,
    };

    return <img src={props.src} onLoad={onLoad} onError={onError} {...attrs} />;
  };
  return (
    <div
      className={classNames(bem({ round: props.round }), props.className)}
      style={style}
      onClick={props.onClick}
    >
      {renderImage()}
      {renderPlaceholder()}
      {props.children}
    </div>
  );
};

Image.defaultProps = {
  fit: 'fill',
  radius: 0,
  round: false,
  showError: true,
  showLoading: true,
};

export default Image;
