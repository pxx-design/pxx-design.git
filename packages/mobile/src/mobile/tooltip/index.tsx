import * as React from 'react';
import { Toast } from '../toast';

export interface TooltipProps
  extends Omit<React.HTMLAttributes<HTMLSpanElement>, 'title'> {
  title?: React.ReactNode;
}

const Tooltip = React.forwardRef<HTMLSpanElement, TooltipProps>(
  ({ title, onClick, ...props }, ref) => {
    const handleClick: React.MouseEventHandler<HTMLSpanElement> = (event) => {
      if (onClick) {
        onClick(event);
      } else {
        Toast({ message: title });
      }
    };
    return <span ref={ref} onClick={handleClick} {...props}></span>;
  },
);

export default Tooltip;
