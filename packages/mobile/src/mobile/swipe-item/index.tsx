import * as React from 'react';
import classNames from 'classnames';
import { useMount, useSetState } from 'ahooks';
import { SwipeContext } from '../swipe/context';
import { useNamespace } from '@parallel-line/common/es/hooks';

interface SwipeItemProps
  extends React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLDivElement>,
    HTMLDivElement
  > {}

export interface SwipeItemInstance {
  uid: Symbol;
  setOffset: (offset: number) => void;
}

function useParent(
  instance: SwipeItemInstance,
  root: React.RefObject<HTMLDivElement>,
) {
  const { children, parent, push, pop } = React.useContext(SwipeContext);
  const index = React.useMemo(
    () => children.findIndex((child) => instance.uid === child.uid),
    [root.current],
  );
  React.useEffect(() => {
    push(instance);
    return () => pop(instance);
  }, []);

  return { parent, index };
}

const SwipeItem: React.ForwardRefRenderFunction<
  SwipeItemInstance,
  SwipeItemProps
> = (props, ref) => {
  let rendered: boolean;
  const [name, bem] = useNamespace('swipe-item');
  const root = React.useRef<HTMLDivElement>(null);
  const [state, setState] = useSetState({
    offset: 0,
    inited: false,
    mounted: false,
  });

  const setOffset = (offset: number) => {
    setState({ offset });
  };
  const instance = React.useMemo<SwipeItemInstance>(
    () => ({ uid: Symbol(name), setOffset }),
    [],
  );
  React.useImperativeHandle(ref, () => instance);

  const { parent, index } = useParent(instance, root);

  if (!parent) {
    if (process.env.NODE_ENV !== 'production') {
      console.error('[Vant] <SwipeItem> must be a child component of <Swipe>.');
    }
  }

  const style = React.useMemo(() => {
    const style: React.CSSProperties = { ...props.style };
    const vertical = parent?.props.vertical;

    if (parent?.size) {
      style[vertical ? 'height' : 'width'] = `${parent.size}px`;
    }

    if (state.offset) {
      style.transform = `translate${vertical ? 'Y' : 'X'}(${state.offset}px)`;
    }
    return style;
  }, [props.style, parent?.props.vertical, parent?.size, state.offset]);

  const shouldRender = React.useMemo(() => {
    const { loop, lazyRender } = parent?.props || {};

    if (!lazyRender || rendered) {
      return true;
    }

    // wait for all item to mount, so we can get the exact count
    if (!state.mounted) {
      return false;
    }
    if (parent) {
      const active = parent.activeIndicator;
      const maxActive = parent.count - 1;
      const prevActive = active === 0 && loop ? maxActive : active - 1;
      const nextActive = active === maxActive && loop ? 0 : active + 1;
      rendered =
        index === active || index === prevActive || index === nextActive;

      return rendered;
    }
    return false;
  }, [
    parent?.count,
    parent?.props?.loop,
    parent?.props?.lazyRender,
    parent?.activeIndicator,
    state.mounted,
  ]);

  useMount(() => {
    setTimeout(() => {
      setState({ mounted: true });
    });
  });

  return (
    <div
      ref={root}
      {...props}
      className={classNames(bem(), props.className)}
      style={style}
    >
      {/* {shouldRender ? props.children : null} */}
      {props.children}
    </div>
  );
};

export default React.forwardRef(SwipeItem);
