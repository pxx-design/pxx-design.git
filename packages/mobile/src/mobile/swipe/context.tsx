import * as React from 'react';
import { SwipeInstance } from '.';
import { SwipeItemInstance } from '../swipe-item';

export interface SwipeConsumerProps {
  parent: SwipeInstance | null;
  children: SwipeItemInstance[];
  push: (item: SwipeItemInstance) => void;
  pop: (item: SwipeItemInstance) => void;
}

export const SwipeContext = React.createContext<SwipeConsumerProps>({
  parent: null,
  children: [],
  push() {},
  pop() {},
});

export const SwipeConsumer = SwipeContext.Consumer;
