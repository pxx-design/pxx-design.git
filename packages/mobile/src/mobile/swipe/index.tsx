import * as React from 'react';
import classNames from 'classnames';
import { calc } from '@parallel-line/utils';
import { useMount, useUnmount, useSize, useSetState } from 'ahooks';
import { SwipeContext } from './context';
import { useNamespace, useTouch } from '@parallel-line/common/es/hooks';
import {
  clamp,
  doubleRaf,
  isHidden,
  preventDefault,
} from '@parallel-line/common/es/utils';
import { SwipeItemInstance } from '../swipe-item';

export interface SwipeToOptions {
  immediate?: boolean;
}

export interface SwipeInstance {
  $el?: HTMLDivElement | null;
  size: number;
  count: number;
  props: SwipeProps;
  activeIndicator: number;

  resize(): void;
  prev(): void;
  next(): void;
  swipeTo(index: number, options?: SwipeToOptions): void;
}

export interface SwipeProps<T = any> {
  loop?: boolean;
  className?: string;
  style?: React.CSSProperties;
  width?: number | string;
  height?: number | string;
  vertical?: boolean;
  touchable?: boolean;
  lazyRender?: boolean;
  indicatorColor?: string;
  showIndicators?: boolean;
  stopPropagation?: boolean;
  autoplay?: number | string;
  duration?: number | string;
  initialSwipe?: number | string;

  onChange?(active: number): void;

  children: React.ReactElement<
    React.PropsWithoutRef<any> & React.RefAttributes<T>
  >[];
  renderIndicator?: (args: { active: number }) => React.ReactNode;
}

interface SwipeState {
  rect: { width?: number; height?: number } | null;
  width: number;
  height: number;
  offset: number;
  active: number;
  swiping: boolean;
}

const InlineSwipe: React.ForwardRefRenderFunction<SwipeInstance, SwipeProps> = (
  props,
  ref,
) => {
  const [, bem] = useNamespace('swipe');
  const { autoplay: pAutoplay = 0, duration = 500, initialSwipe = 0 } = props;
  const touch = useTouch();
  const children = React.useRef<SwipeItemInstance[]>([]);
  const root = React.useRef<HTMLDivElement>(null);
  const touchStartTime = React.useRef<number>(0);
  const windowSize = useSize(document.body);
  const [state, setState] = useSetState<SwipeState>({
    rect: null,
    width: 0,
    height: 0,
    offset: 0,
    active: 0,
    swiping: false,
  });

  const count = children.current.length;

  const size = state[props.vertical ? 'height' : 'width'];

  const delta = React.useMemo(
    () => (props.vertical ? touch.deltaY.current : touch.deltaX.current),
    [props.vertical, touch.deltaY.current, touch.deltaX.current],
  );

  const minOffset = React.useMemo(() => {
    if (state.rect) {
      const base = props.vertical ? state.rect.height : state.rect.width;
      return calc(`${base} - ${size} * ${count}`);
    }
    return 0;
  }, [state.rect, props.vertical, size, count]);

  const maxCount = React.useMemo(
    () => Math.ceil(calc(`${Math.abs(minOffset)} / ${size}`)),
    [minOffset, size],
  );

  const trackSize = React.useMemo(() => calc(`${count} * ${size}`), [
    count,
    size,
  ]);

  const activeIndicator = React.useMemo(
    () => calc(`(${state.active} + ${count}) % ${count}`),
    [state.active, count],
  );

  const getIsCorrectDirection = () => {
    const expect = props.vertical ? 'vertical' : 'horizontal';
    return touch.direction.current === expect;
  };

  const trackStyle = React.useMemo(() => {
    const style: React.CSSProperties = {
      transitionDuration: `${state.swiping ? 0 : duration}ms`,
      transform: `translate${props.vertical ? 'Y' : 'X'}(${state.offset}px)`,
    };

    if (size) {
      const mainAxis = props.vertical ? 'height' : 'width';
      const crossAxis = props.vertical ? 'width' : 'height';
      style[mainAxis] = `${trackSize}px`;
      style[crossAxis] = props[crossAxis] ? `${props[crossAxis]}px` : '';
    }

    return style;
  }, [
    state.swiping,
    duration,
    props.vertical,
    state.offset,
    props.width,
    props.height,
  ]);

  const getTargetActive = (pace: number) => {
    const { active } = state;

    if (pace) {
      if (props.loop) {
        return clamp(active + pace, -1, count);
      }
      return clamp(active + pace, 0, maxCount);
    }
    return active;
  };

  const getTargetOffset = (targetActive: number, offset = 0) => {
    let currentPosition = calc(`${targetActive} * ${size}`);
    if (!props.loop) {
      currentPosition = Math.min(currentPosition, -minOffset);
    }

    let targetOffset = offset - currentPosition;
    if (!props.loop) {
      targetOffset = clamp(targetOffset, minOffset, 0);
    }

    return targetOffset;
  };

  const push = React.useCallback((item: SwipeItemInstance) => {
    const idx = children.current.findIndex((child) => child.uid === item.uid);
    if (idx > 0) return;
    children.current.push(item);
  }, []);

  const pop = React.useCallback((item: SwipeItemInstance) => {
    const start = children.current.findIndex((child) => child.uid === item.uid);
    children.current.splice(start, 1);
  }, []);

  const move = ({
    pace = 0,
    offset = 0,
    emitChange,
  }: {
    pace?: number;
    offset?: number;
    emitChange?: boolean;
  }) => {
    if (count <= 1) {
      return;
    }

    const { active } = state;
    const targetActive = getTargetActive(pace);
    const targetOffset = getTargetOffset(targetActive, offset);

    // auto move first and last swipe in loop mode
    if (props.loop) {
      if (children.current[0] && targetOffset !== minOffset) {
        const outRightBound = targetOffset < minOffset;
        children.current[0].setOffset(outRightBound ? trackSize : 0);
      }

      if (children.current[count - 1] && targetOffset !== 0) {
        const outLeftBound = targetOffset > 0;
        children.current[count - 1].setOffset(outLeftBound ? -trackSize : 0);
      }
    }

    setState({ active: targetActive, offset: targetOffset });

    if (emitChange && targetActive !== active) {
      // TODO: 这里需要针对 React.useMemo 和 setState 同步时产生数据不同步异常处理
      const activeIndicator = calc(`(${targetActive} + ${count}) % ${count}`);
      props.onChange?.(activeIndicator);
    }
  };

  const correctPosition = () => {
    setState({ swiping: true });

    if (state.active <= -1) {
      move({ pace: count });
    } else if (state.active >= count) {
      move({ pace: -count });
    }
  };

  // swipe to prev item
  const prev = () => {
    correctPosition();
    touch.reset();

    doubleRaf(() => {
      setState({ swiping: false });

      move({
        pace: -1,
        emitChange: true,
      });
    });
  };

  // swipe to next item
  const next = () => {
    correctPosition();
    touch.reset();

    doubleRaf(() => {
      setState({ swiping: false });

      move({
        pace: 1,
        emitChange: true,
      });
    });
  };

  let autoplayTimer: NodeJS.Timeout;

  const stopAutoplay = () => clearTimeout(autoplayTimer);

  const autoplay = () => {
    stopAutoplay();
    if (pAutoplay > 0 && count > 1) {
      autoplayTimer = setTimeout(() => {
        next();
        autoplay();
      }, +pAutoplay);
    }
  };

  // initialize swipe position
  const initialize = (active = +initialSwipe) => {
    if (!root) {
      return;
    }

    const newState: Partial<SwipeState> = {};

    if (!isHidden(root.current)) {
      const rect = {
        width: root.current?.offsetWidth,
        height: root.current?.offsetHeight,
      };
      newState.rect = rect;
      newState.width = +(props.width ?? rect.width ?? 0);
      newState.height = +(props.height ?? rect.height ?? 0);
    }

    if (count) {
      active = Math.min(count - 1, active);
    }

    newState.active = active;
    newState.swiping = true;
    newState.offset = getTargetOffset(active);
    children.current.forEach((swipe) => {
      swipe?.setOffset?.(0);
    });

    setState(newState);
  };

  const resize = () => initialize(state.active);

  const onTouchStart: any = (event: TouchEvent) => {
    if (!props.touchable) return;

    touch.start(event);
    touchStartTime.current = Date.now();

    stopAutoplay();
    correctPosition();
  };

  const onTouchMove: any = (event: TouchEvent) => {
    if (props.touchable && state.swiping) {
      touch.move(event);

      if (getIsCorrectDirection()) {
        preventDefault(event, props.stopPropagation);
        move({ offset: delta });
      }
    }
  };

  const onTouchEnd: any = () => {
    if (!props.touchable || !state.swiping) {
      return;
    }

    const duration = Date.now() - touchStartTime.current;
    const speed = delta / duration;
    const shouldSwipe = Math.abs(speed) > 0.25 || Math.abs(delta) > size / 2;

    if (shouldSwipe && getIsCorrectDirection()) {
      const offset = props.vertical ? touch.offsetY : touch.offsetX;

      let pace = 0;

      if (props.loop) {
        pace = offset.current > 0 ? (delta > 0 ? -1 : 1) : 0;
      } else {
        pace = -Math[delta > 0 ? 'ceil' : 'floor'](delta / size);
      }

      move({
        pace,
        emitChange: true,
      });
    } else if (delta) {
      move({ pace: 0 });
    }

    setState({ swiping: false });
    autoplay();
  };

  const swipeTo = (index: number, options: SwipeToOptions = {}) => {
    correctPosition();
    touch.reset();

    doubleRaf(() => {
      let targetIndex;
      if (props.loop && index === count) {
        targetIndex = state.active === 0 ? 0 : index;
      } else {
        targetIndex = index % count;
      }

      if (options.immediate) {
        doubleRaf(() => {
          setState({ swiping: false });
        });
      } else {
        setState({ swiping: false });
      }

      move({
        pace: targetIndex - state.active,
        emitChange: true,
      });
    });
  };

  const renderDot = (_: number, index: number) => {
    const active = index === activeIndicator;
    const style = active
      ? {
          backgroundColor: props.indicatorColor,
        }
      : undefined;

    return (
      <i key={index} style={style} className={bem('indicator', { active })} />
    );
  };

  const renderIndicator = () => {
    if (props.renderIndicator) {
      return props.renderIndicator?.({
        active: activeIndicator,
      });
    }

    if (props.showIndicators && count > 1) {
      return (
        <div className={bem('indicators', { vertical: props.vertical })}>
          {Array(count).fill('').map(renderDot)}
        </div>
      );
    }
  };

  React.useEffect(() => {
    initialize(+initialSwipe);
  }, [initialSwipe]);

  React.useEffect(() => {
    initialize(state.active);
  }, [count]);

  React.useEffect(() => {
    autoplay();
  }, [count, pAutoplay]);

  React.useEffect(() => {
    resize();
  }, [windowSize.width, windowSize.height]);

  // TODO usePageVisibility
  // watch(usePageVisibility(), (visible) => {
  //   if (visible === 'visible') {
  //     autoplay();
  //   } else {
  //     stopAutoplay();
  //   }
  // });

  useMount(initialize);
  // onPopupReopen(() => initialize(state.active));
  useUnmount(stopAutoplay);

  const instance = React.useMemo<SwipeInstance>(
    () => ({
      $el: root.current,
      size,
      count,
      props,
      activeIndicator,
      resize,
      swipeTo,
      prev,
      next,
    }),
    [
      size,
      count,
      props,
      activeIndicator,
      root.current,
      resize,
      swipeTo,
      prev,
      next,
    ],
  );

  React.useImperativeHandle(ref, () => instance);

  return (
    <SwipeContext.Provider
      value={{
        parent: instance,
        children: children.current,
        push,
        pop,
      }}
    >
      <div
        ref={root}
        className={classNames(bem(), props.className)}
        style={props.style}
      >
        <div
          style={trackStyle}
          className={bem('track', { vertical: props.vertical })}
          onTouchStart={onTouchStart}
          onTouchMove={onTouchMove}
          onTouchEnd={onTouchEnd}
          onTouchCancel={onTouchEnd}
        >
          {props.children}
        </div>
        {renderIndicator()}
      </div>
    </SwipeContext.Provider>
  );
};

const Swipe = React.forwardRef(InlineSwipe);

Swipe.defaultProps = {
  duration: 500,
  initialSwipe: 0,
  // width: 'auto',
  // height: 'auto',
  loop: true,
  showIndicators: true,
  vertical: false,
  touchable: true,
  stopPropagation: true,
  lazyRender: false,
  indicatorColor: '#1989fa',
};

export default Swipe;
