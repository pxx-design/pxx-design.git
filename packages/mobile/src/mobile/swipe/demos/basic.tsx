/**
 * title: 基础用法
 * desc: 每个 SwipeItem 代表一张轮播卡片，可以通过 `autoplay` 属性设置自动轮播的间隔。
 */

import React from 'react';
import styled from 'styled-components';
import { Swipe, SwipeItem } from '@parallel-line/mobile';

const BasicSwipe = styled(Swipe)`
  .pxx-mobile-swipe-item {
    color: #fff;
    font-size: 20px;
    line-height: 150px;
    text-align: center;
    background-color: #39a9ed;
  }
`;

export default () => {
  return (
    <BasicSwipe indicatorColor="white">
      <SwipeItem key={1}>1</SwipeItem>
      <SwipeItem key={2}>2</SwipeItem>
      <SwipeItem key={3}>3</SwipeItem>
      <SwipeItem key={4}>4</SwipeItem>
    </BasicSwipe>
  );
};
