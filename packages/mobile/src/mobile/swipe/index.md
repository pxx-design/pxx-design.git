---
title: Swipe 轮播
mobile: true
---

# Swipe 轮播

用于循环播放一组图片或内容。

## 代码演示

<code src="./demos/basic.tsx"></code>

<!-- <API exports='["default"]'></API> -->
