import React from 'react';
import classNames from "classnames";
import {useControllableValue} from 'ahooks';
import {useNamespace} from '@parallel-line/common/es/hooks';

interface SwitchProps {
  value?: any;
  checkedValue?: any;
  className?: string;
  defaultValue?: boolean;
  style?: React.CSSProperties;
  onChange?: (value: any) => void;
}

const Switch = (props: SwitchProps) => {
  const {
    style,
    className,
    checkedValue,
  } = props;
  const [cls, bem] = useNamespace('switch');
  const [value, setValue] = useControllableValue(props)
  const handleClick = () => {
    setValue(!value ? (checkedValue ?? true) : undefined);
  }
  return (
    <div style={style}
         onClick={handleClick}
         className={classNames(cls,
           {[bem('checked')]: value},
           className)}>
      <div className={bem('dot-wrapper')}>
        <div className={bem('dot')}/>
      </div>
    </div>
  );
};

export default Switch;
