import React from 'react';
import {Switch, Space} from '@parallel-line/mobile';

const Basic = () => {
  return (
    <Space direction="vertical">
      <Switch value="123" onChange={(value: any) => console.log(value)}/>
    </Space>
  );
};

export default Basic;
