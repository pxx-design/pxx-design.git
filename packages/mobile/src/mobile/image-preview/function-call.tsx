import * as React from 'react';
import { render, createElement } from '@parallel-line/utils';
import { isArray, extend } from 'lodash-es';
import ConfigProvider from '../config-provider';
import ImagePreviewComponent, { ImagePreviewProps } from './ImagePreview';
import { inBrowser } from '@parallel-line/common/es/utils';

export interface ImagePreviewOptions
  extends Omit<ImagePreviewProps, 'show' | 'onShowChange'> {}

const defaultConfig: ImagePreviewOptions = {
  loop: true,
  images: [],
  maxZoom: 3,
  minZoom: 1 / 3,
  onScale: undefined,
  onClose: undefined,
  onChange: undefined,
  className: '',
  showIndex: true,
  closeable: true,
  transition: undefined,
  beforeClose: undefined,
  overlayStyle: undefined,
  startPosition: 0,
  swipeDuration: 300,
  showIndicators: false,
  closeOnPopstate: true,
  closeIconPosition: 'top-right',
};

let instance: { open(options: ImagePreviewOptions): void };

function initInstance() {
  const E = createElement();

  function render(props: ImagePreviewProps) {
    setTimeout(() => {
      E.render(
        <ConfigProvider>
          <ImagePreviewComponent {...props} />
        </ConfigProvider>,
      );
    });
  }

  function close(options: ImagePreviewOptions) {
    render(extend({}, options, { show: false }));
  }

  function open(options: ImagePreviewOptions) {
    render(extend({}, options, { show: true, onClosed: () => close(options) }));
  }

  instance = { open };
  return instance;
}

const ImagePreview = (
  images: string[] | ImagePreviewOptions,
  startPosition = 0,
) => {
  /* istanbul ignore if */
  if (!inBrowser) {
    return;
  }

  // if (!instance) {
  //   initInstance();
  // }

  const options = isArray(images) ? { images, startPosition } : images;
  const instance = render(
    ImagePreviewComponent,
    extend({}, defaultConfig, options),
  );

  instance.open();

  return instance;
};

export { ImagePreview };
