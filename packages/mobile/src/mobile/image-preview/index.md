---
title: ImagePreview 图片预览
mobile: true
---

# ImagePreview 图片预览

图片放大预览，支持函数调用和组件调用两种方式。

## 代码演示

<code src="./demos/basic.tsx"></code>

<!-- <API exports='["default"]'></API> -->
