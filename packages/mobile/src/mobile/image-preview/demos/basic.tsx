/**
 * title: 基础用法
 * desc: 直接传入图片数组，即可展示图片预览。
 */

import React from 'react';
import { Button,ImagePreview } from '@parallel-line/mobile';

export default () => {
  return (
    <div>
      <Button
        type="primary"
        onClick={() => {
          ImagePreview([
            'https://img01.yzcdn.cn/vant/apple-1.jpg',
            'https://img01.yzcdn.cn/vant/apple-2.jpg',
            'https://img01.yzcdn.cn/vant/apple-3.jpg',
            'https://img01.yzcdn.cn/vant/apple-4.jpg',
          ]);
        }}
      >
        预览图片
      </Button>
    </div>
  );
};
