import * as React from 'react';
import classNames from 'classnames';
import { CloseOutlined } from '@parallel-line/icons';
import { useMount, useSize, useBoolean, useSetState } from 'ahooks';
import { useNamespace } from '@parallel-line/common/es/hooks';
import ImagePreviewItem from './ImagePreviewItem';
import Swipe, { SwipeInstance, SwipeToOptions } from '../swipe';
import Popup, { PopupCloseIconPosition } from '../popup';
import {
  pick,
  callInterceptor,
  Interceptor,
} from '@parallel-line/common/es/utils';

export interface ImagePreviewProps {
  loop?: boolean;
  overlay?: boolean;
  closeable?: boolean;
  showIndex?: boolean;
  className?: string;
  transition?: string;
  beforeClose?: Interceptor;
  overlayStyle?: React.CSSProperties;
  showIndicators?: boolean;
  closeOnPopstate?: boolean;
  images: string[];
  minZoom?: number | string;
  maxZoom?: number | string;
  swipeDuration?: number | string;
  startPosition?: number | string;
  closeIconPosition?: PopupCloseIconPosition;
  /** 自定义关闭图标 */
  closeIcon?: (props: {
    className?: string;
    onClick?: React.MouseEventHandler<HTMLSpanElement>;
  }) => React.ReactNode;

  renderIndex?: (args: { index: number }) => React.ReactNode;
  renderCover?: () => React.ReactNode;

  onScale?(args: ScaleEventParams): void;
  onClose?(args: { index: number; url: string }): void;
  onClosed?(): void;
  onChange?(active: number): void;
  onShowChange?(show: boolean): void;

  visible?: boolean;
  afterClose?: () => void;
}

export interface ScaleEventParams {
  scale: number;
  index: number;
}

interface ImagePreviewState {
  active: number;
  rootWidth: number;
  rootHeight: number;
}

const ImagePreview = React.forwardRef<unknown, ImagePreviewProps>(
  (props, ref) => {
    const [, bem] = useNamespace('image-preview');
    const swipeRef = React.useRef<SwipeInstance>(null);
    const windowSize = useSize(document.body);
    const {
      maxZoom = 3,
      minZoom = 1 / 3,
      swipeDuration = 300,
      startPosition = 0,
    } = props;
    const [show, { toggle }] = useBoolean();
    const [state, setState] = useSetState<ImagePreviewState>({
      active: 0,
      rootWidth: 0,
      rootHeight: 0,
    });

    const resize = () => {
      if (swipeRef.current) {
        const rect = swipeRef.current?.$el?.getBoundingClientRect();
        const newState: Partial<ImagePreviewState> = {};
        newState.rootWidth = rect?.width;
        newState.rootHeight = rect?.height;
        swipeRef.current?.resize();
        setState(newState);
      }
    };

    const emitScale = (args: ScaleEventParams) => props.onScale?.(args);

    const updateShow = (show: boolean) => {
      toggle(show);
      props.onShowChange?.(show);
    };

    const emitClose = () => {
      callInterceptor({
        interceptor: props.beforeClose,
        args: [state.active],
        done: () => updateShow(false),
      });
    };

    const setActive = (active: number) => {
      setState({ active });
      props.onChange?.(active);
    };

    const renderIndex = () => {
      if (props.showIndex) {
        return (
          <div className={bem('index')}>
            {props.renderIndex
              ? props.renderIndex({ index: state.active })
              : `${state.active + 1} / ${props.images.length}`}
          </div>
        );
      }
    };

    const renderCover = () => {
      if (props.renderCover) {
        return <div className={bem('cover')}>{props.renderCover()}</div>;
      }
    };

    const renderImages = () => (
      <span>
        <Swipe
          ref={swipeRef}
          lazyRender
          loop={props.loop}
          className={bem('swipe')}
          duration={swipeDuration}
          initialSwipe={props.startPosition}
          showIndicators={props.showIndicators}
          indicatorColor="white"
          onChange={setActive}
        >
          {props.images.map((image) => (
            <ImagePreviewItem
              key={image}
              src={image}
              show={show}
              active={state.active}
              maxZoom={maxZoom}
              minZoom={minZoom}
              rootWidth={state.rootWidth}
              rootHeight={state.rootHeight}
              onScale={emitScale}
              onClose={emitClose}
            />
          ))}
        </Swipe>
      </span>
    );

    const renderClose = () => {
      if (props.closeable) {
        return (
          <span className={bem('close-icon', props.closeIconPosition)}>
            <CloseOutlined onClick={emitClose} />
          </span>
        );
      }
    };

    const onClosed = () => props.onClosed?.();

    const swipeTo = (index: number, options?: SwipeToOptions) =>
      swipeRef.current?.swipeTo(index, options);

    useMount(() => {
      toggle(props.visible);
      resize();
    });

    React.useEffect(() => {
      resize();
    }, [windowSize.width, windowSize.height]);

    React.useEffect(() => {
      setActive(+startPosition);
    }, [props.startPosition]);

    React.useEffect(() => {
      toggle(props.visible);
    }, [props.visible]);

    React.useEffect(() => {
      const { images } = props;
      if (show) {
        setActive(+startPosition);
        setTimeout(() => {
          resize();
          swipeTo(+startPosition, { immediate: true });
        });
      } else {
        props.onClose?.({
          index: state.active,
          url: images[state.active],
        });
      }
    }, [show]);

    return (
      <Popup
        ref={ref}
        visible={show}
        className={classNames([bem(), props.className])}
        overlayClass={bem('overlay')}
        onClosed={onClosed}
        {...pick(props, ['transition', 'overlayStyle', 'closeOnPopstate'])}
        onChange={updateShow}
      >
        {renderClose()}
        {renderImages()}
        {renderIndex()}
        {renderCover()}
      </Popup>
    );
  },
);

export default ImagePreview;
