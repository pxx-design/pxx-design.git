import * as React from 'react';
import { useSetState } from 'ahooks';
import { calc } from '@parallel-line/utils';
import Image from '../image';
import SwipeItem from '../swipe-item';
import { useTouch, useNamespace } from '@parallel-line/common/es/hooks';
import { clamp, preventDefault } from '@parallel-line/common/es/utils';

interface ImagePreviewItemProps {
  src?: string;
  show?: boolean;
  active: number;
  minZoom: number | string;
  maxZoom: number | string;
  rootWidth?: number;
  rootHeight?: number;

  onScale?(p: { scale: number; index: number }): void;
  onClose?(): void;
}

interface ImagePreviewItemState {
  scale: number;
  moveX: number;
  moveY: number;
  moving: boolean;
  zooming: boolean;
  imageRatio: number;
  displayWidth: number;
  displayHeight: number;

  startMoveX?: number;
  startMoveY?: number;
  startScale?: number;
  startDistance?: number;
  doubleTapTimer?: NodeJS.Timeout | null;
  touchStartTime?: number;
}

function getDistance(touches: TouchList) {
  return Math.sqrt(
    (touches[0].clientX - touches[1].clientX) ** 2 +
      (touches[0].clientY - touches[1].clientY) ** 2,
  );
}

const ImagePreviewItem: React.FC<ImagePreviewItemProps> = (props) => {
  const [, bem] = useNamespace('image-preview');
  const touch = useTouch();
  const [state, setState] = useSetState<ImagePreviewItemState>({
    scale: 1,
    moveX: 0,
    moveY: 0,
    moving: false,
    zooming: false,
    imageRatio: 0,
    displayWidth: 0,
    displayHeight: 0,
  });

  const vertical = React.useMemo(() => {
    const rootRatio = calc(`${props.rootHeight} / ${props.rootWidth}`);
    return state.imageRatio > rootRatio;
  }, [props.rootWidth, props.rootHeight, state.imageRatio]);

  const imageStyle = React.useMemo(() => {
    const { scale, moveX, moveY, moving, zooming } = state;

    const style: React.CSSProperties = {
      transitionDuration: zooming || moving ? '0s' : '.3s',
    };

    if (scale !== 1) {
      const offsetX = calc(`${moveX} / ${scale}`);
      const offsetY = calc(`${moveY} / ${scale}`);
      style.transform = `scale(${scale}, ${scale}) translate(${offsetX}px, ${offsetY}px)`;
    }

    return style;
  }, [state.scale, state.moveX, state.moveY, state.moving, state.zooming]);

  const maxMoveX = React.useMemo(() => {
    if (state.imageRatio) {
      const displayWidth = vertical
        ? calc(`${props.rootHeight} / ${state.imageRatio}`)
        : props.rootWidth;

      return Math.max(
        0,
        calc(`(${state.scale} * ${displayWidth} - ${props.rootWidth}) / 2`),
      );
    }

    return 0;
  }, [state.imageRatio, state.scale, props.rootWidth, props.rootHeight]);

  const maxMoveY = React.useMemo(() => {
    if (state.imageRatio) {
      const displayHeight = vertical
        ? props.rootHeight
        : calc(`${props.rootWidth} * ${state.imageRatio}`);

      return Math.max(
        0,
        calc(`(${state.scale} * ${displayHeight} - ${props.rootHeight}) / 2`),
      );
    }

    return 0;
  }, [state.imageRatio, state.scale, props.rootWidth, props.rootHeight]);

  const setScale = (scale: number) => {
    scale = clamp(scale, +props.minZoom, +props.maxZoom);

    if (scale !== state.scale) {
      setState({ scale });
      props.onScale?.({ scale, index: props.active });
    }
  };

  const resetScale = () => {
    setScale(1);
    setState({ moveX: 0, moveY: 0 });
  };

  const toggleScale = () => {
    const scale = state.scale > 1 ? 1 : 2;

    setScale(scale);
    setState({ moveX: 0, moveY: 0 });
  };

  const onTouchStart: any = (event: TouchEvent) => {
    const { touches } = event;
    const { offsetX } = touch;
    const newState: Partial<ImagePreviewItemState> = {};

    touch.start(event);

    newState.startMoveX = state.moveX;
    newState.startMoveY = state.moveY;
    newState.touchStartTime = Date.now();

    state.moving = touches.length === 1 && state.scale !== 1;
    state.zooming = touches.length === 2 && !offsetX.current;

    if (state.zooming) {
      newState.startScale = state.scale;
      newState.startDistance = getDistance(event.touches);
    }

    setState(newState);
  };

  const onTouchMove: any = (event: TouchEvent) => {
    const { touches } = event;

    touch.move(event);

    if (state.moving || state.zooming) {
      preventDefault(event, true);
    }

    if (state.moving) {
      const { deltaX, deltaY } = touch;
      const moveX = calc(`${deltaX.current} + ${state.startMoveX}`);
      const moveY = calc(`${deltaY.current} + ${state.startMoveY}`);
      state.moveX = clamp(moveX, -maxMoveX, maxMoveX);
      state.moveY = clamp(moveY, -maxMoveY, maxMoveY);
    }

    if (state.zooming && touches.length === 2) {
      const distance = getDistance(touches);
      const scale = calc(
        `(${state.startScale} * ${distance}) / ${state.startDistance}`,
      );

      setScale(scale);
    }
  };

  const checkTap = () => {
    const { offsetX, offsetY } = touch;
    const newState: Partial<ImagePreviewItemState> = {};
    const deltaTime = calc(`${Date.now()} - ${state.touchStartTime}`);
    const TAP_TIME = 250;
    const TAP_OFFSET = 10;

    if (
      offsetX.current < TAP_OFFSET &&
      offsetY.current < TAP_OFFSET &&
      deltaTime < TAP_TIME
    ) {
      if (state.doubleTapTimer) {
        clearTimeout(state.doubleTapTimer);
        newState.doubleTapTimer = null;
        toggleScale();
      } else {
        newState.doubleTapTimer = setTimeout(() => {
          props.onClose?.();
          setState({ doubleTapTimer: null });
        }, TAP_TIME);
      }
    }

    setState(newState);
  };

  const onTouchEnd: any = (event: TouchEvent) => {
    let stopPropagation = false;
    const newState: Partial<ImagePreviewItemState> = {};

    /* istanbul ignore else */
    if (state.moving || state.zooming) {
      stopPropagation = true;

      if (
        state.moving &&
        state.startMoveX === state.moveX &&
        state.startMoveY === state.moveY
      ) {
        stopPropagation = false;
      }

      if (!event.touches.length) {
        if (state.zooming) {
          state.moveX = clamp(state.moveX, -maxMoveX, maxMoveX);
          state.moveY = clamp(state.moveY, -maxMoveY, maxMoveY);
          state.zooming = false;
        }

        state.moving = false;
        newState.startMoveX = 0;
        newState.startMoveY = 0;
        newState.startScale = 1;

        if (state.scale < 1) {
          resetScale();
        }
      }

      setState(newState);
    }

    // eliminate tap delay on safari
    preventDefault(event, stopPropagation);

    checkTap();
    touch.reset();
  };

  const onLoad = (event: Event) => {
    const { naturalWidth, naturalHeight } = event.target as HTMLImageElement;
    state.imageRatio = naturalHeight / naturalWidth;
  };

  React.useEffect(() => {
    resetScale();
  }, [props.active]);

  React.useEffect(() => {
    if (!props.show) {
      resetScale();
    }
  }, [props.show]);

  return (
    <SwipeItem
      className={bem('swipe-item')}
      onTouchStart={onTouchStart}
      onTouchMove={onTouchMove}
      onTouchEnd={onTouchEnd}
      onTouchCancel={onTouchEnd}
    >
      <Image
        src={props.src}
        fit="contain"
        className={bem('image', { vertical: vertical })}
        style={imageStyle}
        onLoad={onLoad}
      />
    </SwipeItem>
  );
};

export default ImagePreviewItem;
