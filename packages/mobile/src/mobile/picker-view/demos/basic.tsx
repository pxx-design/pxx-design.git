import React from 'react';
import {PickerView} from '@parallel-line/mobile';
import {Space} from 'antd';

const columns = [
  ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N'],
  ['1', '222222222222222222222222222222222', '3'],
]

const Basic = () => {
  return (
    <Space style={{width: '100%'}} direction="vertical">
      <PickerView columns={columns} />
    </Space>
  );
};

export default Basic;
