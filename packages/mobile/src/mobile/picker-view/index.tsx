import React from 'react';
import { useNamespace } from '@parallel-line/common/es/hooks';
import { PickerView } from 'ant-design-mobile';
import { PickerViewProps } from 'ant-design-mobile/es/components/picker-view';
import { withNativeProps } from 'ant-design-mobile/es/utils/native-props';

declare module 'React' {
  interface Attributes {
    enterKeyHint?: unknown;
  }
}

export type {
  PickerViewProps,
  PickerValue,
  PickerColumnItem,
  PickerColumn,
} from 'ant-design-mobile/es/components/picker-view';

export default (props: PickerViewProps) => {
  const [, bem] = useNamespace('picker-view');
  return withNativeProps(props, <PickerView className={bem()} {...props} />);
};
