import * as React from 'react';
import { tuple, Breakpoint } from '@parallel-line/common/es/utils';

const RowAligns = tuple('top', 'middle', 'bottom', 'stretch');
const RowJustify = tuple(
  'start',
  'end',
  'center',
  'space-around',
  'space-between',
);

export type Gutter = number | Partial<Record<Breakpoint, number>>;
export interface RowProps extends React.HTMLAttributes<HTMLDivElement> {
  gutter?: Gutter | [Gutter, Gutter];
  align?: typeof RowAligns[number];
  justify?: typeof RowJustify[number];
  prefixCls?: string;
  wrap?: boolean;
}

const Row = React.forwardRef<HTMLDivElement, RowProps>((props, ref) => {
  return <div ref={ref} {...props}></div>;
});

Row.displayName = 'Row';

export default Row;
