import React, { useRef, useState, useContext } from 'react';
import {
  useMount,
  useSize,
  useEventListener,
  useRequest,
  useBoolean,
} from 'ahooks';
import { LoadingOutlined } from '@parallel-line/icons';
import classNames from 'classnames';
import { get, global, calc } from '@parallel-line/utils';
import PullRefresh from '../pull-refresh';
import { isHidden, getRect } from '@parallel-line/common/es/utils';
import { useNamespace, useScrollParent } from '@parallel-line/common/es/hooks';
import ConfigProvider from '../config-provider';

export type ListDirection = 'up' | 'down';

export interface ListViewProps {
  className?: string;
  style?: React.CSSProperties;
  /**
   * 默认 false。 即在初始化时自动执行 request。
   *
   * 如果设置为 true，则需要手动调用 emit 触发执行。
   */
  manual?: boolean;
  /**
   * 请求唯一标识。
   */
  uniqueKey: string;
  /**
   * 默认每页的数量
   */
  defaultPageSize?: number;
  /**
   * 请求数据的 service
   */
  request: (p: { current: number; pageSize: number }) => Promise<any>;
  /**
   * 格式化请求结果
   */
  formatResult?: (
    response: any,
  ) => {
    total: number;
    list: any[];
    [key: string]: any;
  };
  children?(list: any[]): React.ReactNode;
  /** 加载失败后的提示文案 */
  errorText?: string;
  /** 加载过程中的提示文案 */
  loadingText?: string;
  /** 加载完成后的提示文案 */
  finishedText?: string;
  /** 滚动条与底部距离小于 offset 时触发load事件 */
  offset?: number | string;
  /** 滚动触发加载的方向，可选值为up */
  direction?: ListDirection;
  /** 默认文案设置 */
  locale?: {
    /** 自定义空数据时的提示 */
    empty?: React.ReactNode;
    /** 自定义加载失败后的提示 */
    error?: React.ReactNode;
    /** 自定义底部加载中提示 */
    loading?: React.ReactNode;
    /** 自定义加载完成后的提示 */
    finished?: React.ReactNode;
  };
}

const ListView: React.FC<ListViewProps> = (props) => {
  const page = useRef(1);
  const [list, setList] = useState<any[]>([]);
  const [, bem, t] = useNamespace('list-view');
  const [error, { toggle: setError }] = useBoolean(false);
  const [spinning, { setTrue, setFalse }] = useBoolean(true);
  const [finished, { toggle: setFinished }] = useBoolean(false);
  const { hd } = useContext(ConfigProvider.Context);
  const root = useRef<HTMLDivElement | null>(null);
  const parent = useRef<HTMLDivElement | null>(null);
  const statusEl = useRef<HTMLDivElement | null>(null);
  const placeholder = useRef<HTMLDivElement | null>(null);
  const scrollParent = useScrollParent(root);
  const { height: wrapperHeight } = useSize(parent);
  const { height: statusHeight } = useSize(statusEl);
  const request: ListViewProps['request'] = (d) =>
    props.request({ ...d, current: page.current });
  const contentHeight = React.useMemo(
    () => calc(`${wrapperHeight ?? 0} - ${statusHeight ?? 0}`) || undefined,
    [wrapperHeight, statusHeight],
  );

  const { loading, pagination } = useRequest(request, {
    manual: true,
    paginated: true,
    throwOnError: true,
    cacheKey: props.uniqueKey,
    defaultPageSize: props.defaultPageSize,
    formatResult: props.formatResult as any,
    onSuccess: (res) => {
      setFalse();
      if (res?.list?.length) {
        check();
        const newList = [...list, ...(res?.list || [])];
        setList(newList);
        setFinished(newList.length >= (res?.total || 0));
      } else {
        setFinished(true);
      }
    },
    onError: (error: any) => {
      setFalse();
      setError(true);
    },
  });

  useMount(() => {
    if (!props.manual) {
      setTimeout(onRefresh, 0);
    }
  });

  global.eventEmitter &&
    global.eventEmitter.useSubscription(({ uniqueKey, action = 'refresh' }) => {
      if (uniqueKey === props.uniqueKey) {
        switch (action) {
          case 'refresh':
            onRefresh();
            break;

          default:
            break;
        }
      }
    });

  const onLoad = () => {
    setError(false);
    page.current += 1;
    pagination.changeCurrent?.(page.current);
  };

  const onRefresh = () => {
    setTrue();
    setList([]);
    setError(false);
    setFinished(false);
    page.current = 1;
    pagination.changeCurrent?.(page.current);
  };

  const check = () => {
    setTimeout(() => {
      if (
        loading ||
        finished ||
        error
        // skip check when inside an inactive tab
        // tabStatus?.value === false
      ) {
        return;
      }

      const { offset = hd(300), direction } = props;
      const scrollParentRect = getRect(scrollParent.current);

      if (!scrollParentRect.height || isHidden(root.current)) {
        return;
      }

      let isReachEdge = false;
      const placeholderRect = getRect(placeholder.current);

      if (direction === 'up') {
        isReachEdge = scrollParentRect.top - placeholderRect.top <= offset;
      } else {
        isReachEdge =
          placeholderRect.bottom - scrollParentRect.bottom <= offset;
      }

      if (isReachEdge) {
        onLoad();
      }
    }, 0);
  };

  useEventListener('scroll', check, { target: scrollParent });

  const renderFinishedText = () => {
    if (finished && list?.length) {
      const text = props.locale?.finished
        ? props.locale?.finished
        : props.finishedText || t('finished');
      if (text) {
        return <div className={classNames(bem('finished-text'))}>{text}</div>;
      }
    }
    return null;
  };

  const clickErrorText = () => {
    setError(false);
    onRefresh();
  };

  const renderErrorText = () => {
    if (error) {
      const text = props.locale?.error
        ? props.locale?.error
        : props.errorText || t('error');
      if (text) {
        return (
          <div
            className={classNames(bem('error-text'))}
            onClick={clickErrorText}
          >
            {text}
          </div>
        );
      }
    }
    return null;
  };

  const renderLoading = () => {
    if (loading && !finished && !spinning) {
      return (
        <div className={classNames(bem('loading'))}>
          {props.locale?.loading ? (
            props.locale?.loading
          ) : (
            <>
              <LoadingOutlined className={classNames(bem('loading-icon'))} />
              {props.loadingText || t('loading')}
            </>
          )}
        </div>
      );
    }
    return null;
  };

  const renderContent = () => {
    if (!loading && list.length <= 0) {
      return (
        <div
          className={classNames(bem('empty'))}
          style={{ height: contentHeight }}
        >
          {props.locale?.empty ? props.locale?.empty : '暂无数据'}
        </div>
      );
    }
    return props.children?.(list);
  };
  const Placeholder = (
    <div ref={placeholder} className={classNames(bem('placeholder'))} />
  );

  return (
    <PullRefresh
      ref={parent}
      loading={spinning}
      onRefresh={onRefresh}
      className={props.className}
      style={props.style}
    >
      <div
        ref={root}
        role="feed"
        className={classNames(bem())}
        aria-busy={loading}
      >
        {props.direction === 'down' ? renderContent() : Placeholder}
        <div ref={statusEl}>
          {renderLoading()}
          {renderFinishedText()}
          {renderErrorText()}
        </div>
        {props.direction === 'up' ? renderContent() : Placeholder}
      </div>
    </PullRefresh>
  );
};

ListView.defaultProps = {
  direction: 'down',
  offset: 300,
  formatResult: (response: any) => ({
    list: get(response, 'data.rows', []),
    total: get(response, 'data.rowTotal', 0),
  }),
};

export default ListView;
