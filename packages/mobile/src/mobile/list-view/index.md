---
title: ListView 长列表
mobile: true
---

# ListView 长列表

适用于显示同类的长列表数据类型，对渲染性能有一定的优化效果。

## 代码演示

<code src="./demos/basic.tsx"></code>
<code src="./demos/empty.tsx"></code>

<API></API>
