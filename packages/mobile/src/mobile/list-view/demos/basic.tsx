/**
 * title: 基本
 * desc: 最简单的用法
 */

import React from 'react';
import { ListView } from '@parallel-line/mobile';

const dataSource: any[] = Array.from({ length: 50 }, (v, i) => ({
  id: i,
  title: `ScrollView title ${i}`,
}));

const asyncFn = ({ pageSize, offset }: any): Promise<any> =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve({
        data: {
          rowTotal: dataSource.length,
          rows: dataSource.slice(offset, offset + pageSize),
        },
      });
    }, 1000);
  });

const Basic = () => {
  return (
    <ListView
      uniqueKey="BasicListView"
      request={(d) =>
        asyncFn({
          offset: (d?.current - 1) * d?.pageSize,
          pageSize: d?.pageSize,
        })
      }
    >
      {(list) =>
        list.map((item, inx) => (
          <div
            key={inx}
            style={{
              height: 32,
              lineHeight: '32px',
              border: '1px solid #f2f2f2',
            }}
          >
            {item.title}
          </div>
        ))
      }
    </ListView>
  );
};

export default Basic;
