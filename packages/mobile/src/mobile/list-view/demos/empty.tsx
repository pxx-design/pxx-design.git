/**
 * title: 空数据
 * desc: 空数据
 */

import React from 'react';
import { ListView } from '@parallel-line/mobile';

const asyncFn = (): Promise<any> =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve({
        data: {
          rowTotal: 0,
          rows: [],
        },
      });
    }, 1000);
  });

const Basic = () => {
  console.log(ListView.displayName);

  return (
    <ListView uniqueKey="BasicListView" request={asyncFn}>
      {(list) => list.map((item, inx) => <div key={inx}>{item.title}</div>)}
    </ListView>
  );
};

export default Basic;
