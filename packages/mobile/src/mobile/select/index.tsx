import React from 'react';
import classNames from 'classnames';
import { MoreArrowOutlined } from '@parallel-line/icons';
import { useNamespace } from '@parallel-line/common/es/hooks';

interface SelectProps {
  label?: React.ReactNode;
  disabled?: boolean;
  className?: string;
  placeholder?: string;
  onClick?: React.DOMAttributes<HTMLDivElement>['onClick'];
}

const Select = (props: SelectProps) => {
  const { label, disabled = false, placeholder = '请选择' } = props;
  const [cls, bem] = useNamespace('select');
  return (
    <div
      className={classNames(bem('wrapper', { disabled }), props.className)}
      onClick={props.onClick}
    >
      <div className={cls}>
        {label ?? <div className={bem('placeholder')}>{placeholder}</div>}
      </div>
      <div className={bem('icon')}>
        <MoreArrowOutlined />
      </div>
    </div>
  );
};

export default Select;
