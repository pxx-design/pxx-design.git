import React from 'react';
import {Select,Input} from '@parallel-line/mobile';
import {Space} from 'antd'

const Basic = () => {
  return (
    <Space style={{width: '100%'}} direction="vertical">
      <Input/>
      <Input disabled value="牵引车/13.5米/柴油/5轴"/>
      <Select label="牵引车/13.5米/柴油/5轴"/>
    </Space>
  );
};

export default Basic;
