import React, { ReactNode } from 'react';
import { useNamespace } from '@parallel-line/common/es/hooks';
import Button from '../button';

export interface PickerContainerProps {
  title?: string;
  disabled?: string;
  children?: ReactNode;
  footer?: ReactNode;
  onClose?(): void;
  onConfirm?: (
    props: { onClose?(): void },
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ) => void;
}

export function Container(props: PickerContainerProps) {
  const [, bem] = useNamespace('picker__container');
  const {
    title,
    disabled,
    children,
    onConfirm,
    onClose,
    footer = (
      <Button block type="primary" onClick={(e) => onConfirm?.({ onClose }, e)}>
        {disabled ? '我知道了' : '确定'}
      </Button>
    ),
  } = props;
  return (
    <div className={bem({ disabled })}>
      <div className={bem('title')}>{title}</div>
      <div className={bem('content')}>{children}</div>
      <footer className={bem('footer')}>{footer}</footer>
    </div>
  );
}
