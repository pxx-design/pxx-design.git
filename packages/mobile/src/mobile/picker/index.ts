import { Picker } from './picker';
import { prompt } from './prompt';
import { Container } from './container';
import { attachPropertiesToComponent } from 'ant-design-mobile/es/utils/attach-properties-to-component';

export type { PickerProps } from './picker';

export type {
  PickerValue,
  PickerColumnItem,
  PickerColumn,
} from '../picker-view';

export default attachPropertiesToComponent(Picker, {
  Container,
  prompt,
});
