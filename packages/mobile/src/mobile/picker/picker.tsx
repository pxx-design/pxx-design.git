import React, { cloneElement, ReactNode } from 'react';
import { omit } from 'lodash-es';
import { useNewControllableValue } from 'ant-design-mobile/es/utils/use-controllable-value';
import { PickerValue, PickerViewProps } from '../picker-view';
import { PickerContainerProps } from './container';
import { prompt } from './prompt';

export interface PickerProps
  extends PickerViewProps,
    Omit<PickerContainerProps, 'onConfirm' | 'onClose' | 'children'> {
  children: (value: PickerValue[]) => ReactNode;
  onConfirm?: (value: PickerValue[]) => void;
  onCancel?: () => void;
}

export function Picker(props: PickerProps) {
  const { children } = props;
  const [value, setValue] = useNewControllableValue<PickerValue[]>({
    value: props.value,
    defaultValue: props.defaultValue ?? [],
    onChange: props.onChange,
  });
  const onClick = (onClick: () => void) => {
    prompt({
      ...omit(props, ['children', 'onConfirm', 'value', 'onChange']),
      defaultValue: value,
    }).then((v) => {
      setValue(v ?? []);
      props.onConfirm?.(v ?? []);
    });
    onClick();
  };

  return (React.Children.map(children(value), (child: any) => {
    return cloneElement(child, {
      ...child.props,
      onClick: (...args: any[]) =>
        onClick(() => child?.props?.onClick?.(...args)),
    });
  }) as unknown) as JSX.Element;
}
