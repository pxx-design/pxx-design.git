import React from 'react';
import { render } from '@parallel-line/utils';
import { useNewControllableValue } from 'ant-design-mobile/es/utils/use-controllable-value';
import Popup from '../popup';
import PickerView from '../picker-view';
import { Container } from './container';
import type { PickerProps, PickerValue } from './index';

function Content({
  title = '选择',
  disabled,
  onConfirm,
  ...props
}: Omit<PickerProps, 'children'>) {
  const [value, setValue] = useNewControllableValue<PickerValue[]>({
    value: props.value,
    defaultValue: props.defaultValue ?? [],
    onChange: props.onChange,
  });
  return (
    <Container
      title={title}
      disabled={disabled}
      {...props}
      onConfirm={({ onClose }) => {
        onClose?.();
        onConfirm?.(value);
      }}
    >
      <PickerView {...props} value={value} onChange={setValue} />
    </Container>
  );
}

export function prompt({
  onCancel,
  ...props
}: Omit<PickerProps, 'children' | 'onConfirm'>) {
  return new Promise<PickerValue[] | null>((resolve) => {
    render(Popup, {
      round: true,
      closeable: true,
      position: 'bottom',
      onClose: () => {
        onCancel?.();
        resolve(null);
      },
      content: (
        <Content
          {...props}
          onConfirm={(value) => {
            resolve(value);
          }}
        />
      ),
    }).open();
  });
}
