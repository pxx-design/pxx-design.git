import React from 'react';
import { Select, Picker } from '@parallel-line/mobile';
import { Space, Divider } from 'antd';

const columns = [
  ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N'],
  ['1', '222222222222222222222222222222222', '3'],
];

const Basic = () => {
  return (
    <Space style={{ width: '100%' }} direction="vertical">
      <div>
        <Divider orientation="left">基础用法</Divider>
        <Picker title="选择器" columns={columns}>
          {(val) => <Select label={val.length ? val.join('/') : undefined} />}
        </Picker>
      </div>
    </Space>
  );
};

export default Basic;
