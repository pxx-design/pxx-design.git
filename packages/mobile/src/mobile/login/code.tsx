import React from 'react';
import Form from "../form";
import Input from "../input";
import Button from "../button";
import classNames from 'classnames';
import Whiteboard from '../whiteboard';
import Agreement from "../login/agreement";
import {Space} from "@parallel-line/common";
import {useNamespace} from '@parallel-line/common/es/hooks';
import {PasswordOutlined, WechatOutlined} from "@parallel-line/icons";

interface PhoneProps {
  phone?: string;
  loading?: boolean;
  isDriver?: boolean;
  isWorker?: boolean;
  isCountdown?: boolean;
  onWeChat?: () => void;
  isEnterprise?: boolean;
  onPassword?: () => void;
  countdown?: React.ReactNode;
  onFinish?: (values: any) => void;
  onAgreementChange?: (key: string) => void;
}

const Code = (props: PhoneProps) => {
  const {phone, countdown} = props;
  const [cls, bem] = useNamespace('login');

  return (
    <Whiteboard white noSpace className={cls} padding="0 24px">
      <div className={bem('phone-tip')}>
        <div>请输入验证码</div>
        <div className={bem('phone')}>验证码已发送至手机：{phone}</div>
      </div>
      <Form className={bem('form')} onFinish={props.onFinish}>
        <Space direction="vertical" size={16}>
          <Form.Item name="code"
                     rules={[ {required: true,message:'请输入验证码'},]}>
            <Input type="number"
                   maxLength={4}
                   placeholder="请输入验证码"
                   className={bem('input')}/>
          </Form.Item>
        </Space>
        <div>
          {countdown}
        </div>
        <Button size="large"
                type="primary"
                htmlType="submit"
                loading={props.loading}
                className={bem('submit')}>
          登录/注册
        </Button>
      </Form>
      <Agreement isDriver={props.isDriver}
                 isWorker={props.isWorker}
                 isEnterprise={props.isEnterprise}
                 onChange={props.onAgreementChange}/>
      <Space size={52} className={bem('bottom-btn')}>
        {
          props.onWeChat && (
            <button className={bem('round-btn')} onClick={props.onWeChat}>
              <div className={bem('round-btn-icon')} style={{backgroundColor: '#00b86b'}}>
                <WechatOutlined/>
              </div>
              <div>微信登录</div>
            </button>
          )
        }
        {
          props.onPassword && (
            <button className={bem('round-btn')} onClick={props.onPassword}>
              <div className={bem('round-btn-icon')} style={{backgroundColor: '#1552eb'}}>
                <PasswordOutlined/>
              </div>
              <div>密码登录</div>
            </button>
          )
        }
      </Space>
    </Whiteboard>
  );
};

export default Code;
