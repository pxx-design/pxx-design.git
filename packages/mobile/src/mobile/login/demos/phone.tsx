import React from 'react';
import {Login} from '@parallel-line/mobile';
import {global} from "@parallel-line/utils";

const Phone = () => {
  global.PXX_ENV.isDriver = true;
  return (
    <div style={{display: 'flex', height: '100%'}}>
      <Login.Phone/>
    </div>
  );
};

export default Phone;
