import React from 'react';
import {Login} from '@parallel-line/mobile';
import {global} from '@parallel-line/utils';

const Basic = () => {
  global.PXX_ENV.isDriver = true;
  return (
    <div style={{display: 'flex', height: '100%'}}>
      <Login />
    </div>
  );
};

export default Basic;
