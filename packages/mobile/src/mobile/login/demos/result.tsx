import React from 'react';
import {Login} from '@parallel-line/mobile';
import {global} from "@parallel-line/utils";

const Result = () => {
  global.PXX_ENV.isDriver = true;
  return (
    <div style={{display: 'flex', height: '100%'}}>
      <Login.Result message="您已提交申请，正在为您加速处理，请耐心等待"/>
    </div>
  );
};

export default Result;
