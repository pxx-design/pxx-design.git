import React from 'react';
import {useCountDown} from 'ahooks';
import {Login} from '@parallel-line/mobile';
import {global} from '@parallel-line/utils';

const Code = () => {
  global.PXX_ENV.isDriver = true;
  const [countdown, setTargetDate] = useCountDown();
  const isCountdowning = countdown !== 0;
  const handleSendSms = () => {
    if (isCountdowning) return;
    setTargetDate(Date.now() + 59000); // 60s倒计时
  };
  return (
    <div style={{display: 'flex', height: '100%'}}>
      <Login.Code phone="18888888888"
                  isCountdown={countdown !== 0}
                  countdown={(
                    <div onClick={handleSendSms}>
                      {isCountdowning
                        ? `${Math.round(countdown / 1000)}s后重新发送`
                        : "重新发送"}
                    </div>
                  )}/>
    </div>
  );
};

export default Code;
