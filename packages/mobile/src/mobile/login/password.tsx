import React from 'react';
import Form from '../form';
import Input from '../input';
import Button from '../button';
import Agreement from "./agreement";
import Whiteboard from '../whiteboard';
import {Space} from '@parallel-line/common';
import {global, verification} from '@parallel-line/utils';
import {useNamespace} from '@parallel-line/common/es/hooks';
import {WechatOutlined, PhoneOutlined, SpinnerOutlined} from "@parallel-line/icons";


interface PasswordProps {
  loading?: boolean;
  onCode?: () => void;
  onWeChat?: () => void;
  onFinish?: () => void;
  onSwitchEnv?: () => void;
  onAgreementChange?: (code: string) => void;
}

const Password = (props: PasswordProps) => {
  const {isDriver} = global.PXX_ENV;
  const [cls, bem] = useNamespace('login');
  return (
    <Whiteboard white noSpace className={cls} padding="0 24px">
      <div className={bem('welcome')}>
        <div>Hi~</div>
        <div>欢迎来到平行线{isDriver && '司机版'}</div>
      </div>
      <Form className={bem('form')} onFinish={props.onFinish}>
        <Space direction="vertical" size={16}>
          <Form.Item name="username"
                     rules={[
                       {required: true},
                       verification.phoneNumber.rule,
                     ]}>
            <Input type="number"
                   maxLength={11}
                   placeholder="请输入手机号"
                   className={bem('input')}/>
          </Form.Item>
          <Form.Item name="password"
                     rules={[{required: true}]}>
            <Input type="password"
                   placeholder="请输入密码"
                   className={bem('input')}/>
          </Form.Item>
        </Space>
        <Button size="large"
                type="primary"
                htmlType="submit"
                className={bem('submit')}>
          登录/注册
        </Button>
      </Form>
      <Agreement onChange={props.onAgreementChange}/>
      <Space size={52} className={bem('bottom-btn')}>
        {
          props.onWeChat &&
          <button className={bem('round-btn')} onClick={props.onWeChat}>
            <div className={bem('round-btn-icon')} style={{backgroundColor: '#00b86b'}}>
              <WechatOutlined/>
            </div>
            <div>微信登录</div>
          </button>
        }
        {
          props.onCode &&
          <button className={bem('round-btn')} onClick={props.onCode}>
            <div className={bem('round-btn-icon')} style={{backgroundColor: '#18a6fd'}}>
              <PhoneOutlined/>
            </div>
            <div>验证码登录</div>
          </button>
        }
      </Space>
    </Whiteboard>
  );
};

export default Password;
