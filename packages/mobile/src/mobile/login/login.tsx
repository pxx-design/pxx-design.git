import React from 'react';
import Button, { ButtonProps } from '../button';
import Agreement from './agreement';
import Whiteboard from '../whiteboard';
import { Space } from '@parallel-line/common';
import { global } from '@parallel-line/utils';
import { useNamespace } from '@parallel-line/common/es/hooks';
import {
  WechatOutlined,
  PasswordOutlined,
  PhoneOutlined,
} from '@parallel-line/icons';

interface LoginProps {
  loading?: boolean;
  onCode?: React.DOMAttributes<HTMLButtonElement>['onClick'];
  onWeChat?: ButtonProps['onGetPhoneNumber'];
  onPassword?: React.DOMAttributes<HTMLButtonElement>['onClick'];
  onSwitchEnv?: ButtonProps['onClick'];
  onAgreementChange?: (code: string) => void;
}

const Login = (props: LoginProps) => {
  const { isDevTest,isUat } = global.PXX_ENV;
  const [cls, bem] = useNamespace('login');
  return (
    <Whiteboard white noSpace className={cls} padding="0 24px">
      <div className={bem('logo')}>
        <img
          alt=""
          className={bem('logo-img')}
          src={`${global.assetsUrl}/image/login-logo.png`}
        />
      </div>
      <Space direction="vertical" size={4}>
        {(isDevTest || isUat) && (
          <Button
            size="large"
            type="warning"
            onClick={props.onSwitchEnv}
            className={bem('switch-btn')}
          >
            环境切换
          </Button>
        )}
        <Button
          size="large"
          type="primary"
          loading={props.loading}
          icon={<WechatOutlined />}
          openType="getPhoneNumber"
          onGetPhoneNumber={props.onWeChat}
        >
          微信帐号一键登录
        </Button>
      </Space>
      <Agreement onChange={props.onAgreementChange} />
      <Space size={52} className={bem('bottom-btn')}>
        {props.onPassword && (
          <button className={bem('round-btn')} onClick={props.onPassword}>
            <div
              className={bem('round-btn-icon')}
              style={{ backgroundColor: '#1552eb' }}
            >
              <PasswordOutlined />
            </div>
            <div>密码登录</div>
          </button>
        )}
        {props.onCode && (
          <button className={bem('round-btn')} onClick={props.onCode}>
            <div
              className={bem('round-btn-icon')}
              style={{ backgroundColor: '#18a6fd' }}
            >
              <PhoneOutlined />
            </div>
            <div>验证码登录</div>
          </button>
        )}
      </Space>
    </Whiteboard>
  );
};

export default Login;
