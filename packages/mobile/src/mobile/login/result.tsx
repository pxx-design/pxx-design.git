import React from 'react';
import Button from '../button';
import classNames from "classnames";
import Whiteboard from '../whiteboard';
import {CheckOutlined} from "@parallel-line/icons";
import {useNamespace} from '@parallel-line/common/es/hooks';

interface ResultProps {
  message?: string;
  onClick?: () => void;
}

const Result = (props: ResultProps) => {
  const [cls, bem] = useNamespace('login');
  return (
    <Whiteboard white className={classNames(cls, bem('result'))}>
      <div className={bem('result-content')}>
        <div className={bem('result-icon')}>
          <CheckOutlined/>
        </div>
        <div className={bem('result-message')}>{props.message}</div>
        <div className={bem('result-describe')}>
          审核结果会第一时间以短信的形式通知您，通过审核即可登录
        </div>
        <Button
          size="large"
          type="primary"
          onClick={props.onClick}
        >
          好的
        </Button>
      </div>
    </Whiteboard>
  );
};

export default Result;
