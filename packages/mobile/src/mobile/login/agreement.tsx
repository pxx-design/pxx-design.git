import React from 'react';
import classNames from "classnames";
import {global} from '@parallel-line/utils';
import {useNamespace} from '@parallel-line/common/es/hooks';

const config: any = {
  driver: [{
    key: "oidmda",
    title: "运输服务协议"
  }, {
    key: "kspxer",
    title: "平行线隐私政策"
  }, {
    key: "dr0u4p",
    title: "平行线登录服务协议"
  }],
  enterprise: [{
    key: "dr0u4p",
    title: "平行线登录服务协议"
  }, {
    key: "kspxer",
    title: "平行线隐私政策"
  }],
  worker: [
    {
      key: "dr0u4p",
      title: "平行线登录服务协议"
    }, {
      key: "kspxer",
      title: "平行线隐私政策"
    },
    {
      key: "tgvzio",
      title: "用工服务协议"
    }
  ]
}

interface AgreementProps {
  isDriver?: boolean;
  isWorker?: boolean;
  isEnterprise?: boolean;
  onChange?: (key: string) => void;
}

const Agreement = (props: AgreementProps) => {
  const {isDriver, isEnterprise, isWorker} = global.PXX_ENV;
  const [cls, bem] = useNamespace('login-agreement');
  const type = classNames({
    driver: props.isDriver ?? isDriver,
    worker: props.isWorker ?? isWorker,
    enterprise: props.isEnterprise ?? isEnterprise,
  })
  const handleChange = (key: string) => {
    props.onChange?.(key)
  }
  return (
    <div className={cls}>
      <span>登录视为已阅读并同意</span>
      {
        config[type]?.map((item: any) => {
          return (
            <span key={item.key}
                  className={bem('link')}
                  onClick={() => handleChange(item.key)}>
            《{item.title}》
            </span>
          )
        })
      }
    </div>
  );
};

export default Agreement;
