import Code from './Code';
import Phone from './phone';
import Result from './result';
import Password from './password';
import InternalLogin from './login';

type InternalLoginType = typeof InternalLogin;

interface LoginInterface extends InternalLoginType {
  Code: typeof Code;
  Phone: typeof Phone;
  Result: typeof Result;
  Password: typeof Password;
}

const Login = InternalLogin as LoginInterface;

Login.Code = Code;
Login.Phone = Phone;
Login.Result = Result;
Login.Password = Password;

export default Login;
