import React from 'react';
import Form from "../form";
import Input from "../input";
import Button from "../button";
import {isFunction} from 'lodash-es';
import Whiteboard from '../whiteboard';
import Agreement from "../login/agreement";
import {Space} from "@parallel-line/common";
import {global, verification} from "@parallel-line/utils";
import {useNamespace} from '@parallel-line/common/es/hooks';
import {PasswordOutlined, WechatOutlined} from "@parallel-line/icons";

interface PhoneProps {
  loading?: boolean;
  isDriver?: boolean;
  isWorker?: boolean;
  onWeChat?: () => void;
  isEnterprise?: boolean;
  onPassword?: () => void;
  onFinish?: (values: any) => void;
  onAgreementChange?: (code: string) => void;
}

const Phone = (props: PhoneProps) => {
  const isDriver = props.isDriver ?? global.PXX_ENV.isDriver;
  const [cls, bem] = useNamespace('login');
  return (
    <Whiteboard white noSpace className={cls} padding="0 24px">
      <div className={bem('welcome')}>
        <div>Hi~</div>
        <div>欢迎来到平行线{isDriver && '司机版'}</div>
      </div>
      <Form className={bem('form')} onFinish={props.onFinish}>
        <Space direction="vertical" size={16}>
          <Form.Item name="phoneNumber"
                     rules={[
                       {required: true,message:'请输入手机号'},
                       verification.phoneNumber.rule,
                     ]}>
            <Input type="number"
                   maxLength={11}
                   placeholder="请输入手机号"
                   className={bem('input')}/>
          </Form.Item>
        </Space>
        <Button size="large"
                type="primary"
                htmlType="submit"
                className={bem('submit')}>
          下一步
        </Button>
      </Form>
      <Agreement isDriver={isDriver}
                 isWorker={props.isWorker}
                 isEnterprise={props.isEnterprise}
                 onChange={props.onAgreementChange}/>
      <Space size={52} className={bem('bottom-btn')}>
        {
          isFunction(props.onWeChat) &&
          <button className={bem('round-btn')} onClick={props.onWeChat}>
            <div className={bem('round-btn-icon')} style={{backgroundColor: '#00b86b'}}>
              <WechatOutlined/>
            </div>
            <div>微信登录</div>
          </button>
        }
        {
          isFunction(props.onPassword) &&
          <button className={bem('round-btn')} onClick={props.onPassword}>
            <div className={bem('round-btn-icon')} style={{backgroundColor: '#1552eb'}}>
              <PasswordOutlined/>
            </div>
            <div>密码登录</div>
          </button>
        }
      </Space>
    </Whiteboard>
  );
};

export default Phone;
