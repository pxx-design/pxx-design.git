import React from 'react';
import {Badge, Button, Card, RouteInfo, SpaceBetween, Whiteboard} from '@parallel-line/mobile';

const Basic = () => {
  return (
    <div style={{height: "100%", backgroundColor: '#e6e6e6', display: 'flex'}}>
      <Whiteboard>
        <Card header={<Badge status="warning" text="待处理"/>}
              footer={
                <SpaceBetween>
                  <div>更多</div>
                  <div>
                    <Button size="mini"
                            color="gray">
                      编辑
                    </Button>
                    <Button size="mini"
                            color="gray">
                      发布
                    </Button>
                  </div>
                </SpaceBetween>
              }>
          <RouteInfo endAdcode="130110"
                     startAdcode="110101"/>
          <SpaceBetween>
            <div>货主：张三</div>
            <div>啦啦啦啦啦</div>
          </SpaceBetween>
        </Card>
        <Card header={<Badge status="warning" text="待处理"/>}
              footer={
                <SpaceBetween>
                  <div>更多</div>
                  <div>
                    <Button size="mini"
                            color="gray">
                      编辑
                    </Button>
                    <Button size="mini"
                            color="gray">
                      发布
                    </Button>
                  </div>
                </SpaceBetween>
              }>
          <RouteInfo endAdcode="130110"
                     startAdcode="110101"
                     count={20}
                     time={false}/>
          <SpaceBetween>
            <div>货主：张三</div>
            <div>啦啦啦啦啦</div>
          </SpaceBetween>
        </Card>
      </Whiteboard>
    </div>
  );
};

export default Basic;
