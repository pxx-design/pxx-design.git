import React from 'react';
import classNames from 'classnames';
import {Space} from '@parallel-line/common';
import {useNamespace} from "@parallel-line/common/es/common/hooks";

interface WhiteboardProps {
  size?: any;
  white?: boolean;
  noSpace?: boolean;
  padding?: string;
  className?: string;
  children?: React.ReactNode;
  style?: React.CSSProperties;
}

const Whiteboard = (props: WhiteboardProps) => {
  const {children, padding = '16px', size, style, className, white = false, noSpace = false} = props;
  const [cls, bem] = useNamespace('whiteboard')
  return (
    <div className={classNames(
      cls,
      {[bem('white')]: white},
      className)} style={{padding, ...style}}>
      {
        noSpace ? children :
          <Space size={size}
                 direction="vertical">
            {children}
          </Space>
      }
    </div>
  );
};

export default Whiteboard;
