/**
 * title: 基本
 * desc: 最简单的用法
 */

import React, { useState } from 'react';
import { useBoolean } from 'ahooks';
import { PullRefresh } from '@parallel-line/mobile';

const Basic = () => {
  const [count, setCount] = useState(0);
  const [loading, { toggle }] = useBoolean();
  const onRefresh = () => {
    toggle();
    setTimeout(() => {
      setCount(count + 1);
      toggle();
    }, 1600);
  };
  return (
    <PullRefresh loading={loading} onRefresh={onRefresh}>
      <p style={{ height: '100vh' }}>刷新次数: {count}</p>
    </PullRefresh>
  );
};

export default Basic;
