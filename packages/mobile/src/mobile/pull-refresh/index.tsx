import React, { useRef, useEffect, useContext } from 'react';
import classNames from 'classnames';
import { LoadingOutlined } from '@parallel-line/icons';
import { useSetState, useSize } from 'ahooks';
import {
  useTouch,
  useNamespace,
  useScrollParent,
} from '@parallel-line/common/es/hooks';
import { preventDefault, getScrollTop } from '@parallel-line/common/es/utils';
import ConfigProvider from '../config-provider';

export interface PullRefreshProps {
  className?: string;
  style?: React.CSSProperties;
  /** 是否处于加载中状态 */
  loading?: boolean;
  disabled?: boolean;
  /** 刷新成功提示文案 */
  successText?: string;
  /** 下拉过程提示文案 */
  pullingText?: string;
  /** 释放过程提示文案 */
  loosingText?: string;
  /** 加载过程提示文案 */
  loadingText?: string;
  /** 触发下拉刷新的距离 */
  pullDistance?: number | string;
  /** 刷新成功提示展示时长(ms) */
  successDuration?: number | string;
  /** 动画时长 */
  animationDuration?: number | string;
  /** 顶部内容高度 */
  headHeight?: number | string;
  pullRefreshStatus?: Record<PullRefreshStatus, React.ReactNode>;

  onRefresh?(): void;
}

type PullRefreshStatus =
  | 'normal'
  | 'loading'
  | 'loosing'
  | 'pulling'
  | 'success';

interface PullRefreshState {
  loading?: boolean;
  reachTop: boolean;
  status: PullRefreshStatus;
  distance: number;
  duration: number;
}

const DEFAULT_HEAD_HEIGHT = 50;
const TEXT_STATUS = ['pulling', 'loosing', 'success'];

const PullRefresh = React.forwardRef<
  HTMLDivElement,
  React.PropsWithChildren<PullRefreshProps>
>((props, ref) => {
  const [, bem, t] = useNamespace('pull-refresh');
  const { hd } = useContext(ConfigProvider.Context);
  const {
    headHeight = hd(DEFAULT_HEAD_HEIGHT),
    successDuration = hd(500),
    animationDuration = hd(300),
    pullRefreshStatus = {} as Record<PullRefreshStatus, React.ReactNode>,
  } = props;
  const root = useRef<HTMLDivElement>(null);
  const wrapper = useRef<HTMLDivElement>(null);
  const scrollParent = useScrollParent(root);
  const touch = useTouch();
  const { height: wrapperHeight } = useSize(wrapper);
  const [state, setState] = useSetState<PullRefreshState>({
    reachTop: false,
    status: 'normal',
    distance: 0,
    duration: 0,
  });

  React.useImperativeHandle(ref, () => wrapper.current!);

  useEffect(() => {
    setState({ loading: props.loading, duration: +animationDuration });

    if (props.loading) {
      setStatus(+headHeight, true);
    } else if (pullRefreshStatus.success || props.successText) {
      showSuccessTip();
    } else {
      setStatus(0, false);
    }
  }, [props.loading]);

  const getHeadStyle = () => {
    if (props.headHeight !== DEFAULT_HEAD_HEIGHT) {
      return {
        height: `${props.headHeight}px`,
      };
    }
  };

  const isTouchable = () =>
    state.status !== 'loading' && state.status !== 'success' && !props.disabled;

  const ease = (distance: number) => {
    const pullDistance = +(props.pullDistance || headHeight);

    if (distance > pullDistance) {
      if (distance < pullDistance * 2) {
        distance = pullDistance + (distance - pullDistance) / 2;
      } else {
        distance = pullDistance * 1.5 + (distance - pullDistance * 2) / 4;
      }
    }

    return Math.round(distance);
  };

  const setStatus = (distance: number, isLoading?: boolean) => {
    const pullDistance = +(props.pullDistance || headHeight);
    const newState: Partial<PullRefreshState> = { distance };

    if (isLoading) {
      newState.status = 'loading';
    } else if (distance === 0) {
      newState.status = 'normal';
    } else if (distance < pullDistance) {
      newState.status = 'pulling';
    } else {
      newState.status = 'loosing';
    }

    setState(newState);
  };

  const getStatusText = () => {
    const { status } = state;
    if (status === 'normal') {
      return '';
    }
    return (props as any)[`${status}Text`] || t(status);
  };

  const renderStatus = () => {
    const { status, distance } = state;

    if (React.isValidElement(pullRefreshStatus[status])) {
      return React.cloneElement(pullRefreshStatus[status] as any, {
        distance,
      });
    }

    const nodes: React.ReactNode[] = [];

    if (TEXT_STATUS.includes(status)) {
      nodes.push(
        <div className={classNames(bem('text'))} key="status">
          {getStatusText()}
        </div>,
      );
    }
    if (status === 'loading') {
      nodes.push(
        <React.Fragment key="loading">
          <LoadingOutlined className={classNames(bem('loading'))} />
          {getStatusText()}
        </React.Fragment>,
      );
    }

    return nodes;
  };

  const showSuccessTip = () => {
    setState({ status: 'success' });

    setTimeout(() => {
      setStatus(0);
    }, +successDuration);
  };

  const checkPosition = (event: TouchEvent) => {
    const reachTop = getScrollTop(scrollParent.current!) === 0;

    if (reachTop) {
      setState({ reachTop, duration: 0 });
      touch.start(event);
    }
  };

  const onTouchStart: any = (event: TouchEvent) => {
    if (isTouchable()) {
      checkPosition(event);
    }
  };

  const onTouchMove: any = (event: TouchEvent) => {
    if (isTouchable()) {
      if (!state.reachTop) {
        checkPosition(event);
      }

      const { deltaY } = touch;
      touch.move(event);

      if (state.reachTop && deltaY.current >= 0 && touch.isVertical()) {
        preventDefault(event);
        setStatus(ease(deltaY.current));
      }
    }
  };

  const onTouchEnd = () => {
    if (state.reachTop && touch.deltaY.current && isTouchable()) {
      setState({ distance: +animationDuration });

      if (state.status === 'loosing') {
        setStatus(+headHeight, true);

        setState({ loading: false });

        // ensure value change can be watched
        setTimeout(() => {
          props.onRefresh?.();
        }, 66);
      } else {
        setStatus(0);
      }
    }
  };

  const trackStyle: React.CSSProperties = {
    minHeight: wrapperHeight,
    transitionDuration: `${state.duration}ms`,
    transform: state.distance ? `translate3d(0,${state.distance}px, 0)` : '',
  };

  return (
    <div
      ref={wrapper}
      className={classNames(bem('wrapper'), props.className)}
      style={props.style}
    >
      <div ref={root} className={classNames(bem())}>
        <div
          className={classNames(bem('track'))}
          style={trackStyle}
          onTouchStart={onTouchStart}
          onTouchMove={onTouchMove}
          onTouchEnd={onTouchEnd}
          onTouchCancel={onTouchEnd}
        >
          <div className={classNames(bem('head'))} style={getHeadStyle()}>
            {renderStatus()}
          </div>
          {props.children}
        </div>
      </div>
    </div>
  );
});

PullRefresh.defaultProps = {
  loading: false,
};

export default PullRefresh;
