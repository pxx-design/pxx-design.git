---
title: PullRefresh 下拉刷新
mobile: true
---

# PullRefresh 下拉刷新

用于提供下拉刷新的交互操作。

## 代码演示

<code src="./demos/basic.tsx"></code>

<API exports='["default"]'></API>
