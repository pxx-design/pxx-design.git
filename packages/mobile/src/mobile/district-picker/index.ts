import { attachPropertiesToComponent } from 'ant-design-mobile/es/utils/attach-properties-to-component';
import { DistrictPicker } from './DistrictPicker';
import { prompt } from './prompt';

export type { DistrictPickerProps } from './DistrictPicker';

export default attachPropertiesToComponent(DistrictPicker, {
  prompt,
});
