import React, { cloneElement, ReactNode } from 'react';
import { omit } from 'lodash-es';
import { useNewControllableValue } from 'ant-design-mobile/es/utils/use-controllable-value';
import { PickerContainerProps } from '../picker/container';
import type { DistrictPickerViewProps } from './DistrictPickerView';
import { prompt } from './prompt';

export interface DistrictPickerProps
  extends Omit<DistrictPickerViewProps, 'value' | 'defaultValue' | 'onChange'>,
    Omit<PickerContainerProps, 'onConfirm' | 'onClose' | 'children'> {
  value?: string;
  defaultValue?: string;
  onChange?: (value: string | null) => void;
  children: (value: string | null) => ReactNode;
  onConfirm?: (value: string | null) => void;
  onCancel?: () => void;
}

export function DistrictPicker(props: DistrictPickerProps) {
  const { children } = props;
  const [value, setValue] = useNewControllableValue<string | null>({
    value: props.value,
    defaultValue: props.defaultValue ?? null,
    onChange: props.onChange,
  });
  const onClick = (onClick: () => void) => {
    prompt({
      ...omit(props, ['children', 'onConfirm', 'value', 'onChange']),
      defaultValue: value ?? undefined,
    }).then((v) => {
      setValue(v ?? null);
    });
    onClick();
  };

  return (React.Children.map(children(value), (child: any) => {
    return cloneElement(child, {
      ...child.props,
      onClick: (...args: any[]) =>
        onClick(() => child?.props?.onClick?.(...args)),
    });
  }) as unknown) as JSX.Element;
}
