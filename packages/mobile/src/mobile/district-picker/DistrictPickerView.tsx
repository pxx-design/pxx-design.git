import React, { useMemo } from 'react';
import { isArray } from 'lodash-es';
import { global } from '@parallel-line/utils';
import type { PickerValue } from '../picker-view';
import { CascadePickerView, CascadePickerProps } from './CascadePickerView';
import { withNativeProps } from 'ant-design-mobile/es/utils/native-props';
import { withDefaultProps } from 'ant-design-mobile/es/utils/with-default-props';
import { useNewControllableValue } from 'ant-design-mobile/es/utils/use-controllable-value';
export interface FieldNamesType {
  value?: string | number;
  label?: string;
  children?: string;
}
export interface CascaderOptionType {
  value?: string | number;
  label?: React.ReactNode;
  disabled?: boolean;
  isLeaf?: boolean;
  loading?: boolean;
  children?: Array<CascaderOptionType>;
  [key: string]: any;
}

export interface DistrictPickerViewProps
  extends Omit<CascadePickerProps, 'options'> {
  /**
   * 可选城市列表数据源，默认 global.countryTree
   */
  options?: CascaderOptionType[];
  fieldNames?: FieldNamesType;
  level?: LevelType;
}

const defaultProps = {};

const levelMap = ['province', 'city', 'district'] as const;
export type LevelType = typeof levelMap[number];

export const DistrictPickerView = withDefaultProps(
  defaultProps,
)<DistrictPickerViewProps>((props) => {
  const {
    options = global.countryTree,
    level = 'district',
    fieldNames,
  } = props;
  const { children: key = 'districts', label = 'name', value: v = 'adcode' } =
    fieldNames || {};
  const [value, setValue] = useNewControllableValue<PickerValue[]>({
    value: props.value,
    defaultValue: props.defaultValue ?? [],
    onChange: props.onChange,
  });

  const columns = useMemo(() => {
    const citycodes = ['021', '023', '022', '010'];
    const index = levelMap.indexOf(level);
    const levelArray = levelMap.slice(0, index);
    const loop = (data: any) =>
      data.map((t: any) => {
        const item = {
          ...t,
          children: t[key]?.length ? loop(t[key]) : t[key],
          label: t[label],
          value: t[v],
        };
        if (level === 'district') return item;
        if (!levelArray.includes(t.level))
          return { ...item, children: undefined };
        return {
          ...item,
          children:
            isArray(t[key]) &&
            t[key].length > 0 &&
            !citycodes.includes(t.citycode)
              ? loop(t[key])
              : undefined,
        };
      });
    return loop(options);
  }, [options, level]);

  return withNativeProps(
    props,
    <CascadePickerView options={columns} value={value} onChange={setValue} />,
  );
});
