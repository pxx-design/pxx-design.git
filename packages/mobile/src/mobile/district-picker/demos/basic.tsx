import React from 'react';
import { obtainPrevDistrict } from '@parallel-line/utils';
import { DistrictPicker, Select } from '@parallel-line/mobile';
import { Space, Divider } from 'antd';

function convertCodeToDistrict(adcode: string | null) {
  if (!adcode) return undefined;
  const { province, city, district, level } = obtainPrevDistrict({ adcode });
  if (level === 'municipality')
    return [city?.name, district?.name].filter((v) => !!v).join('/');
  return [province?.name, city?.name, district?.name]
    .filter((v) => !!v)
    .join('/');
}

const Basic = () => {
  return (
    <Space style={{ width: '100%' }} direction="vertical">
      <Divider orientation="left">基础用法</Divider>
      <DistrictPicker>
        {(val) => <Select label={convertCodeToDistrict(val)} />}
      </DistrictPicker>
      <Divider orientation="left">level == city</Divider>
      <DistrictPicker level="city">
        {(val) => <Select label={convertCodeToDistrict(val)} />}
      </DistrictPicker>
      <Divider orientation="left">level == province</Divider>
      <DistrictPicker level="province">
        {(val) => <Select label={convertCodeToDistrict(val)} />}
      </DistrictPicker>
    </Space>
  );
};

export default Basic;
