import React, { useState } from 'react';
import { render, getCitysCode } from '@parallel-line/utils';
import { last } from 'lodash-es';
import Popup from '../popup';
import type { PickerValue } from '../picker-view';
import { Container } from '../picker/container';
import type { DistrictPickerProps } from './index';
import { DistrictPickerView } from './DistrictPickerView';

function Content({
  title = '城市选择',
  disabled,
  onConfirm,
  defaultValue,
  ...props
}: Omit<DistrictPickerProps, 'children'>) {
  const [value, setValue] = useState<PickerValue[]>(
    convertDistrictToStringArray(props.value ?? defaultValue ?? null),
  );

  return (
    <Container
      title={title}
      disabled={disabled}
      {...props}
      onConfirm={({ onClose }) => {
        onClose?.();
        onConfirm?.(convertStringArrayToDistrict(value ?? []));
      }}
    >
      <DistrictPickerView {...props} value={value} onChange={setValue} />
    </Container>
  );
}

export function prompt({
  onCancel,
  ...props
}: Omit<DistrictPickerProps, 'children' | 'onConfirm'>) {
  return new Promise<string | null>((resolve) => {
    render(Popup, {
      round: true,
      closeable: true,
      position: 'bottom',
      onClose: () => {
        onCancel?.();
        resolve(null);
      },
      content: (
        <Content
          {...props}
          onConfirm={(value) => {
            resolve(value);
          }}
        />
      ),
    }).open();
  });
}

function convertDistrictToStringArray(district: string | null): string[] {
  return getCitysCode(district ?? undefined).map((k) => String(k));
}

function convertStringArrayToDistrict(
  value: (string | null | undefined)[],
): string | null {
  return last(value.filter((v) => !!v)) ?? null;
}
