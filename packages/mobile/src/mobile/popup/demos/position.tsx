/**
 * title: 弹出位置
 * desc: 通过 `position` 属性设置弹出位置，默认居中弹出，可以设置为 `top`、`bottom`、`left`、`right`。
 */

import React, { useState } from 'react';
import { useBoolean } from 'ahooks';
import { Space } from 'antd';
import { Button,Popup } from '@parallel-line/mobile';

export default () => {
  const [show, { toggle, setTrue }] = useBoolean();
  const [position, setPosition] = useState<
    'top' | 'left' | 'bottom' | 'right' | 'center'
  >('center');
  return (
    <div>
      <Space direction="vertical">
        <Button
          onClick={() => {
            setTrue();
            setPosition('top');
          }}
        >
          顶部弹出
        </Button>
        <Button
          onClick={() => {
            setTrue();
            setPosition('bottom');
          }}
        >
          底部弹出
        </Button>
        <Button
          onClick={() => {
            setTrue();
            setPosition('left');
          }}
        >
          左侧弹出
        </Button>
        <Button
          onClick={() => {
            setTrue();
            setPosition('right');
          }}
        >
          右侧弹出
        </Button>
      </Space>

      <Popup visible={show} position={position} onChange={toggle}>
        <div style={{ padding: 50 }}>内容</div>
      </Popup>
    </div>
  );
};
