/**
 * title: 圆角弹窗
 * desc: 设置 `round` 属性后，弹窗会根据弹出位置添加不同的圆角样式。
 */

import React from 'react';
import { useBoolean } from 'ahooks';
import { Button,Popup } from '@parallel-line/mobile';

export default () => {
  const [show, { toggle, setTrue }] = useBoolean();
  return (
    <div>
      <Button type="primary" onClick={setTrue}>
        圆角弹窗
      </Button>
      <Popup round visible={show} position="bottom" onChange={toggle}>
        <div style={{ padding: 50 }}>内容</div>
      </Popup>
    </div>
  );
};
