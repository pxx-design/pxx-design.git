/**
 * title: 指定挂载位置
 * desc: 弹出层默认挂载到组件所在位置，可以通过 `getContainer` 属性指定挂载位置。
 */

import React from 'react';
import { useBoolean } from 'ahooks';
import { Button,Popup } from '@parallel-line/mobile';

export default () => {
  const [show, { toggle, setTrue }] = useBoolean();
  return (
    <div>
      <Button type="primary" onClick={setTrue}>
        指定挂载节点
      </Button>
      <Popup visible={show} onChange={toggle} getContainer>
        <div style={{ padding: 50 }}>内容</div>
      </Popup>
    </div>
  );
};
