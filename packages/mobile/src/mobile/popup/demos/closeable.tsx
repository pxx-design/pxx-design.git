/**
 * title: 关闭图标
 * desc: 设置 `closeable` 属性后，会在弹出层的右上角显示关闭图标，并且可以通过 `closeIcon` 属性自定义图标，使用 `closeOnPopstate` 属性可以自定义图标位置。
 */

import React from 'react';
import { Space } from 'antd';
import { useBoolean } from 'ahooks';
import { CloseCircleOutlined } from '@ant-design/icons';
import { Button,Popup } from '@parallel-line/mobile';

export default () => {
  const [show, { toggle, setTrue }] = useBoolean();
  const [show2, { toggle: toggle2, setTrue: setTrue2 }] = useBoolean();
  const [show3, { toggle: toggle3, setTrue: setTrue3 }] = useBoolean();
  return (
    <div>
      <Space direction="vertical">
        <Button onClick={setTrue}>关闭图标</Button>
        <Button onClick={setTrue2}>自定义图标</Button>
        <Button onClick={setTrue3}>图标位置</Button>
      </Space>

      <Popup visible={show} closeable position="bottom" onChange={toggle}>
        <div style={{ padding: 50 }}>关闭图标</div>
      </Popup>

      <Popup
        visible={show2}
        closeable
        position="bottom"
        closeIcon={(props) => <CloseCircleOutlined {...props} />}
        onChange={toggle2}
      >
        <div style={{ padding: 50 }}>自定义图标</div>
      </Popup>

      <Popup
        visible={show3}
        closeable
        position="bottom"
        closeIconPosition="top-left"
        onChange={toggle3}
      >
        <div style={{ padding: 50 }}>图标位置</div>
      </Popup>
    </div>
  );
};
