/**
 * title: 基本
 * desc: 最简单的用法
 */

import React from 'react';
import { useBoolean } from 'ahooks';
import { Button, Popup } from '@parallel-line/mobile';

export default () => {
  const [show, { toggle, setTrue }] = useBoolean();
  return (
    <div>
      <Button type="primary" onClick={setTrue}>
        展示弹出层
      </Button>
      <Popup visible={show} onChange={toggle}>
        <div style={{ padding: 50 }}>内容</div>
      </Popup>
    </div>
  );
};
