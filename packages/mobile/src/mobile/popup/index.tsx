import React, { useRef, useEffect } from 'react';
import classNames from 'classnames';
import { isFunction } from 'lodash-es';
import { createPortal } from '@parallel-line/utils';
import { useMount, useUnmount, useBoolean, useUpdateEffect } from 'ahooks';
import { useMemoDeep } from '@parallel-line/hooks';
import { CloseOutlined } from '@parallel-line/icons';
import { Transition } from '@parallel-line/common';
import Overlay from '../overlay';
import {
  useNamespace,
  useLazyRender,
  useEventListener,
  useLockScroll,
} from '@parallel-line/common/es/hooks';
import { isDef } from '@parallel-line/common/es/utils';

export type PopupPosition = 'top' | 'left' | 'bottom' | 'right' | 'center' | '';

export type PopupCloseIconPosition =
  | 'top-left'
  | 'top-right'
  | 'bottom-left'
  | 'bottom-right';

// eslint-disable-next-line
export const POPUP_TOGGLE_KEY: any = Symbol();

interface PopupSharedProps {
  /** 是否显示弹出层 */
  visible?: boolean;
  // z-index
  zIndex?: number | string;
  /** 是否显示遮罩层 */
  overlay?: boolean;
  /** 动画时长，单位秒 */
  duration?: number | string;
  content?: React.ReactNode;
  children?: React.ReactNode;
  /** 指定挂载的节点 */
  getContainer?: true | (() => Element);
  /** 是否锁定背景滚动 */
  lockScroll?: boolean;
  /** 是否在显示弹层时才渲染节点 */
  lazyRender?: boolean;
  /** 自定义遮罩层样式 */
  overlayStyle?: React.CSSProperties;
  /** 自定义遮罩层类名 */
  overlayClass?: string;
  /** 是否在初始渲染时启用过渡动画 */
  transitionAppear?: boolean;
  /** 是否在点击遮罩层后关闭 */
  closeOnClickOverlay?: boolean;
}

export interface PopupProps extends PopupSharedProps {
  className?: string;
  style?: React.CSSProperties;
  /** 内容 */
  content?: React.ReactNode;
  /** 内容的JSX写法 */
  children?: React.ReactNode;
  /** 是否显示圆角 */
  round?: boolean;
  /** 是否显示关闭图标 */
  closeable?: boolean;
  /** 动画类名，等价于 transtion 的name属性 */
  transition?: string;
  /** 是否在页面回退时自动关闭 */
  closeOnPopstate?: boolean;
  /** 是否开启底部安全区适配 */
  safeAreaInsetBottom?: boolean;
  /** 弹出位置，可选值为 `top` `bottom` `right` `left` */
  position?: PopupPosition;
  /** 自定义关闭图标 */
  closeIcon?: (props: {
    className?: string;
    onClick?: React.MouseEventHandler<HTMLSpanElement>;
  }) => React.ReactNode;
  /** 关闭图标位置，可选值为`top-left` `bottom-left` `bottom-right` */
  closeIconPosition?: PopupCloseIconPosition;

  overlayContent?: React.ReactNode;

  onChange?(show?: boolean): void;
  onOpen?(): void;
  onClose?(): void;
  onClick?(event: MouseEvent): void;
  onOpened?(): void;
  onClosed?(): void;
  afterClose?: () => void;
  onOverlayClick?(event: MouseEvent): void;
  onCloseIcon?(event: MouseEvent): void;
}

let globalZIndex = 2000;

const InternalPopup: React.FC<PopupProps> = (props) => {
  const [, bem] = useNamespace('popup');
  const zIndex = useRef<number>();
  const popupRef = useRef<HTMLDivElement>(null);
  const [opened, { toggle: setOpened }] = useBoolean();
  const [shouldReopen, { toggle: setShouldReopen }] = useBoolean();

  const show = useMemoDeep(() => props.visible || !props.lazyRender, [
    props.visible,
    props.lazyRender,
  ]);
  const lazyRender = useLazyRender(show);

  const onChange = (show?: boolean) => {
    props.onChange?.(show);
  };

  const style = useMemoDeep(() => {
    const style: React.CSSProperties = {
      zIndex: zIndex.current,
    };

    if (isDef(props.duration)) {
      const key =
        props.position === 'center'
          ? 'animationDuration'
          : 'transitionDuration';
      style[key] = `${props.duration}s`;
    }

    return style;
  }, [zIndex.current, props.duration, props.position]);

  const open = () => {
    if (!opened) {
      if (props.zIndex !== undefined) {
        globalZIndex = +props.zIndex;
      }

      setOpened(true);
      zIndex.current = ++globalZIndex;
    }
  };

  const close = () => {
    if (opened) {
      setOpened(false);
      onChange(false);
    }
  };

  const onOpened = () => {
    if (props.visible) props.onOpened?.();
  };

  const onClosed = () => {
    if (props.visible) {
      props.onClosed?.();
      props.afterClose?.();
    }
  };

  const onClickOverlay = (event: MouseEvent) => {
    props.onOverlayClick?.(event);

    if (props.closeOnClickOverlay) {
      close();
    }
  };

  const renderOverlay = () => {
    if (props.overlay) {
      return (
        <Overlay
          show={props.visible}
          className={props.overlayClass}
          zIndex={zIndex.current}
          duration={props.duration}
          customStyle={props.overlayStyle}
          onClick={onClickOverlay}
        >
          {props.overlayContent}
        </Overlay>
      );
    }
    return null;
  };

  const onClickCloseIcon: any = (event: MouseEvent) => {
    props.onCloseIcon?.(event);
    close();
  };

  const renderCloseIcon = () => {
    if (props.closeable) {
      const closeIconProps = {
        className: classNames(bem('close-icon', props.closeIconPosition)),
        onClick: onClickCloseIcon,
      };
      if (props.closeIcon) {
        return props.closeIcon(closeIconProps);
      }
      return <CloseOutlined role="button" tabIndex={0} {...closeIconProps} />;
    }
  };

  const renderChildNode = () => {
    const childNode: any = props.children || props.content;
    return React.Children.map(childNode, (child) => {
      if (isFunction(child?.type)) {
        return React.cloneElement(childNode, {
          onClose: (...arg: any[]) => {
            close();
            child?.props?.onClose?.(...arg);
          },
        });
      }
      return child;
    });
    // return childNode;
  };

  const renderPopup = lazyRender(() => {
    const { round, position = 'center', safeAreaInsetBottom } = props;
    return (
      <div
        ref={popupRef}
        style={{ ...style, ...props.style }}
        className={classNames(
          bem({
            round,
            [position]: position,
            'safe-area-inset-bottom': safeAreaInsetBottom,
          }),
          props.className,
        )}
        onClick={props.onClick as any}
      >
        {renderChildNode()}
        {renderCloseIcon()}
      </div>
    );
  });

  const renderTransition = () => {
    const { position, transition, transitionAppear } = props;
    const name =
      position === 'center'
        ? 'pxx-mobile-fade'
        : classNames(bem(`slide-${position}`));

    return (
      <Transition
        show={props.visible}
        name={transition || name}
        appear={transitionAppear}
        onAfterEnter={onOpened}
        onAfterLeave={onClosed}
      >
        {renderPopup()}
      </Transition>
    );
  };

  useEffect(() => {
    if (props.visible) {
      open();
    } else {
      close();
    }
  }, [props.visible]);

  useLockScroll(popupRef, [props.visible, props.lockScroll]);

  useEventListener('popstate', () => {
    if (props.closeOnPopstate) {
      close();
      setShouldReopen(false);
    }
  });

  // onActivated
  useMount(() => {
    if (shouldReopen) {
      onChange(true);
      setShouldReopen(false);
    }
  });

  // onDeactivated
  useUnmount(() => {
    if (props.visible) {
      close();
      setShouldReopen(true);
    }
  });

  const render = () => {
    if (props.getContainer) {
      return createPortal(
        <>
          {renderOverlay()}
          {renderTransition()}
        </>,
        isFunction(props.getContainer) ? props.getContainer() : document.body,
      );
    }

    return (
      <>
        {renderOverlay()}
        {renderTransition()}
      </>
    );
  };

  return render();
};

InternalPopup.defaultProps = {
  overlay: true,
  visible: false,
  lockScroll: true,
  lazyRender: true,
  closeOnClickOverlay: true,
  position: 'center',
  closeIconPosition: 'top-right',
};

export const Popup = React.forwardRef<unknown, PopupProps>((props, ref) => {
  const [show, { toggle }] = useBoolean();
  React.useImperativeHandle(ref, () => null);

  useMount(() => {
    toggle(props.visible);
  });

  useUpdateEffect(() => {
    toggle(props.visible);
  }, [props.visible]);

  const onChange = (v?: boolean) => {
    toggle(v);
    props.onChange?.(v);
  };

  return (
    <div>
      <InternalPopup {...props} visible={show} onChange={onChange} />
    </div>
  );
});

export default Popup;
