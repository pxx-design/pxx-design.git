---
title: Popup 弹出层
mobile: true
---

# Popup 弹出层

弹出层容器，用于展示弹窗、信息提示等内容，支持多个弹出层叠加展示。

## 代码演示

<code src="./demos/basic.tsx"></code>
<code src="./demos/position.tsx"></code>
<code src="./demos/closeable.tsx"></code>
<code src="./demos/round.tsx"></code>
<code src="./demos/teleport.tsx"></code>

<API></API>
