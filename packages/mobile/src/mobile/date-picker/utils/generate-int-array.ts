export function generateIntArray(from: number, to: number) {
  const array: any[] = []
  for (let i = from; i <= to; i++) {
    array.push({label: i, value: i})
  }
  return array
}
