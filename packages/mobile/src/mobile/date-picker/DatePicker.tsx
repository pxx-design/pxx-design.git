import React, { cloneElement, ReactNode } from 'react';
import { omit } from 'lodash-es';
import { useNewControllableValue } from 'ant-design-mobile/es/utils/use-controllable-value';
import { PickerContainerProps } from '../picker/container';
import type { DatePickerViewProps } from './DatePickerView';
import { prompt } from './prompt';

export interface DatePickerProps
  extends DatePickerViewProps,
    Omit<PickerContainerProps, 'onConfirm' | 'onClose' | 'children'> {
  children: (value: Date | null) => ReactNode;
  onConfirm?: (value: Date | null) => void;
  onCancel?: () => void;
}

export function DatePicker(props: DatePickerProps) {
  const { children } = props;
  const [value, setValue] = useNewControllableValue<Date | null>({
    value: props.value,
    defaultValue: props.defaultValue ?? null,
    onChange: props.onChange,
  });
  const onClick = (onClick: () => void) => {
    prompt({
      ...omit(props, ['children', 'onConfirm', 'value', 'onChange']),
      defaultValue: value ?? undefined,
    }).then((v) => {
      setValue(v ?? null);
      props.onConfirm?.(v ?? null);
    });
    onClick();
  };

  return (React.Children.map(children(value), (child: any) => {
    return cloneElement(child, {
      ...child.props,
      onClick: (...args: any[]) =>
        onClick(() => child?.props?.onClick?.(...args)),
    });
  }) as unknown) as JSX.Element;
}
