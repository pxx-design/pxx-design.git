import React from 'react';
import { render } from '@parallel-line/utils';
import { useNewControllableValue } from 'ant-design-mobile/es/utils/use-controllable-value';
import Popup from '../popup';
import { Container } from '../picker/container';
import type { DatePickerProps } from './index';
import { DatePickerView } from './DatePickerView';

function Content({
  title = '时间选择',
  disabled,
  onConfirm,
  ...props
}: Omit<DatePickerProps, 'children'>) {
  const [value, setValue] = useNewControllableValue<Date | null>({
    value: props.value,
    defaultValue: props.defaultValue ?? null,
    onChange: props.onChange,
  });
  return (
    <Container
      title={title}
      disabled={disabled}
      {...props}
      onConfirm={({ onClose }) => {
        onClose?.();
        onConfirm?.(value ?? null);
      }}
    >
      <DatePickerView
        {...props}
        value={value ?? undefined}
        onChange={setValue}
      />
    </Container>
  );
}

export function prompt({
  onCancel,
  ...props
}: Omit<DatePickerProps, 'children' | 'onConfirm'>) {
  return new Promise<Date | null>((resolve) => {
    render(Popup, {
      round: true,
      closeable: true,
      position: 'bottom',
      onClose: () => {
        onCancel?.();
        resolve(null);
      },
      content: (
        <Content
          {...props}
          onConfirm={(value) => {
            resolve(value);
          }}
        />
      ),
    }).open();
  });
}
