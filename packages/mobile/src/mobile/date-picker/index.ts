import { attachPropertiesToComponent } from 'ant-design-mobile/es/utils/attach-properties-to-component';
import { DatePicker } from './DatePicker';
import { prompt } from './prompt';

export type { DatePickerProps } from './DatePicker';

export default attachPropertiesToComponent(DatePicker, {
  prompt,
});
