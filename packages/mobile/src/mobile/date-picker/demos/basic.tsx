import React from 'react';
import moment from 'moment';
import {
  Select,
  ButtonGroup,
  DatePicker,
  DatePickerProps,
} from '@parallel-line/mobile';
import { Space, Divider } from 'antd';
// 'year' | 'month' | 'day' | 'hour' | 'minute' | 'second'
const options = [
  { value: '年', key: 'year' },
  { value: '月', key: 'month' },
  { value: '日', key: 'day' },
  { value: '时', key: 'hour' },
  { value: '分', key: 'minute' },
  { value: '秒', key: 'second' },
];

const Basic = () => {
  const [precision, setPrecision] = React.useState<
    DatePickerProps['precision']
  >('day');
  return (
    <Space style={{ width: '100%' }} direction="vertical">
      <Divider orientation="left">基础用法</Divider>
      <div>
        精度:
        <ButtonGroup
          mode="radio"
          options={options}
          value={precision}
          onChange={setPrecision}
        />
      </div>
      <DatePicker
        title="时间选择"
        precision={precision}
        defaultValue={moment().toDate()}
      >
        {(val) => (
          <Select
            label={val ? moment(val).format('YYYY-MM-DD HH:mm:ss') : undefined}
          />
        )}
      </DatePicker>

      <Divider orientation="left">受控模式</Divider>
    </Space>
  );
};

export default Basic;
