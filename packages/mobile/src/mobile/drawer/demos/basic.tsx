/**
 * title: 基本
 * desc: 最简单的用法
 */

import React from 'react';
import {Drawer, Button} from '@parallel-line/mobile';
import {render} from '@parallel-line/utils';

const Content = () => {
  return <div>Content</div>
}

export default () => {
  const handleClick = () => {
    render(Drawer, {
      title: 'title',
      content: <Content/>,
    }).open();
  }
  return (
    <Button onClick={handleClick}>打开抽屉</Button>
  );
};
