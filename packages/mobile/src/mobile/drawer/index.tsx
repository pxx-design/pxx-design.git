import React, { forwardRef } from 'react';
import { Popup } from '../popup';
import classNames from 'classnames';
import { useNamespace } from '@parallel-line/common/es/hooks';
import { CloseOutlined } from '@parallel-line/icons';
import { isFunction } from 'lodash-es';

interface DrawerProps {
  title?: any;
  visible?: boolean;
  prefixCls?: string;
  className?: string;
  onClose?: () => void;
  afterClose?: () => void;
  header?: React.ReactNode;
  content?: React.ReactNode;
  children?: React.ReactNode;
}

const Drawer = (
  props: DrawerProps,
  ref:
    | ((instance: unknown) => void)
    | React.RefObject<unknown>
    | null
    | undefined,
) => {
  const {
    title,
    header,
    content,
    onClose,
    children,
    className,
    afterClose,
    visible = false,
  } = props;
  const [cls] = useNamespace('drawer');
  const Component = content ?? children;
  return (
    <Popup
      round
      ref={ref}
      visible={visible}
      position="bottom"
      afterClose={afterClose}
    >
      <div className={classNames(cls, className)}>
        {header ?? (
          <div className={`${cls}-header`}>
            <div className={`${cls}-title`}>{title}</div>
            <div className={`${cls}-close`} onClick={onClose}>
              <CloseOutlined />
            </div>
          </div>
        )}
        <div className={`${cls}-content`}>
          {React.Children.map(Component, (child: any) => {
            if (isFunction(child?.type)) {
              return React.cloneElement(child, {
                onClose: (...arg: any[]) => {
                  onClose?.();
                  child?.props?.onClose?.(...arg);
                },
              });
            }
            return child;
          })}
        </div>
      </div>
    </Popup>
  );
};

export default forwardRef(Drawer);
