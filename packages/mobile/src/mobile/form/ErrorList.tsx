import * as React from 'react';
import classNames from 'classnames';
import useMemo from 'rc-util/lib/hooks/useMemo';
import useCacheErrors from './hooks/useCacheErrors';
import { Transition } from '@parallel-line/common';
import { useForceUpdate, useLazyRender } from '@parallel-line/common/es/hooks';
import { FormItemPrefixContext } from './context';

const EMPTY_LIST: React.ReactNode[] = [];

export interface ErrorListProps {
  errors?: React.ReactNode[];
  /** @private Internal Usage. Do not use in your production */
  help?: React.ReactNode;
  /** @private Internal Usage. Do not use in your production */
  onDomErrorVisibleChange?: (visible: boolean) => void;
}

export default function ErrorList({
  errors = EMPTY_LIST,
  help,
  onDomErrorVisibleChange,
}: ErrorListProps) {
  const forceUpdate = useForceUpdate();
  const { prefixCls, status } = React.useContext(FormItemPrefixContext);

  const [visible, cacheErrors] = useCacheErrors(
    errors,
    (changedVisible) => {
      if (changedVisible) {
        /**
         * We trigger in sync to avoid dom shaking but this get warning in react 16.13.
         *
         * So use Promise to keep in micro async to handle this.
         * https://github.com/ant-design/ant-design/issues/21698#issuecomment-593743485
         */
        Promise.resolve().then(() => {
          onDomErrorVisibleChange?.(true);
        });
      }
      forceUpdate();
    },
    !!help,
  );

  const memoErrors = useMemo(
    () => cacheErrors,
    visible,
    (_, nextVisible) => nextVisible,
  );

  // Memo status in same visible
  const [innerStatus, setInnerStatus] = React.useState(status);
  React.useEffect(() => {
    if (visible && status) {
      setInnerStatus(status);
    }
  }, [visible, status]);
  const lazyRender = useLazyRender(visible);

  const render = lazyRender(() => (
    <div
      className={classNames(baseClassName, {
        [`${baseClassName}-${innerStatus}`]: innerStatus,
      })}
    >
      {memoErrors.map((error, index) => (
        <div key={index} role="alert">
          {error}
        </div>
      ))}
    </div>
  ));

  const baseClassName = `${prefixCls}-item-explain`;

  return (
    <Transition
      show={visible}
      key="help"
      name="pxx-show-help"
      onAfterLeave={() => {
        onDomErrorVisibleChange?.(false);
      }}
    >
      {render()}
    </Transition>
  );
}
