import * as React from 'react';
import Form, { useForm, FormProps } from './Form';
import FormItem, { FormItemProps } from './FormItem';

interface SchemaInfo<C = string> extends Omit<FormItemProps, 'label'> {
  title?: string;
  component?: C;
  componentProps?: any;
  items?: Record<string, SchemaInfo>;
}

export interface SchemaFormProps<Values = any, C extends string = string>
  extends FormProps<Values> {
  components?: Record<C, any>;
  schema: Record<string, SchemaInfo<C>>;
}

function SchemaForm<C extends string = string, Values = any>(
  props: SchemaFormProps<Values, C>,
) {
  const { form, schema, components: comps, children, ...formProps } = props;
  const components: Record<string, any> = comps || {};
  const [wrapForm] = useForm(form);

  function render(map: SchemaInfo) {
    const { items, componentProps } = map;
    if (items) {
      return Object.entries(items).map(([k, v]) => renderItem(k, v));
    }
    const Component = map.component && components[map.component];
    if (Component) {
      return <Component {...componentProps} />;
    }
    return null;
  }

  function renderItem(name: string, map: SchemaInfo) {
    const { title, component, componentProps, ...formItemProps } = map;

    return (
      <FormItem key={name} name={name} label={title} {...formItemProps}>
        {render(map)}
      </FormItem>
    );
  }

  return (
    <Form form={wrapForm} layout="vertical" {...formProps}>
      {schema
        ? Object.entries(schema).map(([k, v]) => renderItem(k, v as string))
        : children}
    </Form>
  );
}

export default SchemaForm;
