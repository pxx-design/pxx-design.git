/**
 * title: 配置型表单
 * desc: 基本的表单数据域控制展示，包含布局、初始化、验证、提交。
 */

import React from 'react';
import styled from 'styled-components';
import { InfoCircleOutlined } from '@ant-design/icons';
import { Form, Input, Button } from '@parallel-line/mobile';

const { SchemaForm } = Form;

const Container = styled.div`
  width: 100%;
  min-height: 100vh;
  background: #f1f2f4;
  padding-bottom: 24px;
`;

const Vertical = styled.div`
  width: 100%;
  padding: 24px;
  background: #fff;
`;

export default () => {
  const [form] = Form.useForm();

  const onSubmit = () => {
    form.submit();
  };
  const onFinish = (values: any) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Container>
      <Vertical>
        <SchemaForm
          form={form}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          components={{ Input }}
          schema={{
            basic: { title: '常规文本', component: 'Input' },
            required: { title: '必填文本', component: 'Input', required: true },
            min: { title: '校验文本', component: 'Input', rules: [{ min: 6 }] },
            min_required: {
              title: '校验A必填文本',
              component: 'Input',
              required: true,
              rules: [{ min: 6 }],
            },
            tooltip: {
              title: '带解释图标',
              component: 'Input',
              tooltip: 'This is a tooltip field',
            },
            customize_tooltip: {
              title: '自定义解释图标',
              component: 'Input',
              tooltip: {
                title: 'Tooltip with customize icon',
                icon: <InfoCircleOutlined />,
              },
            },
            required_tooltip: {
              title: '带解释图标',
              component: 'Input',
              required: true,
              tooltip: 'This is a tooltip field',
            },
          }}
        />
      </Vertical>

      <Button block type="primary" onClick={onSubmit}>
        提交
      </Button>
    </Container>
  );
};
