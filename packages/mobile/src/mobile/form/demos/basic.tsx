/**
 * title: 基本使用
 * desc: 基本的表单数据域控制展示，包含布局、初始化、验证、提交。
 */

import React from 'react';
import styled from 'styled-components';
import {
  MinusCircleOutlined,
  InfoCircleOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import { Form, Card, Input, Button } from '@parallel-line/mobile';

const Container = styled.div`
  width: 100%;
  min-height: 100vh;
  background: #f1f2f4;
`;

const Vertical = styled.div`
  width: 100%;
  padding: 24px;
  background: #fff;
`;

export default () => {
  const [form] = Form.useForm();

  const onSubmit = () => {
    form.submit();
  };
  const onFinish = (values: any) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Container>
      <Form
        form={form}
        name="basic"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Vertical>
          <Form.Item label="常规文本" name="basic">
            <Input />
          </Form.Item>
          <Form.Item label="必填文本" name="required" required>
            <Input />
          </Form.Item>
          <Form.Item label="校验文本" name="min" rules={[{ min: 6 }]}>
            <Input />
          </Form.Item>
          <Form.Item
            label="校验A必填文本"
            name="min_required"
            required
            rules={[{ min: 6 }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="带解释图标"
            name="tooltip"
            tooltip="This is a tooltip field"
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="自定义解释图标"
            name="customize_tooltip"
            tooltip={{
              title: 'Tooltip with customize icon',
              icon: <InfoCircleOutlined />,
            }}
          >
            <Input />
          </Form.Item>
          <Form.Item
            required
            label="必填带解释图标"
            name="required_tooltip"
            tooltip="This is a required field"
          >
            <Input />
          </Form.Item>
        </Vertical>

        <Form.List name="names">
          {(fields, { add, remove }, { errors }) => (
            <>
              {fields.map((field, index) => (
                <Card style={{ margin: 12 }} key={field.key}>
                  <div>{index === 0 ? 'Passengers' : ''} </div>
                  <Form.Item
                    {...field}
                    rules={[
                      {
                        required: true,
                        whitespace: true,
                        message:
                          "Please input passenger's name or delete this field.",
                      },
                    ]}
                  >
                    <Input placeholder="passenger name" />
                  </Form.Item>

                  {fields.length > 1 ? (
                    <MinusCircleOutlined
                      className="dynamic-delete-button"
                      onClick={() => remove(field.name)}
                    />
                  ) : null}
                </Card>
              ))}
              <Form.Item>
                <Button
                  size="small"
                  onClick={() => add()}
                  icon={<PlusOutlined />}
                  style={{ marginTop: '20px' }}
                >
                  Add field
                </Button>
                <br />
                <Button
                  onClick={() => {
                    add('The head item', 0);
                  }}
                  size="small"
                  style={{ marginTop: '20px' }}
                  icon={<PlusOutlined />}
                >
                  Add field at head
                </Button>
                <Form.ErrorList errors={errors} />
              </Form.Item>
            </>
          )}
        </Form.List>

        <Form.Item>
          <Button
            type="primary"
            style={{ marginTop: '20px' }}
            onClick={onSubmit}
          >
            提交
          </Button>
        </Form.Item>
      </Form>
    </Container>
  );
};
