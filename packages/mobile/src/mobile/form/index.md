---
title: Form 表单
mobile: true
---

# Form 表单

高性能表单控件，自带数据域管理。包含数据录入、校验以及对应样式。

## 代码演示

<code src="./demos/basic.tsx"></code>
<code src="./demos/schema.tsx"></code>
