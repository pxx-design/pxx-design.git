import { Rule, RuleObject, RuleRender } from 'rc-field-form/lib/interface';
import InternalForm, { useForm, FormInstance, FormProps } from './Form';
import SchemaForm, {SchemaFormProps} from './SchemaForm';
import Item, { FormItemProps } from './FormItem';
import ErrorList, { ErrorListProps } from './ErrorList';
import List, { FormListProps } from './FormList';
import { FormProvider } from './context';

type InternalFormType = typeof InternalForm;

interface FormInterface extends InternalFormType {
  useForm: typeof useForm;
  Item: typeof Item;
  List: typeof List;
  SchemaForm: typeof SchemaForm;
  ErrorList: typeof ErrorList;
  Provider: typeof FormProvider;

  /** @deprecated Only for warning usage. Do not use. */
  create: () => void;
}

const Form = InternalForm as FormInterface;

Form.Item = Item;
Form.List = List;
Form.ErrorList = ErrorList;
Form.SchemaForm = SchemaForm;
Form.useForm = useForm;
Form.Provider = FormProvider;

export type {
  FormInstance,
  FormProps,
  FormItemProps,
  ErrorListProps,
  SchemaFormProps,
  Rule,
  RuleObject,
  RuleRender,
  FormListProps,
};

export default Form;
