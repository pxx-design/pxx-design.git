import React from 'react';
import classNames from 'classnames';
import { isNil } from 'lodash-es';
import { EllipsisOutlined } from '@parallel-line/icons';
import { obtainPrevDistrict, toTime, global } from '@parallel-line/utils';
import { useNamespace } from '@parallel-line/common/es/hooks';

interface RouteInfoProps {
  time?: boolean;
  startAdcode?: string;
  endAdcode?: string;
  endAddress?: string;
  startAddress?: string;
  startTime?: string;
  endTime?: string;
  className?: string;
  count?: string | number;
}

const format = 'MM月DD日 HH:mm';

const RouteInfo = (props: RouteInfoProps) => {
  const { isFTL, isLTL } = global.PXX_SOLUTION;
  const {
    className,
    startAddress,
    endAddress,
    startAdcode,
    endAdcode,
    startTime,
    endTime,
    count,
    time = true,
  } = props;
  const isCount = !isNil(count);
  const [cls, bem] = useNamespace('route-info');
  const cityName = (adcode: any) => {
    const { province, district, city, level } = obtainPrevDistrict({
      adcode: `${adcode}`,
    });
    if (!province?.name) return '--';
    if (level === 'municipality') {
      return `${province.name}${district?.name || ''}`;
    }
    return `${province.name}${city?.name || ''}`;
  };
  return (
    <div className={classNames(cls, className)}>
      <div className={bem('address')}>
        <div className={bem('item')}>
          {isLTL &&
            (isCount ? (
              <div className={bem('block')}>1</div>
            ) : (
              <div className={bem('block')}>发</div>
            ))}
          {isFTL && <div className={bem('block')}>发</div>}
          <div className={bem('ellipsis')}>
            {startAddress ?? cityName(startAdcode)}
          </div>
        </div>
        <div className={bem('icon')}>
          <EllipsisOutlined />
          {isLTL && isCount && <div className={bem('count')}>共{count}站</div>}
          {isFTL && isCount && <div className={bem('count')}>经停{count}</div>}
        </div>
        <div className={bem('item')}>
          {isLTL &&
            (isCount ? (
              <div className={bem('block')}>{count}</div>
            ) : (
              <div className={bem('block')}>收</div>
            ))}
          {isFTL && <div className={bem('block')}>收</div>}
          <div className={bem('ellipsis')}>
            {endAddress ?? cityName(endAdcode)}
          </div>
        </div>
      </div>
      <div className={bem('time')}>
        {time && (
          <>
            <div className={bem('item')}>
              {toTime(startTime, { format, errorResult: '无要求' })}
            </div>
            <div className={bem('item')}>
              {toTime(endTime, { format, errorResult: '无要求' })}
            </div>
          </>
        )}
      </div>
    </div>
  );
};

export default RouteInfo;
