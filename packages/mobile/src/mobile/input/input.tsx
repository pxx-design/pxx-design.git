import React from 'react';
import classNames from 'classnames';
import { omit } from 'lodash-es';
import { isEmpty, isMiniAPP } from '@parallel-line/utils';
import { CloseCircleFill } from '@parallel-line/icons';
import { useControllableValue, useToggle } from 'ahooks';
import { useNamespace } from '@parallel-line/common/es/hooks';

declare module 'react' {
  interface InputHTMLAttributes<T> {
    focus?: boolean;
  }
}

export interface InputProps {
  type?: string;
  value?: string;
  disabled?: boolean;
  maxLength?: number;
  className?: string;
  allowClear?: boolean;
  placeholder?: string;
  defaultValue?: string;
  prefix?: React.ReactNode;
  suffix?: React.ReactNode;
  style?: React.CSSProperties;
  onClear?: () => void;
  onBlur?: (e: any) => void;
  onFocus?: (e: any) => void;
  onChange?: (value: any) => void;
  onConfirm?: (value: any) => void; // 字段预留以后添加
}

const Input = (props: InputProps) => {
  const {
    style,
    onBlur,
    prefix,
    suffix,
    onClear,
    onFocus,
    disabled,
    className,
    maxLength,
    type = 'text',
    allowClear = true,
    placeholder = '请输入',
  } = props;
  const [isFocus, { toggle: focusToggle }] = useToggle(false);
  const [, bem] = useNamespace('input');
  const [value, setValue] = useControllableValue(props);

  const handleChange = (e: any) => {
    setValue(e.target.value);
  };
  const handleBlur = (e: any) => {
    focusToggle();
    onBlur?.(e);
  };
  const handleFocus = (e: any) => {
    focusToggle();
    onFocus?.(e);
  };
  const handleClear = () => {
    setValue('');
    onClear?.();
  };
  // fix  Received `false` for a non-boolean attribute `focus`.
  let inputProps: React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > = {
    type,
    disabled,
    // fix A component is changing an uncontrolled input of type text to be controlled. Input elements should not switch from uncontrolled to controlled (or vice versa). Decide between using a controlled or uncontrolled input element for the lifetime of the component.
    value: value || '',
    maxLength,
    placeholder,
    focus: isFocus,
    className: bem(),
    onBlur: handleBlur,
    onFocus: handleFocus,
    onChange: handleChange,
  };
  return (
    <div
      className={classNames(
        bem('wrapper', { disabled, focus: isFocus }),
        className,
      )}
      style={style}
    >
      {!!prefix && <div className={bem('prefix')}>{prefix}</div>}
      {/* fix  Received `false` for a non-boolean attribute `focus`. */}
      <input {...(isMiniAPP() ? inputProps : omit(inputProps, ['focus']))} />
      {allowClear && !isEmpty(value) && (
        <div onClick={handleClear} className={bem('clear')}>
          <CloseCircleFill />
        </div>
      )}
      {!!suffix && <div className={bem('suffix')}>{suffix}</div>}
    </div>
  );
};

export default Input;
