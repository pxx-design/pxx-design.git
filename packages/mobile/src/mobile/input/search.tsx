import React, {useRef} from 'react';
import Button from '../button';
import classNames from "classnames";
import Input, {InputProps} from './input';
import {SearchOutlined} from '@parallel-line/icons';
import {useNamespace} from '@parallel-line/common/es/hooks';


const Search = (props: InputProps) => {
  const {
    className,
    onConfirm,
    prefix = <SearchOutlined/>,
    ...otherProps
  } = props;
  const valueRef = useRef();
  const [, bem] = useNamespace('input-search');
  const handleConfirm = () => {
    onConfirm?.(valueRef.current === '' ? undefined : valueRef.current)
  }
  const handleChange = (value: any) => {
    valueRef.current = value;
  };
  return (
    <Input {...otherProps}
           prefix={prefix}
           onChange={handleChange}
           suffix={<Button onClick={handleConfirm}>搜索</Button>}
           className={classNames(bem('wrapper'), className)}/>
  );
};

export default Search;
