import InternalInput, { InputProps } from './input';
import Search from './search';

type InternalInputType = typeof InternalInput;

interface InputInterface extends InternalInputType {
  Search: typeof Search;
}

const Input = InternalInput as InputInterface;

Input.Search = Search;

export type { InputProps };

export default Input;
