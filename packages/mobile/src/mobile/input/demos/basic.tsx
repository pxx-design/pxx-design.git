import React from 'react';
import { Input } from '@parallel-line/mobile';
import { Space } from 'antd';

const Basic = () => {
  return (
    <Space style={{ width: '100%' }} direction="vertical">
      <Input />
      <Input disabled value="Input disabled" />
      <Input.Search />
    </Space>
  );
};

export default Basic;
