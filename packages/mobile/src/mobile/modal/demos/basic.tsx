/**
 * title: 基本
 * desc: 最简单的用法
 */

import React from 'react';
import { useBoolean } from 'ahooks';
import { Button,Modal } from '@parallel-line/mobile';

export default () => {
  const [show, { setFalse, setTrue }] = useBoolean();
  return (
    <div>
      <Button type="primary" onClick={setTrue}>
        显示对话框
      </Button>
      <Modal
        title="Basic Modal"
        visible={show}
        onOk={setFalse}
        onCancel={setFalse}
      >
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </Modal>
    </div>
  );
};
