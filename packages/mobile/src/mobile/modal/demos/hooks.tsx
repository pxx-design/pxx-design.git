/**
 * title: 使用 hooks 获得上下文
 * desc: 通过 `Modal.useModal` 创建支持读取 `context` 的 `contextHolder。`
 */

import * as React from 'react';
import { Space } from 'antd';
import { Button, Modal } from '@parallel-line/mobile';

const ReachableContext = React.createContext<string>(undefined);
const UnreachableContext = React.createContext<string>(undefined);

const ConfirmDialog: React.FC = () => {
  const reachable = React.useContext(ReachableContext);
  const unreachable = React.useContext(UnreachableContext);

  return (
    <div>
      <div>{`Reachable: ${reachable}!`}</div>
      <div>{`Unreachable: ${unreachable}!`}</div>
    </div>
  );
};
const config = {
  title: 'Use Hook!',
  content: <ConfirmDialog />,
};

export default () => {
  const [modal, contextHolder] = Modal.useModal();

  return (
    <ReachableContext.Provider value="Light">
      <Space direction="vertical">
        <Button
          onClick={() => {
            modal.confirm(config);
          }}
        >
          Confirm
        </Button>
        <Button
          onClick={() => {
            modal.warning(config);
          }}
        >
          Warning
        </Button>
        <Button
          onClick={() => {
            modal.info(config);
          }}
        >
          Info
        </Button>
        <Button
          onClick={() => {
            modal.error(config);
          }}
        >
          Error
        </Button>
      </Space>
      {/* `contextHolder` should always under the context you want to access */}
      {contextHolder}

      {/* Can not access this context since `contextHolder` is not in it */}
      <UnreachableContext.Provider value="Bamboo" />
    </ReachableContext.Provider>
  );
};
