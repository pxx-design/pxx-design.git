/**
 * title: 信息提示
 * desc: 各种类型的信息提示，只提供一个按钮用于关闭。
 */

import React from 'react';
import { Space } from 'antd';
import { Button,Modal } from '@parallel-line/mobile';

export default () => {
  return (
    <Space direction="vertical">
      <Button
        onClick={() => {
          Modal.info({
            title: 'Info',
            content: 'some messages...some messages...',
          });
        }}
      >
        Info
      </Button>
      <Button
        onClick={() => {
          Modal.success({
            title: 'Success',
            content: 'some messages...some messages...',
          });
        }}
      >
        Success
      </Button>
      <Button
        onClick={() => {
          Modal.error({
            title: 'Error',
            content: 'some messages...some messages...',
          });
        }}
      >
        Error
      </Button>
      <Button
        onClick={() => {
          Modal.warning({
            title: 'Warning',
            content: 'some messages...some messages...',
          });
        }}
      >
        Warning
      </Button>
    </Space>
  );
};
