/**
 * title: 确认对话框
 * desc: 使用 `confirm()` 可以快捷地弹出确认框。onCancel/onOk 返回 promise 可以延迟关闭。
 */

import React from 'react';
import { Space } from 'antd';
import { Button,Modal } from '@parallel-line/mobile';

function showPropsConfirm() {
  Modal.confirm({
    title: 'Are you sure delete this task?',
    content: 'Some descriptions',
    okText: 'Yes',
    okButtonProps: {
      disabled: true,
    },
    cancelText: 'No',
    onOk() {
      console.log('OK');
    },
    onCancel() {
      console.log('Cancel');
    },
  });
}

export default () => {
  return (
    <Space direction="vertical">
      <Button
        onClick={() => {
          Modal.confirm({
            title: 'Confirm',
            content: 'some messages...some messages...',
          });
        }}
      >
        Confirm
      </Button>
      <Button
        onClick={() => {
          Modal.confirm({
            title: 'With promise',
            content: 'some messages...some messages...',
            async onOk() {
              try {
                return new Promise((resolve, reject) => {
                  setTimeout(() => {
                    Math.random() > 0.5 ? resolve() : reject();
                  }, 1000);
                });
              } catch (e) {
                return console.log('Oops errors!');
              }
            },
          });
        }}
      >
        With promise
      </Button>
      <Button
        onClick={() => {
          Modal.success({
            okCancel: true,
            title: 'Success',
            content: 'some messages...some messages...',
          });
        }}
      >
        Success
      </Button>
      <Button
        onClick={() => {
          Modal.error({
            okCancel: true,
            title: 'Error',
            content: 'some messages...some messages...',
          });
        }}
      >
        Error
      </Button>
      <Button
        onClick={() => {
          Modal.warning({
            okCancel: true,
            title: 'Warning',
            content: 'some messages...some messages...',
          });
        }}
      >
        Warning
      </Button>
      <Button onClick={showPropsConfirm} type="dashed">
        With extra props
      </Button>
    </Space>
  );
};
