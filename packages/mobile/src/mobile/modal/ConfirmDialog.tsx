import * as React from 'react';
import classNames from 'classnames';
import { useBoolean } from 'ahooks';
import Button from '../button';
import { CloseCircleFill, CheckCircleFill,WarningCircleFill } from '@parallel-line/icons';
import Dialog, { ModalFuncProps } from './Modal';
import { useNamespace } from '@parallel-line/common/es/hooks';

export interface ConfirmDialogProps extends ModalFuncProps {
  close?(...args: any[]): void;
}

const ConfirmDialog: React.FC<ConfirmDialogProps> = (props) => {
  const [, bem, t] = useNamespace('modal');
  const { icon, title, okCancel, content, type, close, ...dialogProps } = props;
  const [ok, { toggle: toggleOK }] = useBoolean();
  const [cancel, { toggle: toggleCancel }] = useBoolean();

  const onOk = async (e: React.MouseEvent<HTMLElement>) => {
    try {
      const okRes = props.onOk?.(e);
      if (okRes instanceof Promise) {
        toggleOK(true);
        await okRes;
      }
    } catch (error) {
      throw error;
    } finally {
      close?.(e);
      toggleOK(false);
    }
  };

  const onCancel = async (e: React.MouseEvent<HTMLElement>) => {
    try {
      const cancelRes = props.onCancelClick?.(e);
      const cancelRes2 = props.onCancel?.(e);
      if (cancelRes instanceof Promise || cancelRes2 instanceof Promise) {
        toggleCancel(true);
        await cancelRes;
        await cancelRes2;
      }
    } catch (error) {
      throw error;
    } finally {
      close?.(e);
      toggleCancel(false);
    }
  };

  const renderFooter = () => {
    if (props.footer === null) return null;
    if (props.footer) return props.footer;
    if (!okCancel) {
      return (
        <div className={classNames(bem('footer'))}>
          <Button
            loading={ok}
            color="gray"
            {...props.okButtonProps}
            onClick={onOk}
          >
            {props.okText || t('justOkText')}
          </Button>
        </div>
      );
    }
    return (
      <div className={classNames(bem('footer'))}>
        <Button
          loading={cancel}
          color="gray"
          {...props.cancelButtonProps}
          onClick={onCancel}
        >
          {props.cancelText || t('cancelText')}
        </Button>
        <Button
          loading={ok}
          type="primary"
          {...props.okButtonProps}
          onClick={onOk}
        >
          {props.okText || t('okText')}
        </Button>
      </div>
    );
  };
  const renderIcon = (): React.ReactNode => {
    if (icon) return icon;
    switch (type) {
      case 'success':
        return <CheckCircleFill />;
      case 'error':
        return <CloseCircleFill />;
      case 'warn':
      case 'warning':
        return <WarningCircleFill />;
      default:
        return null;
    }
  };
  const renderTitle = () => {
    const iconrender = renderIcon();
    return (
      <div className={classNames(bem('title-container'))}>
        {iconrender !== null && (
          <div className={classNames(bem('icon', type))}>{iconrender}</div>
        )}
        <div className={classNames(bem('title'))}>{title}</div>
      </div>
    );
  };

  return (
    <Dialog {...dialogProps} title={renderTitle()} footer={renderFooter()}>
      {content}
    </Dialog>
  );
};

export default ConfirmDialog;
