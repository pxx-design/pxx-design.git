import React, { useContext } from 'react';
import classNames from 'classnames';
import { isNumber } from 'lodash-es';
import { useBoolean, useMount, useUpdateEffect } from 'ahooks';
import Button, { ButtonProps } from '../button';
import { useNamespace } from '@parallel-line/common/es/hooks';
import Popup from '../popup';
import ConfigProvider from '../config-provider';

export const destroyFns: Array<() => void> = [];

export interface ModalProps {
  className?: string;
  style?: React.CSSProperties;
  /** 对话框是否可见 */
  visible?: boolean;
  /** 标题 */
  title?: React.ReactNode | string;
  /** 宽度 */
  width?: string | number;
  /** 底部内容，当不需要默认底部按钮时，可以设为 footer={null} */
  footer?: React.ReactNode;
  /** 确认按钮文字 */
  okText?: React.ReactNode;
  /** 取消按钮文字 */
  cancelText?: React.ReactNode;
  /** ok 按钮 props */
  okButtonProps?: ButtonProps;
  /** cancel 按钮 props */
  cancelButtonProps?: ButtonProps;
  /** 点击确定回调 */
  onOk?: (e: React.MouseEvent<HTMLElement>) => void;
  /** 点击模态框右上角叉、取消按钮、Props.maskClosable 值为 true 时的遮罩层的回调 */
  onCancel?: (e: React.MouseEvent<HTMLElement>) => void;
  /** 点击取消按钮的回调 */
  onCancelClick?: (e: React.MouseEvent<HTMLElement>) => void;
  /** Modal 完全关闭后的回调 */
  afterClose?: () => void;
}

export interface ModalFuncProps
  extends Omit<ModalProps, 'onOk' | 'onCancel' | 'onCancelClick'> {
  /** 图标 */
  icon?: React.ReactNode;
  okCancel?: boolean;
  content?: React.ReactNode;
  type?: 'info' | 'success' | 'error' | 'warn' | 'warning' | 'confirm';
  /** 点击确定回调 */
  onOk?: (e: React.MouseEvent<HTMLElement>) => void | Promise<void>;
  /** 点击模态框右上角叉、取消按钮、Props.maskClosable 值为 true 时的遮罩层的回调 */
  onCancel?: (e: React.MouseEvent<HTMLElement>) => void | Promise<void>;
  /** 点击取消按钮的回调 */
  onCancelClick?: (e: React.MouseEvent<HTMLElement>) => void | Promise<void>;
}

const Modal: React.FC<ModalProps> = (props) => {
  const [, bem, t] = useNamespace('modal');
  const { hd } = useContext(ConfigProvider.Context);
  const [visible, { toggle }] = useBoolean(false);

  useMount(() => {
    toggle(props.visible);
  });

  useUpdateEffect(() => {
    toggle(props.visible);
  }, [props.visible]);

  const onOk = (e: React.MouseEvent<HTMLElement>) => {
    props.onOk?.(e);
  };

  const onCancel = (e: React.MouseEvent<HTMLElement>) => {
    props.onCancel?.(e);
    props.onCancelClick?.(e);
  };

  const renderFooter = () => {
    if (props.footer === null) return null;
    if (props.footer) return props.footer;
    return (
      <div className={classNames(bem('footer'))}>
        <Button color="gray" {...props.okButtonProps} onClick={onCancel}>
          {props.cancelText || t('cancelText')}
        </Button>
        <Button type="primary" {...props.cancelButtonProps} onClick={onOk}>
          {props.okText || t('okText')}
        </Button>
      </div>
    );
  };

  return (
    <Popup
      round
      visible={visible}
      onChange={toggle}
      className={props.className}
      style={{
        width: isNumber(props.width) ? hd(props.width) : props.width,
        ...props.style,
      }}
      closeOnClickOverlay={false}
      onClosed={props.afterClose}
    >
      <div className={classNames(bem())}>
        <div className={classNames(bem('title-wrapper'))}>{props.title}</div>
        <div className={classNames(bem('content'))}>{props.children}</div>
        {renderFooter()}
      </div>
    </Popup>
  );
};

Modal.defaultProps = {
  width: '80%',
  visible: false,
};

export default Modal;
