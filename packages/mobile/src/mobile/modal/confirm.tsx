import * as React from 'react';
import { createElement } from '@parallel-line/utils';
import ConfigProvider from '../config-provider';
import {
  InfoCircleFill,
  CloseCircleFill,
  CheckCircleFill,
  WarningCircleFill,
} from '@parallel-line/icons';
import { ModalFuncProps, destroyFns } from './Modal';
import ConfirmDialog, { ConfirmDialogProps } from './ConfirmDialog';

type ConfigUpdate =
  | ModalFuncProps
  | ((prevConfig: ModalFuncProps) => ModalFuncProps);

export type ModalFunc = (
  props: ModalFuncProps,
) => {
  destroy: () => void;
  update: (configUpdate: ConfigUpdate) => void;
};

export type ModalStaticFunctions = Record<
  NonNullable<ModalFuncProps['type']>,
  ModalFunc
>;

export default function confirm(config: ModalFuncProps) {
  const E = createElement();
  // eslint-disable-next-line @typescript-eslint/no-use-before-define
  let currentConfig: ConfirmDialogProps = {
    ...config,
    close,
    visible: true,
    afterClose: () => {
      if (typeof config.afterClose === 'function') {
        config.afterClose();
      }
      destroy();
    },
  };

  function destroy(...args: any[]) {
    E.destroy();
    const triggerCancel = args.some((param) => param && param.triggerCancel);
    if (config.onCancel && triggerCancel) {
      // @ts-ignore
      config.onCancel(...args);
    }
    for (let i = 0; i < destroyFns.length; i++) {
      const fn = destroyFns[i];
      // eslint-disable-next-line @typescript-eslint/no-use-before-define
      if (fn === close) {
        destroyFns.splice(i, 1);
        break;
      }
    }
  }

  function render({
    okText,
    cancelText,
    // prefixCls: customizePrefixCls,
    ...props
  }: ConfirmDialogProps) {
    /**
     * https://github.com/ant-design/ant-design/issues/23623
     *
     * Sync render blocks React event. Let's make this async.
     */
    setTimeout(() => {
      E.render(
        <ConfigProvider>
          <ConfirmDialog {...props} okText={okText} cancelText={cancelText} />
        </ConfigProvider>,
      );
    });
  }

  function close() {
    currentConfig = {
      ...currentConfig,
      visible: false,
    };
    render(currentConfig);
  }

  function update(configUpdate: ConfigUpdate) {
    if (typeof configUpdate === 'function') {
      currentConfig = configUpdate(currentConfig);
    } else {
      currentConfig = {
        ...currentConfig,
        ...configUpdate,
      };
    }
    render(currentConfig);
  }

  render(currentConfig);

  destroyFns.push(close);

  return {
    destroy: close,
    update,
  };
}

export function withWarn(props: ModalFuncProps): ModalFuncProps {
  return {
    icon: <WarningCircleFill />,
    okCancel: false,
    ...props,
    type: 'warning',
  };
}

export function withInfo(props: ModalFuncProps): ModalFuncProps {
  return {
    icon: <InfoCircleFill />,
    okCancel: false,
    ...props,
    type: 'info',
  };
}

export function withSuccess(props: ModalFuncProps): ModalFuncProps {
  return {
    icon: <CheckCircleFill />,
    okCancel: false,
    ...props,
    type: 'success',
  };
}

export function withError(props: ModalFuncProps): ModalFuncProps {
  return {
    icon: <CloseCircleFill />,
    okCancel: false,
    ...props,
    type: 'error',
  };
}

export function withConfirm(props: ModalFuncProps): ModalFuncProps {
  return {
    icon: <WarningCircleFill />,
    okCancel: true,
    ...props,
    type: 'confirm',
  };
}
