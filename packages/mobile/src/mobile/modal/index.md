---
title: Modal 对话框
mobile: true
---

# Modal 对话框

模态对话框。

## 何时使用

需要用户处理事务，又不希望跳转页面以致打断工作流程时，可以使用 Modal 在当前页面正中打开一个浮层，承载相应的操作。

另外当需要一个简洁的确认框询问用户时，可以使用 Modal.confirm() 等语法糖方法。

## 代码演示

<code src="./demos/basic.tsx"></code>
<code src="./demos/info.tsx"></code>
<code src="./demos/confirm.tsx"></code>
<code src="./demos/hooks.tsx"></code>

<API src="./Modal.tsx" exports='["default"]'></API>

<!-- <API src="./ConfirmDialog.tsx" exports='["default"]'></API> -->
