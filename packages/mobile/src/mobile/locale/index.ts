// import { useContext } from 'react';
import defaultMessages from './lang/zh-CN';
// import ConfigProvider from '../config-provider';

type Message = Record<string, any>;

function useLocale() {
  // const { locale } = useContext(ConfigProvider.Context);
  function messages(): Message {
    return defaultMessages;
  }

  return { messages };
}

export default useLocale;
export { useLocale };
