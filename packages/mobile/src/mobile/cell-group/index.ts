import InternalCellGroup from "./cell-group";
import Cell from "./cell";

type InternalCellGroupType = typeof InternalCellGroup;

interface CellGroupInterface extends InternalCellGroupType {
  Cell: typeof Cell;
}

const CellGroup = InternalCellGroup as CellGroupInterface;

CellGroup.Cell = Cell;

export default CellGroup;
