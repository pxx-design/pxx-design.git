import React from "react";
import classNames from "classnames";
import {useNamespace} from '@parallel-line/common/es/hooks';

interface CellGroupProps {
  children?: React.ReactNode;
  style?: React.CSSProperties;
}

const CellGroup = (props: CellGroupProps) => {
  const [cls] = useNamespace('cell-group');
  return (
    <div className={classNames(cls)} style={props.style}>{props.children}</div>
  );
};
export default CellGroup;
