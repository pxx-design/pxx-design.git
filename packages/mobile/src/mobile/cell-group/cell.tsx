import React from "react";
import classNames from "classnames";
import {useNamespace} from '@parallel-line/common/es/hooks';

interface CellProps {
  className?: string;
  prefixCls?: string;
  text?: React.ReactNode;
  prefix?: React.ReactNode;
  suffix?: React.ReactNode;
  onClick?: () => void;
}

const Cell = (props: CellProps) => {
  const {prefix, suffix} = props;
  const [cls] = useNamespace('cell');
  return (
    <div onClick={props.onClick} className={classNames(cls, props.className)}>
      {
        prefix &&
        <div className={`${cls}-prefix`}>
          {prefix}
        </div>
      }
      <div className={`${cls}-container`}>
        <div className={`${cls}-text`}>{props.text}</div>
        <div className={`${cls}-suffix`}>
          {suffix}
        </div>
      </div>
    </div>
  );
};

export default Cell;
