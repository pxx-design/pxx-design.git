/**
 * title: 基本
 * desc: 最简单的用法
 */

import React from 'react';
import {CellGroup} from '@parallel-line/mobile';
import { LockOutlined, MoreArrowOutlined, PhotoFailOutlined,BalancePayOutlined,CheckOutlined,InfoCircleFill,ShareOutlined } from '@parallel-line/icons';


export default () => {
  return (
    <div style={{background: '#efefef', height: '100%', padding: 16, overflow: 'auto'}}>
      <CellGroup style={{marginBottom: 20}}>
        <CellGroup.Cell text="这是测试1"
                        prefix={<LockOutlined />}
                        suffix={<MoreArrowOutlined />}/>
        <CellGroup.Cell text="这是测试1"
                        prefix={<PhotoFailOutlined />}
                        suffix={<MoreArrowOutlined />}/>
        <CellGroup.Cell text="这是测试1"
                        prefix={<InfoCircleFill />}
                        suffix={<MoreArrowOutlined />}/>
        <CellGroup.Cell text="这是测试1"
                        prefix={<BalancePayOutlined/>}
                        suffix={<MoreArrowOutlined />}/>
        <CellGroup.Cell text="这是测试1"
                        prefix={<ShareOutlined />}
                        suffix={<MoreArrowOutlined />}/>
      </CellGroup>
      <CellGroup>
        <CellGroup.Cell text="这是测试1"
                        prefix={<LockOutlined style={{fontSize: 32}} />}
                        suffix={<CheckOutlined />}/>
        <CellGroup.Cell text="这是测试1"
                        prefix={<PhotoFailOutlined style={{fontSize: 32}} />}
                        suffix={<CheckOutlined />}/>
        <CellGroup.Cell text="这是测试1"
                        prefix={<InfoCircleFill style={{fontSize: 32}} />}
                        suffix={<CheckOutlined />}/>
        <CellGroup.Cell text="这是测试1"
                        prefix={<BalancePayOutlined style={{fontSize: 32}}/>}
                        suffix={<CheckOutlined />}/>
        <CellGroup.Cell text="这是测试1"
                        prefix={<ShareOutlined  style={{fontSize: 32}}/>}
                        suffix={<CheckOutlined />}/>
      </CellGroup>
    </div>
  );
};
