import * as React from 'react';
import { isObject, extend } from 'lodash-es';
import { render, createElement } from '@parallel-line/utils';
import ToastComponent, { ToastType, ToastProps } from './Toast';
import ConfigProvider from '../config-provider';
import { inBrowser } from '@parallel-line/common/es/utils';

export interface ToastOptions
  extends Omit<ToastProps, 'visible' | 'onChange' | 'onClosed'> {}

const defaultOptions: ToastOptions = {
  icon: '',
  type: 'text',
  message: '',
  className: '',
  overlay: false,
  onClose: undefined,
  onOpened: undefined,
  duration: 2000,
  position: 'middle',
  transition: 'pxx-fade',
  forbidClick: false,
  overlayClass: '',
  overlayStyle: undefined,
  closeOnClick: false,
  closeOnClickOverlay: false,
};

interface ComponentInstance {
  open(options: ToastOptions): void;
  clear(): void;
}

let instance: ComponentInstance;

let queue: ComponentInstance[] = [];
let allowMultiple = false;
let currentOptions = extend({}, defaultOptions);

// default options of specific type
let defaultOptionsMap: Record<string, ToastOptions | null> = {};

function parseOptions(message: string | ToastOptions): ToastOptions {
  if (isObject(message)) {
    return message;
  }
  return { message };
}
function createInstance() {
  const E = createElement();

  function render(props: ToastProps) {
    setTimeout(() => {
      E.render(
        <ConfigProvider>
          <ToastComponent {...props} />
        </ConfigProvider>,
      );
    });
  }

  function close(options: ToastOptions) {
    render(extend({}, options, { show: false }));
  }

  function open(options: ToastOptions) {
    render(extend({}, options, { show: true, onClosed: () => close(options) }));
  }

  instance = { open, clear: E.destroy };
  return instance;
}

function getInstance() {
  if (!queue.length || allowMultiple) {
    const instance = createInstance();
    queue.push(instance);
  }

  return queue[queue.length - 1];
}

function Toast(options: string | ToastOptions = {}) {
  if (!inBrowser) {
    return;
  }

  const parsedOptions = parseOptions(options);

  const toast = render(
    ToastComponent,
    extend(
      {},
      currentOptions,
      defaultOptionsMap[parsedOptions.type || currentOptions.type!],
      parsedOptions,
    ),
  );

  toast.open();

  return toast;
}

const createMethod = (type: ToastType) => (options: string | ToastOptions) =>
  Toast(extend({ type }, parseOptions(options)));

Toast.loading = createMethod('loading');
Toast.success = createMethod('success');
Toast.fail = createMethod('fail');

Toast.clear = (all?: boolean) => {
  if (queue.length) {
    if (all) {
      queue.forEach((toast) => {
        toast.clear();
      });
      queue = [];
    } else if (!allowMultiple) {
      queue[0].clear();
    } else {
      queue.shift()!.clear();
    }
  }
};

function setDefaultOptions(options: ToastOptions): void;
function setDefaultOptions(type: ToastType, options: ToastOptions): void;
function setDefaultOptions(type: ToastType | ToastOptions, options?: any) {
  if (typeof type === 'string') {
    defaultOptionsMap[type] = options;
  } else {
    extend(currentOptions, type);
  }
}

Toast.setDefaultOptions = setDefaultOptions;

Toast.resetDefaultOptions = (type?: ToastType) => {
  if (typeof type === 'string') {
    defaultOptionsMap[type] = null;
  } else {
    currentOptions = extend({}, defaultOptions);
    defaultOptionsMap = {};
  }
};

Toast.allowMultiple = (value = true) => {
  allowMultiple = value;
};

export { Toast };
