import * as React from 'react';
import classNames from 'classnames';
import {
  CheckCircleFill,
  WarningCircleFill,
  LoadingOutlined,
} from '@parallel-line/icons';
import { Popup } from '../popup';
import { useNamespace } from '@parallel-line/common/es/hooks';
import { isDef } from '@parallel-line/common/es/utils';

export type ToastType = 'text' | 'loading' | 'success' | 'fail';
export type ToastPosition = 'top' | 'middle' | 'bottom';

export interface ToastProps {
  icon?: React.ReactNode;
  type?: ToastType;
  mask?: boolean;
  message?: React.ReactNode;
  overlay?: boolean;
  duration?: number;
  position?: ToastPosition;
  className?: string;
  transition?: string;
  forbidClick?: boolean;
  closeOnClick?: boolean;
  overlayClass?: string;
  overlayStyle?: Record<string, any>;
  closeOnClickOverlay?: boolean;

  onChange?(show?: boolean): void;
  onClosed?: () => void;
  onOpened?: () => void;

  visible?: boolean;
  onClose?: () => void;
  afterClose?: () => void;
}
const iconMap: { [key: string]: any } = {
  success: CheckCircleFill,
  fail: WarningCircleFill,
  loading: LoadingOutlined,
};

export const Toast = React.forwardRef<unknown, ToastProps>((props, ref) => {
  const [, bem] = useNamespace('toast');
  const [show, onChange] = React.useState(props.visible);
  const {
    icon,
    type = 'text',
    overlay = false,
    duration = 2000,
    position = 'middle',
    transition = 'pxx-fade',
  } = props;
  let timer: NodeJS.Timeout;

  React.useEffect(() => {
    onChange(props.visible);
  }, [props.visible]);

  const updateShow = (show: boolean) => {
    props.onChange?.(show);
    onChange(show);
  };

  const onClick = () => {
    if (props.closeOnClick) {
      updateShow(false);
    }
  };

  const onClosed = () => {
    clearTimer();
    updateShow(false);
    props.onClosed?.();
  };

  const clearTimer = () => {
    clearTimeout(timer);
  };

  const renderIcon = () => {
    if (type === 'success' || type === 'fail' || type === 'loading') {
      return <span className={bem('icon')}>{iconMap[type]}</span>;
    }

    return <span className={bem('icon')}>{icon}</span>;
  };

  const renderMessage = () => {
    const { message } = props;

    if (isDef(message) && message !== '') {
      return <div className={bem('text')}>{message}</div>;
    }
    return null;
  };

  React.useEffect(() => {
    clearTimer();

    // updateShow(true)
    if (show && duration > 0) {
      timer = setTimeout(() => {
        updateShow(false);
      }, duration);
    }
  }, [show, type, props.message, duration]);

  return (
    <Popup
      ref={ref}
      visible={show}
      className={classNames([
        bem([position, { [type]: !icon }]),
        props.className,
      ])}
      overlay={overlay}
      lockScroll={false}
      transition={transition}
      overlayClass={props.overlayClass}
      overlayStyle={props.overlayStyle}
      closeOnClickOverlay={props.closeOnClickOverlay}
      onClick={onClick}
      onClosed={onClosed}
      afterClose={props.afterClose}
      onChange={updateShow}
    >
      {renderIcon()}
      {renderMessage()}
    </Popup>
  );
});

export default Toast;
