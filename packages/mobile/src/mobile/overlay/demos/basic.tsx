/**
 * title: 基本
 * desc: 最简单的用法
 */

import React from 'react';
import { useBoolean } from 'ahooks';
import { Button,Overlay } from '@parallel-line/mobile';

export default () => {
  const [show, { setFalse, setTrue }] = useBoolean();
  return (
    <div>
      <Button type="primary" onClick={setTrue}>
        显示遮罩层
      </Button>
      <Overlay show={show} onClick={setFalse}></Overlay>
    </div>
  );
};
