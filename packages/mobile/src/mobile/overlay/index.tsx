import React from 'react';
import classNames from 'classnames';
import { extend } from 'lodash-es';
import { Transition } from '@parallel-line/common';
import { useNamespace, useLazyRender } from '@parallel-line/common/es/hooks';
import {
  noop,
  isDef,
  getZIndexStyle,
  preventDefault,
} from '@parallel-line/common/es/utils';

export interface OverlayProps {
  className?: string;
  style?: React.CSSProperties;
  /** 是否展示遮罩层 */
  show?: boolean;
  /** z-index 层级 */
  zIndex?: number | string;
  /** 动画时长，单位秒 */
  duration?: number | string;
  /** 是否锁定背景滚动，锁定时蒙层里的内容也将无法滚动 */
  lockScroll?: boolean;
  /** 自定义样式 */
  customStyle?: React.CSSProperties;

  onClick?(event: MouseEvent): void;
}

const Overlay: React.FC<OverlayProps> = (props) => {
  const [, bem] = useNamespace('overlay');

  const lazyRender = useLazyRender(props.show);

  const preventTouchMove: any = (event: TouchEvent) => {
    preventDefault(event, true);
  };

  const renderOverlay = lazyRender((isTransition: boolean) => {
    const style: React.CSSProperties = extend(
      getZIndexStyle(props.zIndex),
      props.customStyle,
      props.style,
    );

    if (isDef(props.duration)) {
      style.animationDuration = `${props.duration}s`;
    }

    if (isTransition) {
      return (
        <div
          style={style}
          className={classNames(bem(['transition']), props.className)}
          onTouchMove={props.lockScroll ? preventTouchMove : noop}
          onClick={props.onClick as any}
        >
          {props.children}
        </div>
      );
    }

    return (
      <div
        style={style}
        className={classNames(bem(), props.className)}
        onTouchMove={props.lockScroll ? preventTouchMove : noop}
      />
    );
  });
  return (
    <>
      {renderOverlay(false)}
      <Transition show={props.show} name="pxx-fade">
        {renderOverlay(true)}
      </Transition>
    </>
  );
};

Overlay.defaultProps = {
  lockScroll: true,
};

export default Overlay;
