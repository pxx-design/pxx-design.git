---
title: Overlay 遮罩层
mobile: true
---

# Overlay 遮罩层

创建一个遮罩层，用于强调特定的页面元素，并阻止用户进行其他操作。

## 代码演示

<code src="./demos/basic.tsx"></code>

<API></API>
