import React from 'react';
import defaultMessages from '../locale/lang/zh-CN';
import {
  ConfigProvider,
  ConfigConsumer,
  ConfigProviderProps,
  ConfigConsumerProps,
} from '@parallel-line/common/es/components';

const ConfigContext = ConfigProvider.Context;
const SizeContext = ConfigProvider.SizeContext;

const InlineConfigProvider: React.FC<ConfigProviderProps> & {
  Context: typeof ConfigContext;
  SizeContext: typeof SizeContext;
} = (props) => <ConfigProvider {...props} />;

InlineConfigProvider.defaultProps = {
  prefixCls: 'pxx-mobile',
  locale: defaultMessages,
};

InlineConfigProvider.Context = ConfigContext;
InlineConfigProvider.SizeContext = SizeContext;

export { ConfigConsumer, ConfigContext };

export type { ConfigProviderProps, ConfigConsumerProps };

export default InlineConfigProvider;
