import React, {Children, cloneElement} from 'react';
import {useNamespace} from "@parallel-line/common/es/hooks";

interface TabBarProps {
  activeKey?: string;
  children: React.ReactNode;
  onChange?: (key: string) => void;
}

const TabBar = (props: TabBarProps) => {
  const [cls] = useNamespace('tab-bar');

  return (
    <div className={cls}>
      {
        Children.map(props.children, ((child: any) => {
          return cloneElement(child, {
            isActive: props.activeKey === child.key,
            onClick: () => props.onChange?.(child.key)
          })
        }))
      }
    </div>
  );
};

export default TabBar;
