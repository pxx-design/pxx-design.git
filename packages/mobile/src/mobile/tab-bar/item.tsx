import React from 'react';
import classNames from 'classnames';
import {useNamespace} from "@parallel-line/common/es/hooks";

interface ItemProps {
  title?: string;
  onClick?: () => void;
  icon?: React.ReactNode;
  isActive?: boolean;
}

const Item = (props: ItemProps) => {
  const [cls, bem] = useNamespace('tab-bar-item');
  return (
    <div className={classNames(cls, {
      [bem('active')]: props.isActive
    })} onClick={props.onClick}>
      <div>
        <div className={bem('icon')}>
          {props.icon}
        </div>
        <div>{props.title}</div>
      </div>
    </div>
  );
};

export default Item;
