import React from 'react';
import {TabBar} from '@parallel-line/mobile';
import {InfoCircleOutlined} from '@parallel-line/icons'

const Basic = () => {
  return (
    <div style={{backgroundColor: '#ccc', height: '100%'}}>
      <TabBar activeKey="1">
        <TabBar.Item key="1"
                     title="首页"
                     icon={<InfoCircleOutlined/>}/>
        <TabBar.Item key="2"
                     title="首页"
                     icon={<InfoCircleOutlined/>}/>
      </TabBar>
    </div>
  );
};

export default Basic;
