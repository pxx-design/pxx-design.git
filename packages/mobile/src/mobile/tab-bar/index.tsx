import InternalTabBar from './tab-bar';
import Item from './Item';

type InternalTabBarType = typeof InternalTabBar;

interface TabBarInternal extends InternalTabBarType {
  Item: typeof Item;
}

const TabBar = InternalTabBar as TabBarInternal;


TabBar.Item = Item;

export default TabBar;
