import React from 'react';
import classNames from "classnames";
import {useNamespace} from '@parallel-line/common/es/hooks';

interface CardProps {
  className?: string;
  footer?: React.ReactNode;
  header?: React.ReactNode;
  children?: React.ReactNode;
  style?: React.CSSProperties;
  onClick?: () => void;
}

const Card = (props: CardProps) => {
  const {header, children, footer, onClick, className, style} = props;
  const [cls, bem] = useNamespace('card')
  return (
    <div className={classNames(cls, className)} style={style}>
      {header && <div className={bem('header')}>{header}</div>}
      <div onClick={onClick}>
        {children}
      </div>
      {footer && <div className={bem('footer')}>{footer}</div>}
    </div>
  );
};

export default Card;
