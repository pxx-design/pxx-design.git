import React from 'react';
import {Card, RouteInfo, Badge, SpaceBetween, Button, Space} from '@parallel-line/mobile';

const Basic = () => {
  return (
    <div style={{backgroundColor: '#e6e6e6', height: '100%', overflow: 'hidden'}}>
      <Space size={[12, 16]} direction="vertical">
        <Card header={<Badge status="warning" text="待处理"/>}
              footer={
                <SpaceBetween>
                  <div>更多</div>
                  <div>
                    <Button size="mini"
                            color="gray">
                      编辑
                    </Button>
                    <Button size="mini"
                            color="gray">
                      发布
                    </Button>
                  </div>
                </SpaceBetween>
              }>
          <RouteInfo endAdcode="130110"
                     startAdcode="110101"/>
          <SpaceBetween>
            <div>货主：张三</div>
            <div>啦啦啦啦啦</div>
          </SpaceBetween>
        </Card>
        <Card header={<Badge status="warning" text="待处理"/>}
              footer={
                <SpaceBetween>
                  <div>更多</div>
                  <div>
                    <Button size="mini"
                            color="gray">
                      编辑
                    </Button>
                    <Button size="mini"
                            color="gray">
                      发布
                    </Button>
                  </div>
                </SpaceBetween>
              }>
          <RouteInfo endAdcode="130110"
                     startAdcode="110101"
                     count={20}
                     time={false}/>
          <SpaceBetween>
            <div>货主：张三</div>
            <div>啦啦啦啦啦</div>
          </SpaceBetween>
        </Card>
      </Space>
    </div>
  );
};

export default Basic;
