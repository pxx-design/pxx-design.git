---
title: Button 按钮
mobile: true
---

# Button 按钮

按钮用于触发一个操作，如提交表单。

## 代码演示

<code src="./demos/color.tsx"></code>
<code src="./demos/basic.tsx"></code>
<code src="./demos/plain.tsx"></code>
<code src="./demos/size.tsx"></code>
<code src="./demos/icon.tsx"></code>
<code src="./demos/loading.tsx"></code>
<code src="./demos/disabled.tsx"></code>

<API></API>
