/**
 * title: 朴素按钮
 * desc: 通过 `plain` 属性将按钮设置为朴素按钮，朴素按钮的文字为按钮颜色，背景为白色。
 */

import React from 'react';
import { Space } from 'antd';
import { Button } from '@parallel-line/mobile';

export default () => {
  return (
    <Space direction="vertical">
      <Button plain type="primary">主要按钮</Button>
      <Button plain type="default">默认按钮</Button>
      <Button plain type="warning">警告按钮</Button>
      <Button plain type="danger">危险按钮</Button>
    </Space>
  );
};
