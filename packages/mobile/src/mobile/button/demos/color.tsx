/**
 * title: 自定义颜色
 * desc: 通过 `color` 属性可以自定义按钮的颜色。
 */

import React from 'react';
import { Space } from 'antd';
import { Button } from '@parallel-line/mobile';

export default () => {
  return (
    <Space direction="vertical">
      <Button type="primary">填充式主按钮</Button>
      <Button color="gray">填充式次按钮</Button>
      <Button plain type="primary">
        线框式主按钮
      </Button>
      <Button type="default">线框式次按钮</Button>
      <Button type="warning">警告按钮</Button>
      <Button type="danger">危险按钮</Button>
      <div style={{ background: '#f5f5f7', padding: 16 }}>
        <Button color="white">白色按钮</Button>
      </div>
    </Space>
  );
};
