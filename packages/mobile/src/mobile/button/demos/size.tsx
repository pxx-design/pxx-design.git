/**
 * title: 按钮尺寸
 * desc: 支持 `large`、`normal`、`small`、`mini` 四种尺寸，默认为 `normal`。
 */

import React from 'react';
import { Space } from 'antd';
import { Button } from '@parallel-line/mobile';

export default () => {
  return (
    <Space direction="vertical">
      <Button type="primary" size="large">
        大号按钮
      </Button>
      <div>多用于页面底部</div>
      <Button type="primary" size="normal">
        普通按钮
      </Button>
      <div>多用于弹窗、动作面板、反馈结果界面</div>
      <Button type="primary" size="small">
        小型按钮
      </Button>
      <div>多用于卡片内，以及空状态提示操作</div>
      <Button type="primary" size="mini">
        迷你按钮
      </Button>
    </Space>
  );
};
