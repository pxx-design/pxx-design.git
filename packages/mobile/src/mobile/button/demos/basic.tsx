/**
 * title: 按钮类型
 * desc: 按钮支持 `default`、`primary`、`info`、`warning`、`danger` 五种类型，默认为 `default`。
 */

import React from 'react';
import { Space } from 'antd';
import { Button } from '@parallel-line/mobile';

export default () => {
  return (
    <Space direction="vertical">
      <Button type="primary">主要按钮</Button>
      <Button type="default">默认按钮</Button>
      <Button type="warning">警告按钮</Button>
      <Button type="danger">危险按钮</Button>
    </Space>
  );
};
