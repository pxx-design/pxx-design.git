/**
 * title: 禁用状态
 * desc: 通过 `disabled` 属性来禁用按钮，禁用状态下按钮不可点击。
 */

import React from 'react';
import { Space } from 'antd';
import { Button } from '@parallel-line/mobile';

export default () => {
  return (
    <Space direction="vertical">
      <Button disabled type="primary">填充式主按钮</Button>
      <Button disabled color="gray">填充式次按钮</Button>
      <Button disabled plain type="primary">
        线框式主按钮
      </Button>
      <Button disabled type="default">线框式次按钮</Button>
      <Button disabled type="warning">警告按钮</Button>
      <Button disabled type="danger">危险按钮</Button>
      <div style={{ background: '#f5f5f7', padding: 16 }}>
        <Button disabled color="white">白色按钮</Button>
      </div>
    </Space>
  );
};
