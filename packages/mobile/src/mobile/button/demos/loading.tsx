/**
 * title: 加载状态
 * desc: 通过 `loading` 属性设置按钮为加载状态，加载状态下默认会隐藏按钮文字，可以通过 `loadingText` 设置加载状态下的文字。
 */

 import React from 'react';
 import { Space } from 'antd';
 import { SpinnerOutlined } from '@parallel-line/icons';
 import { Button } from '@parallel-line/mobile';

 export default () => {
   return (
     <Space direction="vertical">
       <Button loading type="primary">默认</Button>
       <Button loading loadingIcon={<SpinnerOutlined />} type="primary" >自定义图标</Button>
       <Button loading loadingText="加载中..." type="primary">自定义文本</Button>
     </Space>
   );
 };
