/**
 * title: 图标按钮
 * desc: 通过 `icon` 属性设置按钮图标，支持 Icon 组件里的所有图标，也可以传入图标 URL。
 */

import React from 'react';
import { Space } from 'antd';
import { Button } from '@parallel-line/mobile';
import { QuestionCircleOutlined } from '@ant-design/icons';

export default () => {
  return (
    <Space direction="vertical">
      <Button icon={<QuestionCircleOutlined />} type="primary" size="large">
        大号按钮
      </Button>
      <div>多用于页面底部</div>
      <Button icon={<QuestionCircleOutlined />} type="primary" size="normal">
        普通按钮
      </Button>
      <div>多用于弹窗、动作面板、反馈结果界面</div>
      <Button icon={<QuestionCircleOutlined />} type="primary" size="small">
        小型按钮
      </Button>
      <div>多用于卡片内，以及空状态提示操作</div>
      <Button icon={<QuestionCircleOutlined />} type="primary" size="mini">
        迷你按钮
      </Button>
    </Space>
  );
};
