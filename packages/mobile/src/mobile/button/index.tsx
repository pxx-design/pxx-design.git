import * as React from 'react';
import classNames from 'classnames';
import { useDebounceFn } from 'ahooks';
import { isObject } from 'lodash-es';
import type { DebounceOptions } from 'ahooks/es/useDebounce/debounceOptions';
import { LoadingOutlined } from '@parallel-line/icons';
import { useNamespace } from '@parallel-line/common/es/hooks';
import { BORDER_SURROUND } from '@parallel-line/common/es/constant';

export type ButtonType =
  | 'default'
  | 'primary'
  | 'success'
  | 'warning'
  | 'danger';

export type ButtonSize = 'large' | 'normal' | 'small' | 'mini';

export interface GetPhoneNumberEvent {
  detail: {
    /** 包括敏感数据在内的完整用户信息的加密数据 */
    encryptedData: string;
    /** 加密算法的初始向量 */
    iv: string;
    /** 敏感数据对应的云 ID，开通云开发的小程序才会返回，可通过云调用直接获取开放数据 */
    cloudID: string;
    [k: string]: any;
  };
  [k: string]: any;
}

export interface ButtonProps {
  /** 按钮根节点的 HTML 标签 */
  component?: string | React.FC<any> | React.ComponentClass<any>;
  /** 左侧图标 */
  icon?: React.ReactNode;
  /** 按钮颜色，支持传入 `linear-gradient` 渐变色 */
  color?: string;
  /** 是否为块级元素 */
  block?: boolean;
  /** 是否为朴素按钮 */
  plain?: boolean;
  /** 是否为圆形按钮 */
  round?: boolean;
  square?: boolean;
  /** 是否显示为加载状态 */
  loading?: boolean;
  /** 是否使用 0.5px 边框 */
  hairline?: boolean;
  /** 是否禁用按钮 */
  disabled?: boolean;
  /** 自定义加载中图标 */
  loadingIcon?: React.ReactNode;
  /** 加载状态提示文字 */
  loadingText?: string;
  /** 类型 */
  type?: ButtonType;
  /** 尺寸 */
  size?: ButtonSize;
  /** 图标展示位置 */
  iconPosition?: 'left' | 'right';
  className?: string;
  style?: React.CSSProperties;

  /** 设置 button 原生的 type 值，可选值请参考 HTML 标准 */
  htmlType?: string;

  /** 微信开放能力, 在Taro环境下有效 */
  openType?: string;
  /** 获取用户手机号回调，component=button&&openType=getPhoneNumber时有效 */
  onGetPhoneNumber?(e: GetPhoneNumberEvent): void;
  /** 客服消息回调，component=button&&openType="contact"时有效 */
  onContact?(e: any): void;
  /** 当使用开放能力时，发生错误的回调，component=button&&openType=launchApp时有效 */
  onError?(e: any): void;
  /** 在打开授权设置页后回调，component=button&&openType=openSetting时有效 */
  onOpenSetting?(e: any): void;
  /** 打开 APP 成功的回调，component=button&&openType=launchApp时有效 */
  onLaunchApp?(e: any): void;

  /** 点击是否防抖 */
  isDebounceClick?: boolean | DebounceOptions;
  /** 点击按钮，且按钮状态不为加载或禁用时触发 */
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
}
function removeEmpty<T extends Record<string, any>>(obj: T) {
  let newObj: any = {};
  Object.keys(obj).forEach((key) => {
    if (obj[key] !== undefined) newObj[key] = obj[key];
  });
  return newObj as T;
}

const Button: React.FC<ButtonProps> = (props) => {
  const [, bem] = useNamespace('button');
  const {
    component = 'button',
    type,
    size,
    block,
    round,
    plain,
    square,
    loading,
    disabled,
    hairline,
    htmlType = 'button',
    isDebounceClick = true,
  } = props;

  const renderLoadingIcon = () => {
    return (
      <div className={bem('loading')}>
        {props.loadingIcon || <LoadingOutlined />}
      </div>
    );
  };

  const renderIcon = () => {
    if (props.loading) {
      return renderLoadingIcon();
    }

    if (props.icon) {
      return <div className={bem('icon')}>{props.icon}</div>;
    }
  };

  const renderText = () => {
    let text;
    if (props.loading) {
      text = props.loadingText || props.children;
    } else {
      text = props.children;
    }

    if (text) {
      return <div className={bem('text')}>{text}</div>;
    }
  };

  const getStyle = () => {
    const { color, plain } = props;
    const style: React.CSSProperties = {
      ...props.style,
    };
    if (color) {
      style.color = plain ? color : 'white';

      if (!plain) {
        // Use background instead of backgroundColor to make linear-gradient work
        style.background = color;
      }

      // hide border when color is linear-gradient
      if (color.includes('gradient')) {
        style.border = 0;
      } else {
        style.borderColor = color;
      }

      if (['gray', 'white'].includes(color)) {
        if (!plain) style.border = 0;
        style.color = '#1E56FF';
        switch (color) {
          case 'gray':
            style.background = '#F0F1F5';
            style.borderColor = '#F0F1F5';
            break;
        }
      }

      return style;
    }
    return style;
  };

  function onEvent<T>(event: T) {
    if (!event) return;
    if (props.loading) return;
    if (props.disabled) return;
    return event;
  }

  const onClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
    if (props.loading) {
      event.preventDefault();
    } else if (!props.disabled) {
      props.onClick?.(event);
    }
  };

  const debounceOptions: DebounceOptions = isObject(isDebounceClick)
    ? { wait: 300, ...isDebounceClick }
    : { wait: isDebounceClick ? 300 : 0 };
  const { run } = useDebounceFn(onClick, debounceOptions);

  const classes = classNames([
    bem([
      type,
      size,
      {
        plain,
        block,
        round,
        square,
        loading,
        disabled,
        hairline,
      },
    ]),
    { [BORDER_SURROUND]: hairline },
    props.className,
  ]);
  const componentProps = {
    className: classes,
    style: getStyle(),
    onClick: run,
    type: htmlType,
    openType: props.openType,
    onError: props.onError,
    onGetPhoneNumber: onEvent(props.onGetPhoneNumber),
    onContact: onEvent(props.onContact),
    onOpenSetting: onEvent(props.onOpenSetting),
    onLaunchApp: onEvent(props.onLaunchApp),
  };

  return React.createElement(
    component,
    removeEmpty(componentProps),
    <div className={bem('content')}>
      {props.iconPosition === 'left' && renderIcon()}
      {renderText()}
      {props.iconPosition === 'right' && renderIcon()}
    </div>,
  );
};

Button.defaultProps = {
  iconPosition: 'left',
  type: 'default',
  size: 'normal',
};

export default Button;
