import { TopTips, TopTipsOptions } from './function-call';
import type { TopTipsType } from './TopTips';

export default TopTips;
export { TopTips };
export type { TopTipsType, TopTipsOptions };
