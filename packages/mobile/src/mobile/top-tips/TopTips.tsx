import * as React from 'react';
import classNames from 'classnames';
import {
  CheckCircleFill,
  CloseCircleFill,
  CloseOutlined,
} from '@parallel-line/icons';
import { Popup } from '../popup';
import { useNamespace } from '@parallel-line/common/es/hooks';
import { isDef } from '@parallel-line/common/es/utils';

export type TopTipsType = 'text' | 'success' | 'fail';

export interface TopTipsProps {
  icon?: React.ReactNode;
  type?: TopTipsType;
  message?: React.ReactNode;
  className?: string;
  forbidClick?: boolean;
  closeOnClick?: boolean;
  overlayClass?: string;
  overlayStyle?: Record<string, any>;
  closeOnClickOverlay?: boolean;

  onChange?(show?: boolean): void;
  onClosed?: () => void;
  onOpened?: () => void;

  visible?: boolean;
  onClose?: () => void;
  afterClose?: () => void;
}

export const TopTips = React.forwardRef<unknown, TopTipsProps>((props, ref) => {
  const [, bem] = useNamespace('top-tips');
  const [show, onChange] = React.useState(props.visible);
  const { icon, type = 'text' } = props;

  React.useEffect(() => {
    onChange(props.visible);
  }, [props.visible]);

  const updateShow = (show: boolean) => {
    props.onChange?.(show);
    onChange(show);
  };

  const onClick = () => {
    if (props.closeOnClick) {
      updateShow(false);
    }
  };

  const onClose = () => {
    updateShow(false);
    props.onClose?.();
  };

  const renderIcon = () => {
    if (type === 'fail') {
      return <CloseCircleFill className={bem('icon')} />;
    }
    if (type === 'success') {
      return <CheckCircleFill className={bem('icon')} />;
    }

    return icon;
  };

  const renderMessage = () => {
    const { message } = props;

    if (isDef(message) && message !== '') {
      return <div className={bem('text')}>{message}</div>;
    }
  };

  return (
    <Popup
      ref={ref}
      visible={show}
      className={classNames([bem({ [type]: !props.icon }), props.className])}
      overlay={false}
      lockScroll={false}
      transition={bem(`slide-top`)}
      overlayClass={props.overlayClass}
      overlayStyle={props.overlayStyle}
      closeOnClickOverlay={props.closeOnClickOverlay}
      onClick={onClick}
      onClosed={props.onClosed}
      afterClose={props.afterClose}
      onChange={updateShow}
    >
      <div className={bem('container')}>
        {renderIcon()}
        {renderMessage()}
        <CloseOutlined className={bem('close')} onClick={onClose} />
      </div>
    </Popup>
  );
});

export default TopTips;
