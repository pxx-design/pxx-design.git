import * as React from 'react';
import { isObject, extend } from 'lodash-es';
import { render, createElement } from '@parallel-line/utils';
import ConfigProvider from '../config-provider';
import TopTipsComponent, { TopTipsType, TopTipsProps } from './TopTips';
import { inBrowser } from '@parallel-line/common/es/utils';

export interface TopTipsOptions
  extends Omit<TopTipsProps, 'visible' | 'onChange' | 'onClosed'> {}

const defaultOptions: TopTipsOptions = {
  icon: '',
  type: 'text',
  message: '',
  className: '',
  onClose: undefined,
  onOpened: undefined,
  forbidClick: false,
  overlayClass: '',
  overlayStyle: undefined,
  closeOnClick: false,
  closeOnClickOverlay: false,
};

interface ComponentInstance {
  open(options: TopTipsOptions): void;
  clear(): void;
}

let instance: ComponentInstance;

let queue: ComponentInstance[] = [];
let allowMultiple = false;
let currentOptions = extend({}, defaultOptions);

// default options of specific type
let defaultOptionsMap: Record<string, TopTipsOptions | null> = {};

function parseOptions(message: string | TopTipsOptions): TopTipsOptions {
  if (isObject(message)) {
    return message;
  }
  return { message };
}
function createInstance() {
  const E = createElement();

  function render(props: TopTipsProps) {
    setTimeout(() => {
      E.render(
        <ConfigProvider>
          <TopTipsComponent {...props} />
        </ConfigProvider>,
      );
    });
  }

  function close(options: TopTipsOptions) {
    render(extend({}, options, { show: false }));
  }

  function open(options: TopTipsOptions) {
    render(extend({}, options, { show: true, onClosed: () => close(options) }));
  }

  instance = { open, clear: E.destroy };
  return instance;
}

function getInstance() {
  if (!queue.length || allowMultiple) {
    const instance = createInstance();
    queue.push(instance);
  }

  return queue[queue.length - 1];
}

function TopTips(options: string | TopTipsOptions = {}) {
  if (!inBrowser) {
    return;
  }

  const parsedOptions = parseOptions(options);

  const toast = render(
    TopTipsComponent,
    extend(
      {},
      currentOptions,
      defaultOptionsMap[parsedOptions.type || currentOptions.type!],
      parsedOptions,
    ),
  );

  toast.open();

  return toast;
}

const createMethod = (type: TopTipsType) => (
  options: string | TopTipsOptions,
) => TopTips(extend({ type }, parseOptions(options)));

TopTips.success = createMethod('success');
TopTips.fail = createMethod('fail');

TopTips.clear = (all?: boolean) => {
  if (queue.length) {
    if (all) {
      queue.forEach((toast) => {
        toast.clear();
      });
      queue = [];
    } else if (!allowMultiple) {
      queue[0].clear();
    } else {
      queue.shift()!.clear();
    }
  }
};

function setDefaultOptions(options: TopTipsOptions): void;
function setDefaultOptions(type: TopTipsType, options: TopTipsOptions): void;
function setDefaultOptions(type: TopTipsType | TopTipsOptions, options?: any) {
  if (typeof type === 'string') {
    defaultOptionsMap[type] = options;
  } else {
    extend(currentOptions, type);
  }
}

TopTips.setDefaultOptions = setDefaultOptions;

TopTips.resetDefaultOptions = (type?: TopTipsType) => {
  if (typeof type === 'string') {
    defaultOptionsMap[type] = null;
  } else {
    currentOptions = extend({}, defaultOptions);
    defaultOptionsMap = {};
  }
};

TopTips.allowMultiple = (value = true) => {
  allowMultiple = value;
};

export { TopTips };
