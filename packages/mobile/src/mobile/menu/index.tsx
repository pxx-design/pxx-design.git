import InternalMenu from './Menu';
import Item from './Item';

type InternalMenuType = typeof InternalMenu;

interface MenuInternal extends InternalMenuType {
  Item: typeof Item;
}

const Menu = InternalMenu as MenuInternal;


Menu.Item = Item;

export default Menu;
