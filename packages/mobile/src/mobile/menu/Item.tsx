import React from 'react';
import classNames from "classnames";
import {useNamespace} from "@parallel-line/common/es/hooks";

interface ItemProps {
  isActive?: boolean;
  className?: string;
  onClick?: () => void;
  children?: React.ReactNode;
}

const Item = (props: ItemProps) => {
  const {className, children, isActive, onClick} = props;
  const [cls] = useNamespace('menu-item');
  return (
    <div className={classNames(
      cls,
      {[`${cls}-active`]: isActive},
      className
    )} onClick={onClick}>
      {children}
    </div>
  );
};

export default Item;
