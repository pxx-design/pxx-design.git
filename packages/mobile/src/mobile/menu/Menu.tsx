import React, {Children, cloneElement} from 'react';
import classNames from 'classnames';
import {useControllableValue} from 'ahooks';
import {useNamespace} from "@parallel-line/common/es/hooks";

interface MenuProps {
  className?: string;
  children?: React.ReactNode;
  value?: string;
  defaultValue?: string;
  onChange?: (value: string) => void;
}

const Menu = (props: MenuProps) => {
  const {
    children,
    className,
  } = props;
  const [cls] = useNamespace('menu');
  const [value, setValue] = useControllableValue(props);
  const handleClick = (key: string) => {
    setValue(key);
  }
  return (
    <div className={classNames(cls, className)}>
      {
        Children.map(children, ((child: any) => {
          return cloneElement(child, {
            isActive: value === child.key,
            onClick: () => handleClick(child.key)
          })
        }))
      }
    </div>
  );
};

export default Menu;
