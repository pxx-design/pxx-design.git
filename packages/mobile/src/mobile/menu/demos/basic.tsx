import React from 'react';
import {Menu} from '@parallel-line/mobile';

const Basic = () => {
  return (
    <div>
      <Menu>
        <Menu.Item key="1">
          菜单1
        </Menu.Item>
      </Menu>
      <Menu value={"2"}>
        <Menu.Item key="1">
          菜单1
        </Menu.Item>
        <Menu.Item key="2">
          菜单2
        </Menu.Item>
        <Menu.Item key="3">
          菜单3
        </Menu.Item>
      </Menu>
      <Menu defaultValue={'2'}>
        <Menu.Item key="1">
          菜单1菜单1
        </Menu.Item>
        <Menu.Item key="2">
          菜单2菜单2
        </Menu.Item>
        <Menu.Item key="3">
          菜单3菜单3
        </Menu.Item>
      </Menu>
      <Menu>
        <Menu.Item key="1">
          菜单1
        </Menu.Item>
        <Menu.Item key="2">
          菜单2
        </Menu.Item>
        <Menu.Item key="3">
          菜单3
        </Menu.Item>
        <Menu.Item key="4">
          菜单4
        </Menu.Item>
        <Menu.Item key="5">
          菜单5
        </Menu.Item>
        <Menu.Item key="6">
          菜单6
        </Menu.Item>
        <Menu.Item key="7">
          菜单7
        </Menu.Item>
      </Menu>
    </div>
  );
};

export default Basic;
