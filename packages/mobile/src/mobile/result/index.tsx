import React, { FC, ReactNode } from 'react';
import {
  CheckCircleFill,
  CloseCircleFill,
  InfoCircleFill,
  TimeFill,
  WarningCircleFill,
} from '@parallel-line/icons';
import {
  NativeProps,
  withNativeProps,
} from '@parallel-line/common/es/common/util/native-props';
import { useNamespace } from '@parallel-line/common/es/hooks';
import Button from '../button';

const iconRecord = {
  success: CheckCircleFill,
  error: CloseCircleFill,
  info: InfoCircleFill,
  waiting: TimeFill,
  warning: WarningCircleFill,
};

export type ResultProps = {
  status: 'success' | 'error' | 'info' | 'waiting' | 'warning';
  title: string;
  description?: string;
  subTitle?: string;
  okText?: string;
  icon?: ReactNode;
  onOk?: () => void;
} & NativeProps;

const Result: FC<ResultProps> = (props) => {
  const { status, title, description, subTitle, onOk, okText, icon } = props;
  const [, bem] = useNamespace('result');
  if (!status) return null;
  const resultIcon = icon || React.createElement(iconRecord[status]);

  return withNativeProps(
    props,
    <div className={bem({ [status]: true })}>
      <div>
        <div className={bem('icon')}>{resultIcon}</div>
        <div className={bem('title')}>{title}</div>
        {subTitle ? <div className={bem('subTitle')}>{subTitle}</div> : null}
        {description ? (
          <div className={bem('description')}>{description}</div>
        ) : null}
      </div>
      {onOk ? (
        <div>
          <Button className={bem('button')} onClick={onOk} color="gray">
            {okText || '知道了'}
          </Button>
        </div>
      ) : null}
    </div>,
  );
};

export default Result;
