import React from 'react';
import {Button, ActionSheet} from '@parallel-line/mobile';
import {render} from '@parallel-line/utils';

const Basic = () => {
  const handleClick = () => {
    render(ActionSheet, {
      title: '请选择',
      options: [
        {key: "0", value: "已报价", code: null, children: []},
        {key: "10", value: "待定标", code: null, children: []},
        {key: "20", value: "已中标", code: null, children: []},
        {key: "30", value: "未中标", code: null, children: []},
        {key: "99", value: "已失效", code: null, children: []}
      ]
    }).open()
  }
  return (
    <Button type="primary" onClick={handleClick}>点击触发</Button>
  );
};

export default Basic;
