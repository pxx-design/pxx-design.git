import React, { forwardRef } from 'react';
import { Popup } from '../popup';
import classNames from 'classnames';
import { useNamespace } from '@parallel-line/common/es/common/hooks';

export interface ActionSheetProps {
  title?: string;
  round?: boolean;
  visible?: boolean;
  cancelText?: string;
  options?: any[];
  fieldNames?: {
    label: string;
    value: string;
  };
  onClose?: () => void;
  onChange?: (value: any, option: any) => void;
  afterClose?: () => void;
}

const ActionSheet = forwardRef<unknown, ActionSheetProps>((props, ref) => {
  const {
    round = true,
    options,
    onClose,
    onChange,
    afterClose,
    title = '请选择',
    visible = false,
    cancelText = '取消',
    fieldNames = { label: 'value', value: 'key' },
  } = props;
  const [cls, bem] = useNamespace('action-sheet');
  const handleClick = (item: any) => {
    onChange?.(item[fieldNames.value], item);
    onClose?.();
  };
  return (
    <Popup
      ref={ref}
      visible={visible}
      position="bottom"
      round={round}
      afterClose={afterClose}
    >
      <div className={classNames(cls)}>
        <div className={bem('title')}>{title}</div>
        {options?.map((item) => (
          <div onClick={() => handleClick(item)} className={bem('item')}>
            {item[fieldNames.label]}
          </div>
        ))}
        <div className={bem('cancelText')} onClick={onClose}>
          {cancelText}
        </div>
      </div>
    </Popup>
  );
});

export default ActionSheet;
