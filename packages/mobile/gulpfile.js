var gulp = require('gulp');
var del = require('del');
var less = require('gulp-less');
var LessNpmImport = require('less-plugin-npm-import');

gulp.task('clean', () => del(['dist/**/*']));

gulp.task('less', () =>
  gulp
    .src('src/mobile/**/style/index.less')
    .pipe(
      less({
        plugins: [new LessNpmImport({ prefix: '~' })],
        javascriptEnabled: true,
        // modifyVars: theme,
      }),
    )
    .pipe(gulp.dest('lib/mobile/'))
    .pipe(gulp.dest('es/mobile/')),
);

gulp.task('less_style', () =>
  gulp
    .src('src/mobile/style/*.less')
    .pipe(
      less({
        plugins: [new LessNpmImport({ prefix: '~' })],
        javascriptEnabled: true,
        // modifyVars: theme,
      }),
    )
    .pipe(gulp.dest('dist/')),
);

gulp.task('default', gulp.series('clean', 'less', 'less_style'));
