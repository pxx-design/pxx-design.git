export default {
  esm: {
    type: 'babel',
    importLibToEs: true,
  },
  cjs: 'babel',
  extractCSS: true,
  extraBabelPlugins: [
    [
      'import',
      {
        libraryName: '@parallel-line/common',
        libraryDirectory: 'es/common',
        style: true,
      },
      'pxx',
    ],
  ],
  target: 'browser',
};
