# @parallel-line/utils

> 平行线前端常用工具库。

## 📦 安装

```bash
npm install @parallel-line/utils --save
```

```bash
yarn add @parallel-line/utils
```

## 更多

[使用文档](https://supermoonlmy.gitee.io/parallel-line/)
