import moment from 'moment';
import React from 'react';
import { Text } from '@parallel-line/components';
import { toTime } from '@parallel-line/utils';

const formatMoment = () => {
  return (
    <div style={{ width: 400 }}>
      <Text fs={16}>输入的待转化内容是当前的moment对象: moment()</Text>
      <Text mt={20} fs={16}>
        结果：{JSON.stringify(toTime(moment()))}
      </Text>
    </div>
  );
};

export default formatMoment;
