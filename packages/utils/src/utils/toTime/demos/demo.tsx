import React, {useState} from 'react';
import {Input, Text} from '@parallel-line/components';
import {toTime} from '@parallel-line/utils';

const demo = () => {
  const [value, setValue] = useState<string>(
    '今天是2020年10月8日，又是5点40分',
  );
  const handleChange = (value: any) => setValue(value);
  console.log(toTime('2021-11-12T13:58:24', {format: 'YYYY/MM/DD HH:mm:ss'}))
  return (
    <div style={{width: 400}}>
      <Input value={value} onChange={handleChange}/>
      <Text mt={20} fs={16}>
        结果：{JSON.stringify(toTime(value))}
      </Text>
    </div>
  );
};

export default demo;
