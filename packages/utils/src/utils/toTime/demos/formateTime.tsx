import React, { useState } from 'react';
import { Input, Text } from '@parallel-line/components';
import { toTime } from '@parallel-line/utils';

const formateMoment = () => {
  const [value, setValue] = useState<string>(
    '高考会在在6月8日下午17点30分00秒准时结束',
  );
  const [curFormat, setCurFormat] = useState<string>('MM-DD HH:mm:ss');
  const [format, setFormat] = useState<string>('MM-DD HH:mm');
  const [errorResult, setErrorResult] = useState<string>('---');
  const handleValueChange = (value: any) => setValue(value);
  const handleCurFormatChange = (value: any) => setCurFormat(value);
  const handleFormatChange = (value: any) => setFormat(value);
  const handleErrorResult = (value: any) => setErrorResult(value);
  return (
    <div style={{ width: 400 }}>
      待转化字串: <Input value={value} onChange={handleValueChange} />
      <div style={{ marginBottom: '16px' }} />
      当前的格式:
      <Input value={curFormat} onChange={handleCurFormatChange} />
      <div style={{ marginBottom: '16px' }} />
      输出的格式:
      <Input value={format} onChange={handleFormatChange} />
      <div style={{ marginBottom: '16px' }} />
      输入异常的返回值:
      <Input value={errorResult} onChange={handleErrorResult} />
      <Text mt={20} fs={16}>
        结果：
        {JSON.stringify(toTime(value, { format, curFormat, errorResult }))}
      </Text>
    </div>
  );
};

export default formateMoment;
