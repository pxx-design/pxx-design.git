import moment from 'moment';
import { isEmpty } from '../isEmpty';
import { isArray, isString, words } from 'lodash-es';

export interface PayloadType<T = any> {
  /** 是否返回ISO数据格式，2020-08-01T23:59:59 */
  isISO?: boolean;
  /** 要返回的格式 */
  format?: string;
  /** 是否返回Moment对象 */
  isMoment?: boolean;
  /** 当前格式 */
  curFormat?: string;
  /** 如果错误了，返回的字符 */
  errorResult?: T;
  /** 如果要隐藏时间为23:59:59，设置为true */
  hideTime?: boolean;
}

/**
 * 时间格式
 *
 * 任意带有时间信息的字符，转换为可用的时间格式
 *
 * @param time
 * @param payload
 * @returns
 */
function toTime<R = any, T = any>(
  time: any,
  payload?: PayloadType<T> | undefined,
): T | R;
function toTime<T = any>(time: any, payload?: PayloadType<T>) {
  const {
    isISO = false,
    isMoment = false,
    format = 'YYYY/MM/DD HH:mm',
    curFormat = 'YYYY-MM-DD HH:mm:ss',
    errorResult,
    hideTime = false,
  } = payload || {};
  let _moment: any;

  if (isArray(time)) return time?.map((t: any) => toTime(t, payload));

  if (moment.isMoment(time)) _moment = time;

  const _time = words(time, /[^\D]+/g).join(' ');

  if (isString(time)) _moment = moment(_time, curFormat);

  if (isEmpty(_moment) || !_moment?.isValid()) return errorResult;

  if (isMoment) return _moment;

  if (isISO) return _moment.format('YYYY-MM-DDTHH:mm:ss');

  if (
    hideTime &&
    _moment.get('hour') === 23 &&
    _moment.get('minute') === 59 &&
    _moment.get('second') === 59
  ) {
    return _moment.format('YYYY/MM/DD');
  }
  return _moment.format(format);
}

export { toTime };
