---
title: toTime
group:
  title: toTime
---

## 时间格式

> 任意带有时间信息的字符，转换为可用的时间格式

```tsx | pure
import { toTime } from '@parallel-line/utils';

toTime('今天是2020年10月8日，又是5点40分。'); //2020/10/08 05:40:00

toTime('今天是10月8日，又是5点40分。', {
  isISO:true,//是否返回ISO数据格式，2020-08-01T23:59:59
  isMoment:true,//是否返回Moment对象
  curFormat: 'MM-DD',//当前格式
  format: 'MM-DD',//要返回的格式
  errorResult:'--',//如果错误了，返回的字符
  hideTime:true//如果要隐藏时间为23:59:59，设置为true
}); //10-08
```

## 演示

#### 基础使用:
<code src="./demos/demo.tsx" />

#### 完整示例:
<code src="./demos/formateTime.tsx" />

Tips: 输入的待转化内容也可以是moment对象
<code src="./demos/formateMoment.tsx" />
