import { calc } from '../calc';
/**
 * 数组对象累积处理
 *
 * @param columns // 要处理的对象属性集合
 * @param list // 要处理的数组
 * @return // {...columns} 计算后的值，对象类型
 */
const accumulateCalc = (columns: string[]) => (arr: any[]) =>
  arr?.reduce(
    (a, b) =>
      columns.reduce(
        (c, d) => ({ ...c, [d]: calc(`${a[d] || 0} + ${b[d] || 0}`) }),
        {},
      ),
    {},
  );

export { accumulateCalc };
