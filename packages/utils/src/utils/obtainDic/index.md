---
title: 获取字典 obtainDic
group:
  title: 获取字典 obtainDic
---
# 获取字典 obtainDic
### 基础使用
```tsx | pure
import { obtainDic, DicInfo } from '@parallel-line/utils';

obtainDic({
  dicKey:'字典key', 
  dicItemKey:'字典字项key，类型可以是数组', 
  errorResult:'发生错误希望返回结果' as DicInfo
})
```
### 获取字典
```tsx | pure
import { obtainDic } from '@parallel-line/utils';

obtainDic({
  dicKey:'bidding.bidding.depositStatus',
})

结果：[
  {key: "1", value: "冻结", code: null, children: []},
  {key: "2", value: "可赎回", code: null, children: []},
  {key: "3", value: "已赎回", code: null, children: []},
  {key: "4", value: "扣款", code: null, children: []}
]
```
### 获取字典中的某一项
```tsx | pure
import { obtainDic } from '@parallel-line/utils';

obtainDic({
  dicKey:'bidding.bidding.depositStatus',
  dicItemKey:"1"
})

结果：{key: "1", value: "冻结", code: null, children: []}
```
### 获取字典中的某多项
⚠️ dicItemKey参数入参什么类型出参数什么类型
```tsx | pure
import { obtainDic } from '@parallel-line/utils';

obtainDic({
  dicKey:'bidding.bidding.depositStatus',
  dicItemKey:["1","2","3"]
})

结果：[
  {key: "1", value: "冻结", code: null, children: []},
  {key: "2", value: "可赎回", code: null, children: []},
  {key: "3", value: "已赎回", code: null, children: []},
]
```
### 获取字典中的某项，发生错误
```tsx | pure
import { obtainDic, DicInfo } from '@parallel-line/utils';

obtainDic({
  dicKey:'bidding.bidding.depositStatus',
  dicItemKey:"0",
  errorResult: { value: '--', key: '--' } as DicInfo
})

结果：{ key: '--', value: '--' }
```
