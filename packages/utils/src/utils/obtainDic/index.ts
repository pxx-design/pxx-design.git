import { global } from '../global';
import { get } from '../get';
import { castArray, isUndefined, find, isArray } from 'lodash-es';
import { TagInfo } from '../getDic';

export interface DicInfo extends TagInfo {}
type ItemKey = string | number | string[] | number[] | undefined;

interface obtainDicParam<T extends ItemKey, D extends string | undefined> {
  /** 字典的key */
  dicKey?: D;
  /** 字典子项的key */
  dicItemKey?: T;
  /** 发生错误想要返回的结果 */
  errorResult?: any;
}

/**
 * 获取字典
 * @param dicKey 字典的key
 * @param dicItemKey 字典子项的key
 * @param errorResult 发生错误想要返回的结果
 */
function obtainDic<
  T extends ItemKey = undefined,
  D extends string | undefined = undefined
>(
  params: obtainDicParam<T, D>,
): D extends string
  ? T extends string
    ? DicInfo
    : T extends number
    ? DicInfo
    : T extends string[]
    ? Array<DicInfo>
    : T extends number[]
    ? Array<DicInfo>
    : T extends undefined
    ? Array<DicInfo>
    : any
  : any;
function obtainDic({
  dicKey,
  dicItemKey,
  errorResult,
}: obtainDicParam<ItemKey, string>): any {
  if (!dicKey) return errorResult;
  const dicItem = get(global.dictionary, dicKey);
  if (isUndefined(dicItemKey) || isUndefined(dicItem))
    return dicItem || errorResult;
  const result = castArray(dicItemKey as ItemKey).map((key) => {
    return find(dicItem, { key: `${key}` }) || errorResult;
  });
  return isArray(dicItemKey) ? result : result[0];
}

export { obtainDic };
