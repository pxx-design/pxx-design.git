import { forEach } from 'lodash-es';
import {
  extend,
  Context,
  RequestInterceptor,
  ResponseInterceptor,
} from 'umi-request';
import { sleep } from '../sleep';
import { global } from '../global';
import { pxxStorage } from '../storage';
import {
  getAccessToken,
  objectToQueryString,
  refreshTokenRequest,
} from './utils';
import { PostData, dataFormat } from './helpers';

// 扩展或收窄 `umi-request` 的 一下声明
declare module 'umi-request' {
  interface RequestOptionsInit {
    /** 是否需要 `AccessToken`， 默认为true */
    needAccessToken?: boolean;
    /** 延迟一段时间后再请求 */
    delayed?: number;
    /** 隐藏错误提示 */
    hideError?: boolean;
  }

  interface IRequestResponse<T = any> extends Record<string, any> {
    data: T;
    response?: Response;
    code?: number;
    success?: boolean;
    msg?: string;
  }

  interface RequestMethod<R = false> {
    <T = any>(url: string, options?: RequestOptionsInit): R extends true
      ? Promise<IRequestResponse<T>>
      : Promise<T>;
  }
}

export type OnionMiddleware = (
  ctx: Context,
  next: () => void | Promise<void>,
) => void;

const codeMessage: Record<string, string> = {
  429: '发送操作频繁，请稍后再试',
};
const codeUrl: Record<string, string> = {
  '30001': '/invite/not-applied.html',
};

export const request = extend({
  headers: {
    Accept: 'application/json',
  },
  getResponse: true,
});

const requestHandler: RequestInterceptor = (url, options) => {
  const { data, needAccessToken = true } = options;

  if (needAccessToken) {
    options.headers = {
      Authorization: getAccessToken(),
      ...options.headers,
    };
  }

  if (data && data instanceof PostData) {
    const { normalData, fileData } = data.getParsedPostData();
    const hasFileData = Object.keys(fileData).length > 0;
    if (hasFileData) {
      const formData = new FormData();
      forEach(normalData, (value, key) => {
        formData.set(key as any, value);
      });
      forEach(fileData, (value, key) => {
        formData.set(key as any, value);
      });
      options.data = formData;
    } else {
      options.data = objectToQueryString(normalData);
      options.headers = {
        ...options.headers,
        'Content-Type': 'application/x-www-form-urlencoded',
      };
    }
  }

  return { url, options };
};
const responseHandler: ResponseInterceptor = async (resp: any, options) => {
  await sleep(options.delayed);
  const response: Response = resp?.response ?? resp;
  if (
    response.url.includes('/yuque') ||
    response.url.includes('/bmapi') ||
    response.url.includes('/tsapi') ||
    response.url.includes('/restapi') ||
    response.url.includes('/oauth/init') ||
    response.url.includes('/zip')
  ) {
    return response;
  }
  if (response && codeMessage[response.status]) {
    global.message.error(codeMessage[response.status]);
    throw { response };
  }
  if ([401, 402, 403].includes(response?.status)) {
    const { isLogin } = global.PXX_USER_STATE;
    if (isLogin) {
      global.message.warn('您暂无权限访问。');
      throw { response };
    } else {
      refreshTokenRequest();
      return { response };
    }
  }
  const accessToken = response.headers.get('access-token');
  if (accessToken) pxxStorage.setItem('pxx_accessToken', accessToken);
  const data = await response.clone().json();

  if ([10089, 10090].includes(data.code)) {
    // TODO 针对协议管理新建协议校验手机号的特殊处理
    return {
      ...data,
      response,
      data: {
        isError: true,
        msg: data.msg,
      },
    };
  }
  if (data && data.code === 200 && data.success) {
    const { pathname } = new URL(response.url);
    const res = await dataFormat(
      data,
      `${options.method} ${pathname}`,
      options,
    );
    return { ...res, response };
  }

  // TODO 统一codeUrl在多端的应用
  if (global.PXX_ENV.isH5 && data?.code && codeUrl[data.code]) {
    global.history?.replace(codeUrl[data.code]);
    return { ...data, response };
  }
  if (
    [401, 402, 403].includes(data?.code) &&
    !response.url.includes('/oauth/refresh')
  ) {
    refreshTokenRequest();
    return { ...data, response };
  }
  if (!options.hideError && !data?.success && data?.msg) {
    global.message.error(data.msg);
  }
  throw { data, response, options };
};
request.interceptors.request.use(requestHandler, { core: true });
request.interceptors.response.use(responseHandler, { core: true });
