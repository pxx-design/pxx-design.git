import { request as inlineRequest } from './request';
import { PostData, FileData } from './helpers';
import { uploadFile, uploadOCR } from './services';

type RequestType = typeof inlineRequest;
interface IHandleDataFn {
  (data: any, options?: any): any | Promise<any>;
}

interface RequestInterface extends RequestType {
  PostData: typeof PostData;
  FileData: typeof FileData;
  uploadOCR: typeof uploadOCR;
  uploadFile: typeof uploadFile;
  middlewarData: Record<string, IHandleDataFn>;
}

const request = inlineRequest as RequestInterface;
request.PostData = PostData;
request.FileData = FileData;
request.uploadOCR = uploadOCR;
request.uploadFile = uploadFile;
request.middlewarData = {};

export { request };
