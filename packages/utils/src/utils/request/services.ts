import moment from 'moment';
import { request } from './request';
import { global } from '../global';
import { pxxStorage } from '../storage';
import { PostData, FileData } from './helpers';

interface PxxFile extends File {
  id?: string;
  uid?: string;
}

/**
 * 更新pxx_expiresIn
 * @param expiresIn
 */
export const updateExpiresIn = (expiresIn: number) => {
  const curTime = moment().format('x');
  const time = +curTime + expiresIn * 1000;
  pxxStorage.setItem('pxx_expiresIn', `${time}`);
};

/** 重置 `token` */
export async function refreshToken() {
  const res = await request.post('/oauth/refresh');
  if (!res?.data?.expiresIn) return undefined;
  updateExpiresIn(res?.data?.expiresIn);
  return res;
}

interface UploadFileData {
  /** 业务ID， 默认为 企业ID ?? 用户ID */
  businessId?: string;
  /** 文件来源 */
  fileSource?: number;
  /** 服务名称 */
  serviceName?: string;
  /** 模块名称 */
  moduleName?: string;
  [k: string]: any;
}

export async function uploadFile(
  file: PxxFile,
  data: UploadFileData = {},
  url = '/host/uploadFile',
) {
  return request.post<string>(url, {
    data: new PostData({
      businessId: global?.companyInfo?.id ?? pxxStorage.getItem('pxx_userId'),
      ...data,
      file: new FileData(file),
    }),
  });
}
export async function uploadOCR<T = any>(
  file: PxxFile,
  data: UploadFileData = {},
) {
  return (uploadFile(file, data, '/host/ocr') as unknown) as T;
}
