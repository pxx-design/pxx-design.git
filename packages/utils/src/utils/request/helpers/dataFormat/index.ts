import { request } from '../..';

export const dataFormat = async (data: any, url: string, options: any) => {
  const key: any = Object.keys(request.middlewarData).find((urlKey) =>
    new RegExp(urlKey).test(url),
  );
  if (request.middlewarData[key]) {
    const newData = await request.middlewarData[key](data, options);
    return {
      ...data,
      data: newData,
    };
  }
  return data;
};
