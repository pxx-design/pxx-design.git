import * as rawTaro from '@tarojs/taro';
import { isObject, forEach, isArray, isDate } from 'lodash-es';

export function getTaro(): typeof rawTaro {
  // @ts-ignore
  const Taro = require('@tarojs/taro') as any;

  return Taro?.default || Taro;
}

/**
 * Update an Error with the specified config, error code, and response.
 *
 * @param error The error to update.
 * @param config The config.
 * @param code The error code (for example, 'ECONNABORTED').
 * @param request The request.
 * @param response The response.
 * @returns The error.
 */
export function enhanceError(
  error: any,
  config: any,
  code: string | null,
  request: any,
  response?: any,
): Error {
  error.config = config;
  if (code) {
    error.code = code;
  }

  error.request = request;
  error.response = response;
  error.isAxiosError = true;

  error.toJSON = function toJSON() {
    return {
      // Standard
      message: this.message,
      name: this.name,
      // Microsoft
      description: this.description,
      number: this.number,
      // Mozilla
      fileName: this.fileName,
      lineNumber: this.lineNumber,
      columnNumber: this.columnNumber,
      stack: this.stack,
      // Axios
      config: this.config,
      code: this.code,
      status:
        this.response && this.response.status ? this.response.status : null,
    };
  };
  return error;
}

/**
 * Create an Error with the specified message, config, error code, request and response.
 *
 * @param message The error message.
 * @param config The config.
 * @param code The error code (for example, 'ECONNABORTED').
 * @param request The request.
 * @param response The response.
 * @returns The created error.
 */
export function createError(
  message: string,
  config: any,
  code: string | null,
  request: any,
  response?: any,
): Error {
  var error = new Error(message);
  return enhanceError(error, config, code, request, response);
}

export function settle(resolve: Function, reject: Function, response: any) {
  var validateStatus = response.config.validateStatus;
  if (!response.status || !validateStatus || validateStatus(response.status)) {
    resolve(response);
  } else {
    reject(
      createError(
        'Request failed with status code ' + response.status,
        response.config,
        null,
        response.request,
        response,
      ),
    );
  }
}

function encode(val: string) {
  return encodeURIComponent(val)
    .replace(/%3A/gi, ':')
    .replace(/%24/g, '$')
    .replace(/%2C/gi, ',')
    .replace(/%20/g, '+')
    .replace(/%5B/gi, '[')
    .replace(/%5D/gi, ']');
}

function isURLSearchParams(val: any) {
  return (
    typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams
  );
}

export function buildURL(
  url: string,
  params: any,
  paramsSerializer?: Function,
) {
  /*eslint no-param-reassign:0*/
  if (!params) {
    return url;
  }

  var serializedParams;
  if (paramsSerializer) {
    serializedParams = paramsSerializer(params);
  } else if (isURLSearchParams(params)) {
    serializedParams = params.toString();
  } else {
    var parts: any[] = [];

    forEach(params, function serialize(val, key) {
      if (val === null || typeof val === 'undefined') {
        return;
      }

      if (isArray(val)) {
        key = key + '[]';
      } else {
        val = [val];
      }

      forEach(val, function parseValue(v) {
        if (isDate(v)) {
          v = v.toISOString();
        } else if (isObject(v)) {
          v = JSON.stringify(v);
        }
        parts.push(encode(key) + '=' + encode(v));
      });
    });

    serializedParams = parts.join('&');
  }

  if (serializedParams) {
    var hashmarkIndex = url.indexOf('#');
    if (hashmarkIndex !== -1) {
      url = url.slice(0, hashmarkIndex);
    }

    url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
  }

  return url;
}

function isAbsoluteURL(url: string) {
  // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
  // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
  // by any combination of letters, digits, plus, period, or hyphen.
  return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
}

function combineURLs(baseURL: string, relativeURL: string) {
  return relativeURL
    ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '')
    : baseURL;
}

export function buildFullPath(baseURL: string, requestedURL: string) {
  if (baseURL && !isAbsoluteURL(requestedURL)) {
    return combineURLs(baseURL, requestedURL);
  }
  return requestedURL;
}
