---
title: request 网络请求库
group:
  title: request 网络请求库
---

# request 网络请求库

网络请求库，统一多端的请求处理，并提供诸如缓存, 超时, 字符编码处理, 错误处理等常用功能。

## 使用前设置


> global.ts 示例，注意，不同端引入的库不一致哦～

```jsx | pure
import { history } from 'umi';
import { message } from 'antd';
import { global } from '@parallel-line/utils';
global.message = {
  info(msg) {
    message.info(msg);
  },
  success(msg) {
    message.success(msg);
  },
  error(msg) {
    message.error(msg);
  },
  warn(msg) {
    message.warn(msg);
  },
};

global.history = {
  push: history.push.bind(history),
  replace: history.replace.bind(history),
  reload() {
    window.location.reload();
  },
}
```

## 演示

### 基本使用

```jsx | pure
import React from 'react';
import { Button, Space, Input } from 'antd';
import { request } from '@parallel-line/utils';

export default () => {
  const [res, setRes] = React.useState('');
  const handleClick = async () => {
    setRes('loading...');
    const res = await request.post('/web/home/biz/todo', {
      data: { query: { entireRequest: 0 } },
    });
    setRes(JSON.stringify(res, null, '\t'));
  };
  return (
    <Space direction="vertical" style={{ width: 480 }}>
      <Button onClick={handleClick}>发起请求</Button>
      <div>
        Output： <Input.TextArea value={res} disabled autoSize />
      </div>
    </Space>
  );
};
```

### 文件上传

```jsx | pure
import React from 'react';
import { Upload, Button } from 'antd';
import { request, pxxStorage } from '@parallel-line/utils';

export default () => {
  return (
    <Upload
      onChange={({ file, fileList }) => {
        console.log(file, fileList);
      }}
      customRequest={({ file, data, onSuccess, onError }) => {
        request
          .uploadFile(file, {
            ...data,
            businessId: pxxStorage.getItem('pxx_userId')!,
          })
          .then((res) => {
            console.log(res);
            onSuccess?.(res, null);
          })
          .catch((error) => {
            onError?.(null, error);
          });
      }}
    >
      <Button>上传文件</Button>
    </Upload>
  );
};
```

### 自定义文件上传

```jsx | pure
import React from 'react';
import { Upload, Button } from 'antd';
import { request, pxxStorage } from '@parallel-line/utils';

const { PostData, FileData } = request;

export default () => {
  return (
    <Upload
      onChange={({ file, fileList }) => {
        console.log(file, fileList);
      }}
      customRequest={({ file, data, onSuccess, onError }) => {
        request
          .post(`/manager/version/test/person/import/${appVersionId}`, {
            data: new PostData({
              multipartFile: new FileData(multipartFile),
            }),
          })
          .then((res) => {
            console.log(res);
            onSuccess?.(res, null);
          })
          .catch((error) => {
            onError?.(null, error);
          });
      }}
    >
      <Button>上传文件</Button>
    </Upload>
  );
};
```
