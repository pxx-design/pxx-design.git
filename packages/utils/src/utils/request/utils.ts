import moment from 'moment';
import { forEach, isArray, isNil, cloneDeep } from 'lodash-es';
import { global } from '../global';
import { pxxStorage } from '../storage';
import { obtainDic } from '../obtainDic';
import { refreshToken } from './services';

/**
 * 配置request请求时的默认参数
 */
export const getAccessToken = () => pxxStorage.getItem('pxx_accessToken') ?? '';

export let isRefreshing = false;

/** 刷新token */
export async function refreshTokenRequest() {
  try {
    isRefreshing = true;
    await refreshToken();
    isRefreshing = false;
  } catch (error) {
    console.log('失败返回登录页');
    isRefreshing = false;
    pxxStorage.clear();
    if (global.history) {
      global.history.replace('/login');
      global.history.reload?.(); // window.location.reload();
    }
  }
}

export function objectToQueryString(obj: Record<string, any>) {
  const result: string[] = [];
  forEach(obj, (value, key) => {
    result.push(`${encodeURIComponent(key)}=${encodeURIComponent(value)}`);
  });
  return result.join('&');
}

export const arrChangeObj = (arr: Record<string, unknown>[], key: string) => {
  if (!arr) return {};
  const newArr = cloneDeep(arr);
  return newArr.reduce((prev: any, cur: any) => {
    prev[`${cur[key]}`] = cur;
    return prev;
  }, {});
};

/**
 * 时间转回moment格式
 */
export const timeToMoment = (time: string | string[], dateFormat?: string) => {
  if (isArray(time)) {
    return (time as any[]).map((t: string) => {
      return moment(t, dateFormat);
    });
  }
  return moment(time, dateFormat);
};

/**
 * 根据递归获取省市区信息
 * @deprecated
 * @param code 城市编码
 * @param list 省市区列表
 * @param result
 * @return { cityIds: [], cityNames: [] }
 */
export const getCitysInfosByLoop = (
  code: number | string,
  list: any[] = global.countryTree,
  result: any = { cityIds: [], cityNames: [] },
): { cityIds: string[]; cityNames: string[] } => {
  const adcode = String(code);
  for (let i = 0; i < list.length; i += 1) {
    if (adcode === list[i].adcode) {
      result.cityIds.unshift(list[i].adcode);
      result.cityNames.unshift(list[i].name);
      break;
    } else {
      const childrenList = list[i].districts;
      if (childrenList && childrenList.length > 0) {
        const value = getCitysInfosByLoop(adcode, childrenList, result);
        if (value.cityIds.length > 0) {
          result.cityIds.unshift(list[i].adcode);
          result.cityNames.unshift(list[i].name);
          break;
        }
      }
    }
  }
  return result;
};

/**
 * @deprecated
 */
export const getDictionaryValueObjByKey = (
  dicKey: string,
  key: string | number,
): any => {
  try {
    return (
      global.dictionary[dicKey]?.find(
        (item: any) => String(item.key) === String(key),
      ) || {}
    );
  } catch (err) {
    console.log(err);
    return {};
  }
};

/**
 * @deprecated
 * @param dicKey 字典字段key
 * @param key
 * @param connectStr 连接符
 * @return 字典key对应的值
 */
export const getDictionaryValueByKey = (
  dicKey: string,
  key: string | number | string[] | number[],
  connectStr: string = '，',
) => {
  try {
    const keys = isArray(key) ? key : [key];
    const values: any[] = keys.reduce(
      (prev: string[], item: string | number) => {
        const val = getDictionaryValueObjByKey(dicKey, item)?.value;
        if (val) prev.push(val);
        return prev;
      },
      [],
    );
    return values.join(connectStr);
  } catch (err) {
    console.log(err);
    return '--';
  }
};

/**
 * 获取车型
 * @deprecated
 * @param codes 车型codes
 * @returns
 */
export const getVehicleTypes = (codes: string) => {
  if (isNil(codes)) return '--';
  const _types = obtainDic({
    dicKey: 'entire.vehicle_type_code',
    dicItemKey: codes && codes.split(','),
  }) as any;
  return _types
    ? _types.map((type: Record<string, any>) => type.value).join('/')
    : '--';
};
