import { isMiniAPP } from '../reactNode';
import { taroHandler } from './helpers/taroHandler';
import { request, OnionMiddleware } from './request';

const middlewarHandler: OnionMiddleware = async (ctx, next) => {
  if (isMiniAPP()) {
    // 小程序环境自定义请求
    const response = await taroHandler({
      ...ctx.req.options,
      url: ctx.req.url,
    });
    // 将响应数据写入 ctx 中
    ctx.res = response;
  }

  await next();

  // try {
  //   ctx.res = await responseHandler(ctx.res, ctx.req.options);
  // } catch (error) {
  //   throw error;
  // }
};

request.use(middlewarHandler, { core: true });
