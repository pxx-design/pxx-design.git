import { isNumber } from 'lodash-es';

/**
 * 空数据处理
 *
 * 判断是否空数据，若是则返回 out
 * @param enter
 * @param out
 * @returns
 */
const emptyString = <T, O>(enter: T, out: O = <any>'--') => {
  try {
    if (isNumber(enter)) return enter;
    return enter || out;
  } catch (e) {
    return out;
  }
};

export { emptyString };
