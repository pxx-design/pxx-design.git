import React, { useState } from 'react';
import { Input, Text } from '@parallel-line/components';
import { emptyString } from '@parallel-line/utils';

const demo = () => {
  const [value, setValue] = useState<string>();
  const handleChange = (value: any) => setValue(value);
  return (
    <div style={{ width: 400 }}>
      <Input value={value} onChange={handleChange} />
      <Text mt={20} fs={16}>
        结果：{JSON.stringify(emptyString(value))}
      </Text>
    </div>
  );
};

export default demo;
