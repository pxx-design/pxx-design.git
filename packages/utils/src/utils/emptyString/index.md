---
title: 空数据处理
group:
  title: 空数据处理
---

# 空数据处理

```jsx | pure
import React from 'react';
import { emptyString } from '@parallel-line/utils';

emptyString(0); //0

emptyString(''); //--

emptyString(null); //--

emptyString(undefined); //--

emptyString(undefined.a, '123'); //123
```

## 演示

<code src="./demos/demo.tsx" ></code>
