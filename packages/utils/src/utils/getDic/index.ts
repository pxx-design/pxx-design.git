/* eslint-disable eqeqeq */
/* eslint-disable @typescript-eslint/unified-signatures */
import { global } from '../global';
import { get, isArray, isNil } from 'lodash-es';

interface TagInfo {
  key: string;
  value: string;
  code?: string;
  status?: 'error' | 'success' | 'default' | 'processing' | 'warning';
  color?: string;
  [k: string]: any;
}

interface GetDicFn {
  /**
   * 获取字典方法
   * @deprecated
   * @param dicKey
   */
  (dicKey: string): TagInfo[];

  /**
   * 获取字典方法
   * @deprecated
   * @param dicKey
   */
  (dicKey: string[]): TagInfo[];

  /**
   * 获取字典方法
   * @deprecated
   * @param dicKey
   * @param itemKey
   */
  (dicKey: [string], itemKey: [string]): [TagInfo];

  /**
   * 获取字典方法
   * @deprecated
   * @param dicKey
   * @param itemKey
   */
  (dicKey: [string, string], itemKey: [string, string]): [TagInfo, TagInfo];

  /**
   * 获取字典方法
   * @deprecated
   * @param dicKey
   * @param itemKey
   */
  (dicKey: [string, string, string], itemKey: [string, string, string]): [
    TagInfo,
    TagInfo,
    TagInfo,
  ];

  /**
   * 获取字典方法
   * @deprecated
   * @param dicKey
   * @param itemKey
   */
  (
    dicKey: [string, string, string, string],
    itemKey: [string, string, string, string],
  ): [TagInfo, TagInfo, TagInfo, TagInfo];

  /**
   * 获取字典方法
   * @deprecated
   * @param dicKey
   * @param itemKey
   */
  (
    dicKey: [string, string, string, string, string],
    itemKey: [string, string, string, string, string],
  ): [TagInfo, TagInfo, TagInfo, TagInfo, TagInfo];

  /**
   * 获取字典方法
   * @deprecated
   * @param dicKey
   * @param itemKey
   */
  (dicKey: string[], itemKey: string[]): TagInfo[];

  /**
   * 获取字典方法
   * @deprecated
   * @param dicKey
   * @param itemKey
   */
  (dicKey: string[], itemKey: string): TagInfo[];

  /**
   * 获取字典方法
   * @deprecated
   * @param dicKey
   * @param itemKey
   */
  (dicKey: string, itemKey: string): TagInfo;
}

const getDic: GetDicFn = (
  dicKey: string | string[],
  itemKey?: string | string[],
) => {
  if (!dicKey) throw Error('请输入字典的key');
  const getDicItem = (path: string, index: number = 0) => {
    const dicItem: TagInfo[] = get(global.dictionary, path, []);
    if (isNil(itemKey)) return dicItem;
    if (isArray(itemKey)) {
      const resItem = dicItem.reduce((prev: any[], cur: any) => {
        if (cur.key == itemKey[index]) prev.push(cur);
        return prev;
      }, []);
      return resItem[0];
    }
    return dicItem.find((t: any) => t.key == itemKey) || {};
  };
  if (isArray(dicKey)) return dicKey.map((k, i) => getDicItem(k, i));
  return getDicItem(dicKey);
};

export type { TagInfo, GetDicFn };

export { getDic };
