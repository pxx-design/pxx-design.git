---
title: mobile
mobile: true
group:
  title: render
---

# render

将任何组件以ReactDOM.render的形式渲染

## 代码演示

### 基础使用
<code src="./demos/basic.tsx"></code>

### 多层使用
<code src="./demos/multiStorey.tsx"></code>

<!-- ## hooks
<code src="./demos/hooks.tsx"></code> -->

## API

| 参数 | 说明 | 类型 | 默认值 | 
| --- | --- | --- | --- | 
| Component | 要push的组件, 该组件的Props需要具备`visible`、`onClose`和`afterClose` | React.ComponentType | - | 
| properties | 组件的Props值 | object | - | 
