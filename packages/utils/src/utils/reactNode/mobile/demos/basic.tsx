import React from 'react';
import { Space } from 'antd';
import { render } from '@parallel-line/utils';
import { Button, Popup } from '@parallel-line/mobile';

function Demo(props: any) {
  return (
    <div style={{ padding: 16 }}>
      测试
      <br />
      <Button style={{ marginTop: 40 }} onClick={props.onClose}>
        关闭
      </Button>
    </div>
  );
}

const basic = () => {
  const handlePopover = () => {
    render(Popup, {
      closeable: true,
      position: 'bottom',
      content: <Demo />,
    }).open();
  };
  return (
    <Space>
      <Button onClick={handlePopover}>打开Popover</Button>
    </Space>
  );
};

export default basic;
