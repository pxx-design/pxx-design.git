import React from 'react';
import { render } from '@parallel-line/utils';
import { Button, Popup } from '@parallel-line/mobile';

const Content = () => {
  const handleClick = () => {
    render(Popup, {
      closeable: true,
      position: 'top',
      content: <Content />,
    }).open();
  };
  return <Button onClick={handleClick}>打开内部Popover</Button>;
};

const MultiStorey = () => {
  const handleClick = () => {
    render(Popup, {
      closeable: true,
      position: 'bottom',
      content: <Content />,
    }).open();
  };
  return <Button onClick={handleClick}>打开Popover</Button>;
};

export default MultiStorey;
