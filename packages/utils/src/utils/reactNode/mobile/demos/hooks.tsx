/**
 * title: 使用 hooks 获得上下文
 * desc: 通过 Popup.usePopup 创建支持读取 context 的 contextHolder。
 */

import React from 'react';
import { Space } from 'antd';
import { Button, Popup, Popover } from '@parallel-line/mobile';

const ReachableContext = React.createContext(undefined);
const UnreachableContext = React.createContext(undefined);

const Content = ({ onClick, onClose }: any) => {
  return (
    <Space direction="vertical">
      <ReachableContext.Consumer>
        {(name) => `Reachable: ${name}!`}
      </ReachableContext.Consumer>
      <br />
      <UnreachableContext.Consumer>
        {(name) => `Unreachable: ${name}!`}
      </UnreachableContext.Consumer>
      <Button onClick={onClick}>打开内部抽屉</Button>

      <Button style={{ marginTop: 40 }} onClick={onClose}>
        关闭当前Drawer
      </Button>
    </Space>
  );
};

export default () => {
  const [popup, contextHolder] = Popup.usePopup();

  const handleClick = () => {
    popup(Popover, {
      position: 'top',
      content: <Content onClick={handleClick} />,
    });
  };

  const onClick = () => {
    popup(Popover, {
      position: 'bottom',
      content: <Content onClick={handleClick} />,
    });
  };

  return (
    <ReachableContext.Provider value="Light">
      <Button onClick={onClick}>打开抽屉</Button>
      {/* `contextHolder` should always under the context you want to access */}
      {contextHolder}

      {/* Can not access this context since `contextHolder` is not in it */}
      <UnreachableContext.Provider value="Bamboo" />
    </ReachableContext.Provider>
  );
};
