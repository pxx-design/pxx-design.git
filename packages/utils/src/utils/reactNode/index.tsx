import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { global } from '../global';

/** 根节点 */
export const CONTAINER_KEY = Symbol('RootContainer');

interface Renderer {
  (
    element:
      | React.FunctionComponentElement<any>
      | Array<React.FunctionComponentElement<any>>,
    callback?: () => void,
  ): void;
}

export interface ElementInstance {
  render: Renderer;

  destroy(): void;
}

/** 是否是小程序环境 */
export function isMiniAPP() {
  if (global.PXX_ENV.isH5) return false;
  return (
    global.PXX_ENV.isWeapp ||
    global.PXX_ENV.isSWAN ||
    global.PXX_ENV.isAlipay ||
    global.PXX_ENV.isTT ||
    global.PXX_ENV.isQQ ||
    global.PXX_ENV.isJD
  );
}

/**
 * createPortal 目前Taro容器下不支持
 */
function createPortal(
  children: React.ReactNode,
  container: Element = document.body,
  key?: string | null | undefined,
): React.ReactPortal {
  if (isMiniAPP()) return children as any;
  return ReactDOM.createPortal(children, container, key);
}

/**
 * createElement 创建一个节点并提供render和destroy
 */
function createElement(
  container: Element | DocumentFragment = document.body,
): ElementInstance {
  let instance: ElementInstance;
  if (isMiniAPP()) {
    const componentKey = Symbol();
    instance = {
      render: (element, callback) => {
        global.eventEmitter.emit({
          uniqueKey: CONTAINER_KEY,
          action: 'render',
          component: element,
          componentKey,
          callback,
        });
      },
      destroy: () => {
        global.eventEmitter.emit({
          uniqueKey: CONTAINER_KEY,
          action: 'destroy',
          componentKey,
        });
      },
    };
    return instance;
  }

  const div = document.createElement('div');
  container.appendChild(div);

  instance = {
    render: (element, callback) => ReactDOM.render(element, div, callback),
    destroy: () => {
      const unmountResult = ReactDOM.unmountComponentAtNode(div);
      if (unmountResult && div.parentNode) {
        div.parentNode.removeChild(div);
      }
    },
  };
  return instance;
}

function useCreateElement(
  container: Element | DocumentFragment = document.body,
) {
  const instance = React.useRef(createElement(container));

  return instance.current;
}

interface PopupProps {
  uniqueKey?: string;
  visible?: boolean;
  getContainer?: () => any;
  onClose?: () => Promise<any>;
}

class NodeInstance<Instance = unknown> {
  public readonly pageKey: any;
  public readonly uniqueKey: any;
  #el: ElementInstance;
  #component: any;
  #properties: any;
  #visible: boolean | undefined;
  #instance = React.createRef<Instance>();
  ConfigProvider: any;

  constructor(component: any, properties: any = {}) {
    this.#component = component;
    this.#properties = properties;
    this.#el = createElement();
    this.ConfigProvider = global.ConfigProvider || React.Fragment;
  }

  get instance() {
    return this.#instance.current;
  }

  get properties() {
    return this.#properties;
  }

  public open = () => {
    this.#visible = true;
    this.render();
    return this;
  };

  public close = () => {
    this.#visible = false;
    this.render();
    // this.destroy();
    return this;
  };

  public destroy = () => {
    this.#el.destroy();
  };

  public render() {
    const componentProps: PopupProps & Record<string, any> = {
      ref: this.#instance,
      uniqueKey: this.uniqueKey,
      visible: this.#visible,
      onClose: this.close,
      afterClose: this.destroy,
      ...this.properties,
    };
    this.#el.render(
      <this.ConfigProvider>
        {React.createElement(this.#component, componentProps)}
      </this.ConfigProvider>,
    );
  }
}

function render<Instance = unknown, P = unknown>(
  Component: React.ComponentType<P>,
  properties?: P,
): NodeInstance<Instance>;
function render(Component: any, properties?: any) {
  return new NodeInstance(Component, properties);
}

export { NodeInstance, render, createPortal, createElement, useCreateElement };
