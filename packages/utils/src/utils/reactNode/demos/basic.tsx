import React from 'react';
import { Space } from 'antd';
import { render } from '@parallel-line/utils';
import { Button, Drawer, Modal } from '@parallel-line/components';

function Demo(props: any) {
  return (
    <div style={{ padding: 16 }}>
      测试
      <br />
      <Button style={{ marginTop: 40 }} onClick={props.onClose}>
        关闭
      </Button>
    </div>
  );
}

const basic = () => {
  const handleDrawer = () => {
    render(Drawer, {
      title: 'demo',
      content: <Demo />,
    }).open();
  };
  const handleModal = () => {
    render(Modal, {
      title: 'demo',
      content: <Demo />,
    }).open();
  };
  return (
    <Space>
      <Button onClick={handleDrawer}>打开Drawer</Button>
      <Button onClick={handleModal}>打开Modal</Button>
    </Space>
  );
};

export default basic;
