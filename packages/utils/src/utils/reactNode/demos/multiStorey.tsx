import React from 'react';
import { Space } from 'antd';
import { render } from '@parallel-line/utils';
import { Drawer, Button } from '@parallel-line/components';

const Content = (props: any) => {
  const handleClick = () => {
    render(Drawer, {
      title: 'demo1',
      content: <Content />,
    }).open();
  };
  return (
    <Space direction="vertical">
      <Button onClick={handleClick}>打开内部抽屉</Button>

      <Button style={{ marginTop: 40 }} onClick={props.onClose}>
        关闭当前抽屉
      </Button>
    </Space>
  );
};

const MultiStorey = () => {
  const handleClick = () => {
    render(Drawer, {
      title: 'demo',
      content: <Content />,
    }).open();
  };
  return <Button onClick={handleClick}>打开抽屉</Button>;
};

export default MultiStorey;
