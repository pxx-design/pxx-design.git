---
title: verification
group:
  title: verification
---

## verification 公共正则验证



```tsx | pure
import { verification } from '@parallel-line/utils';

verification.IDCard // /^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/,

verification.phoneNumber // /^1\d{10}$/,

verification.userName // /^[A-Za-z0-9]+$/,

verification.licensePlate // /^[A-HJ-NP-Z0-9\u4e00-\u9fa5]+$/,

```


