function test(
  this: {
    test: (value: any) => any;
    rule: { message: string; pattern: RegExp };
  },
  value: any,
) {
  return this.rule.pattern.test(value);
}

const verification = {
  IDCard: {
    test,
    rule: {
      message: '身份证号码格式不正确',
      pattern: /^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/,
    },
  },
  phoneNumber: {
    test,
    rule: {
      pattern: /^1\d{10}$/,
      message: '手机号码格式不正确',
    },
  },
  userName: {
    test,
    rule: {
      pattern: /^[A-Za-z0-9]+$/,
      message: '账号格式不正确',
    },
  },
  licensePlate: {
    test,
    rule: {
      pattern: /^[A-HJ-NP-Z0-9\u4e00-\u9fa5]+$/,
      message: '车牌号码格式不正确',
    },
  },
};
export { verification };
