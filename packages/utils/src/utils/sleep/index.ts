/**
 * setTimeout 的 Promise 版本
 */
const sleep = (time: number = 0) =>
  new Promise<void>((resolve) => {
    setTimeout(() => {
      resolve();
    }, time);
  });

export { sleep };
