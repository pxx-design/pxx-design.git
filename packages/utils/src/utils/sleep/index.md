---
title: sleep
group:
  title: sleep
---

# sleep

> setTimeout 的 Promise 版本

```tsx | pure
import { sleep } from '@parallel-line/utils';

(async function() {
  await sleep(100);
  //to do...
})();
```
