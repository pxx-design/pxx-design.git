---
title: 介绍
nav:
  title: 工具库
  order: 5
---

> 平行线前端常用工具库。

## 📦 安装

```bash
npm install @parallel-line/utils --save
```

```bash
yarn add @parallel-line/utils
```
