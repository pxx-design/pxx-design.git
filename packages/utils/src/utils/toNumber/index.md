---
title: 字符串转化成数值
group:
  title: 字符串转化成数值
---

# 字符串转化成数值
注：第二个参数可选，无法转成数值时，返回该参数，默认为undefined

```jsx | pure
import { toNumber } from '@parallel-line/utils';

toNumber(null); // undefined

toNumber(undefined); // undefined

toNumber({}); // undefined

toNumber({}, '--'); // --

toNumber('12'); // 12

toNumber(''); // undefined
```
