import { isNil, isString } from 'lodash-es';

/**
 * 字符串转化成number
 */

function toNumber(arg: any, errOut?: any) {
  if (isNil(arg) || (isString(arg) && arg.trim().length === 0)) return errOut;
  const result = Number(arg);
  if (Number.isNaN(result)) return errOut;
  return result;
}

export { toNumber };
