import { initDicTree } from '../initDicTree';
import { getVehicleTree } from '../getVehicleTree';
import dictionaryData from './dictionary';
import vehiclePropFitData from './vehiclePropFit';

// 正序排序
const positiveSortKeys = [
  'resource.vehicle.vehicle_length',
  'matching.map.radius_meter',
];
// 倒序排序
const negativeSortKeys = [
  'matching.request.request_type',
  'matching.shipment.shipment_type',
];

const constant = {
  // 货物签收标签
  'shipment.shipment.sign': {
    S50: { color: 'rgba(255,139,28,0.13)' },
    S60: { color: 'rgba(255,139,28,0.13)' },
  },
  // 签收状态
  'wechat.wechat.shipment_sign_status': {
    '20': { status: 'processing' },
    '30': { status: 'success' },
  },
  // 订单标签字典
  'request.request_shipment.tag': {
    R50: { color: 'rgba(240,54,74,0.13)' },
    R60: { color: 'rgba(240,54,74,0.13)' },
    R70: { color: 'rgba(240,54,74,0.13)' },
    S10: { color: 'rgba(240,54,74,0.13)' },
    S20: { color: 'rgba(240,54,74,0.13)' },
    S30: { color: 'rgba(240,54,74,0.13)' },
    S40: { color: 'rgba(240,54,74,0.13)' },
    S99: { color: 'rgba(240,54,74,0.13)' },
  },
  'request.cargo_request.tag': {
    R10: { color: 'rgba(30,86,255,0.13)' },
    R20: { color: 'rgba(30,86,255,0.13)' },
    R30: { color: 'rgba(30,86,255,0.13)' },
    R40: { color: 'rgba(30,86,255,0.13)' },
    R50: { color: 'rgba(30,86,255,0.13)' },
    R60: { color: 'rgba(30,86,255,0.13)' },
    R70: { color: 'rgba(30,86,255,0.13)' },
    R80: { color: 'rgba(30,86,255,0.13)' },
    R90: { color: 'rgba(0,184,107,0.13)' },
  },
  'resource.vehicle.vehicle_plate_color_code': {
    '1': { status: 'processing', color: '#1890ff' },
    '2': { status: 'warning', color: '#faad14' },
    '5': { status: 'success', color: '#52c41a' },
  },
  'shipment.shipment.waybill_status': {
    '-1': { status: 'warning' },
    0: { status: 'warning' },
    10: { status: 'processing' },
    20: { status: 'success' },
    30: { status: 'success' },
    99: { status: 'default' },
  },
  'matching.shipment.shipment_status': {
    '-1': { status: 'warning' },
    0: { status: 'warning' },
    1: { status: 'warning' },
    10: { status: 'processing' },
    20: { status: 'success' },
    30: { status: 'success' },
    99: { status: 'default' },
    999: { status: 'warning' },
  },
  'matching.shipment.capacity_shipment_status': {
    '-1': { status: 'warning' },
    0: { status: 'warning' },
    10: { status: 'processing' },
    20: { status: 'success' },
    30: { status: 'success' },
    99: { status: 'default' },
  },
  'resource.vehicle.vehicle_type_code': {
    '0': 'delete',
    '1001': { icon: 'iconfengbiyiti' },
    '1002': { icon: 'iconxiangshiche' },
    '1003': { icon: 'iconpingbanche' },
    '1004': { icon: 'icongaolanche' },
    '1005': { icon: 'iconlengcangche' },
    '1006': { icon: 'iconfeiyiche' },
    '1007': { icon: 'icongaodibanche' },
  },
  'matching.request.request_status': {
    0: { status: 'warning' },
    1: { status: 'processing' },
    2: { status: 'success' },
    99: { status: 'default' },
  },
  'matching.shipment.request_status': {
    0: { status: 'warning' },
    20: { status: 'processing' },
    30: { status: 'success' },
    99: { status: 'default' },
  },
  'matching.bidding.bidding_status': {
    0: { status: 'warning' },
    10: { status: 'processing' },
    20: { status: 'success' },
    30: { status: 'success' },
    99: { status: 'default' },
  },
  'matching.request.receipt_audit_status': {
    // 回单状态
    0: { status: 'warning' },
    10: { status: 'processing' },
    20: { status: 'processing' },
    30: { status: 'success' },
    40: { status: 'default' },
  },
  'web.basicsInfo.role': {
    10: { icon: 'icontuoyunfang' },
    20: { icon: 'iconchengyunfang' },
  },
  'resource.vehicle.vehicle_length': {
    '0': 'delete',
  },
  'matching.request.point_type': {
    '10': { icon: 'iconfahuo' },
    '20': { icon: 'iconshouhuo' },
    '30': { icon: 'iconjingting' },
  },
  'matching.shipment.operation_type': {
    '0': { desc: '暂未靠车' },
    '10': { desc: '已靠车' },
    '20': { desc: '已发车' },
    '30': { desc: '已到车' },
    '40': { desc: '已发车' },
    '50': { desc: '已到车' },
    '99': { desc: '卸货完成' },
  },
  // 支付状态
  'trade.tran_stream.tran_status': {
    '0': { status: 'success' },
    '1': { status: 'error' },
    '2': { status: 'default' },
    '3': { status: 'warning' },
    '4': { status: 'processing' },
  },
  // 对账状态
  'trade.tran_stream.bill_status': {
    '0': { status: 'warning' },
    '1': { status: 'success' },
  },
  // 审核状态
  'common.common.audit': {
    '0': { status: 'warning' },
    '1': { status: 'success' },
    '2': { status: 'error' },
    '3': { status: 'default' },
  },

  // 网络开票管理 结算状态
  'trade.tran_stream.settle_status': {
    '0': { status: 'success' },
    '1': { status: 'warning' },
  },
  // 财务管理 财务对账 账单状态
  'finance.ebill.shipment_ebillStatus': {
    '0': { status: 'warning' },
    '1': { status: 'success' },
    '2': { status: 'processing' },
  },

  // 开票管理 托运开票状态
  'finance.ebill.ebill_status': {
    '0': { status: 'warning' },
    '10': { status: 'warning' },
    '40': { status: 'success' },
  },
  // 开票管理 托运结算状态
  'finance.ebill.payment_status': {
    '20': { status: 'warning' },
    '30': { status: 'warning' },
    '40': { status: 'success' },
  },
  // 个人所得税审核状态
  'web.state.tax': {
    '0': { status: 'warning' },
    '1': { status: 'success' },
    '2': { status: 'error' },
    '999': { status: 'default' },
  },
  // 证件状态
  'resource.license.auth_status': {
    '10': { status: 'success' },
    '20': { status: 'warning' },
    '30': { status: 'error' },
  },

  // 开票管理 承运开票状态
  'finance.ebill_carrier.carrier_ebill_status': {
    '11': { status: 'warning' },
    '14': { status: 'success' },
  },
  // 开票管理 承运结算状态
  'finance.ebill.carrier_payment_status': {
    '20': { status: 'warning' },
    '30': { status: 'warning' },
    '40': { status: 'success' },
  },
  // 是否启用
  'file.file.show_enable': {
    '0': { status: 'warning' },
    '1': { status: 'success' },
  },
  // 账单配置 配置状态
  'finance.bill_config.config_status': {
    '0': { status: 'default' },
    '1': { status: 'success' },
  },
  // 数据报表 订单明细订单状态 配置
  'matching.order.order_status': {
    '0': { status: 'warning' },
    '10': { status: 'processing' },
    '20': { status: 'warning' },
    '30': { status: 'success' },
    '40': { status: 'default' },
  },
  // 数据报表 订单明细订单状态 配置
  'web.message.state': {
    '0': { status: 'success' },
    '1': { status: 'default' },
  },

  'web.message.type': {
    '10': { icon: 'iconxitongxiaoxi1', color: '#4992ff' },
    '11': { icon: 'iconyujingxiaoxi1', color: '#ff7360' },
    '12': { icon: 'iconcaiwuxiaoxi', color: '#ffa236' },
  },

  // 开票记录 开票状态
  'finance.invoice_cargo.invoice_status': {
    '0': { status: 'default' },
    '10': { status: 'processing' },
    '20': { status: 'success' },
    '30': { status: 'error' },
  },
  // 对公汇款 充值状态
  'bill.bill.pay_status': {
    '01': { status: 'default' },
    '10': { status: 'success' },
  },
  // 计价是否启用
  'valuation.valuation.valuation_status': {
    '1': { status: 'success' },
    '2': { status: 'error' },
  },

  'finance.ebill.confirm_status': {
    '0': { status: 'warning' },
    '1': { status: 'success' },
    '2': { status: 'default' },
  },
  //会场状态
  'bidding.bidding_recruit.bidding_recruit_venue_status': {
    '10': { status: 'processing' },
    '20': { status: 'success' },
    '30': { status: 'default' },
  },
  // 签约状态
  'bidding.bidding.sign_status': {
    '10': { status: 'processing' },
    '1': { status: 'success' },
    '30': { status: 'default' },
    '40': { status: 'default' },
  },
  'bidding.bidding-recruit.bidding_status': {
    '1': { status: 'processing' },
    '2': { status: 'success' },
    '3': { status: 'success' },
    '4': { status: 'default' },
  },
};

export function dicTree() {
  const dictionary = initDicTree({
    constant,
    positiveSortKeys,
    negativeSortKeys,
    dictionary: dictionaryData,
  });
  let dicVehicle = {};
  dicVehicle = getVehicleTree(vehiclePropFitData ?? [], dictionary);
  return { ...dictionary, ...dicVehicle };
}
