import { History } from 'history-with-query';
import { EventEmitter, Subscription, EEInstance } from './EventEmitter';
import { countryTree } from './countryTree';
import { CountryInfo } from './interface';
import { dicTree } from './dicTree';
import type { TagInfo } from '../getDic';

export type { Subscription, EEInstance };

export interface MessageInstance {
  info(content: string): void;
  success(content: string): void;
  error(content: string): void;
  warn(content: string): void;
}

export interface HistoryInstance {
  push: History['push'];
  replace: History['replace'];
  reload?(): void;
}

export interface GlobalTagProps {
  light?: boolean;
  color: string;
  name: string;
}

export interface EventEmitterVal {
  uniqueKey: string | symbol;
  action?: string;
  params?: any;

  [propName: string]: any;
}

export interface GlobalENVType {
  /** 是否是dev环境 */
  isDev: boolean;
  __isDev__: GlobalTagProps;
  /** 是否是test环境 */
  isTest: boolean;
  __isTest__: GlobalTagProps;
  /** 是否是dev和test环境 */
  isDevTest: boolean;
  /** 是否是运营平台 */
  isManager: boolean;
  /** 是否是企业端 */
  isEnterprise: boolean;
  /** 是否是百度小程序环境 */
  isSWAN: boolean;
  /** 是否是支付宝小程序环境 */
  isAlipay: boolean;
  /** 是否是字节跳动小程序环境 */
  isTT: boolean;
  /** 是否是快应用 */
  isQuickApp: boolean;
  /** 是否是QQ小程序环境 */
  isQQ: boolean;
  /** 是否是京东小程序环境 */
  isJD: boolean;
  /** 是否是微信小程序环境 */
  isWeapp: boolean;
  /** 是否是WEB环境 */
  isWeb: boolean;
  /** 是否是H5环境 */
  isH5: boolean;

  [k: string]: any;
}

export interface GlobalUserStateType {
  /** 用户是否登录 */
  isLogin: boolean;
  /** 用户是否认证（准入）过 */
  isAutentication: boolean;

  [k: string]: any;
}

export interface GlobalSolutionType {
  /** 零担 */
  isLTL: boolean;
  /** 整车 */
  isFTL: boolean;
  /** 外部订单号 */
  isExt: boolean;
  /** 承运商 */
  isCarrier: boolean;
  /** 货主、托运人 */
  isShipper: boolean;
  /** 网络货运 */
  isNetGoods: boolean;
  /** 抢单 */
  isGrab: boolean;
  /** 平台承运 */
  isAssignPlatform: boolean;
  /** 派单 平台承责 */
  isAssignDuty: boolean;
  /** 抢单 平台承责 */
  isGrabDuty: boolean;
  /** 竞价 */
  isQuote: boolean;
  /** 竞价 平台承责 */
  isQuoteDuty: boolean;

  __isLTL__: GlobalTagProps;
  __isFTL__: GlobalTagProps;
  __isCarrier__: GlobalTagProps;
  __isShipper__: GlobalTagProps;

  [k: string]: any;
}

export interface GlobalType {
  /** 城市列表 */
  countryTree: CountryInfo[];
  /** 字典 */
  dictionary: Record<string, TagInfo[]>;
  /** 环境变量 */
  PXX_ENV: GlobalENVType;
  PXX_USER_STATE: GlobalUserStateType;
  PXX_SOLUTION: GlobalSolutionType;
  eventEmitter: EventEmitter<EventEmitterVal>;
  /** 权限码集合 */
  accessCodes: string[];
  /** 用于工具库中调用的 message，在PC端就是 antd 的 message */
  message: MessageInstance;
  /** 用于工具库中调用的 history ，在PC端就是 umi 的 history */
  history?: HistoryInstance;
  getPageRouteInfo(pathname: string): any;

  [k: string]: any;
}

const event$ = new EventEmitter<EventEmitterVal>();
// 一些静态资源放在服务器上
const assetsUrl =
  'https://prod-1302801272.cos.ap-guangzhou.myqcloud.com/assets';
const global: GlobalType = {
  accessCodes: [],
  dictionary: dicTree(),
  countryTree,
  eventEmitter: event$,
  assetsUrl,
  message: {
    info(msg) {
      console.info(msg);
    },
    success(msg) {
      console.log(msg);
    },
    error(msg) {
      console.error(msg);
    },
    warn(msg) {
      console.warn(msg);
    },
  },
  theme: {
    logo: `${assetsUrl}/image/logo-pxx.svg`,
    logoShrink: `${assetsUrl}/image/logo-pxx-on.svg`,
    sider: {
      backgroundColor: '#2c4570',
    },
  },
  PXX_ENV: {
    isDev: false,
    isTest: false,
    isDevTest: false,
    isManager: false,
    isEnterprise: false,
    isH5: false,
    isWeb: false,
    isWeapp: false,
    isAlipay: false,
    isJD: false,
    isQQ: false,
    isQuickApp: false,
    isSWAN: false,
    isTT: false,
    __isDev__: { color: 'success', name: '开发版' },
    __isTest__: { color: 'success', name: '测试版' },
  },
  PXX_USER_STATE: {
    isLogin: false,
    isAutentication: false,
  },
  PXX_SOLUTION: {
    isLTL: false,
    isFTL: false,
    isExt: false,
    isCarrier: false,
    isShipper: false,
    isNetGoods: false,
    isGrab: false,
    isAssignPlatform: false,
    isAssignDuty: false,
    isGrabDuty: false,
    isQuote: false,
    isQuoteDuty: false,
    __isFTL__: { color: 'processing', name: '整车' },
    __isLTL__: { color: 'processing', name: '零担' },
    __isCarrier__: { color: 'warning', name: '承运商' },
    __isShipper__: { color: 'warning', name: '货主' },
    __isNetGoods__: { color: 'lime', name: '网络货运' },
    __isGrab__: { color: 'green', name: '抢单' },
    __isAssignPlatform__: { color: 'cyan', name: '平台承运' },
    __isAssignDuty__: { color: 'blue', name: '派单/平台承运' },
    __isGrabDuty__: { color: 'geekblue', name: '抢单/平台承运' },
    __isQuote__: { color: 'purple', name: '竞价' },
    __isQuoteDuty__: { color: 'volcano', name: '竞价/平台承运' },
  },
  getPageRouteInfo: () => {},
};

// Object.defineProperty(global, 'eventEmitter', {
//   get: () => event$,
// });

export { global };
