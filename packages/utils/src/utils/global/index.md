---
title: global 
group:
  title: 全局变量
---

# global

```jsx | pure
import { global } from '@parallel-line/utils';
```
<table>
  <tr>
    <td rowspan='5'>PXX_ENV</td>
    <td>isDev</td>
    <td>是否是dev环境</td>
  </tr>
  <tr>
    <td>isTest</td>
    <td>是否是test环境</td>
  </tr>
 <tr>
    <td>isDevTest</td>
    <td>是否是test环境</td>
  </tr>
  <tr>
    <td>isManager</td>
    <td>是否是运营平台</td>
  </tr>
  <tr>
    <td>isEnterprise</td>
    <td>是否是企业端</td>
  </tr>
  <tr>
    <td rowspan='2'>PXX_USER_STATE</td>
    <td>isLogin</td>
    <td>用户是否登录</td>
  </tr>
  <tr>
    <td>isAutentication</td>
    <td>用户是否认证（准入）过</td>
  </tr>
  <tr>
    <td rowspan='6'>PXX_SOLUTION</td>
    <td>isLTL</td>
    <td>是否是零担</td>
  </tr>
  <tr>
    <td>isFTL</td>
    <td>是否是整车</td>
  </tr>
  <tr>
    <td>isExt</td>
    <td>是否开启外部订单号</td>
  </tr>
  <tr>
    <td>isCarrier</td>
    <td>是否是承运商</td>
  </tr>
  <tr>
    <td>isShipper</td>
    <td>是否是货主、托运人</td>
  </tr>
  <tr>
    <td>isNetGoods</td>
    <td>是否是网络货运</td>
  </tr>
<table>

