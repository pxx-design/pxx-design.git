import { useRef, useEffect } from 'react';

export type Subscription<T> = (val: T) => void;
export interface EEInstance {
  off(): void;
}

export class EventEmitter<T> {
  private subscriptions = new Set<Subscription<T>>();

  emit = (val: T) => {
    for (const subscription of this.subscriptions) {
      subscription(val);
    }
  };

  /** useSubscription 非hooks版 */
  many = (callback: Subscription<T>): EEInstance => {
    function subscription(val: T) {
      if (callback) {
        callback(val);
      }
    }
    this.subscriptions.add(subscription);
    return {
      off: () => {
        this.subscriptions.delete(subscription);
      },
    };
  };

  useSubscription = (callback: Subscription<T>) => {
    const callbackRef = useRef<Subscription<T>>();
    callbackRef.current = callback;
    useEffect(() => {
      function subscription(val: T) {
        if (callbackRef.current) {
          callbackRef.current(val);
        }
      }
      this.subscriptions.add(subscription);
      return () => {
        this.subscriptions.delete(subscription);
      };
    }, []);
  };
}
