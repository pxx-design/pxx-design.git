/**
 * title: 父组件向子组件共享事件
 * desc: 在 MessageBox 中调用 `global.eventEmitter.emit` ，InputBox 和 ManyBox组件就可以收到通知。
 */

import React, { useRef, FC, Component } from 'react';
import { global } from '@parallel-line/utils';
import { random } from 'lodash-es';

const MessageBox: FC = function () {
  return (
    <div style={{ paddingBottom: 24 }}>
      <p>You received a message</p>
      <button
        type="button"
        onClick={() => {
          global.eventEmitter.emit({
            uniqueKey: 'MessageBox',
            params: { inputVal: random(100, 600) },
          });
        }}
      >
        Reply
      </button>
    </div>
  );
};

class ManyBox extends Component {
  off: () => void;
  state = {
    value: '',
  };

  constructor(props: {}) {
    super(props);
    const { off } = global.eventEmitter.many(({ params }) => {
      this.setState({ value: params.inputVal });
    });
    this.off = off;
  }

  componentWillUnmount() {
    this.off();
  }

  render() {
    return <input value={this.state.value} placeholder="ManyBox" style={{ width: '100%', padding: '4px', margin: '8px 0' }} />;
  }
}

const InputBox: FC = function () {
  const inputRef = useRef<any>();
  global.eventEmitter.useSubscription(() => {
    inputRef.current.focus();
  });
  return (
    <input
      ref={inputRef}
      placeholder="Enter reply"
      style={{ width: '100%', padding: '4px' }}
    />
  );
};

export default function () {
  return (
    <>
      <MessageBox />
      <InputBox />
      <ManyBox />
    </>
  );
}
