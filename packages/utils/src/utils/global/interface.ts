export interface CountryInfo {
  adcode: string;
  center: string;
  districts?: CountryInfo[];
  level: string;
  name: string;
  [k: string]: any;
}
