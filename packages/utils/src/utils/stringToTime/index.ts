import moment from 'moment';
import { isArray, isString, words } from 'lodash-es';

export interface DateFormatType {
  dateFormat?: string;
  curDateFormat?: string;
}

/**
 * 时间格式
 *
 * 任意带有时间信息的字符，转换为可用的时间格式
 *
 * @param date
 * @param format
 * @returns
 */
const stringToTime = (
  date: string | string[] | moment.Moment | moment.Moment[],
  format: DateFormatType = {},
) => {
  const {
    dateFormat = 'YYYY-MM-DD HH:mm:ss',
    curDateFormat = 'YYYY,MM,DD,HH,mm,ss',
  } = format;

  if (isArray(date))
    return (date as any).map((d: string | moment.Moment) => stringToTime(d));

  if (moment.isMoment(date)) return date.format(dateFormat);

  if (isString(date))
    return moment(words(date, /[^\D]+/g).join(','), curDateFormat).format(
      dateFormat,
    );
};
export { stringToTime };
