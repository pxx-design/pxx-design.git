import React, { useState } from 'react';
import { toThousandth } from '@parallel-line/utils';
import { Input, Text } from '@parallel-line/components';

const demo = () => {
  const [value, setValue] = useState<string>('1000000');
  const handleChange = (value: any) => setValue(value);
  return (
    <div style={{ width: 400 }}>
      <Input value={value} onChange={handleChange} />
      <Text mt={20} fs={16}>
        结果：{JSON.stringify(toThousandth(value))}
      </Text>
    </div>
  );
};

export default demo;
