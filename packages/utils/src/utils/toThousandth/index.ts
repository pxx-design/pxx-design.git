/**
 * 转千分位
 */
const toThousandth = (value: number | string = 0) => {
  const _value = String(value);
  if (_value.includes('.')) {
    const array = _value.split('.');
    array[0] = array[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return array.join('.');
  }
  return _value.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

export { toThousandth };
