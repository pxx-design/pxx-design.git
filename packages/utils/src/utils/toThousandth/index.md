---
title: 转千分位
group:
  title: 转千分位
---

# 转千分位

```tsx | pure
import { toThousandth } from '@parallel-line/utils';

toThousandth(1000000); //1000,000
```

## 演示

<code src="./demos/demo.tsx" />
