---
title: 获取网址参数
group:
  title: 获取网址参数
---

# 获取网址参数

```tsx | pure
import { getPageQuery } from '@parallel-line/utils';

getPageQuery('https://freight.pxxtech.com?id=50'); //{id:50}
```

## 演示

<code src="./demos/demo.tsx" ></code>
