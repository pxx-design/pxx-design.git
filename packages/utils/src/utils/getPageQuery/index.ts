import { parse } from 'querystring';

/**
 * 获取网址参数
 * @returns
 */
function getPageQuery<T = any>(
  url: string = window?.location.href,
): Record<string, T> {
  if (url.includes('?')) return parse(url.split('?')[1]) as any;
  return parse(url) as any;
}

export { getPageQuery };
