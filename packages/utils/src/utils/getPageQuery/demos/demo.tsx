import React, { useState } from 'react';
import { Input, Text } from '@parallel-line/components';
import { getPageQuery } from '@parallel-line/utils';

const demo = () => {
  const [value, setValue] = useState<string>(
    'https://freight.pxxtech.com?id=50',
  );
  const handleChange = (value: any) => setValue(value);
  return (
    <div style={{ width: 400 }}>
      <Input value={value} onChange={handleChange} />
      <Text mt={20} fs={16}>
        结果：{JSON.stringify(getPageQuery(value))}
      </Text>
    </div>
  );
};

export default demo;
