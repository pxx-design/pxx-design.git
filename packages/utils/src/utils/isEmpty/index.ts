import { isEmpty as lodash_isEmpty, isUndefined, isString } from 'lodash-es';

/**
 * 判断参数是 空对象 空数组 空字符 null undefined,之外都返回false
 * @param arg 传入不同类型的参数
 */
const isEmpty = <T>(arg: T) => {
  try {
    if (isUndefined(arg)) return true;
    if (typeof arg === 'object' || isString(arg)) return lodash_isEmpty(arg);
    return false;
  } catch (e) {
    return true;
  }
};

export { isEmpty };
