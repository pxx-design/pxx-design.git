---
title: 判断参数是为空
group:
  title: 判断参数是为空
---

# 空数据处理

```jsx | pure
import React from 'react';
import { isEmpty } from '@parallel-line/utils';

isEmpty(null); // true

isEmpty(); // true

isEmpty({}); // true

isEmpty([]); // true

isEmpty(''); // true

isEmpty(0); // false
```
