const initDicTree = (config: any) => {
  const {
    constant = {},
    dictionary = {},
    positiveSortKeys = [],
    negativeSortKeys = [],
  } = config;
  return Object.keys(dictionary).reduce((dic: any, key: any) => {
    if (constant[key]) {
      dic[key] = dictionary[key]?.reduce((pre: any, cur: any) => {
        if (constant[key][cur.key] === 'delete') return pre;
        pre.push({
          ...cur,
          ...constant[key][cur.key],
        });
        return pre;
      }, []);
    } else {
      dic[key] = dictionary[key];
    }
    if (positiveSortKeys.includes(key)) {
      dic[key].sort((a: any, b: any) => +a.key - +b.key);
    }
    if (negativeSortKeys.includes(key)) {
      dic[key].sort((a: any, b: any) => +b.key - +a.key);
    }
    return dic;
  }, {});
};
export { initDicTree };
