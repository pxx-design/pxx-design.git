---
title: 计算
group:
  title: 计算
---

# 计算

## 按需使用

通过以下两个链接，查看支持的`factories`

https://gitee.com/pxx-design/pxx-design/blob/master/packages/utils/src/utils/calc/index.ts#L16

https://mathjs.org/docs/expressions/syntax.html

PS: 如果需要更多的`factories`支持，请通过[issues](https://gitee.com/pxx-design/pxx-design/issues)

## 此方法是为了解决 javaScript 计算精度问题

```tsx | pure
import { calc } from '@parallel-line/utils';

calc('0.1+0.2'); //0.3

calc('0.1-0.2'); //-0.1

calc('0.1*0.2'); //0.02

calc('0.1/0.2'); //0.5
```

## 演示

<code src="./demos/demo.tsx" ></code>
