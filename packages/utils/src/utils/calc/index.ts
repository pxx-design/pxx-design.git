import {
  create,
  formatDependencies,
  bignumberDependencies,
  evaluateDependencies,
  divideDependencies,
  multiplyDependencies,
  addDependencies,
  subtractDependencies,
  modDependencies,
  unaryMinusDependencies,
  unaryPlusDependencies,
} from 'mathjs';
import { isNaN } from 'lodash-es';

// 决定使用的factories https://mathjs.org/docs/expressions/syntax.html
export const math = create(
  {
    formatDependencies,
    bignumberDependencies,
    evaluateDependencies,
    divideDependencies,
    multiplyDependencies,
    addDependencies,
    subtractDependencies,
    modDependencies,
    unaryMinusDependencies,
    unaryPlusDependencies,
  },
  {
    number: 'BigNumber',
    precision: 64,
  },
);

/**
 * 计算
 *
 * lg: `calc('0.1+0.2');`
 */
function calc(string: string): number;
function calc<E>(string: string, errorResult: E): number | E;
function calc<E>(string: string, errorResult?: E) {
  try {
    const value: any = math?.format?.(math.evaluate?.(string));
    if (isNaN(value)) return errorResult ?? NaN;
    return +value;
  } catch (e) {
    return errorResult ?? NaN;
  }
}

export { calc };
