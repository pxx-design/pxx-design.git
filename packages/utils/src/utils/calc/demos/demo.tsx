import React, { useState } from 'react';
import { Input, Text } from '@parallel-line/components';
import { calc } from '@parallel-line/utils';

const demo = () => {
  const [value, setValue] = useState<string>('0.1+0.2');
  const handleChange = (value: any) => setValue(value);
  return (
    <div style={{ width: 400 }}>
      <Input value={value} onChange={handleChange} />
      <Text mt={20} fs={16}>
        结果：{calc(value)}
      </Text>
    </div>
  );
};

export default demo;
