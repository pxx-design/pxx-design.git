/**
 * title: 基本使用
 * desc: 基本使用。
 */

import React, { useState } from 'react';
import { Input, Button, message } from 'antd';
import { get } from '@parallel-line/utils';

var object = { a: [{ b: { c: 3, d: null } }] };

export default () => {
  const [path, setPath] = useState('a[0].b.c');
  const [defaultValue, setDefaultValue] = useState('default');
  return (
    <div>
      {`var object = { a: [{ b: { c: 3, d: null } }] };`}
      <Input
        value={path}
        placeholder="path"
        onChange={(e) => setPath(e.target.value)}
      />
      <Input
        value={defaultValue}
        onChange={(e) => setDefaultValue(e.target.value)}
      />
      <Button
        onClick={() => {
          message.info(`输出：${get(object, path, defaultValue)}`);
        }}
      >
        提交
      </Button>
    </div>
  );
};
