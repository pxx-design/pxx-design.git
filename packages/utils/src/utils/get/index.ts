import { get as lodash_get } from 'lodash-es';

type Many<T> = T | ReadonlyArray<T>;
type PropertyName = string | number | symbol;
type PropertyPath = Many<PropertyName>;
interface NumericDictionary<T> {
  [index: number]: T;
}

interface GetStatic {
  /**
   * 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。
   *
   * @param object 要检索的对象。
   * @param path 要获取属性的路径。
   * @param defaultValue 如果解析值是 undefined 或 null ，这值会被返回。
   * @return 返回解析的值。
   */
  <TObject extends object, TKey extends keyof TObject>(
    object: TObject,
    path: TKey | [TKey],
  ): TObject[TKey];
  /**
   * 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。
   *
   * @param object 要检索的对象。
   * @param path 要获取属性的路径。
   * @param defaultValue 如果解析值是 undefined 或 null ，这值会被返回。
   * @return 返回解析的值。
   */
  <TObject extends object, TKey extends keyof TObject>(
    object: TObject | null | undefined,
    path: TKey | [TKey],
  ): TObject[TKey] | undefined;
  /**
   * 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。
   *
   * @param object 要检索的对象。
   * @param path 要获取属性的路径。
   * @param defaultValue 如果解析值是 undefined 或 null ，这值会被返回。
   * @return 返回解析的值。
   */
  <TObject extends object, TKey extends keyof TObject, TDefault>(
    object: TObject | null | undefined,
    path: TKey | [TKey],
    defaultValue: TDefault,
  ): Exclude<TObject[TKey], undefined> | TDefault;
  /**
   * 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。
   *
   * @param object 要检索的对象。
   * @param path 要获取属性的路径。
   * @param defaultValue 如果解析值是 undefined 或 null ，这值会被返回。
   * @return 返回解析的值。
   */
  <
    TObject extends object,
    TKey1 extends keyof TObject,
    TKey2 extends keyof TObject[TKey1]
  >(
    object: TObject,
    path: [TKey1, TKey2],
  ): TObject[TKey1][TKey2];
  /**
   * 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。
   *
   * @param object 要检索的对象。
   * @param path 要获取属性的路径。
   * @param defaultValue 如果解析值是 undefined 或 null ，这值会被返回。
   * @return 返回解析的值。
   */
  <
    TObject extends object,
    TKey1 extends keyof TObject,
    TKey2 extends keyof TObject[TKey1]
  >(
    object: TObject | null | undefined,
    path: [TKey1, TKey2],
  ): TObject[TKey1][TKey2] | undefined;
  /**
   * 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。
   *
   * @param object 要检索的对象。
   * @param path 要获取属性的路径。
   * @param defaultValue 如果解析值是 undefined 或 null ，这值会被返回。
   * @return 返回解析的值。
   */
  <
    TObject extends object,
    TKey1 extends keyof TObject,
    TKey2 extends keyof TObject[TKey1],
    TDefault
  >(
    object: TObject | null | undefined,
    path: [TKey1, TKey2],
    defaultValue: TDefault,
  ): Exclude<TObject[TKey1][TKey2], undefined> | TDefault;
  /**
   * 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。
   *
   * @param object 要检索的对象。
   * @param path 要获取属性的路径。
   * @param defaultValue 如果解析值是 undefined 或 null ，这值会被返回。
   * @return 返回解析的值。
   */
  <
    TObject extends object,
    TKey1 extends keyof TObject,
    TKey2 extends keyof TObject[TKey1],
    TKey3 extends keyof TObject[TKey1][TKey2]
  >(
    object: TObject,
    path: [TKey1, TKey2, TKey3],
  ): TObject[TKey1][TKey2][TKey3];
  /**
   * 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。
   *
   * @param object 要检索的对象。
   * @param path 要获取属性的路径。
   * @param defaultValue 如果解析值是 undefined 或 null ，这值会被返回。
   * @return 返回解析的值。
   */
  <
    TObject extends object,
    TKey1 extends keyof TObject,
    TKey2 extends keyof TObject[TKey1],
    TKey3 extends keyof TObject[TKey1][TKey2]
  >(
    object: TObject | null | undefined,
    path: [TKey1, TKey2, TKey3],
  ): TObject[TKey1][TKey2][TKey3] | undefined;
  /**
   * 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。
   *
   * @param object 要检索的对象。
   * @param path 要获取属性的路径。
   * @param defaultValue 如果解析值是 undefined 或 null ，这值会被返回。
   * @return 返回解析的值。
   */
  <
    TObject extends object,
    TKey1 extends keyof TObject,
    TKey2 extends keyof TObject[TKey1],
    TKey3 extends keyof TObject[TKey1][TKey2],
    TDefault
  >(
    object: TObject | null | undefined,
    path: [TKey1, TKey2, TKey3],
    defaultValue: TDefault,
  ): Exclude<TObject[TKey1][TKey2][TKey3], undefined> | TDefault;
  /**
   * 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。
   *
   * @param object 要检索的对象。
   * @param path 要获取属性的路径。
   * @param defaultValue 如果解析值是 undefined 或 null ，这值会被返回。
   * @return 返回解析的值。
   */
  <
    TObject extends object,
    TKey1 extends keyof TObject,
    TKey2 extends keyof TObject[TKey1],
    TKey3 extends keyof TObject[TKey1][TKey2],
    TKey4 extends keyof TObject[TKey1][TKey2][TKey3]
  >(
    object: TObject,
    path: [TKey1, TKey2, TKey3, TKey4],
  ): TObject[TKey1][TKey2][TKey3][TKey4];
  /**
   * 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。
   *
   * @param object 要检索的对象。
   * @param path 要获取属性的路径。
   * @param defaultValue 如果解析值是 undefined 或 null ，这值会被返回。
   * @return 返回解析的值。
   */
  <
    TObject extends object,
    TKey1 extends keyof TObject,
    TKey2 extends keyof TObject[TKey1],
    TKey3 extends keyof TObject[TKey1][TKey2],
    TKey4 extends keyof TObject[TKey1][TKey2][TKey3]
  >(
    object: TObject | null | undefined,
    path: [TKey1, TKey2, TKey3, TKey4],
  ): TObject[TKey1][TKey2][TKey3][TKey4] | undefined;
  /**
   * 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。
   *
   * @param object 要检索的对象。
   * @param path 要获取属性的路径。
   * @param defaultValue 如果解析值是 undefined 或 null ，这值会被返回。
   * @return 返回解析的值。
   */
  <
    TObject extends object,
    TKey1 extends keyof TObject,
    TKey2 extends keyof TObject[TKey1],
    TKey3 extends keyof TObject[TKey1][TKey2],
    TKey4 extends keyof TObject[TKey1][TKey2][TKey3],
    TDefault
  >(
    object: TObject | null | undefined,
    path: [TKey1, TKey2, TKey3, TKey4],
    defaultValue: TDefault,
  ): Exclude<TObject[TKey1][TKey2][TKey3][TKey4], undefined> | TDefault;
  /**
   * 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。
   *
   * @param object 要检索的对象。
   * @param path 要获取属性的路径。
   * @param defaultValue 如果解析值是 undefined 或 null ，这值会被返回。
   * @return 返回解析的值。
   */
  <T>(object: NumericDictionary<T>, path: number): T;
  /**
   * 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。
   *
   * @param object 要检索的对象。
   * @param path 要获取属性的路径。
   * @param defaultValue 如果解析值是 undefined 或 null ，这值会被返回。
   * @return 返回解析的值。
   */
  <T>(object: NumericDictionary<T> | null | undefined, path: number):
    | T
    | undefined;
  /**
   * 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。
   *
   * @param object 要检索的对象。
   * @param path 要获取属性的路径。
   * @param defaultValue 如果解析值是 undefined 或 null ，这值会被返回。
   * @return 返回解析的值。
   */
  <T, TDefault>(
    object: NumericDictionary<T> | null | undefined,
    path: number,
    defaultValue: TDefault,
  ): T | TDefault;
  /**
   * 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。
   *
   * @param object 要检索的对象。
   * @param path 要获取属性的路径。
   * @param defaultValue 如果解析值是 undefined 或 null ，这值会被返回。
   * @return 返回解析的值。
   */
  <TDefault>(
    object: null | undefined,
    path: PropertyPath,
    defaultValue: TDefault,
  ): TDefault;
  /**
   * 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。
   *
   * @param object 要检索的对象。
   * @param path 要获取属性的路径。
   * @param defaultValue 如果解析值是 undefined 或 null ，这值会被返回。
   * @return 返回解析的值。
   */
  (object: null | undefined, path: PropertyPath): undefined;
  /**
   * 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。
   *
   * @param object 要检索的对象。
   * @param path 要获取属性的路径。
   * @param defaultValue 如果解析值是 undefined 或 null ，这值会被返回。
   * @return 返回解析的值。
   */
  (object: any, path: PropertyPath, defaultValue?: any): any;
}

const get = ((object: any, path: any, defaultValue: any) => {
  const val = lodash_get(object, path, defaultValue);
  return val == null ? defaultValue : val;
}) as GetStatic;

export { get };
