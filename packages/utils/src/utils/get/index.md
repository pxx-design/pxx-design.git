---
title: get
group:
  title: get
---

# get

> 根据 object 对象的 path 路径获取值。 如果解析 value 是 undefined 或 null 会以 defaultValue 取代。

## 参数
1. objec *(Object)*: 要检索的对象
2. path *(Array|string)*: 要获取属性的路径
3. [defaultValue] *(\*)*: 如果解析值是 undefined 或 null, 这值会被返回

## 返回
*(\*)*: 返回解析的值

## 例子
```tsx | pure
import { get } from '@parallel-line/utils';

var object = { a: [{ b: { c: 3, d: null } }] };

get(object, 'a[0].b.c');
// => 3

get(object, ['a', '0', 'b', 'c']);
// => 3

get(object, 'a.b.c', 'default');
// => 'default'

get(object, 'a[0].b.d', 'default');
// => 'default'
```

## 演示

<code src="./demos/demo.tsx" ></code>
