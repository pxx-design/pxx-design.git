import { isNaN, round } from 'lodash-es';

/**
 * 小数位数保留
 */
const numberRound = (val: number = 0, precision: number = 2) => {
  const numVal = +val;
  if (isNaN(numVal)) return NaN;
  if (numVal === 0) return 0;
  const loop = (): number => {
    const _val = round(numVal, precision);
    if (_val !== 0) return _val;
    precision++;
    return loop();
  };
  return loop();
};
export { numberRound };
