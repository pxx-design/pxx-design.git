import React, { useState } from 'react';
import { Input, Text } from '@parallel-line/components';
import { numberRound } from '@parallel-line/utils';

const demo = () => {
  const [value, setValue] = useState<any>(0.123);
  const [precision, setPrecision] = useState<any>(2);
  return (
    <div style={{ width: 400 }}>
      <div>
        值：
        <Input value={value} onChange={(value) => setValue(value)} />
      </div>
      <div>
        保留位数：
        <Input
          value={precision}
          onChange={(precision) => setPrecision(precision)}
        />
      </div>
      <Text mt={20} fs={16}>
        结果：{`${numberRound(value, precision)}`}
      </Text>
    </div>
  );
};

export default demo;
