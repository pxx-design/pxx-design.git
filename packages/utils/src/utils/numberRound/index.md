---
title: 小数位数保留
group:
  title: 小数位数保留
---

# 小数位数保留

```tsx | pure
import { numberRound } from '@parallel-line/utils';

numberRound(0.123, 2); //0.12

numberRound(0.001, 2); //0.001
```

## 演示

<code src="./demos/demo.tsx" ></code>
