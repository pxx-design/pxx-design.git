import React, { useState } from 'react';
import { AutoComplete, Typography } from 'antd';
import { Text } from '@parallel-line/components';
import {
  CityLevelEnum,
  getCityLevel,
  getPrevCitys,
  global,
} from '@parallel-line/utils';

declare global {
  interface Window {
    countryTree: any;
  }
}

const { Paragraph } = Typography;
global.countryTree = window.countryTree;

function demoLevel(adcode: string) {
  // 根据 城市级别 获取相应值进行值拼接
  const level = getCityLevel(adcode);
  let province: string | undefined,
    city: string | undefined,
    district: string | undefined;
  if (level === CityLevelEnum.DISTRICT) {
    [province, city, district] = getPrevCitys(adcode, ['name']);
  } else if (level === CityLevelEnum.MUNICIPALITY) {
    [city, district] = getPrevCitys(adcode, ['name']);
  } else if (level === CityLevelEnum.CITY) {
    [province, city] = getPrevCitys(adcode, ['name']);
  }
  return [city, district].filter((f) => !!f).join('/');
}

const demo = () => {
  const [value, setValue] = useState<string>('131102');
  const handleChange = (value: any) => setValue(value);
  return (
    <div>
      <AutoComplete
        value={value}
        style={{ width: 200 }}
        options={[
          { value: '131102', label: '131102' },
          { value: '110101', label: '110101' },
          { value: '131100', label: '131100' },
          { value: '130000', label: '130000' },
        ]}
        onChange={handleChange}
      />
      <Paragraph>
        <pre>getPrevCitys(value)</pre>
      </Paragraph>

      <Text mt={20} fs={16}>
        结果：{JSON.stringify(getPrevCitys(value))}
      </Text>

      <Paragraph>
        <pre>{`getPrevCitys(value, ["name"])`}</pre>
      </Paragraph>
      <Text mt={20} fs={16}>
        结果：{JSON.stringify(getPrevCitys(value, ['name']))}
      </Text>

      <Paragraph>
        <pre>{`getPrevCitys(value, ["name", "center"])`}</pre>
      </Paragraph>
      <Text mt={20} fs={16}>
        结果：
        {JSON.stringify(getPrevCitys(value, ['name', 'center']))}
      </Text>

      <Paragraph>
        <pre>{`getPrevCitys(value, true)`}</pre>
      </Paragraph>
      <Text mt={20} fs={16}>
        结果：{JSON.stringify(getPrevCitys(value, true))}
      </Text>

      <Paragraph>
        <pre>{`getPrevCitys(value, { excludekeys: ["center", "districts"] })`}</pre>
      </Paragraph>
      <Text mt={20} fs={16}>
        结果：
        {JSON.stringify(
          getPrevCitys(value, { excludekeys: ['center', 'districts'] }),
        )}
      </Text>

      <Paragraph>
        <pre>{`
  const level = getCityLevel(adcode);
  let province: string | undefined,
    city: string | undefined,
    district: string | undefined;
  if (level === CityLevelEnum.DISTRICT) {
    [province, city, district] = getPrevCitys(adcode, ["name"]);
  } else if (level === CityLevelEnum.MUNICIPALITY) {
    [city, district] = getPrevCitys(adcode, ["name"]);
  } else if (level === CityLevelEnum.CITY) {
    [province, city] = getPrevCitys(adcode, ["name"]);
  }
  return [city, district].filter((f) => !!f).join("/");
  `}</pre>
      </Paragraph>
      <Text mt={20} fs={16}>
        结果：{JSON.stringify(demoLevel(value))}
      </Text>
    </div>
  );
};

export default demo;
