/* eslint-disable @typescript-eslint/unified-signatures */
/* eslint-disable one-var */
import arrayTreeFilter from 'array-tree-filter';
import { global } from '../global';

export type Key = string | number;

export enum CityLevelEnum {
  /**
   * 直辖市
   */
  MUNICIPALITY,
  /**
   * 省级
   */
  PROVINCE,
  /**
   * 市级
   */
  CITY,
  /**
   * 区级
   */
  DISTRICT,
}

/**
 * 根据adcode规律返回城市级别
 * @param adcode
 */
export const getCityLevel = (adcode: Key): CityLevelEnum => {
  const val = String(adcode);
  if (+val.slice(2, 6) === 0) return CityLevelEnum.PROVINCE;
  if (+val.slice(4, 6) === 0) return CityLevelEnum.CITY;
  const slice2 = +val.slice(0, 2);
  if (slice2 === 11 || slice2 === 12 || slice2 === 50 || slice2 === 31)
    return CityLevelEnum.MUNICIPALITY;
  return CityLevelEnum.DISTRICT;
};

export const getCitysCode = (adcode?: Key): Key[] => {
  if (!adcode) return [];
  const val = String(adcode);
  const level = getCityLevel(adcode);
  if (level === CityLevelEnum.PROVINCE) return [adcode];
  if (level === CityLevelEnum.CITY)
    return [val.slice(0, 2).padEnd(6, '0'), adcode];
  if (level === CityLevelEnum.MUNICIPALITY)
    return [val.slice(0, 2).padEnd(6, '0'), adcode];
  return [
    val.slice(0, 2).padEnd(6, '0'),
    val.slice(0, 4).padEnd(6, '0'),
    adcode,
  ];
};

interface IOptions<T = any> {
  /**
   * 默认值： global.countryTree
   */
  data?: T[];
  keys?: Key[] | true;
  /**
   * 默认值： districts
   */
  childrenKeyName?: string;
  /**
   * 当 keys === true时有效。 默认值： [childrenKeyName]
   */
  excludekeys?: Key[];
}

export type PluginFunc<T = unknown> = (
  option: T,
  c: GetPrevCitys,
  f: GetPrevCitysFunc,
) => void;

type GetPrevCitysFunc = {
  (adcode: Key): Key[];
  <T>(adcode: Key, keys: true): T[];
  <T>(adcode: Key, keys: [Key]): T[];
  <T>(adcode: Key, keys: Key[]): T[];
  <T, K = any>(adcode: Key, options: IOptions<T>): (Key | T | K)[];
};

class GetPrevCitys {
  getPrevCitys(adcode: Key, options?: IOptions<any> | Key[] | true) {
    const codes = getCitysCode(adcode);
    if (options == null) return codes;
    let data: any[] = global.countryTree || [],
      keys: Key[] | true = true,
      excludekeys: Key[] = ['districts'],
      childrenKeyName: string = 'districts';

    if (options === true) {
      keys = true;
    } else if (options instanceof Array) {
      keys = options;
    } else {
      ({
        data = global.countryTree || [],
        keys = true,
        childrenKeyName = 'districts',
      } = options);
      ({ excludekeys = [childrenKeyName] } = options);
    }
    const citys = arrayTreeFilter(
      data,
      (item, level) => item.adcode === codes[level],
      { childrenKeyName },
    );

    return citys
      .map(({ ...item }) => {
        const city: Record<string, any> = {};

        if (keys === true) {
          excludekeys.forEach((k) => {
            delete item[k];
          });
          return item;
        }
        if (keys.length === 1) {
          return item[keys[0]];
        }
        keys.forEach((k) => {
          city[k] = item[k];
        });
        return city;
      })
      .filter((c) => !!c);
  }
}

const getPrevCitys: GetPrevCitysFunc = (
  adcode: Key,
  options?: IOptions<any> | Key[] | true,
) => {
  return new GetPrevCitys().getPrevCitys(adcode, options);
};

export { getPrevCitys };
