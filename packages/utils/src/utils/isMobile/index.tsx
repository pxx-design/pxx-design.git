function isMobile() {
  const { userAgent } = window?.navigator;
  return (
    /(iPhone|iPad|iPod|iOS)/i.test(userAgent) || /(Android)/i.test(userAgent)
  );
}

export { isMobile };
