---
title: 打开新页面 openBlank
group:
  title: 打开新页面 openBlank
---

### 打开新页面

为什么不使用window.open？

当window.open为用户触发事件内部或者加载时，不会被拦截，一旦将弹出代码移动到ajax或者一段异步代码内部，马上就出现被拦截的表现了(浏览器认为这可能是一个广告，不是一个用户希望看到的页面)

<code src="./demos/basic.tsx"></code>
