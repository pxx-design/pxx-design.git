/**
 * openBlank
 * @param url 规定链接指向的页面的 URL。
 * @param target 规定在何处打开链接文档，参数为_blank｜_parent｜_self｜_top｜framename。
 */

const openBlank = (url: string, target: string = '_blank') => {
  const dom = document.createElement('a');
  dom.setAttribute('href', url);
  dom.setAttribute('target', target);
  dom.click();
  dom.remove();
};

export { openBlank };
