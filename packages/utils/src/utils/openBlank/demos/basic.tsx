import React from 'react';
import { Button } from '@parallel-line/components';
import { openBlank } from '@parallel-line/utils';


const Basic = () => {
  const handleOpenBlank = () => {
    openBlank('//freight.pxxtech.com/')
  };
  return (
    <div>
      <Button type='primary'
              onClick={handleOpenBlank}>打开新页面</Button>
    </div>
  );
};

export default Basic;
