import { isObject } from 'lodash-es';

export function params2errorResult(params?: any) {
  if (isObject(params)) {
    return (params as any)?.errorResult;
  }
  return params;
}
