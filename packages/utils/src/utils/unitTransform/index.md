---
title: 单位转换
group:
  title: 单位转换
---

# 单位转换

```tsx | pure
import { unitTransform } from '@parallel-line/utils';

// 分转元
unitTransform.centToYuan(100); // 1

// 立方厘米转立方米
unitTransform.cubicCentimeterToMeter(1000000); // 1

// 立方米转立方厘米
unitTransform.cubicMeterToCentimeter(1); // 1000000

// 小时转分钟
unitTransform.hourToMinute(1); // 60

// 千克转吨
unitTransform.kilogramToTon(1000); // 1

// 克转千克
unitTransform.gramToKilogram(1000); // 1

// 公里转里
unitTransform.kilometreToLi(1); // 0.5

// 公里/千米转米
unitTransform.kilometreToMetre(1); // 1000

// 里转公里
unitTransform.liToKilometre(1); // 0.5

// 米转公里/千米
unitTransform.metreToKilometre(1000); // 1

// 分钟转小时
unitTransform.minuteToHour(60); // 1

// 吨转千克
unitTransform.tonToKilogram(1); // 1000

// 千克转克
unitTransform.kilogramToGram(1); // 1000

// 元转分
unitTransform.yuanToCent(1); // 100

// 转千分位
unitTransform.toThousandth(10000); // 10,000.00

// 转小数位
unitTransform.toFixed(1000); // 1000.00
unitTransform.toFixed(99.334); // 99.33
unitTransform.toFixed(99.335); // 99.34
unitTransform.toFixed(99.336); // 99.34
unitTransform.toFixed(99.3335, 3); // 99.334
```

## 演示

<code src="./demos/demo.tsx" ></code>
