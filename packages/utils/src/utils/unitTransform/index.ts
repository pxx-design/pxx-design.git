import * as InternalUnitTransform from './unitTransform';
import type { UnitTransformFn } from './type';

export type UnitTransformType = typeof InternalUnitTransform;
export type UnitTransformKey = keyof UnitTransformType;

const unitTransform: Record<
  UnitTransformKey,
  UnitTransformFn
> = InternalUnitTransform;

export { unitTransform };
