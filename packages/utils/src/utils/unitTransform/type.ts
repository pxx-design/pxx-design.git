export type UnitTransformFn = (val?: number | string, params?: any) => any;
