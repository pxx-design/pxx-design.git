import React, { useState } from 'react';
import { Input, Select } from 'antd';
import { unitTransform } from '@parallel-line/utils';
import { Text } from '@parallel-line/components';

const options = Object.keys(unitTransform).map((value) => ({
  label: value,
  value,
}));

export default () => {
  const [inputValue, setInputValue] = useState('100');
  const [selectValue, setSelectValue] = useState('centToYuan');
  return (
    <>
      <Input
        value={inputValue}
        style={{ width: 300 }}
        onChange={(e) => setInputValue(e.target.value)}
        addonBefore={
          <Select
            options={options}
            value={selectValue}
            style={{ width: 202 }}
            onSelect={(value) => setSelectValue(value)}
          />
        }
      />
      <Text mt={20} fs={16}>
        结果：{unitTransform[selectValue](inputValue)}
      </Text>
    </>
  );
};
