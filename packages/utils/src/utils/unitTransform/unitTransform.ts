import { isNaN } from 'lodash-es';
import { calc, math } from '../calc';
import { numberRound } from '../numberRound';
import { params2errorResult } from './util';
import type { UnitTransformFn } from './type';

export { toThousandth } from '../toThousandth';

/**
 * 分转元
 */
export const centToYuan: UnitTransformFn = function (val, params) {
  return calc(`${val}/100`, params2errorResult(params));
};

/**
 * 立方厘米转立方米
 */
export const cubicCentimeterToMeter: UnitTransformFn = function (val, params) {
  return calc(`${val}/1000000`, params2errorResult(params));
};

/**
 * 立方米转立方厘米
 */
export const cubicMeterToCentimeter: UnitTransformFn = function (val, params) {
  return calc(`${val}*1000000`, params2errorResult(params));
};

/**
 * 小时转分钟
 */
export const hourToMinute: UnitTransformFn = (val, params) =>
  calc(`${val}*60`, params2errorResult(params));

/**
 * 千克转吨
 */
export const kilogramToTon: UnitTransformFn = (val, params) =>
  calc(`${val}/1000`, params2errorResult(params));

/**
 * 克转千克
 */
export const gramToKilogram: UnitTransformFn = (val, params) =>
  calc(`${val}/1000`, params2errorResult(params));

/**
 * 公里转里
 */
export const kilometreToLi: UnitTransformFn = (val, params) =>
  calc(`${val}/0.5`, params2errorResult(params));

/**
 * 公里/千米转米
 */
export const kilometreToMetre: UnitTransformFn = (val, params) =>
  calc(`${val}*1000`, params2errorResult(params));

/**
 * 里转公里
 */
export const liToKilometre: UnitTransformFn = (val, params) =>
  calc(`${val}*0.5`, params2errorResult(params));

/**
 * 米转公里/千米
 */
export const metreToKilometre: UnitTransformFn = (val, params) =>
  calc(`${val}/1000`, params2errorResult(params));

/**
 * 分钟转小时
 */
export const minuteToHour: UnitTransformFn = (val, params) => {
  const value = calc(`${val}/60`);
  if (isNaN(value)) return params2errorResult(params) || NaN;
  return numberRound(value);
};

/**
 * 吨转千克
 */
export const tonToKilogram: UnitTransformFn = (val, params) =>
  calc(`${val}*1000`, params2errorResult(params));

/**
 * 千克转克
 */
export const kilogramToGram: UnitTransformFn = (val, params) =>
  calc(`${val}*1000`, params2errorResult(params));

/**
 * 元转分
 */
export const yuanToCent: UnitTransformFn = (val, params) =>
  calc(`${val}*100`, params2errorResult(params));

/**
 * 转小数位
 */
export const toFixed: UnitTransformFn = (
  val: any,
  { fractionDigits = 2, errorResult }: any = {},
) => math.bignumber?.(val)?.toFixed(fractionDigits) || errorResult;
