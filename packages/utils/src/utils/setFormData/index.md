---
title: setFormData
group:
  title: setFormData
---

# SetFormData

> 处理对象 key 为路径数据

```tsx | pure
import { setFormData } from '@parallel-line/utils';

setFormData({ 'a.b.c': 1 }); //{a:{b:{c:1}}}
```
