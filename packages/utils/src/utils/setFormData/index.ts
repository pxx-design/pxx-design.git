import { set } from 'lodash-es';

/**
 * 处理对象 key 为路径数据
 * @param formData
 * @returns
 */
const setFormData = (formData: object) => {
  return Object.entries(formData).reduce((prev: any, cur: any) => {
    set(prev, cur[0], cur[1]);
    return prev;
  }, {});
};

export { setFormData };
