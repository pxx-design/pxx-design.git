import React, { useState } from 'react';
import { Input, Text } from '@parallel-line/components';
import { desensitization } from '@parallel-line/utils';
import { InputNumber } from 'antd';

const demo = () => {
  const [value, setValue] = useState<string>();
  const [start, setStart] = useState<number>();
  const [end, setEnd] = useState<number>();
  const handleChange = (value: any) => setValue(value);

  return (
    <div style={{ width: 400 }}>
      输入：
      <Input value={value} onChange={handleChange} />
      <Text mt={20} fs={16}>
        头部展示位数：
        <InputNumber
          value={start}
          onChange={(val) => setStart(val || undefined)}
        />
      </Text>
      <Text mt={20} fs={16}>
        尾部展示位数：
        <InputNumber value={end} onChange={(val) => setEnd(val || undefined)} />
      </Text>
      <Text mt={20} fs={16}>
        结果：{JSON.stringify(desensitization(value || '', start, end))}
      </Text>
    </div>
  );
};

export default demo;
