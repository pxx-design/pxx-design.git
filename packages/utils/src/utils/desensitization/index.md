---
title: 字符串脱敏
group:
  title: 字符串脱敏
---

# 空数据处理

```jsx | pure
import React from 'react';
import { desensitization } from '@parallel-line/utils';

desensitization('18888888888'); //188****8888

desensitization('18888888888', 3); // 188********

desensitization('18888888888', 3, 4); //188****8888

desensitization('18888888888', 0, 4); //*******8888
```

## 演示

<code src="./demos/demo.tsx" ></code>
