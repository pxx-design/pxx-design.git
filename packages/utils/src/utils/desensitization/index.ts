/**
 * 字符串脱敏
 * @param strings
 * @param start 头部可展示位数
 * @param end 尾部可展示位数
 */
const desensitization = (strings: string, start?: number, end?: number) => {
  if (!strings) return strings;
  const len = strings.length;

  let startCount = start || 0;
  let endCount = end || 0;
  let replaceCount = len - startCount - endCount;
  // 未传入需显示的头部位数和尾部位数，使用默认
  if (typeof start === 'undefined' && typeof end === 'undefined') {
    startCount = Math.floor(len / 3);
    replaceCount = Math.ceil(len / 3);
    endCount = len - startCount - replaceCount;
  }
  try {
    const reg = new RegExp(`^(.{${startCount}})(.*)(.{${endCount}})$`);
    return strings.replace(reg, `$1${'*'.repeat(replaceCount)}$3`);
  } catch (error) {
    return strings;
  }
};

export { desensitization };
