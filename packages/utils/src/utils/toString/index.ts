import { isNumber, isString } from 'lodash-es';

/**
 * 数据转化成number
 * 除了string和number类型，其余都返回undefined
 */
const toString = <T>(arg: T) => {
  if (isNumber(arg)) return String(arg);
  if (isString(arg)) return arg;
  return undefined;
};

export { toString };
