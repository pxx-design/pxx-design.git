---
title: 数值转化成字符串
group:
  title: 数值转化成字符串
---

# 数值转化成字符串

```jsx | pure
import { toString } from '@parallel-line/utils';

toString(null); // undefined

toString(undefined); // undefined

toString({}); // undefined

toString(12); // '12'

toString('12'); // 12

toString(''); // ''
```
