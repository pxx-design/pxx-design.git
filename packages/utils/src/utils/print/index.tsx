import ReactDOM from 'react-dom';
import React from 'react';

const handleIframe = (iframe: HTMLIFrameElement, nodeDOM: React.FunctionComponentElement<any>) => {
  const div = document.createElement('div');
  const iframeWindow = iframe.contentWindow;
  // head
  const iframeHead = iframeWindow?.document.querySelector('head');
  document.querySelectorAll('link').forEach((elem: any) => {
    iframeHead?.appendChild(elem.cloneNode(true));
  });
  ReactDOM.render(nodeDOM, div);
  iframeWindow?.document.body.appendChild(div);
  setTimeout(() => {
    iframeWindow?.print();
    iframe.remove();
  }, 100);
};
/**
 * print
 * @param nodeDOM react组件
 */
const print = (nodeDOM: React.FunctionComponentElement<any>) => {
  const { userAgent } = navigator;
  const iframe = document.createElement('iframe');
  iframe.setAttribute('style', 'position:absolute;left:-500px;top:-500px;');
  document.body.appendChild(iframe);
  if (userAgent.includes('Firefox')) {
    iframe.onload = () => {
      handleIframe(iframe, nodeDOM);
    };
  } else {
    handleIframe(iframe, nodeDOM);
  }
};
export { print };
