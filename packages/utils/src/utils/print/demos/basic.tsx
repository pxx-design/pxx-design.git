import React from 'react';
import { BaseTable, Button } from '@parallel-line/components';
import { print } from '@parallel-line/utils';

const dataSource = [
  {
    key: '1',
    name: '胡彦斌',
    age: 32,
    address: '西湖区湖底公园1号',
  },
  {
    key: '2',
    name: '胡彦祖',
    age: 42,
    address: '西湖区湖底公园1号',
  },
];

const columns = [
  {
    title: '姓名',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: '年龄',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: '住址',
    dataIndex: 'address',
    key: 'address',
  },
];


const Basic = () => {
  const handlePrint = () => {
    print(<BaseTable dataSource={dataSource} columns={columns} />);
  };
  return (
    <div>
      <Button type='primary'
              onClick={handlePrint}>打印</Button>
    </div>
  );
};

export default Basic;
