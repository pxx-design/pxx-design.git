---
title: obtainPrevDistrict
group:
  title: obtainPrevDistrict
---
# 获取区域信息

```jsx | pure
import { obtainPrevDistrict } from '@parallel-line/utils';

const {
  province,//省
  city,//市
  district,//区
  level,//当前传入的adcode的级别，municipality直辖市
} = obtainPrevDistrict({ adcode: "110106" });

```
