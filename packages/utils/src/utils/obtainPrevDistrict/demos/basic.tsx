import React from 'react';
import {
  global,
  obtainPrevDistrict,
} from '@parallel-line/utils';

declare global {
  interface Window {
    countryTree: any;
  }
}
global.countryTree = window.countryTree;


const basic = () => {
  const a = obtainPrevDistrict({ adcode: "110106" });
  console.log(a);
  return (
    <div>

    </div>
  );
};

export default basic;
