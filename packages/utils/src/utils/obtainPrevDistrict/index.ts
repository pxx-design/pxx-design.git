import { global } from '../global';
import { isEmpty } from '../isEmpty';
import arrayTreeFilter from 'array-tree-filter';

const obtainCodeLevel = (adcode: string) => {
  const provinceCode = adcode.slice(0, 2).padEnd(6, '0');
  if (adcode.slice(2, 6) === '0000') return [[provinceCode], 'province'];
  const cityCode = adcode.slice(0, 4).padEnd(6, '0');
  if (adcode.slice(4, 6) === '00') return [[provinceCode, cityCode], 'city'];
  if (['11', '12', '31', '50'].includes(adcode.slice(0, 2)))
    return [[provinceCode, adcode], 'municipality'];
  return [[provinceCode, cityCode, adcode], 'district'];
};
/**
 * 根据adcode获取地区信息
 * @param adcode
 * @param errorResult
 */

const obtainPrevDistrict = ({
  adcode,
  errorResult = {
    province: {},
    city: {},
    district: {},
  },
}: any) => {
  const { countryTree } = global;
  if (isEmpty(adcode)) return errorResult;
  const [adcodes, level] = obtainCodeLevel(`${adcode}`);
  const districts = arrayTreeFilter(
    countryTree,
    (item, floor) => {
      return String(item.adcode) === String(adcodes[floor]);
    },
    { childrenKeyName: 'districts' },
  );
  if (level === 'municipality') {
    districts.unshift(districts[0]);
  }
  return {
    province: districts[0],
    city: districts[1],
    district: districts[2],
    level,
  };
};
export { obtainPrevDistrict };
