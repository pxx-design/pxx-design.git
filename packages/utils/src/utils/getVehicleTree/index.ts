const cloneDeep = (data: any) => JSON.parse(JSON.stringify(data))

const getVehicleTree = (relation: any[], dictionary: any) => {
  const vehicle_color =
    dictionary?.['resource.vehicle.vehicle_plate_color_code'];
  const vehicle_type = dictionary?.['resource.vehicle.vehicle_type_code'];
  const vehicle_length = dictionary?.['resource.vehicle.vehicle_length'];
  const vehicle_axis = dictionary?.['resource.vehicle.vehicle_axis_number'];
  const getRelation = ({color, type, length}: any) =>
    relation.filter((t) => {
      let flagColor = true;
      let typeColor = true;
      let lengthColor = true;
      if (color && color != t.vehicle_plate_color_code) flagColor = false;
      // eslint-disable-next-line eqeqeq
      if (type && type != t.vehicle_type_code) typeColor = false;
      // eslint-disable-next-line eqeqeq
      if (length && length != t.vehicle_length) lengthColor = false;
      return flagColor && typeColor && lengthColor;
    });

  const vehicle_color_type_length = vehicle_color?.reduce(
    (prev: any, cur: any) => {
      const types = getRelation({color: cur.key});
      const colorChildren = cloneDeep(vehicle_type).filter((t: any) =>
        // eslint-disable-next-line eqeqeq
        types.some((c: any) => c.vehicle_type_code == t.key),
      );
      colorChildren.forEach((t: any) => {
        const lengths = getRelation({color: cur.key, type: t.key});
        const lengthChildren = vehicle_length.filter((t1: any) =>
          // eslint-disable-next-line eqeqeq
          lengths.some((c: any) => c.vehicle_length == t1.key),
        );
        lengthChildren.forEach((t2: any) => {
          t2.children = undefined;
        });
        t.children = lengthChildren;
      });
      prev.push({
        ...cur,
        children: colorChildren,
      });
      return prev;
    },
    [],
  );
  const vehicle_length_type = vehicle_length.reduce((prev: any, cur: any) => {
    const types = getRelation({length: cur.key});
    const typeChildren = cloneDeep(vehicle_type).filter((t: any) =>
      // eslint-disable-next-line eqeqeq
      types.some((c: any) => c.vehicle_type_code == t.key),
    );
    typeChildren.forEach((t2: any) => {
      t2.children = undefined;
    });
    prev.push({
      ...cur,
      children: typeChildren,
    });
    return prev;
  }, []);
  const vehicle_length_axis_number = vehicle_length.reduce(
    (prev: any, cur: any) => {
      const types = getRelation({length: cur.key});
      const typeChildren = cloneDeep(vehicle_axis).filter((t: any) =>
        // eslint-disable-next-line eqeqeq
        types.some((c: any) => c.vehicle_axis_number == t.key),
      );
      typeChildren.forEach((t2: any) => {
        t2.children = undefined;
      });
      prev.push({
        ...cur,
        children: typeChildren,
      });
      return prev;
    },
    [],
  );
  return {
    vehicle_color_type_length,
    vehicle_length_axis_number,
    vehicle_length_type,
  };
};

export {getVehicleTree};
