const fs = require('fs');

function createIndex() {
  const tempFiles = fs.readdirSync('./src');
  const arrFiles = tempFiles.filter((name) => !name.includes('.'));
  const data = [];
  arrFiles.forEach((folder) => {
    const files = fs.readdirSync(`./src/${folder}`);
    files
      .filter((f) => f.indexOf('.md') === -1)
      .filter((f) => f.indexOf('promiseFinally') == -1)
      .filter((f) => f.indexOf('.DS_Store') == -1)
      .forEach((file) => {
        data.push(`export * from './${folder}/${file}';`);
      });
  });
  fs.writeFile('./src/index.ts', data.join('\n'), function (err) {
    if (err) {
      throw err;
    }
  });
}

createIndex();
