import fs from 'fs';
import path from 'path';
import { promisify } from 'util';
import { extend } from 'umi-request';

const writeFile = promisify(fs.writeFile);

const req = extend({
  headers: {
    'Content-Type': 'application/json',
  },
});

async function request(path: string, dest: fs.PathLike) {
  console.log(`--- 开始 ${path} ---`);
  const res = await req.post(`https://dev.pxxtech.com${path}`, { data: {} });
  if (res.code === 200 && res.data) {
    await writeFile(dest, `export default ${JSON.stringify(res.data)}`);
  }
  console.log(`--- 结束 ${path} ---`);
  return;
}

async function main() {
  try {
    await request(
      '/host/system/dictionary/tree',
      path.resolve('src/utils/global/dictionary.ts'),
    );

    await request(
      '/web/vehiclePropFit/list',
      path.resolve('src/utils/global/vehiclePropFit.ts'),
    );
  } catch (error) {
    console.error(error);
  }
}

main();
