---
title: useEffectDeep
group: { title: useEffectDeep }
---

# useEffectDeep

```tsx | pure
import { useEffectDeep } from "@parallel-line/hooks";

const FC = ({ data }) => {
  useEffectDeep(() => {
    // too code
  }, [data]);
  return null;
};
```
