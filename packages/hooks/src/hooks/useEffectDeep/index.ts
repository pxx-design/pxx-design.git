import { useRef, useEffect, EffectCallback, DependencyList } from 'react';
import { isEqual } from 'lodash-es';

function useEffectDeep(effect: EffectCallback, deps?: DependencyList) {
  // 使用一个数字信号控制是否渲染，简化 react 的计算，也便于调试
  let renderRef = useRef<number | any>(0);
  let depsRef = useRef(deps);
  if (!isEqual(deps, depsRef.current)) {
    renderRef.current++;
  }
  depsRef.current = deps;
  return useEffect(effect, [renderRef.current]);
}

export { useEffectDeep };
