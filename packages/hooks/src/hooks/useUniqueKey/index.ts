import { useRef } from 'react';
import { uniqueId } from 'lodash-es';
import { global } from '@parallel-line/utils';
import { getPathname } from '../../utils';

export interface UniqueKeyParams {
  prefix?: string;
  key?: string;
  /** 大于1时返回多个key， key = 'a' => ['a','a__1','a__2'...] */
  multiple?: number;
  /** 排除与pathname的关联，为true时，相当于 _.uniqueId */
  excludePathname?: boolean;
}

const map = new Map<string, string>();

export function uniqueKey(prefix = 'pxx_id') {
  const pathname = getPathname();
  if (map.has(pathname)) {
    return map.get(pathname)!;
  }
  let code: string = global.getPageRouteInfo(pathname)?.code ?? uniqueId();
  code = `${prefix}_${code}`;
  map.set(pathname, code);
  return code;
}
/**
 * 获取当前页的唯一key
 * @param key 自定义返回的唯一key
 */
function generateUniqueKey(key?: string): string[];
function generateUniqueKey(params?: UniqueKeyParams): string[];
function generateUniqueKey(params?: string | UniqueKeyParams) {
  let {
    key,
    prefix = 'pxx_id',
    multiple = 10,
    excludePathname,
  } = {} as UniqueKeyParams;
  if (typeof params === 'string') {
    key = params;
  } else if (params) {
    ({ key, prefix = 'pxx_id_', multiple = 10, excludePathname } = params);
  }
  const code = key ? key : excludePathname ? uniqueId() : uniqueKey(prefix);
  let m: string[] = [];
  if (multiple > 1) {
    m = Array.from(
      { length: multiple },
      (_, inx) => `${code}${inx === 0 ? '' : `__${inx}`}`,
    );
    return m;
  }
  return [code];
}

/**
 * 获取当前页的唯一key
 * @param key 自定义返回的唯一key
 */
function useUniqueKey(key?: string): string[];
function useUniqueKey(params?: UniqueKeyParams): string[];
function useUniqueKey(params?: any) {
  const m = useRef<string[]>([]);
  if (!m.current.length) {
    m.current = generateUniqueKey(params);
  }
  return m.current;
}

export { useUniqueKey, generateUniqueKey };
