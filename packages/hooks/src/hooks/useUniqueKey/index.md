---
title: useUniqueKey
group: { title: useUniqueKey }
---

# useUniqueKey

获取当前页的唯一key

```tsx | pure
import { useUniqueKey } from '@parallel-line/hooks';

const FC = () => {
  const [uniqueKey] = useUniqueKey();
  return null;
};
```

> 针对一个页面多个页签，需要多个唯一key可以通过数组索引区别。 默认返回长度为10的字符串数组。

```tsx | pure
import { useUniqueKey } from '@parallel-line/hooks';

const FC = () => {
  const [,uniqueKey] = useUniqueKey();
  return null;
};
```

```tsx | pure
import { useUniqueKey } from '@parallel-line/hooks';

const FC = () => {
  const [oneUniqueKey,twoUniqueKey,threeUniqueKey] = useUniqueKey();
  return null;
};
```
