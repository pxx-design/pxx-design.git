import { useRef, EffectCallback, DependencyList } from 'react';
import { isEqual } from 'lodash-es';
import { useUpdateEffect } from 'ahooks';

function useUpdateEffectDeep(fn: EffectCallback, deps: DependencyList) {
  let renderRef = useRef<number>(0);
  let depsRef = useRef(deps);
  if (!isEqual(deps, depsRef.current)) {
    renderRef.current++;
  }
  depsRef.current = deps;
  return useUpdateEffect(fn, [renderRef.current]);
}

export { useUpdateEffectDeep };
