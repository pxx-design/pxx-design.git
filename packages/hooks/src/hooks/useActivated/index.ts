import { useRef } from 'react';
import { useMount } from 'ahooks';
import { global } from '@parallel-line/utils';
import { getPathname } from '../../utils';

const PAGETABS_KEY = Symbol('PageTabs');

function useActivated(cb?: () => void | (() => void)) {
  const pathname = useRef(getPathname());
  const handle = (params: any) => {
    if (params?.pathname === pathname.current) {
      const leave = cb?.();
      if (typeof leave === 'function') {
        onLeave.current = leave;
      }
    } else if (onLeave.current) {
      onLeave.current();
      onLeave.current = null;
    }
  };
  useMount(() => {
    handle({ pathname: pathname.current });
  });
  const onLeave = useRef<null | (() => void)>(null);
  global.eventEmitter.useSubscription(({ uniqueKey, params }) => {
    if (uniqueKey !== PAGETABS_KEY) return;
    handle(params);
  });
}

export { PAGETABS_KEY, useActivated };
