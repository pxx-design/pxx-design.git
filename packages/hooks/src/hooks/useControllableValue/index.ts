import { useMemo } from 'react';
import { useSetState } from 'ahooks';
import { useUpdateEffectDeep } from '../useUpdateEffectDeep';

const useControllableValue = (
  props: any = {},
  defaultState: any = { value: undefined },
) => {
  //返回受控制的state
  const controllableState = useMemo(
    () =>
      Object.entries(defaultState).reduce((prev: any, [key, value]: any) => {
        prev[key] = props[key] || value;
        return prev;
      }, {}),
    [props],
  );

  const [state, setState] = useSetState<any>(controllableState);

  const _setState = (state: any) => {
    setState(state);
    props.onChange && props.onChange(state.value);
  };

  useUpdateEffectDeep(() => {
    _setState(controllableState);
  }, [controllableState]);

  return [state, _setState];
};

export { useControllableValue };
