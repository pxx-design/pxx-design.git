import { useRef, useCallback, DependencyList } from 'react';
import { isEqual } from 'lodash-es';

function useCallbackDeep<T extends (...args: any[]) => any>(
  callback: T,
  deps: DependencyList,
): T {
  const renderRef = useRef(0);
  const depsRef = useRef(deps);
  if (!isEqual(deps, depsRef.current)) {
    renderRef.current++;
  }
  depsRef.current = deps;
  return useCallback(callback, [renderRef.current]);
}

export { useCallbackDeep };
