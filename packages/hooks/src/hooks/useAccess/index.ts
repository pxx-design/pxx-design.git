import { isFunction, isArray } from 'lodash-es';
import { global } from '@parallel-line/utils';

function getAccess(codes?: string | (() => string)): boolean;
function getAccess(codes: string[] | (() => string[])): boolean[];
function getAccess(
  codes?: string | string[] | (() => string) | (() => string[]),
) {
  if (global?.accessCodes?.length > 0 && codes) {
    const inlineCodes = isFunction(codes) ? codes() : codes;
    if (isArray(inlineCodes)) {
      return inlineCodes.map((code) => global.accessCodes.includes(code));
    }
    return global.accessCodes.includes(inlineCodes);
  }
  return true;
}

type UseAccessType = typeof getAccess;
interface UseAccessInterface extends UseAccessType {
  getAccess: typeof getAccess;
}

const useAccess = ((codes?: any) => getAccess(codes)) as UseAccessInterface;
useAccess.getAccess = getAccess;

export { useAccess };
