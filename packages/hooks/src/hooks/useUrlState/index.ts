import { parse, stringify, ParsedQuery } from 'query-string';
import { useMemo, useRef, useState, useEffect } from 'react';
import type { Location } from 'history';
import { useHistory } from '../useHistory';
import { getPathname } from '../../utils';

export interface Options {
  navigateMode?: 'push' | 'replace';
  scope?: boolean;
}

const parseConfig = {
  skipNull: true,
  skipEmptyString: true,
  parseNumbers: false,
  parseBooleans: false,
};
interface UrlState {
  [key: string]: any;
}

function useLocation(): Location {
  const history = useHistory();
  const [location, update] = useState<Location>(history.location);
  useEffect(() => {
    return history.listen((location: any) => {
      update(location);
    });
  }, []);

  return location;
}

export const useUrlState = <S extends UrlState = UrlState>(
  initialState?: S | (() => S),
  options?: Options,
) => {
  type state = Partial<{ [key in keyof S]: any }>;
  const { scope, navigateMode = 'push' } = options || {};
  const location = useLocation();
  const history = useHistory();

  const [, update] = useState(false);

  const initialPathname = useRef(getPathname());
  const search = useRef<{ search: string; query: ParsedQuery<string> }>({
    search: location.search,
    query: parse(location.search, parseConfig),
  });

  const initialStateRef = useRef(
    typeof initialState === 'function'
      ? (initialState as () => S)()
      : initialState || {},
  );

  useEffect(() => {
    search.current = {
      search: location.search,
      query: parse(location.search, parseConfig),
    };
    initialPathname.current = getPathname();
  }, []);

  const queryFromUrl = useMemo(() => {
    if (!scope) return parse(location.search, parseConfig);

    if (
      initialPathname.current === location.pathname &&
      search.current.search !== location.search
    ) {
      search.current = {
        search: location.search,
        query: parse(location.search, parseConfig),
      };
    }
    return search.current.query;
  }, [location.search]);

  const targetQuery: state = useMemo(
    () => ({
      ...initialStateRef.current,
      ...queryFromUrl,
    }),
    [queryFromUrl],
  );

  const setState = (s: React.SetStateAction<state>) => {
    const newQuery = typeof s === 'function' ? (s as Function)(targetQuery) : s;

    // 1. 如果 setState 后，search 没变化，就需要 update 来触发一次更新。比如 demo1 直接点击 clear，就需要 update 来触发更新。
    // 2. update 和 history 的更新会合并，不会造成多次更新
    update((v) => !v);
    history[navigateMode]({
      hash: location.hash,
      search: stringify({ ...queryFromUrl, ...newQuery }, parseConfig) || '?',
    });
  };

  return [targetQuery, setState] as const;
};
