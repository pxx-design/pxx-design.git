import { useRef, useMemo, DependencyList } from 'react';
import { isEqual } from 'lodash-es';

function useMemoDeep<T>(factory: () => T, deps: DependencyList | undefined) {
  const renderRef = useRef(0);
  const depsRef = useRef(deps);
  if (!isEqual(deps, depsRef.current)) {
    renderRef.current++;
  }
  depsRef.current = deps;
  return useMemo<T>(factory, [renderRef.current]);
}

export { useMemoDeep };
