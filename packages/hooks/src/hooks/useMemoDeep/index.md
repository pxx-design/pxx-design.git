---
title: useMemoDeep
group: { title: useMemoDeep }
---

# useMemoDeep

```tsx | pure
import { useMemoDeep } from '@parallel-line/hooks';

const FC = ({ data }) => {
  const newData = useMemoDeep(() => data, [data]);
  return null;
};
```
