import type { History } from 'history';
import { global } from '@parallel-line/utils';

function useHistory(): History {
  if (!global.g_history) throw Error(`global.g_history is undefined`);
  return global.g_history;
}

export { useHistory };
