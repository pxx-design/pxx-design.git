// TODO 应该通过 useHistory 获取而不是通过 window.location 获取。 目前由于 web端 DVA 原因导致通过 useHistory 获取有问题。
export function getPathname(): string {
  const hash = window.location.hash.split('#');
  let pathname = '';
  if (hash.length > 1) {
    pathname = hash[1];
  }
  pathname = pathname.split('?')[0];
  return pathname;
}
