import * as path from 'path';
import * as fs from 'fs';
import { promisify } from 'util';
import { extend } from 'umi-request';
// eslint-disable-next-line import/no-extraneous-dependencies
import { template } from 'lodash';
import { fetchXml } from 'iconfont-parser';
import { iconfontCode } from '../package.json';

const request = extend({});
const SYMBOL_URL = `https://at.alicdn.com/t/${iconfontCode}.js`;
const SYMBOL_CSS_URL = `https://at.alicdn.com/t/${iconfontCode}.css`;

const writeFile = promisify(fs.writeFile);
const camelizeRE = /-(\w)/g;
export function camelize(str: string): string {
  return str
    .replace(/^\S/, (s) => s.toUpperCase())
    .replace(camelizeRE, (_, c) => c.toUpperCase());
}

async function generateIcons() {
  const iconsDir = path.join(__dirname, '../src/icons');
  try {
    await promisify(fs.access)(iconsDir);
  } catch (err) {
    await promisify(fs.mkdir)(iconsDir);
  }

  const render = template(
    `// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import Icon, { IconProps } from '../components/Icon';

const <%= svgIdentifier %> = React.forwardRef<HTMLSpanElement, Omit<IconProps, 'type'>>(
  (props, ref) => <Icon {...props} ref={ref} type="<%= symbolId %>" />,
);

<%= svgIdentifier %>.displayName = '<%= svgIdentifier %>';
export default <%= svgIdentifier %>;
`,
  );

  const encodeCSS: string = await request.get(SYMBOL_CSS_URL);

  await writeFile(
    path.resolve(__dirname, `../src/style/encode.less`),
    encodeCSS.replace('.iconfont {', '.pxx-icon {'),
  );

  const data = await fetchXml(SYMBOL_URL);
  const allIconDefs = data.svg.symbol.map(({ $ }) => {
    const symbolId = $.id;
    let svgIdentifier = camelize(symbolId.replace('pxx-icon-', ''));
    if (symbolId.search('fill') > 0 || symbolId.search('outlined') > 0) {
      return { symbolId, svgIdentifier };
    }
    svgIdentifier += 'Outlined';
    return { symbolId, svgIdentifier };
  });

  for (const { svgIdentifier, symbolId } of allIconDefs) {
    // generate icon file
    await writeFile(
      path.resolve(__dirname, `../src/icons/${svgIdentifier}.tsx`),
      render({ svgIdentifier, symbolId }),
    );
  }
  // generate icon index
  const entryText = allIconDefs
    .sort()
    .map(
      ({ svgIdentifier }) =>
        `export { default as ${svgIdentifier} } from './${svgIdentifier}';`,
    )
    .join('\n');

  await promisify(fs.appendFile)(
    path.resolve(__dirname, '../src/icons/index.tsx'),
    `// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

${entryText}
`.trim(),
  );
}

generateIcons();
