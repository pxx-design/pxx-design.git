import { defineConfig } from 'dumi';

export default defineConfig({
  title: 'parallel-utils',
  favicon: './favicon.ico',
  logo: './logo.png',
  outputPath: 'docs-dist',
  base: '/parallel-utils',
  publicPath: '/parallel-utils/',
  exportStatic: {},
});
