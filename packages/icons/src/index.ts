import Icon from './components/Icon';

export * from './icons';
export type {IconProps} from './components/Icon';
export default Icon;
