// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import Icon, { IconProps } from '../components/Icon';

const InvoicingManageOutlined = React.forwardRef<HTMLSpanElement, Omit<IconProps, 'type'>>(
  (props, ref) => <Icon {...props} ref={ref} type="pxx-icon-invoicing-manage" />,
);

InvoicingManageOutlined.displayName = 'InvoicingManageOutlined';
export default InvoicingManageOutlined;
