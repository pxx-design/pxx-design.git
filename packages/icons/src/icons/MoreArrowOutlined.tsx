// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import Icon, { IconProps } from '../components/Icon';

const MoreArrowOutlined = React.forwardRef<HTMLSpanElement, Omit<IconProps, 'type'>>(
  (props, ref) => <Icon {...props} ref={ref} type="pxx-icon-more-arrow-outlined" />,
);

MoreArrowOutlined.displayName = 'MoreArrowOutlined';
export default MoreArrowOutlined;
