// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import Icon, { IconProps } from '../components/Icon';

const CapacityOrderOutlined = React.forwardRef<HTMLSpanElement, Omit<IconProps, 'type'>>(
  (props, ref) => <Icon {...props} ref={ref} type="pxx-icon-capacity-order" />,
);

CapacityOrderOutlined.displayName = 'CapacityOrderOutlined';
export default CapacityOrderOutlined;
