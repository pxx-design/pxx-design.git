// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import Icon, { IconProps } from '../components/Icon';

const LinghuoyonggongOutlined = React.forwardRef<HTMLSpanElement, Omit<IconProps, 'type'>>(
  (props, ref) => <Icon {...props} ref={ref} type="pxx-icon-linghuoyonggong" />,
);

LinghuoyonggongOutlined.displayName = 'LinghuoyonggongOutlined';
export default LinghuoyonggongOutlined;
