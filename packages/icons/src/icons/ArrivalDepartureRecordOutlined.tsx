// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import Icon, { IconProps } from '../components/Icon';

const ArrivalDepartureRecordOutlined = React.forwardRef<HTMLSpanElement, Omit<IconProps, 'type'>>(
  (props, ref) => <Icon {...props} ref={ref} type="pxx-icon-arrival-departure-record" />,
);

ArrivalDepartureRecordOutlined.displayName = 'ArrivalDepartureRecordOutlined';
export default ArrivalDepartureRecordOutlined;
