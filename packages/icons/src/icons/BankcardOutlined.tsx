// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import Icon, { IconProps } from '../components/Icon';

const BankcardOutlined = React.forwardRef<HTMLSpanElement, Omit<IconProps, 'type'>>(
  (props, ref) => <Icon {...props} ref={ref} type="pxx-icon-bankcard-outlined" />,
);

BankcardOutlined.displayName = 'BankcardOutlined';
export default BankcardOutlined;
