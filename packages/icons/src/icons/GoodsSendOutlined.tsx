// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import Icon, { IconProps } from '../components/Icon';

const GoodsSendOutlined = React.forwardRef<HTMLSpanElement, Omit<IconProps, 'type'>>(
  (props, ref) => <Icon {...props} ref={ref} type="pxx-icon-goods-send-outlined" />,
);

GoodsSendOutlined.displayName = 'GoodsSendOutlined';
export default GoodsSendOutlined;
