// GENERATE BY ./scripts/generate.ts
// DON NOT EDIT IT MANUALLY

import * as React from 'react';
import Icon, { IconProps } from '../components/Icon';

const PhotoFailOutlined = React.forwardRef<HTMLSpanElement, Omit<IconProps, 'type'>>(
  (props, ref) => <Icon {...props} ref={ref} type="pxx-icon-photo-fail-outlined" />,
);

PhotoFailOutlined.displayName = 'PhotoFailOutlined';
export default PhotoFailOutlined;
