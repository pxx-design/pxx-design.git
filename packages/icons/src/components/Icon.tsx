import React from 'react';
import classnames from 'classnames';

export interface IconProps extends React.HTMLAttributes<HTMLSpanElement> {
  prefixCls?: string;
  /** 图标类型 */
  type: string;
  /** 图标是否旋转 */
  spin?: boolean;
  /** 图标点击事件 */
  onClick?: React.MouseEventHandler<HTMLSpanElement>;
}

const Icon = React.forwardRef<HTMLSpanElement, IconProps>((props, ref) => {
  const {
    prefixCls = 'pxx-icon',
    type,
    spin,
    className,
    ...spanProps
  } = props;
  return (
    <span
      ref={ref}
      className={classnames(
        prefixCls,
        props.type,
        {
          [`${prefixCls}__spin`]:
            !!props.spin ||
            props.type === 'pxx-icon-spinner' ||
            props.type === 'pxx-icon-loading',
        },
        className,
      )}
      {...spanProps}
    />
  );
});

export default Icon;
